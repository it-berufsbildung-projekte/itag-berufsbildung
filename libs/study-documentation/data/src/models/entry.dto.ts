import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class EntryDto extends BaseDocumentDto {
  content = 'exampleContent';
  creatorEmail = 'example@example.com';
  // hint: is for seeing the history of the entry without the type and just specific entries (published, accepted, rejected)
  entryHistory: Partial<EntryDto>[] = [];
  feedback?: string | undefined;
  feedbackFromEmail?: string | undefined;
  publishedOnTime = true;
  state: 'saved' | 'published' | 'rejected' | 'accepted' | 'reopened' = 'saved';
  tags = ['example', 'tags', 'for', 'entry'];
  tagsText = 'example, tags, for, entry';
  targetDate? = new Date();
  timestampLastPublish = 1;
  title = 'exampleTitle';
  wasAlreadyPublished = true;
  yearKw = '2022-01';

  constructor() {
    super();
  }
}
