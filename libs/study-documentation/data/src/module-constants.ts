import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
      entry: {
        canModifyDelete: 'api-entry-can-modify-delete',
        canRead: 'api-entry-can-read',
        canRemove: 'api-entry-can-remove',
        canShowDeleted: 'api-entry-can-show-deleted',
        canUpdate: 'api-entry-can-update',
        canSeeHistory: 'api-entry-can-see-history',
        // rights for sanitizing entry
        canSeeTagsText: 'api-entry-can-see-tags-text',
        canSetSavedAndPublishedState: 'api-entry-can-set-saved-and-published-state',
        canSetAcceptedAndRejectedState: 'api-entry-can-set-accepted-and-rejected-state',
        pb: 'api-entry-pb',
        ab: 'api-entry-ab',
        bb: 'api-entry-bb',
      },
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
      canShowMenuBbOverview: 'menu-can-show-menu-bb-overview',
      canShowMenuPbOverview: 'menu-can-show-menu-pb-overview',
      canShowMenuAbOverview: 'menu-can-show-menu-ab-overview',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
      entry: {
        canEdit: 'ui-entry-can-edit',
        canSeeFeedback: 'ui-entry-can-see-feedback',
        canSeeOnTime: 'ui-entry-can-see-on-time',
        canView: 'ui-can-view',
        canShowDeleted: 'ui-entry-can-show-deleted',
        canUpdate: 'ui-entry-can-update',
        canModifyDelete: 'ui-entry-can-modify-delete',
        canRemove: 'ui-entry-can-remove',
        canCreate: 'ui-entry-can-create',
        abCanView: 'ui-entry-ab-can-view',
        abCanCreate: 'ui-entry-ab-can-create',
        pbCanView: 'ui-entry-pb-can-view',
        bbCanView: 'ui-entry-bb-can-view',
        pb: 'ui-entry-pb',
        ab: 'ui-entry-ab',
        bb: 'ui-entry-bb',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
    bb: 'bb',
    pb: 'pb',
    ab: 'ab',
  };

  readonly controllerSubRoutes = {
    users: 'users',
  };

  readonly description = 'Beschreibung des study-documentation Modules';

  readonly documentNames = {
    single: {
      entry: 'entry',
    },
    plural: {
      entry: 'entry',
    },
  };

  readonly documents = {
    entry: 'entry',
  };

  readonly moduleName = 'StudyDocumentation';

  readonly routing = {
    base: 'study-documentation',
    sub: {
      pbEntries: 'pb/entries',
      pbEntryWithUuid: 'pb/entry/:uuid',
      pbEntry: 'pb/entry',
      abEntries: 'ab/entries',
      abEntryCreate: 'ab/entry/create',
      abEntryWithUuid: 'ab/entry/:uuid',
      abEntry: 'ab/entry',
      bbEntries: 'bb/entries',
      bbEntryWithUuid: 'bb/entry/:uuid',
      bbEntry: 'bb/entry',
    },
  };

  readonly states = {
    accepted: 'accepted',
    rejected: 'rejected',
    published: 'published',
    saved: 'saved',
  };

  readonly version = '1.0.0';
}
