import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { map, Observable, of } from 'rxjs';
import { ActiveRoleService } from '../services/active-role.service';
import { EntryEntityService } from '../services/entry-entity.service';

@Injectable()
export class EntryResolver implements Resolve<boolean> {

  lastRole = '';

  constructor(
    private readonly entryEntityService: EntryEntityService,
    private readonly activeRoleService: ActiveRoleService,
    private readonly userService: UserService,
  ) {
  }

// eslint-disable-next-line @typescript-eslint/no-unused-vars
  resolve( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> {
    // hack: we subscribe here to the user and just load everything if the user is set.
    // fixes the problem with not displaying everything and firing off 1000 requests until everything crashes.
    this.userService.userDto$.subscribe(u => {
      if ( u ) {
        const activeRole = route.data['activeRole'];
        // hint: there is an issue with switching the views (displays more data). So we check if the role has switched and then load.
        if (this.lastRole !== activeRole) {
          this.lastRole = activeRole;
          this.activeRoleService.setActiveRole(activeRole);
          this.entryEntityService.clearCache();
        }
        return this.entryEntityService.getWithQuery( { showDeleted: 'false', activeRole } ).pipe(
          map( ( entryDtoArr ) => !!entryDtoArr )
        );
      }
      return of(false);
    });
    return of(false);
  }

}
