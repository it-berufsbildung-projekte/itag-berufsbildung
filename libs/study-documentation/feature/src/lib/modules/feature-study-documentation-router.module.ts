import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { EntryDetailComponent } from '../components/entry-detail/entry-detail.component';
import { EntryOverviewComponent } from '../components/entry-overview/entry-overview.component';
import { EntryResolver } from '../resolvers/entry.resolver';


const routes: Routes = [
      {
        path: ModuleConstants.instance.routing.sub.pbEntries,
        component: EntryOverviewComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowMenuPbOverview,
          activeRole: ModuleConstants.instance.availableRoles.pb
        },
        resolve: {
          entry: EntryResolver,
        }
      },
      {
        path: ModuleConstants.instance.routing.sub.bbEntries,
        component: EntryOverviewComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowMenuBbOverview,
          activeRole: ModuleConstants.instance.availableRoles.bb
        },
        resolve: {
          entry: EntryResolver,
        }
      },
      {
        path: ModuleConstants.instance.routing.sub.abEntries,
        component: EntryOverviewComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowMenuAbOverview,
          activeRole: ModuleConstants.instance.availableRoles.ab
        },
        resolve: {
          entry: EntryResolver,
        }
      },
      {
        path: ModuleConstants.instance.routing.sub.abEntryWithUuid,
        component: EntryDetailComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.ui.entry.abCanView,
          activeRole: ModuleConstants.instance.availableRoles.ab
        },
        resolve: {
          entry: EntryResolver,
        }
      },

      {
        path: ModuleConstants.instance.routing.sub.abEntryCreate,
        component: EntryDetailComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.ui.entry.abCanCreate,
        },
      },
      {
        path: ModuleConstants.instance.routing.sub.pbEntryWithUuid,
        component: EntryDetailComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.ui.entry.pbCanView,
          activeRole: ModuleConstants.instance.availableRoles.pb
        },
        resolve: {
          entry: EntryResolver,
        }
      },
      {
        path: ModuleConstants.instance.routing.sub.bbEntryWithUuid,
        component: EntryDetailComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.ui.entry.bbCanView,
          activeRole: ModuleConstants.instance.availableRoles.bb
        },
        resolve: {
          entry: EntryResolver,
        }
      },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureStudyDocumentationRouterModule {}
