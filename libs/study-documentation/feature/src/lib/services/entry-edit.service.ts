import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { EntryDto } from '@itag-berufsbildung/study-documentation/data';
import { BaseGridEditService } from '@itag-berufsbildung/shared/data-ui';
import { EntryEntityService } from './entry-entity.service';

@Injectable({
  providedIn: 'root'
})
export class EntryEditService extends BaseGridEditService<EntryDto> {

  constructor(
    private readonly http: HttpClient,
    private readonly entryEntityService: EntryEntityService,
) {
  super(
    entryEntityService,
  );
  this.entries$ = entryEntityService.entities$;
}

}
