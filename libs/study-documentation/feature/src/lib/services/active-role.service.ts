import { BehaviorSubject } from 'rxjs';

export class ActiveRoleService {

  // hint: we can access the last activeRole from here so in the entry-entity Service we can use it in the query.
  activeRole$: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);

  setActiveRole(activeRole: string): void {
    this.activeRole$.next(activeRole);
  }

}
