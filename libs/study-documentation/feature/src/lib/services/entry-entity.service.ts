import { Injectable } from '@angular/core';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { ActiveRoleService } from './active-role.service';

@Injectable()
export class EntryEntityService extends EntityCollectionServiceBase<EntryDto> {
  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory,
    // hint: if we import the route to access the data we get circular dependencies with the resolver.
    readonly activeRoleService: ActiveRoleService,
  ) {
    super( ModuleConstants.instance.documentNames.single.entry, serviceElementsFactory );
    this.errors$.subscribe({
      next: () => {
        let activeRole = '';
        this.activeRoleService.activeRole$.subscribe(a => {
          activeRole = a as string;
          if (activeRole) {
            this.clearCache();
            this.getWithQuery( { showDeleted: 'false', activeRole: activeRole } );

          }
        });
      }
    });
  }

  // override add( entity: Partial<EntryDto>, options: EntityActionOptions & { isOptimistic: false } ): Observable<EntryDto> {
  //   return super.add( entity, options );
  // }

  // remove( data: Hugo)
}
