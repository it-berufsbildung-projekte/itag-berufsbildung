import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KendoUiModules } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { EntityDataModule, EntityDataService } from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { EntryBaseFormComponent } from './components/entry-base-form/entry-base-form.component';
import { EntityMetadata } from './entity.metadata';
import { EntryDetailComponent } from './components/entry-detail/entry-detail.component';
import { EntryOverviewComponent } from './components/entry-overview/entry-overview.component';
import { EntryOverviewButtonBarComponent } from './components/entry-overview-button-bar/entry-overview-button-bar.component';
import { EntryGridComponent } from './components/entry-grid/entry-grid.component';
import { FeatureStudyDocumentationRouterModule } from './modules/feature-study-documentation-router.module';
import { EntryResolver } from './resolvers/entry.resolver';
import { ActiveRoleService } from './services/active-role.service';
import { EntryDataService } from './services/entry-data.service';
import { EntryEditService } from './services/entry-edit.service';
import { EntryEntityService } from './services/entry-entity.service';

@NgModule({
  imports: [
    CommonModule,
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(EntityMetadata.entityConfig),
    RouterModule,
    KendoUiModules,
    FeatureStudyDocumentationRouterModule,
    NgxJsonViewerModule,
  ],
  declarations: [EntryDetailComponent, EntryOverviewComponent, EntryOverviewButtonBarComponent, EntryGridComponent, EntryBaseFormComponent ],
  providers: [
    EntryDataService,
    EntryEntityService,
    EntryResolver,
    NotificationService,
    {
      deps: [HttpClient, EntryEntityService],
      provide: EntryEditService,
      useFactory: (httpClient: HttpClient, entryEntityService: EntryEntityService) => () => new EntryEditService(httpClient, entryEntityService),
    },
    ActiveRoleService,
  ],
})
export class FeatureStudyDocumentationModule {
  constructor(private readonly entityDataService: EntityDataService, private readonly entryDataService: EntryDataService) {
    entityDataService.registerService(ModuleConstants.instance.documentNames.single.entry, entryDataService);
  }
}
