import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { BaseGrid } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { SelectionEvent } from '@progress/kendo-angular-grid';
import { EntryEditService } from '../../services/entry-edit.service';

@Component( {
  selector: 'itag-berufsbildung-entry-grid',
  templateUrl: './../../../../../../shared/data-ui/src/lib/templates/base-grid.html',
  styleUrls: [ './entry-grid.component.scss' ],
  encapsulation: ViewEncapsulation.Emulated,
} )
export class EntryGridComponent extends BaseGrid<EntryDto> {

  moduleConstants = ModuleConstants.instance;
  user!: UiUserDto;
  showDeleted = false;
  role = '';

  constructor(
    override readonly router: Router,
    private readonly userService: UserService,
    override readonly formBuilder: FormBuilder,
    @Inject( EntryEditService ) editServiceFactory: () => EntryEditService,
    protected route: ActivatedRoute,
  ) {
    super(
      router,
      editServiceFactory(),
      formBuilder,
      [
        {
          columnName: 'creatorEmail',
          columnTitle: 'Auszubildender',
          width: 20,
          type: 'string',
          readonly: true,
        },
        {
          columnName: 'title',
          columnTitle: 'Titel',
          width: 44,
          type: 'string',
          readonly: true,
        },
        {
          columnName: 'tagsText',
          columnTitle: 'Tags',
          width: 45,
          type: 'string',
          readonly: true,
        },
        {
          columnName: 'state',
          columnTitle: 'Status',
          width: 14,
          type: 'string',
          readonly: true,
        },
        {
          columnName: 'publishedOnTime',
          columnTitle: 'Pünktlich abgegeben',
          width: 13,
          type: 'checkbox',
          readonly: true,
        },
        {
          columnName: 'targetDate',
          columnTitle: 'Datum',
          width: 8,
          type: 'date',
          readonly: true,
        },
      ],
      {
        isInlineEdit: false,
        showToolbar: false,
        showCommandColumn: false,
        sortable: true,
        filterable: true,
        pageable: true,
        maxHeightPx: 1000,
        selectable: true,
        showAddButton: false,
        pdfExport: false,
        excelExport: false,

      },
      ModuleConstants.instance.moduleName,
      {
        canShowDeleted: ModuleConstants.instance.availableRights.ui.entry.canShowDeleted,
        canCreate: ModuleConstants.instance.availableRights.ui.entry.canCreate,
        canUpdate: ModuleConstants.instance.availableRights.ui.entry.canUpdate,
        canModifyDelete: ModuleConstants.instance.availableRights.ui.entry.canModifyDelete,
        canRemove: ModuleConstants.instance.availableRights.ui.entry.canRemove,
      },
      [],
      15,
      `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.abEntry}`
    );

    userService.userDto$.subscribe( {
      next: user => {
        if ( user ) {
          this.user = user;
          route.data.subscribe( d => this.role = d['activeRole']);
          switch ( this.role ) {
            case 'ab':
              // the ab doesn't need the creatorEmail
              this.removeDisplayedColumn('creatorEmail');
              break;
              // hint: if we need to do anything else ;)
            case 'pb':
            case 'bb':
              break;

          }
          if ( !user.hasRight( this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.entry.canShowDeleted ) ) {
            this.removeDisplayedColumn( 'isDeleted' );
          }

        }
      },
    } );

  }

  createDtoInstance(): EntryDto {
    return new EntryDto();
  }

  override selectionChange( selection: SelectionEvent ) {
    const entry = selection.selectedRows ? selection.selectedRows[0].dataItem:null;
    switch ( this.role ) {
      case 'ab':
        this.router.navigateByUrl( `/${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.abEntry }/${ entry.uuid }` ).then();
        break;
      case 'pb':
        this.router.navigateByUrl( `/${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.pbEntry }/${ entry.uuid }` ).then();
        break;
      case 'bb':
        this.router.navigateByUrl( `/${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.bbEntry }/${ entry.uuid }` ).then();
        break;
    }

  }
}
