import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { EntryEntityService } from '../../services/entry-entity.service';

@Component({
  selector: 'itag-berufsbildung-entry-overview-button-bar',
  templateUrl: './entry-overview-button-bar.component.html',
  styleUrls: ['./entry-overview-button-bar.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class EntryOverviewButtonBarComponent {

  entries: EntryDto[] = [];
  role = '';

  constructor(
    private readonly entryEntityService: EntryEntityService,
    private readonly route: ActivatedRoute
  ) {
    this.route.data.subscribe( d => this.role = d['activeRole']);
    this.entryEntityService.entities$.subscribe({
      next: value => {
          this.entries = value;
      },
    });
  }

  getRejected(): EntryDto[] {
    return this.entries.filter( e => e.state === ModuleConstants.instance.states.rejected);
  }
}
