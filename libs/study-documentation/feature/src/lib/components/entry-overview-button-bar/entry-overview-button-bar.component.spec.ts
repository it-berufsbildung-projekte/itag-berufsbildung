import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryOverviewButtonBarComponent } from './entry-overview-button-bar.component';

describe('EntryViewButtonBarComponent', () => {
  let component: EntryOverviewButtonBarComponent;
  let fixture: ComponentFixture<EntryOverviewButtonBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntryOverviewButtonBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EntryOverviewButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
