import { Component, Input, OnChanges, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { Update } from '@ngrx/entity';
import { ChipRemoveEvent } from '@progress/kendo-angular-buttons';
import { TextBoxComponent } from '@progress/kendo-angular-inputs';
import { v4 } from 'uuid';
import { EntryDataService } from '../../services/entry-data.service';

@Component({
  selector: 'itag-berufsbildung-entry-base-form',
  templateUrl: './entry-base-form.component.html',
  styleUrls: ['./entry-base-form.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class EntryBaseFormComponent implements OnChanges {
  @Input() entry: Partial<EntryDto> | undefined = undefined;
  @Input() role = '';
  @Input() isHistory = false;

  @ViewChild( 'tagInput' ) tagInput: TextBoxComponent | undefined;

  uiRights = ModuleConstants.instance.availableRights.ui.entry;
  userDto?: UiUserDto;
  moduleName = ModuleConstants.instance.moduleName;
  tags: string[] = [];
  deleteDialogOpened = false;
  reopenDialogOpened = false;


  form: FormGroup = new FormGroup( {
    title: new FormControl( '', [ Validators.required ] ),
    content: new FormControl( '', [ Validators.required ] ),
    targetDate: new FormControl( new Date(), [ Validators.required ] ),
  } );

  constructor(
    private readonly userService: UserService,
    private readonly entryDataService: EntryDataService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) {
    this.userService.userDto$.subscribe( ( userDto ) => {
      if ( userDto ) {
        this.userDto = userDto;
      }
    } );
    if (this.entry) {
      this.patchValue();
    }
  }

  addTag( tag: string ): void {
    // see if the user made a new tag with ,
    if ( tag[tag.length - 1]===',' && tag.length > 1 ) {
      this.tags.push( tag.slice( 0, -1 ) );
      // clearValue
      if ( this.tagInput ) {
        this.tagInput.value = '';
      }
    }
  }

  closeDeleteDialog(confirmed: boolean): void {
    if (confirmed) {
      this.deleteEntry();
    }
    this.deleteDialogOpened = false;
  }

  closeReopenDialog(confirmed: boolean): void {
    if (confirmed) {
      const newEntry = ObjectTools.cloneDeep<EntryDto>(this.entry as EntryDto);
      newEntry.wasAlreadyPublished = true;
      newEntry.state = 'reopened';
      // remove unnecessary properties since it's like a "new" entry with the old content.
      delete newEntry.targetDate;
      delete newEntry.feedback;
      delete newEntry.feedbackFromEmail;
      const updateEntryDto: Update<EntryDto> = {
        id: newEntry.uuid as string,
        changes: newEntry,
      };
      this.entryDataService.update( updateEntryDto, { activeRole: this.role} );
      this.notificationService.showSuccess('Eintrag erfolgreich geöffnet!');
      this.navigateBack();
    }
    this.reopenDialogOpened = false;
  }

  // is for the AB
  createOrUpdateEntry( state: 'published' | 'saved' ): void {
    const newEntry: Partial<EntryDto> = {
      uuid: this.entry?.uuid ? this.entry.uuid:v4(),
      state: state,
      content: this.form.value.content,
      title: this.form.value.title,
      tags: this.tags,
      targetDate: new Date( this.form.value.targetDate ),
      feedback: this.entry?.feedback,
    };
    // set the timestamp for lastPublished and set the alreadyPublished flag.
    if ( state === 'published' ) {
      newEntry.timestampLastPublish = Date.now();
      newEntry.wasAlreadyPublished = true;
    }
    if ( !this.entry ) {
      this.entryDataService.add( newEntry as EntryDto, { activeRole: this.role} );
    } else {
      const updateEntryDto: Update<EntryDto> = {
        id: newEntry.uuid as string,
        changes: newEntry,
      };
      this.entryDataService.update( updateEntryDto, { activeRole: this.role} );
    }
    const msg = `Eintrag erfolgreich ${ state==='published' ? 'veröffentlicht':'gespeichert' }!`;
    this.notificationService.showSuccess( msg );
    this.navigateBack();
  }

  deleteEntry(): void {
    this.entryDataService.delete(this.entry?.uuid as string, { activeRole: this.role});
    this.notificationService.showSuccess( `Eintrag erfolgreich gelöscht!` );
    this.navigateBack();
  }

  // is for the PB
  evaluateEntry( state: 'accepted' | 'rejected' ): void {
    const newEntry: Partial<EntryDto> = {
      uuid: this.entry?.uuid,
      state: state,
      feedback: this.form.value.feedback,
      content: this.entry?.content,
      title: this.entry?.title,
      tags: this.tags,
      targetDate: this.entry?.targetDate,
      timestampLastPublish: this.entry?.timestampLastPublish,
    };
    const updateEntryDto: Update<EntryDto> = {
      id: newEntry.uuid as string,
      changes: newEntry,
    };
    this.entryDataService.update( updateEntryDto, { activeRole: this.role} );
    const msg = `Eintrag erfolgreich ${ state==='accepted' ? 'angenommen':'abgelehnt' }!`;
    this.notificationService.showSuccess( msg );
    this.navigateBack();
  }

  getUserIfCantEdit(): boolean {
    return !this.userDto?.hasRight( this.moduleName, this.uiRights.canEdit ) as boolean;
  }

  // ab can't edit published nor accepted entries.
  isAbViewAndEditable(): boolean {
    return this.role === 'ab' && (this.entry?.state !== 'published' && this.entry?.state !== 'accepted');
  }

  isButtonRejectAndAcceptDisplayed(): boolean {
    return this.role==='pb' && this.entry?.state==='published';
  }

  isButtonReopenDisplayed(): boolean {
    return this.entry?.state === 'accepted' && !this.isHistory && this.role === 'pb';
  }

  isButtonSaveAndPublishDisplayed(): boolean {
    return this.role==='ab' && (!this.entry || this.entry?.state==='rejected' || this.entry?.state==='saved' || this.entry?.state === 'reopened');
  }

  isPbViewAndPublished(): boolean {
    return this.role === 'pb' && this.entry?.state === 'published';
  }

  isFieldReadOnly( fieldType: 'feedback' | 'filling' ): boolean {

    // the field feedback is  readonly if the state is accepted or rejected
    if ( fieldType==='feedback' ) {
      if (this.entry?.state === 'accepted' || this.entry?.state === 'rejected') {
        return true;
      } else {
        // else role decides (pb can write)
        return this.role!=='pb';
      }

      // the field for filling (title, targetDate, tags, content) is readonly if the state is published or accepted
    } else if ( fieldType==='filling' ) {
      if (this.entry?.state === 'published' || this.entry?.state === 'accepted') {
        return true;
      } else {
        // the ab can edit it
        return this.role!=='ab';
      }
    }
    return false;
  }

  navigateBack(): void {
    switch ( this.role ) {
      case 'ab':
        this.router.navigateByUrl( `${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.abEntries }` ).then();
        break;
      case 'pb':
        this.router.navigateByUrl( `${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.pbEntries }` ).then();
        break;
      case 'bb':
        this.router.navigateByUrl( `${ ModuleConstants.instance.routing.base }/${ ModuleConstants.instance.routing.sub.bbEntries }` ).then();
        break;
    }
  }

  // we check the changes to fix an issue with the input not being set properly
  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes['entry']?.currentValue ) {
      this.entry = changes['entry'].currentValue;
      this.patchValue();
    }
    if ( changes['role']?.currentValue ) {
      this.role = changes['role'].currentValue;
      this.patchValue();
    }
    if (changes['isHistory']?.currentValue ) {
      this.isHistory = changes['isHistory'].currentValue;
    }
  }

  onRemove( event: ChipRemoveEvent ): void {
    const index = this.tags.indexOf( event.sender.label );
    this.tags.splice( index, 1 );
  }

  openDeleteDialog(): void {
    this.deleteDialogOpened = true;
  }

  openReopenDialog(): void {
    this.reopenDialogOpened = true;
  }

  // patches the value of tags and the form.
  patchValue(): void {
    if (this.entry) {
      this.form.patchValue( {
        title: this.entry?.title,
        content: this.entry?.content,
        targetDate: this.entry?.targetDate ? new Date( this.entry?.targetDate as unknown as string ) : new Date(),
      } );

      if (this.entry?.tags) {
        this.tags = ObjectTools.cloneDeep<string[]>( this.entry?.tags );
      }
    }
    if (this.role) {
      if ( this.role==='pb' ) {
        this.form.addControl( 'feedback', new FormControl( '', [ Validators.required, Validators.minLength( 10 ) ] ) );
      } else {
        this.form.removeControl('feedback');
      }
    }

    if (this.isHistory) {
      this.form.disable();
    }
  }
}
