import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryBaseFormComponent } from './entry-base-form.component';

describe('EntryBaseFormComponent', () => {
  let component: EntryBaseFormComponent;
  let fixture: ComponentFixture<EntryBaseFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntryBaseFormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EntryBaseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
