import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DateAndTimeTools } from '@itag-berufsbildung/shared/util';
import { EntryDto } from '@itag-berufsbildung/study-documentation/data';
import { EntryEntityService } from '../../services/entry-entity.service';

@Component({
  selector: 'itag-berufsbildung-entry-detail',
  templateUrl: './entry-detail.component.html',
  styleUrls: ['./entry-detail.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class EntryDetailComponent implements OnInit {
  oldEntry: Partial<EntryDto> | undefined;
  entry: EntryDto | undefined;
  role = '';
  windowOpened = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly entryEntityService: EntryEntityService,
  ) {
  }

  closeWindow(): void {
    this.windowOpened = false;
    this.oldEntry = undefined;
  }

  getEntryTitle(): string {
    switch ( this.role ) {
      case 'ab':
        return (
          // if the entry is accepted or published the ab can only watch it
        this.entry?.state==='accepted' || this.entry?.state==='published' ? 'Eintrag anschauen':
          // if the entry is rejected, saved or reopened the ab can edit it so the title has to fit.
          this.entry?.state==='rejected' || this.entry?.state==='saved' || this.entry?.state === 'reopened' ? 'Eintrag bearbeiten':
            // the other possibility is to create an entry.
            'Eintrag erstellen');
      case 'pb':
        return `Eintrag von ${this.entry?.creatorEmail} für Jahr ${this.entry?.yearKw.split('-')[0]} KW ${this.entry?.yearKw.split('-')[1]} bewerten`;
      case 'bb':
        return 'Eintrag anschauen';
    }
    return '';
  }


  getDate( targetDate: Date | undefined): string {
    const date = new Date(targetDate as unknown as string);
    return DateAndTimeTools.getDateKey(date);
  }

  hasHistory(): boolean {
    return !!(this.entry && this.entry.entryHistory.length > 0);
  }

  ngOnInit(): void {
    this.route.params.subscribe( ( p ) => {
      if ( p['uuid'] ) {
        this.entryEntityService.entities$.subscribe( {
          next: ( value ) => {
            this.entry = value.find( ( e ) => e.uuid===p['uuid'] ) as unknown as EntryDto;
          },
        } );
      }
    } );
    this.route.data.subscribe( d => this.role = d['activeRole'] );
  }

  openWindow(entryHistory: Partial<EntryDto>): void {
    this.oldEntry = entryHistory;
    this.windowOpened = true;
  }

}
