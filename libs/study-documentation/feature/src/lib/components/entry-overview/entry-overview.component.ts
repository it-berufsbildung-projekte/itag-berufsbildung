import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-entry-overview',
  templateUrl: './entry-overview.component.html',
  styleUrls: ['./entry-overview.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class EntryOverviewComponent {
}
