import { ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { EntityMetadataMap } from '@ngrx/data';

export class EntityMetadata{

  static get entityConfig() {
    return {
      entityMetadata: EntityMetadata.getEntityMetaData(),
      pluralNames: EntityMetadata.getEntityMetaDataPlural(),
    };
  }

  private static getEntityMetaData(): EntityMetadataMap | undefined {
    const hash: EntityMetadataMap = {};
    // hint: there is an error in the compiler when we just return hash and if we decide to change it to "or undefined" not all code paths return a value... at this point I decided i don't care.
    for ( const key of Object.keys( ModuleConstants.instance.documentNames.single ) ) {
      hash[key] = {
        selectId: baseDocumentDto => baseDocumentDto.uuid,
        entityDispatcherOptions: {
          optimisticUpdate: true,
        }
      };
    }
    return hash;

  }

  private static getEntityMetaDataPlural(): { [name: string]: string; } {

    const hash: { [name: string]: string; } = {};
    for( const key of Object.keys(ModuleConstants.instance.documentNames.single) ){
      const pluralHash: {[key: string]: string } = ModuleConstants.instance.documentNames.plural;
      const pluralKey = pluralHash[key];
      hash[key] = pluralKey as string;
    }
    return hash;
  }

}
