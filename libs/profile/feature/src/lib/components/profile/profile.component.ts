import { Component, Inject } from '@angular/core';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { UiUserDto } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss'],
})
export class ProfileComponent {
  modules: string[] = [];
  picture = '/assets/dummy-user.png';
  userDto!: UiUserDto | undefined;

  constructor(@Inject(UiConstants.instance.services.user) readonly userService: UserService) {
    userService.userDto$.subscribe((userDto) => {
      this.userDto = userDto;
      if (userDto) {
        this.modules = Object.keys(userDto.uiRoles);
        this.picture = userDto?.picture ? userDto.picture : '/assets/dummy-user.png';
      }
    });
  }

  doRefresh(): void {
    if (this.userDto?.moduleInfoArr && this.userDto.moduleInfoArr.length > 0) {
      for(const moduleInfo of this.userDto.moduleInfoArr) {
        if (!moduleInfo?.apiUrl) {
          continue;
        }
        this.userService.refreshUser(moduleInfo.apiUrl);
      }
    }
  }
}
