import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DateService } from '@itag-berufsbildung/shared/data-ui';
import { RoleMembershipInfo } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-role-info',
  templateUrl: './role-info.component.html',
  styleUrls: ['./role-info.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RoleInfoComponent {
  @Input() roleInfo!: RoleMembershipInfo;
  @Input() roleName!: string;

  constructor(
    private readonly date: DateService,
  ) {
  }

  get from(): string {
    if (!this.roleInfo) {
      return 'not set';
    }
    if (this.roleInfo.from === 0) {
      return 'vom ersten Tag';
    }
    return this.date.format(this.roleInfo.from);
  }

  get until(): string {
    if (!this.roleInfo || !this.roleInfo.until) {
      return 'kein Enddatum';
    }
    return this.date.format(this.roleInfo.until);
  }
}
