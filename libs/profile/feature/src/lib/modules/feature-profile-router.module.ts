import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { ModuleConstants } from '@itag-berufsbildung/profile/data';
import { ProfileComponent } from '../components/profile/profile.component'; // CLI imports router

const routes: Routes = [
  {
    path: ModuleConstants.instance.routing.sub.information,
    component: ProfileComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowSubMenuInformation,
    },
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureProfileRouterModule {}
