import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KendoUiModules } from '@itag-berufsbildung/shared/data-ui';

import { ProfileComponent } from './components/profile/profile.component';
import { RoleInfoComponent } from './components/role-info/role-info.component';
import { FeatureProfileRouterModule } from './modules/feature-profile-router.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    KendoUiModules,
    FeatureProfileRouterModule,
  ],
  declarations: [ProfileComponent, RoleInfoComponent],
})
export class FeatureProfileModule {}
