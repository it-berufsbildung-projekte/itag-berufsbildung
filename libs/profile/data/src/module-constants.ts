import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      module: {
        canRead: 'api-module-can-read',
        canCreate: 'api-module-can-create',
      },
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
    },
    menu: {
      canShowMainMenu: 'menu-can-show-main-menu',
      canShowSubMenuInformation: 'menu-can-show-sub-menu-information',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
  };

  readonly description = 'Beschreibung des Profile Modules';

  readonly moduleName = 'Profile';

  readonly routing = {
    base: 'profile',
    sub: {
      information: 'info',
    },
  };

  readonly version = '1.0.0';
}
