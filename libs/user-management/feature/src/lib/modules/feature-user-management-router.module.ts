import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { ModuleConstants } from '@itag-berufsbildung/user-management/data';
import { UserManagementComponent } from '../components/user-management/user-management.component';

const routes: Routes = [
  {
    path: ModuleConstants.instance.routing.sub.roles,
    component: UserManagementComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowSubMenuRoles,
    },
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureUserManagementRouterModule {}
