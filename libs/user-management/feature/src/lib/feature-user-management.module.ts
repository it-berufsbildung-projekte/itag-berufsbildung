import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KendoUiModules } from '@itag-berufsbildung/shared/data-ui';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { FeatureUserManagementRouterModule } from './modules/feature-user-management-router.module';
import { UserManagementModulComponent } from './components/user-management-modul/user-management-modul.component';

@NgModule({
  declarations: [UserManagementComponent, UserManagementModulComponent],
  imports: [CommonModule, KendoUiModules, FeatureUserManagementRouterModule],
})
export class FeatureUserManagementModule {}
