import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementModulComponent } from './user-management-modul.component';

describe('UserManagementModulComponent', () => {
  let component: UserManagementModulComponent;
  let fixture: ComponentFixture<UserManagementModulComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserManagementModulComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UserManagementModulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
