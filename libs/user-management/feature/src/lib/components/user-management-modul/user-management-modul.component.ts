import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { AdminModuleInfoDto, GlobalConstants, RoleHashDto, UserRoleHashDto } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-user-management-modul',
  templateUrl: './user-management-modul.component.html',
  styleUrls: ['./user-management-modul.component.scss'],
})
export class UserManagementModulComponent {
  listItems: string[] = [];
  roleHashDto: RoleHashDto = {};
  openedDelete = false;
  openedAddNew = false;
  openedDuration = false;
  applyForAll = false;

  selectedUserRole: RoleHashDto = {};

  @Output()
  eMailSelected: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  createUserRolesForAllModules: EventEmitter<UserRoleHashDto> = new EventEmitter<UserRoleHashDto>();

  @Output()
  deleteUserRolesForAllModules: EventEmitter<string> = new EventEmitter<string>();

  eMail?: string;

  @Input()
  set selectedEMail(value: string | undefined) {
    this.eMail = value;
    if (!value) {
      return;
    }
    this.getRoles(value);
  }

  private _moduleInfo!: Partial<AdminModuleInfoDto>;

  @Input()
  set modulInfo(value: Partial<AdminModuleInfoDto>) {
    this._moduleInfo = value;
    this.getUsers();
  }

  get modulInfo(): Partial<AdminModuleInfoDto> {
    return this._moduleInfo;
  }

  constructor(private readonly http: HttpClient, private readonly notification: NotificationService) {}

  doAddUser(): void {
    // add a user by a dialog
    this.openedAddNew = true;
  }

  doRemove() {
    // show a dialog to remove the user
    this.openedDelete = true;
  }

  onUserSelectionChanged(eMail: string): void {
    if (!eMail || eMail.length === 0) {
      this.roleHashDto = {};
      this.eMailSelected.emit(eMail);
      return;
    }
    this.eMailSelected.emit(eMail);
    this.getRoles(eMail);
  }

  doEditDateRange(roleName: string): void {
    this.openedDuration = true;
    this.selectedUserRole[roleName] = this.roleHashDto[roleName];
  }

  isSet(roleName: string): boolean {
    return !!this.roleHashDto[roleName];
  }

  onAddRemoveRole(checked: boolean, roleName: string) {
    if (checked) {
      // add role
      const userRoleHash: UserRoleHashDto = {};
      if (this.eMail) {
        userRoleHash[this.eMail] = { [roleName]: { from: 0, until: undefined } };

        const url = `${this._moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}`;
        this.http.post(url, userRoleHash).subscribe({
          next: () => this.getRoles(this.eMail),
          error: (err) => this.notification.showError(err.message),
        });
      }

      return;
    }

    // remove role
    const url = `${this._moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}/${this.eMail}/${roleName}`;
    this.http.delete(url).subscribe({
      next: () => this.getRoles(),
      error: (err) => this.notification.showError(err.message),
    });
  }

  closeDialogRemove(state: 'cancel' | 'no' | 'yes') {
    switch (state) {
      case 'cancel':
        break;
      case 'no':
        break;
      case 'yes': {
        if (this.applyForAll) {
          // remove user for all modules
          this.deleteUserRolesForAllModules.emit(this.eMail);
        } else {
          // remove user for this module
          const url = `${this._moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}/${this.eMail}`;
          this.http.delete(url).subscribe({
            next: () => this.getUsers(),
            error: (err) => this.notification.showError(err.message),
          });
        }
        break;
      }
    }
    this.openedDelete = false;
  }

  closeDialogAdd(state: 'cancel' | 'no' | 'yes', emailToAdd?: string) {
    switch (state) {
      case 'cancel':
        break;
      case 'no':
        break;
      case 'yes':
        if (emailToAdd) {
          const userRoleHash: UserRoleHashDto = {};
          userRoleHash[emailToAdd] = { user: { from: 0, until: undefined } };

          if (this.applyForAll) {
            this.createUserRolesForAllModules.emit(userRoleHash);
          } else {
            const url = `${this._moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}`;
            this.http.post(url, userRoleHash).subscribe({
              next: () => this.getUsers(),
              error: (err) => this.notification.showError(err.message),
            });
          }
        }
        break;
    }
    this.openedAddNew = false;
  }

  closeDialogDuration(state: 'cancel' | 'no' | 'yes') {
    switch (state) {
      case 'cancel':
        break;
      case 'no':
        break;
      case 'yes': {
        const userRoleHash: UserRoleHashDto = {};

        if (this.eMail) {
          userRoleHash[this.eMail] = this.roleHashDto;
        }

        const url = `${this._moduleInfo.apiUrl} /${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}`;
        this.http.put(url, userRoleHash).subscribe({
          error: (err) => this.notification.showError(err.message),
        });
        break;
      }
    }
    this.openedDuration = false;
    this.selectedUserRole = {};
  }

  getDateInstance(timestamp: number | undefined): Date {
    return new Date(timestamp as number);
  }

  getUsers() {
    // get the users for this modul
    const url = `${this.modulInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.users}`;
    this.http.get<string[]>(url).subscribe({
      next: (users) => {
        this.listItems = [...users.sort()];
      },
      error: (err: Error) => this.notification.showError(err.message),
    });
  }

  private getRoles(eMail?: string) {
    if (!eMail) {
      return;
    }
    const url = `${this.modulInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}/${eMail}`;
    this.http.get<RoleHashDto>(url).subscribe({
      next: (hash) => {
        this.roleHashDto = hash;
      },
      error: (err: Error) => this.notification.showError(err.message),
    });
  }
}
