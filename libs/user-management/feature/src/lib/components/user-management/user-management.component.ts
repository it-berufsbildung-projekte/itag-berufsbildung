import { HttpClient } from '@angular/common/http';
import { Component, Inject, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { AdminModuleInfoDto, GlobalConstants, ObjectTools, SorterTools, UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { UserManagementModulComponent } from '../user-management-modul/user-management-modul.component';

@Component({
  selector: 'itag-berufsbildung-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserManagementComponent {
  @ViewChildren(UserManagementModulComponent) userMgmtModuleComponents!: QueryList<UserManagementModulComponent>;
  modules: Partial<AdminModuleInfoDto>[] = [];
  eMail?: string;

  constructor(
    @Inject(UiConstants.instance.services.user) public readonly userService: UserService,
    private readonly http: HttpClient,
    private readonly notification: NotificationService
  ) {
    this.userService.userDto$.subscribe((userDto) => {
      if (userDto) {
        // get for all modules now the appInfo
        for (const moduleInfo of userDto.moduleInfoArr) {
          if (!moduleInfo?.moduleName) {
            alert();
            // ignore if not exist
            continue;
          }
          const url = `${moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.appInfo}`;
          this.http.get<Partial<AdminModuleInfoDto>>(url).subscribe({
            next: (adminModuleInfo) => {
              const arr = ObjectTools.cloneDeep(this.modules);
              if (!arr.find((i) => i.moduleName === adminModuleInfo.moduleName)) {
                // add it
                this.modules.push(adminModuleInfo);
                this.modules = this.modules.sort(SorterTools.dynamicSort('moduleName'));
                // get the users
              }
            },
            error: (err: Error) => this.notification.showError(err.message),
          });
        }
      }
    });
  }

  onCreateUserRolesForAllModules(userRoleHashDtoArr: UserRoleHashDto): void {
    for (const module of this.modules) {
      const url = `${module.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}`;
      this.http.post(url, userRoleHashDtoArr).subscribe({
        next: () =>
          // fetch all users in each module to display the new created user
          this.userMgmtModuleComponents.forEach((component) => component.getUsers()),
        error: (err) => this.notification.showError(err.message),
      });
    }
  }

  onDeleteUserForAllModules(userEmail: string): void {
    for (const module of this.modules) {
      const url = `${module.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.userRoles}/${userEmail}`;
      this.http.delete(url).subscribe({
        // fetch all users again in each module to display the new created user
        next: () => this.userMgmtModuleComponents.forEach((component) => component.getUsers()),
        error: (err) => this.notification.showError(err.message),
      });
    }
  }

  onEMailSelected(eMail: string) {
    this.eMail = eMail;
  }
}
