import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
      // todo: jeden Controller hinzufügen
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
      canShowSubMenuRoles: 'menu-can-show-sub-menu-roles',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
  };

  readonly documents = {
    // todo: jedes document hinzufügen
  };

  readonly description = 'Beschreibung des UserManagement Modules';

  readonly moduleName = 'UserManagement';

  readonly routing = {
    base: 'user-management',
    sub: {
      roles: 'roles',
    },
  };

  readonly version = '1.0.0';
}
