# user-management-data-user-management

für das Module user-management, gemeinsam genutzte Daten etc. für Front- und Backend

im Prinzip alles, was früher mit ng-openapi-gen erzeugt wurde. wird heute gemeinsam benutzt

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test user-management-data-user-management` to execute the unit tests.
