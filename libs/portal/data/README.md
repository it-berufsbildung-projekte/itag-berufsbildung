# portal-data

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test portal-data` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint portal-data` to execute the lint via [ESLint](https://eslint.org/).
