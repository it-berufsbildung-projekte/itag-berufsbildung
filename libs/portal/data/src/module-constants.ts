import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly api = {
    base: 'portal',
    sub: {
      modules: 'modules',
    },
  };

  readonly availableRights = {
    api: {
      module: {
        canRead: 'api-module-can-read',
      },
      generic: {
        canShowAdminInfo: 'api-generic-can-show-admin-info',
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
      },
    },
    menu: {
      canShowMainMenuInfo: 'menu-can-show-main-menu-info',
      canShowMainMenuSignOut: 'menu-can-show-main-menu-sign-out',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
  };

  readonly description = 'Beschreibung des Portal Modules';

  readonly moduleName = 'Portal';

  readonly routing = {
    base: 'portal',
    sub: {
      information: 'information',
    },

  };

  readonly version = '8.0.0';
}
