import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { DailyComponent } from '../components/daily/daily.component';
import { InterFlexComponent } from '../components/inter-flex/inter-flex.component';
import { ReportingComponent } from '../components/reporting/reporting.component';
import { TasksComponent } from '../components/tasks/tasks.component';
import { UsersComponent } from '../components/users/users.component';
import { InterFlexResolver } from '../resolvers/inter-flex.resolver';
import { TasksResolver } from '../resolvers/tasks.resolver';
import { UsersResolver } from '../resolvers/users.resolver';

const routes: Routes = [
  {
    path: ModuleConstants.instance.routing.sub.daily,
    component: DailyComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowSubMenuDaily,
    },
  },
  {
    path: ModuleConstants.instance.routing.sub.reporting,
    component: ReportingComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowSubMenuReporting,
    },
  },
  {
    path: ModuleConstants.instance.routing.sub.admin,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdmin,
    },
    children: [
      {
        path: ModuleConstants.instance.routing.sub.tasks,
        component: TasksComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminTasks,
        },
      },
      {
        path: ModuleConstants.instance.routing.sub.interFlex,
        component: InterFlexComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminInterFlex,
        },
      },
      {
        path: ModuleConstants.instance.routing.sub.users,
        component: UsersComponent,
        canActivate: [ModuleRightGuard],
        data: {
          module: ModuleConstants.instance.moduleName,
          right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminUsers,
        },
        children: [
          {
            path: ModuleConstants.instance.routing.sub.tasks,
            component: TasksComponent,
            canActivate: [ModuleRightGuard],
            data: {
              module: ModuleConstants.instance.moduleName,
              right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminTasks,
            },
            resolve: {
              tasks: TasksResolver,
              interFlex: InterFlexResolver,
            }
          },
          {
            path: ModuleConstants.instance.routing.sub.interFlex,
            component: InterFlexComponent,
            canActivate: [ModuleRightGuard],
            data: {
              module: ModuleConstants.instance.moduleName,
              right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminInterFlex,
            },
            resolve: {
              interFlex: InterFlexResolver,
            },
          },
          {
            path: ModuleConstants.instance.routing.sub.users,
            component: UsersComponent,
            canActivate: [ModuleRightGuard],
            data: {
              module: ModuleConstants.instance.moduleName,
              right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminUsers,
            },
            resolve: {
              users: UsersResolver,
            },
          },
        ],
      },
    ],
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureTimeCounterRouterModule {}
