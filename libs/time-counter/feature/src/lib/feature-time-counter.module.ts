import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { EntityDataModule, EntityDataService } from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ExcelModule, GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { IconsModule } from '@progress/kendo-angular-icons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { CardModule } from '@progress/kendo-angular-layout';
import { DailyComponent } from './components/daily/daily.component';
import { InterFlexGridComponent } from './components/inter-flex-grid/inter-flex-grid.component';
import { ReportingComponent } from './components/reporting/reporting.component';
import { TasksGridComponent } from './components/tasks-grid/tasks-grid.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { InterFlexComponent } from './components/inter-flex/inter-flex.component';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import { UsersComponent } from './components/users/users.component';
import { EntityMetadata } from './entity.metadata';
import { FeatureTimeCounterRouterModule } from './modules/feature-time-counter-router.module';
import { InterFlexResolver } from './resolvers/inter-flex.resolver';
import { TasksResolver } from './resolvers/tasks.resolver';
import { UsersResolver } from './resolvers/users.resolver';
import { InterFlexDataService } from './services/inter-flex-data.service';
import { InterFlexEditService } from './services/inter-flex-edit.service';
import { InterFlexEntityService } from './services/inter-flex-entity.service';
import { TasksDataService } from './services/tasks-data.service';
import { TasksEditService } from './services/tasks-edit.service';
import { TasksEntityService } from './services/tasks-entity.service';
import { UsersDataService } from './services/users-data.service';
import { UsersEditService } from './services/users-edit.service';
import { UsersEntityService } from './services/users-entity.service';

@NgModule({
  declarations: [DailyComponent, ReportingComponent, TasksComponent, InterFlexComponent, UsersComponent, InterFlexGridComponent, UsersGridComponent, TasksGridComponent],
  imports: [
    CommonModule,
    FeatureTimeCounterRouterModule,
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(EntityMetadata.entityConfig),
    GridModule,
    DialogsModule,
    PDFModule,
    ExcelModule,
    ButtonsModule,
    InputsModule,
    FormsModule,
    ReactiveFormsModule,
    DateInputsModule,
    LabelModule,
    IconsModule,
    CardModule,
    DropDownsModule,
  ],
  providers: [
    InterFlexResolver,
    InterFlexDataService,
    InterFlexEntityService,
    {
      deps: [HttpClient, InterFlexEntityService],
      provide: InterFlexEditService,
      useFactory: (httpClient: HttpClient, interFlexEntityService: InterFlexEntityService) => () => new InterFlexEditService(httpClient, interFlexEntityService),
    },
    UsersResolver,
    UsersDataService,
    UsersEntityService,
    {
      deps: [HttpClient, UsersEntityService],
      provide: UsersEditService,
      useFactory: (httpClient: HttpClient, usersEntityService: UsersEntityService) => () => new UsersEditService(httpClient, usersEntityService),
    },
    TasksResolver,
    TasksDataService,
    TasksEntityService,
    {
      deps: [HttpClient, TasksEntityService],
      provide: TasksEditService,
      useFactory: (httpClient: HttpClient, tasksEntityService: TasksEntityService) => () => new TasksEditService(httpClient, tasksEntityService),
    },
  ],
})
export class FeatureTimeCounterModule {
  constructor(
    private entityDataService: EntityDataService,
    private interFlexDataService: InterFlexDataService,
    private usersDataService: UsersDataService,
    private tasksDataService: TasksDataService
  ) {
    entityDataService.registerService(ModuleConstants.instance.documentNames.single.interFlex, interFlexDataService);

    entityDataService.registerService(ModuleConstants.instance.documentNames.single.users, usersDataService);

    entityDataService.registerService(ModuleConstants.instance.documentNames.single.tasks, tasksDataService);
  }
}
