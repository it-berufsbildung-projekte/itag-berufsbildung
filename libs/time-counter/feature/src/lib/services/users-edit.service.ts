import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WorkDataDto } from '@itag-berufsbildung/time-counter/data';
import { BaseGridEditService } from '@itag-berufsbildung/shared/data-ui';
import { UsersEntityService } from './users-entity.service';

@Injectable({
  providedIn: 'root',
})
export class UsersEditService extends BaseGridEditService<WorkDataDto> {
  constructor(private readonly http: HttpClient, private readonly usersEntityService: UsersEntityService) {
    super(usersEntityService);
    this.entries$ = usersEntityService.entities$;
  }
}
