import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InterFlexDto } from '@itag-berufsbildung/time-counter/data';
import { BaseGridEditService } from '@itag-berufsbildung/shared/data-ui';
import { InterFlexEntityService } from './inter-flex-entity.service';

@Injectable({
  providedIn: 'root',
})
export class InterFlexEditService extends BaseGridEditService<InterFlexDto> {
  constructor(private readonly http: HttpClient, private readonly interFlexEntityService: InterFlexEntityService) {
    super(interFlexEntityService);
    this.entries$ = interFlexEntityService.entities$;
  }
}
