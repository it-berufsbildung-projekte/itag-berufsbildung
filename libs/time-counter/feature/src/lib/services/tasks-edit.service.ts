import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TasksDto } from '@itag-berufsbildung/time-counter/data';
import { BaseGridEditService } from '@itag-berufsbildung/shared/data-ui';
import { TasksEntityService } from './tasks-entity.service';

@Injectable({
  providedIn: 'root',
})
export class TasksEditService extends BaseGridEditService<TasksDto> {
  constructor(private readonly http: HttpClient, private readonly tasksEntityService: TasksEntityService) {
    super(tasksEntityService);
    this.entries$ = tasksEntityService.entities$;
  }
}
