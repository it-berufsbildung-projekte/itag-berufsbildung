import { Injectable } from '@angular/core';
import { ModuleConstants, TasksDto } from '@itag-berufsbildung/time-counter/data';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';

@Injectable()
export class TasksEntityService extends EntityCollectionServiceBase<TasksDto> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(ModuleConstants.instance.documentNames.single.tasks, serviceElementsFactory);
    this.errors$.subscribe({
      next: () => {
        this.clearCache();
        this.getAll();
      },
    });
  }
}
