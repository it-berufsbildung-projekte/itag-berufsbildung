import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, TasksDto } from '@itag-berufsbildung/time-counter/data';
import { GenericNgrxDataService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { HttpUrlGenerator } from '@ngrx/data';

@Injectable({
  providedIn: 'root',
})
export class TasksDataService extends GenericNgrxDataService<TasksDto> {
  constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator, readonly user: UserService) {
    super(http, httpUrlGenerator, ModuleConstants.instance.documentNames.single.tasks);

    this.user.userDto$.subscribe({
      next: (userDto) => {
        if (userDto) {
          super.apiUrl = `${userDto.moduleInfoArr.find((x) => x.moduleName === ModuleConstants.instance.moduleName)?.apiUrl}/${
            ModuleConstants.instance.documentNames.single.tasks
          }`;
        }
      },
    });
  }
}
