import { Injectable } from '@angular/core';
import { ModuleConstants, WorkDataDto } from '@itag-berufsbildung/time-counter/data';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';

@Injectable()
export class UsersEntityService extends EntityCollectionServiceBase<WorkDataDto> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(ModuleConstants.instance.documentNames.single.users, serviceElementsFactory);
    this.errors$.subscribe({
      next: () => {
        this.clearCache();
        this.getAll();
      },
    });
  }
}
