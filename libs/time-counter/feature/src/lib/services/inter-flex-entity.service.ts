import { Injectable } from '@angular/core';
import { InterFlexDto, ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';

@Injectable()
export class InterFlexEntityService extends EntityCollectionServiceBase<InterFlexDto> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(ModuleConstants.instance.documentNames.single.interFlex, serviceElementsFactory);
    this.errors$.subscribe({
      next: () => {
        this.clearCache();
        this.getAll();
      },
    });
  }
}
