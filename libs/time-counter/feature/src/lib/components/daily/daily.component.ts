import { Component } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.scss'],
})
export class DailyComponent {}
