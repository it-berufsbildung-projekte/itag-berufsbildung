import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModuleConstants, TasksDto } from '@itag-berufsbildung/time-counter/data';
import { BaseGrid, IDisplayedColumns, IGridOptions } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { InterFlexEntityService } from '../../services/inter-flex-entity.service';
import { TasksEditService } from '../../services/tasks-edit.service';

@Component({
  selector: 'itag-berufsbildung-tasks-grid',
  templateUrl: './../../../../../../shared/data-ui/src/lib/templates/base-grid.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.Emulated,
})
export class TasksGridComponent extends BaseGrid<TasksDto> {
  moduleConstants = ModuleConstants.instance;
  user!: UiUserDto;
  showDeleted = false;

  constructor(
    override readonly router: Router,
    private readonly userService: UserService,
    private readonly interFlexEntityService: InterFlexEntityService,
    override readonly formBuilder: FormBuilder,
    @Inject(TasksEditService) editServiceFactory: () => TasksEditService
  ) {
    const displayedColumns: IDisplayedColumns[] = [
      {
        columnName: 'name',
        width: 10,
        type: 'string',
        readonly: false,
      },
      {
        columnName: 'interFlexUuid',
        width: 10,
        type: 'autocompleteComplex',
        readonly: false,
        dropdownComplexData: interFlexEntityService.entities$,
        valueField: 'uuid',
        textField: 'name',
      },
      {
        columnName: 'sortOrder',
        width: 10,
        type: 'number',
        readonly: false,
      },
    ];

    const gridOptions: IGridOptions = {
      isInlineEdit: true,
      showToolbar: true,
      showCommandColumn: true,
      sortable: true,
      filterable: true,
      pageable: true,
      maxHeightPx: 1000,
      selectable: false,
      showAddButton: true,
      pdfExport: true,
      excelExport: true,
    };

    const rights = {
      canShowDeleted: ModuleConstants.instance.availableRights.ui.tasks.canShowDeleted,
      canCreate: ModuleConstants.instance.availableRights.ui.tasks.canCreate,
      canUpdate: ModuleConstants.instance.availableRights.ui.tasks.canUpdate,
      canModifyDelete: ModuleConstants.instance.availableRights.ui.tasks.canModifyDelete,
      canRemove: ModuleConstants.instance.availableRights.ui.tasks.canRemove,
    };

    super(
      router,
      editServiceFactory(),
      formBuilder,
      displayedColumns,
      gridOptions,
      ModuleConstants.instance.moduleName,
      rights,
      [],
      10
    );

    userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;

          // todo: insert checks for rights to hide columns
          if (!user.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.tasks.canShowDeleted)) {
            this.removeDisplayedColumn('isDeleted');
          }
        }
      },
    });
  }

  createDtoInstance(): TasksDto {
    return new TasksDto();
  }
}
