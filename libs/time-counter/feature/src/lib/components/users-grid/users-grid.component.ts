import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModuleConstants, WorkDataDto } from '@itag-berufsbildung/time-counter/data';
import { BaseGrid, IDisplayedColumns, IGridOptions } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { UsersEditService } from '../../services/users-edit.service';

@Component({
  selector: 'itag-berufsbildung-users-grid',
  templateUrl: './../../../../../../shared/data-ui/src/lib/templates/base-grid.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.Emulated,
})
export class UsersGridComponent extends BaseGrid<WorkDataDto> {
  moduleConstants = ModuleConstants.instance;
  user!: UiUserDto;
  showDeleted = false;

  constructor(
    override readonly router: Router,
    private readonly userService: UserService,
    override readonly formBuilder: FormBuilder,
    @Inject(UsersEditService) editServiceFactory: () => UsersEditService
  ) {
    const displayedColumns: IDisplayedColumns[] = [
      {
        columnName: 'username',
        width: 20,
        type: 'string',
        readonly: false,
      },
      {
        columnName: 'displayName',
        width: 20,
        type: 'string',
        readonly: false,
      },
      {
        columnName: 'eMail',
        width: 20,
        type: 'string',
        readonly: false,
      },
    ];

    const gridOptions: IGridOptions = {
      isInlineEdit: true,
      showToolbar: true,
      showCommandColumn: true,
      sortable: true,
      filterable: true,
      pageable: true,
      maxHeightPx: 1000,
      selectable: false,
      showAddButton: true,
      pdfExport: false,
      excelExport: false,
    };

    const rights = {
      canShowDeleted: ModuleConstants.instance.availableRights.ui.users.canShowDeleted,
      canCreate: ModuleConstants.instance.availableRights.ui.users.canCreate,
      canUpdate: ModuleConstants.instance.availableRights.ui.users.canUpdate,
      canModifyDelete: ModuleConstants.instance.availableRights.ui.users.canModifyDelete,
      canRemove: ModuleConstants.instance.availableRights.ui.users.canRemove,
    };

    super(
      router,
      editServiceFactory(),
      formBuilder,
      displayedColumns,
      gridOptions,
      ModuleConstants.instance.moduleName,
      rights,
      [],
      10
    );

    userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;

          // todo: insert checks for rights to hide columns
          if (!user.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.users.canShowDeleted)) {
            this.removeDisplayedColumn('isDeleted');
          }
        }
      },
    });
  }

  createDtoInstance(): WorkDataDto {
    return new WorkDataDto();
  }
}
