import { Component } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss'],
})
export class ReportingComponent {}
