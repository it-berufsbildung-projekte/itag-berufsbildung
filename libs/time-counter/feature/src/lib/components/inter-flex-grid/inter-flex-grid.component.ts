import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { InterFlexDto, ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { BaseGrid, IDisplayedColumns, IGridOptions } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { InterFlexEditService } from '../../services/inter-flex-edit.service';

@Component({
  selector: 'itag-berufsbildung-inter-flex-grid',
  templateUrl: './../../../../../../shared/data-ui/src/lib/templates/base-grid.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.Emulated,
})
export class InterFlexGridComponent extends BaseGrid<InterFlexDto> {
  moduleConstants = ModuleConstants.instance;
  user!: UiUserDto;
  showDeleted = false;

  constructor(
    override readonly router: Router,
    private readonly userService: UserService,
    override readonly formBuilder: FormBuilder,
    @Inject(InterFlexEditService) editServiceFactory: () => InterFlexEditService
  ) {
    const displayedColumns: IDisplayedColumns[] = [
      {
        columnName: 'name',
        width: 44,
        type: 'string',
        readonly: false,
      },
      {
        columnName: 'sortOrder',
        width: 16,
        type: 'number',
        readonly: false,
      },
    ];

    const gridOptions: IGridOptions = {
      isInlineEdit: true,
      showToolbar: true,
      showCommandColumn: true,
      sortable: true,
      filterable: false,
      pageable: true,
      maxHeightPx: 1000,
      selectable: false,
      showAddButton: true,
      pdfExport: false,
      excelExport: false,
    };

    const rights = {
      canShowDeleted: ModuleConstants.instance.availableRights.ui.interFlex.canShowDeleted,
      canCreate: ModuleConstants.instance.availableRights.ui.interFlex.canCreate,
      canUpdate: ModuleConstants.instance.availableRights.ui.interFlex.canUpdate,
      canModifyDelete: ModuleConstants.instance.availableRights.ui.interFlex.canModifyDelete,
      canRemove: ModuleConstants.instance.availableRights.ui.interFlex.canRemove,
    };

    super(
      router,
      editServiceFactory(),
      formBuilder,
      displayedColumns,
      gridOptions,
      ModuleConstants.instance.moduleName,
      rights,
      [],
      10
    );

    userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;

          // todo: insert checks for rights to hide columns
          if (!user.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.interFlex.canShowDeleted)) {
            this.removeDisplayedColumn('isDeleted');
          }
        }
      },
    });
  }

  createDtoInstance(): InterFlexDto {
    return new InterFlexDto();
  }
}
