import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterFlexGridComponent } from './inter-flex-grid.component';

describe('InterFlexGridComponent', () => {
  let component: InterFlexGridComponent;
  let fixture: ComponentFixture<InterFlexGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterFlexGridComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(InterFlexGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
