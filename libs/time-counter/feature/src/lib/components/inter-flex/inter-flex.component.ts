import { Component } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-inter-flex',
  templateUrl: './inter-flex.component.html',
  styleUrls: ['./inter-flex.component.scss'],
})
export class InterFlexComponent {}
