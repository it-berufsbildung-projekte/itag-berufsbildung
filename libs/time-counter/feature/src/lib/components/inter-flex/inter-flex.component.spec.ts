import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterFlexComponent } from './inter-flex.component';

describe('InterFlexComponent', () => {
  let component: InterFlexComponent;
  let fixture: ComponentFixture<InterFlexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InterFlexComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(InterFlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
