import { Component } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {}
