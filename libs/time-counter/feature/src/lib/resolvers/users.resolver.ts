import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { map, Observable } from 'rxjs';
import { UsersEntityService } from '../services/users-entity.service';

@Injectable()
export class UsersResolver implements Resolve<boolean> {
  constructor(private readonly usersEntityService: UsersEntityService) {}

  resolve(): Observable<boolean> {
    return this.usersEntityService.getWithQuery({ showDeleted: 'false' }).pipe(map((usersDtoArr) => !!usersDtoArr));
  }
}
