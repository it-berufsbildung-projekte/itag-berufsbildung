import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { map, Observable } from 'rxjs';
import { InterFlexEntityService } from '../services/inter-flex-entity.service';

@Injectable()
export class InterFlexResolver implements Resolve<boolean> {
  constructor(private readonly interFlexEntityService: InterFlexEntityService) {}

  resolve(): Observable<boolean> {
    return this.interFlexEntityService.getWithQuery({ showDeleted: 'false' }).pipe(map((interFlexDtoArr) => !!interFlexDtoArr));
  }
}
