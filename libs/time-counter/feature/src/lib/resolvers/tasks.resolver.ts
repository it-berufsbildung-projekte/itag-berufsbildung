import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { map, Observable } from 'rxjs';
import { TasksEntityService } from '../services/tasks-entity.service';

@Injectable()
export class TasksResolver implements Resolve<boolean> {
  constructor(private readonly tasksEntityService: TasksEntityService) {}

  resolve(): Observable<boolean> {
    return this.tasksEntityService.getWithQuery({ showDeleted: 'false' }).pipe(map((tasksDtoArr) => !!tasksDtoArr));
  }
}
