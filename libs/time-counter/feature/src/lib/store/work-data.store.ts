import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, WorkDataDto } from '@itag-berufsbildung/time-counter/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';

export interface IworkDataState {
  workData: WorkDataDto[];
}

@Injectable()
export class workDatastore extends ComponentStore<IworkDataState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: IworkDataState;

  constructor(
    private readonly http: HttpClient,
    private readonly notificationService: NotificationService,
    private readonly userService: UserService,
  ) {
    super(
      {
        workData: [],
      }
    );

    this.userService.userDto$.subscribe( {
          next: (user) => {
            if ( user ) {
              const moduleInfo = user.moduleInfoArr.find( module => module.moduleName === ModuleConstants.instance.moduleName );
              this.apiUrl = moduleInfo?.apiUrl as string;

              this.load();
            }
          }
        } );
  }

  readonly workData$: Observable<WorkDataDto[]> = this.select( state => state.workData );

  private load = this.effect( (origin$: Observable<void>) =>
    origin$.pipe(
      switchMap( () => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.workData}`;
        return this.http.get<WorkDataDto[]>( url );
      } ),
      tapResponse(
        (workDatas: WorkDataDto[]) => this.patchState( () => ( { workData: workDatas } ) ),
        console.error,
      ),
    ),
  );

  add( WorkDataDto: WorkDataDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.workData}`;
      this.http.post<WorkDataDto>( url, WorkDataDto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      return { workData: [  ...state.workData, WorkDataDto ] };
    } );
  }

  update( WorkDataDto: WorkDataDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.workData}/${WorkDataDto}`;
      this.http.patch( url, WorkDataDto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      const index = state.workData.findIndex( e => e.uuid === WorkDataDto.uuid );
      const workDatas = [  ...state.workData ];
      workDatas[index] = WorkDataDto;
      return { workData: workDatas };
    } );
  }

  delete( WorkDataDto: WorkDataDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.workData}/${WorkDataDto.uuid}`;
      this.http.delete( url ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );
      return { workData: state.workData.filter( e => e.uuid !== WorkDataDto.uuid ) };
    } );
  }

  private revertChangesAndShowError( error: HttpErrorResponse ): void {
    this.patchState( () => {
      this.notificationService.showError( error.message );
      return {...this.oldState};
    } );
  }

}
