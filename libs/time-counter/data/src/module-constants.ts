import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
      interFlex: {
        canCreate: 'api-inter-flex-can-create',
        canModifyDelete: 'api-inter-flex-can-modify-delete',
        canRead: 'api-inter-flex-can-read',
        canRemove: 'api-inter-flex-can-remove',
        canShowDeleted: 'api-inter-flex-can-show-deleted',
        canUpdate: 'api-inter-flex-can-update',
        canSeeHistory: 'api-inter-flex-can-see-history',
      },
      users: {
        canCreate: 'api-users-can-create',
        canModifyDelete: 'api-users-can-modify-delete',
        canRead: 'api-users-can-read',
        canRemove: 'api-users-can-remove',
        canShowDeleted: 'api-users-can-show-deleted',
        canUpdate: 'api-users-can-update',
        canSeeHistory: 'api-users-can-see-history',
      },
      tasks: {
        canCreate: 'api-tasks-can-create',
        canModifyDelete: 'api-tasks-can-modify-delete',
        canRead: 'api-tasks-can-read',
        canRemove: 'api-tasks-can-remove',
        canShowDeleted: 'api-tasks-can-show-deleted',
        canUpdate: 'api-tasks-can-update',
        canSeeHistory: 'api-tasks-can-see-history',
      },
      workData: {
        canCreate: 'api-work-data--can-create',
        canModifyDelete: 'api-work-data--can-modify-delete',
        canRead: 'api-work-data--can-read',
        canRemove: 'api-work-data--can-remove',
        canShowDeleted: 'api-work-data--can-show-deleted',
        canUpdate: 'api-work-data--can-update',
        canSeeHistory: 'api-work-data--can-see-history',
      }
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
      canShowSubMenuDaily: 'menu-can-show-sub-menu-daily',
      canShowSubMenuReporting: 'menu-can-show-sub-menu-reporting',

      canShowSubMenuAdmin: 'menu-can-show-sub-menu-admin',
      canShowSubMenuAdminTasks: 'menu-can-show-sub-menu-admin-tasks',
      canShowSubMenuAdminInterFlex: 'menu-can-show-sub-menu-admin-inter-flex',
      canShowSubMenuAdminUsers: 'menu-can-show-sub-menu-admin-users',

      canShowSubMenuWorkData: 'menu-can-show-sub-menu-work-datas',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
      interFlex: {
        canCreate: 'ui-inter-flex-can-create',
        canModifyDelete: 'ui-inter-flex-can-modify-delete',
        canRead: 'ui-inter-flex-can-read',
        canRemove: 'ui-inter-flex-can-remove',
        canShowDeleted: 'ui-inter-flex-can-show-deleted',
        canUpdate: 'ui-inter-flex-can-update',
        canSeeHistory: 'ui-inter-flex-can-see-history',
      },
      users: {
        canCreate: 'ui-users-can-create',
        canModifyDelete: 'ui-users-can-modify-delete',
        canRead: 'ui-users-can-read',
        canRemove: 'ui-users-can-remove',
        canShowDeleted: 'ui-users-can-show-deleted',
        canUpdate: 'ui-users-can-update',
        canSeeHistory: 'ui-users-can-see-history',
      },
      tasks: {
        canCreate: 'ui-tasks-can-create',
        canModifyDelete: 'ui-tasks-can-modify-delete',
        canRead: 'ui-tasks-can-read',
        canRemove: 'ui-tasks-can-remove',
        canShowDeleted: 'ui-tasks-can-show-deleted',
        canUpdate: 'ui-tasks-can-update',
        canSeeHistory: 'ui-tasks-can-see-history',
      },
      workData: {
        canCreate: 'ui-work-data-can-create',
        canModifyDelete: 'ui-work-data-can-modify-delete',
        canRead: 'ui-work-data-can-read',
        canRemove: 'ui-work-data-can-remove',
        canShowDeleted: 'ui-work-data-can-show-deleted',
        canUpdate: 'ui-work-data-can-update',
        canSeeHistory: 'ui-work-data-can-see-history',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
  };

  // hint: ngRx can't be kebab-case and the controller goes to the documents.
  readonly controllerTags = {
    interFlex: 'inter-flex',
  };

  readonly description = 'Beschreibung des Time Counter Modules';

  readonly documents = {
    interFlex: 'inter-flex',
    users: 'users',
    tasks: 'tasks',
    workData: 'work-data'
  };

  readonly documentNames = {
    single: {
      interFlex: 'interFlex',
      users: 'users',
      tasks: 'tasks',
      workData: 'work-data'
    },
    plural: {
      interFlex: 'interFlex',
      users: 'users',
      tasks: 'tasks',
      workData: 'work-data'
    },
  };

  readonly moduleName = 'TimeCounter';

  readonly routing = {
    base: 'time-counter',
    sub: {
      daily: 'daily',
      reporting: 'reporting',
      admin: 'admin',
      tasks: 'tasks',
      interFlex: 'inter-flex',
      users: 'users',
      workData: 'work-data'
    },
  };

  readonly version = '1.0.0';
}
