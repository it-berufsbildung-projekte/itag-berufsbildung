import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class WorkDataDto extends BaseDocumentDto {
  // the eMail of the user working on this data
  user!: string;
  guid!: string;
  methodGuid!: string;
  userGuid?: string;
  fromTimeKey!: string;
  duration?: number;
  text!: string;
  dateKey!: string;

  constructor() {
    super();
  }
}
