import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class TasksDto extends BaseDocumentDto {
  name = 'Tasks example Name';
  sortOrder = 1;
  interFlexUuid = 'example-uuid';

  constructor() {
    super();
  }
}
