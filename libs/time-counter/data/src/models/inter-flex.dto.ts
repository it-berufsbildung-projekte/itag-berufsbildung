import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class InterFlexDto extends BaseDocumentDto {
  name = 'InterFlex example Name';
  sortOrder = '1';

  constructor() {
    super();
  }
}
