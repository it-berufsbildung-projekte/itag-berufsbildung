import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class UsersDto extends BaseDocumentDto {
  username = 'Users example Name';
  displayName = 'Users example display name';
  eMail = 'users@email.com';

  constructor() {
    super();
  }
}
