import { Injectable } from '@angular/core';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { ObjectTools, SorterTools } from '@itag-berufsbildung/shared/util';
import { BehaviorSubject } from 'rxjs';
import { IActiveFilter, NotificationMessage, NotificationMessageType } from '../models';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  messages$: BehaviorSubject<NotificationMessage[]> = new BehaviorSubject<NotificationMessage[]>([]);

  readonly activeFilter: IActiveFilter = {
    showErrors: true,
    showWarnings: true,
    showInfos: false,
    showSuccess: false,
    showDebug: false,
  };

  errors: NotificationMessage[] = [];
  warnings: NotificationMessage[] = [];
  infos: NotificationMessage[] = [];
  successes: NotificationMessage[] = [];
  debugs: NotificationMessage[] = [];

  activeMessages: NotificationMessage[] = [];


  // info, debug, success werden automatisch geschlossen
  private localStoreKeyAutoClose = 'notification-auto-close-active';
  private localStoreKeyActiveFilters = 'notification-active-filters';
  private _autoCloseActive = true;
  get autoCloseActive() {
    return this._autoCloseActive;
  }
  set autoCloseActive(value: boolean) {
    this._autoCloseActive = value;
    this.localStoreService.setBoolean(this.localStoreKeyAutoClose, value);
  }

  private listOfMessages: NotificationMessage[] = [];

  constructor(
    private readonly localStoreService: LocalStoreService,
  ) {

    // read from localStore the autoCloseActive State
    this._autoCloseActive = this.localStoreService.getBoolean(this.localStoreKeyAutoClose);
    // read the previous saved filter from the localStore
    this.activeFilter = this.localStoreService.getObject<IActiveFilter>(this.localStoreKeyActiveFilters, this.activeFilter);
    // ensure all has set once on startup
    const errorFilter = this.activeFilter.showErrors;
    this.setFilter('error', errorFilter);


    // every 500ms we check if there is an item wich is expired
    setInterval(() => {
      if ( !this.autoCloseActive ) {
        // abbruch, wenn nicht automatisch geschlossen werden soll.
        return;
      }
      const now = +(new Date);
      const newList = this.listOfMessages.filter( e => e.untilTimestamp === 0 || e.untilTimestamp > now);
      this.listOfMessages = newList;

      this.messages$.next(newList);

    }, 500);

    this.messages$.subscribe( ( messages) => {
      this.errors = messages.filter( e => e.type === 'error');
      this.warnings = messages.filter( e => e.type === 'warning');
      this.infos = messages.filter( e => e.type === 'info');
      this.successes = messages.filter( e => e.type === 'success');
      this.debugs = messages.filter( e => e.type === 'debug');
      this.setFilteredData();
    });

  }

  invertFilter(showType: NotificationMessageType) {
    let oldValue = false;
    switch (showType) {
      case 'error':
        oldValue = this.activeFilter.showErrors;
        break;
      case 'warning':
        oldValue = this.activeFilter.showWarnings;
        break;
      case 'info':
        oldValue = this.activeFilter.showInfos;
        break;
      case 'success':
        oldValue = this.activeFilter.showSuccess;
        break;
      case 'debug':
        oldValue = this.activeFilter.showDebug;
        break;
    }
    this.setFilter(showType, !oldValue);
  }

  setFilter(showType: NotificationMessageType, value = true) {
    switch (showType) {
      case 'error':
        this.activeFilter.showErrors = value;
        break;
      case 'warning':
        this.activeFilter.showWarnings = value;
        break;
      case 'info':
        this.activeFilter.showInfos = value;
        break;
      case 'success':
        this.activeFilter.showSuccess = value;
        break;
      case 'debug':
        this.activeFilter.showDebug = value;
        break;
    }
    // save the current active Filter to the localStore
    this.localStoreService.setObject(this.localStoreKeyActiveFilters, this.activeFilter);
    this.setFilteredData();
  }


  // get all messages applied by filter
  setFilteredData() {
    let newMessages: NotificationMessage[] = [];
    if (this.activeFilter.showErrors) {
      newMessages =newMessages.concat(this.errors);
    }
    if (this.activeFilter.showWarnings) {
      newMessages =newMessages.concat(this.warnings);
    }
    if (this.activeFilter.showInfos) {
      newMessages =newMessages.concat(this.infos);
    }
    if (this.activeFilter.showSuccess) {
      newMessages =newMessages.concat(this.successes);
    }
    if (this.activeFilter.showDebug) {
      newMessages =newMessages.concat(this.debugs);
    }
    this.activeMessages =  newMessages.sort(SorterTools.dynamicSort('created'));
  }

  closeAll() {
    const active = ObjectTools.cloneDeep<NotificationMessage[]>(this.activeMessages);
    for( const item of active) {
      this.closeMessage(item);
    }
  }

  showDebug(content: string): void {
    if ( this.activeFilter.showDebug) {
      console.log( 'debug:' + content);
    }
    this.addMessage(content, 'debug');
  }

  // show a success message on the top center
  showSuccess(content: string): void {
    if ( this.activeFilter.showSuccess) {
      console.log( 'success:' + content);
    }
    this.addMessage(content, 'success');
  }
  // show an info message on the center top
  showInfo(content: string): void {
    if ( this.activeFilter.showInfos) {
      console.log( 'info:' + content);
    }
    this.addMessage(content, 'info');
  }
  // show a warning message on the left top
  showWarning(content: string): void {
    if ( this.activeFilter.showWarnings) {
      console.warn( content);
    }
    this.addMessage(content, 'warning');
  }
  // show an error message on the right top
  showError(content: string, error?: Error): void {
    if (error) {
      content += '\n\r' + error.message;
      content += '\n\r' + error.stack;
    }
    if ( this.activeFilter.showErrors) {
      console.error( content, error );
    }
    this.addMessage(content, 'error');
  }

  closeMessage( notificationMessage: NotificationMessage | undefined ) {
    if ( !notificationMessage ) {
      return;
    }
    // remove from the list by uuid
    const newList = this.listOfMessages.filter( e => e.uuid !== notificationMessage.uuid );
    this.listOfMessages = newList;
    this.messages$.next(newList);
  }

  private addMessage(message: string, type: NotificationMessageType) {
    const msg = new NotificationMessage(message, type);
    this.listOfMessages.push(msg);
    this.messages$.next(this.listOfMessages);
  }

}
