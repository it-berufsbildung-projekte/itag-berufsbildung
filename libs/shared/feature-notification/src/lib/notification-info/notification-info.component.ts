import { Component, Input } from '@angular/core';
import {  DatePipe, NgClass, NgIf } from '@angular/common';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { NotificationMessage } from '../models';
import { NotificationService } from '../services/notification.service';

@Component({
  standalone: true,
  imports: [
    NgIf,
    ButtonModule,
    NgClass,
    DatePipe,
  ],
  selector: 'itag-berufsbildung-notification-info',
  templateUrl: './notification-info.component.html',
  styleUrls: ['./notification-info.component.scss'],
})
export class NotificationInfoComponent {

  constructor(
    private readonly notificationService: NotificationService,
  ) {
  }

  @Input() notificationMessage?: NotificationMessage;

  closeClick() {
    this.notificationService.closeMessage(this.notificationMessage);
  }

  getClassName( type: 'error' | 'warning' | 'info' | 'success' | 'debug' ): string {
    switch(type) {
      case 'error': return 'k-button-solid-error';
      case 'warning': return 'k-button-solid-warning';
      case 'info': return 'k-button-solid-info';
      case 'success': return 'k-button-solid-success';
      case 'debug': return 'k-button-solid-inverse';
    }
    return '';
  }
}
