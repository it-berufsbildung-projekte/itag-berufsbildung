import { Component } from '@angular/core';
import { NotificationMessageType } from '../models';
import { AsyncPipe, NgForOf, NgIf } from '@angular/common';
import { CardModule } from '@progress/kendo-angular-layout';
import { ButtonGroupModule, ButtonModule } from '@progress/kendo-angular-buttons';
import { WindowModule } from '@progress/kendo-angular-dialog';
import { LabelModule } from '@progress/kendo-angular-label';
import { SwitchModule } from '@progress/kendo-angular-inputs';
import { FormsModule } from '@angular/forms';
import { NotificationInfoComponent } from '../notification-info/notification-info.component';
import { NotificationService } from '../services/notification.service';

@Component({
  standalone: true,
  imports: [
    NgIf,
    ButtonGroupModule,
    WindowModule,
    ButtonModule,
    AsyncPipe,
    LabelModule,
    SwitchModule,
    FormsModule,
    NgForOf,
    NotificationInfoComponent,
    CardModule,
  ],
  selector: 'itag-berufsbildung-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent {

  constructor(
    public readonly notificationService: NotificationService,
  ) {
  }

  getButtonTheme( showType: NotificationMessageType) {
    switch ( showType ) {
      case 'error': return (this.notificationService.activeFilter.showErrors ? 'error' : 'none');
      case 'warning': return (this.notificationService.activeFilter.showWarnings ? 'warning' : 'none');
      case 'info': return (this.notificationService.activeFilter.showInfos ? 'info' : 'none');
      case 'success': return (this.notificationService.activeFilter.showSuccess ? 'success' : 'none');
      case 'debug': return (this.notificationService.activeFilter.showDebug ? 'inverse' : 'none');
      default: return 'none';
    }
  }

  switchShow( showType: NotificationMessageType ) {
    this.notificationService.invertFilter(showType);
  }

  closeAll() {
    this.notificationService.closeAll();
  }
}
