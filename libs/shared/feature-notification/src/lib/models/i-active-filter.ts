export interface  IActiveFilter {
  showErrors: boolean,
  showWarnings: boolean,
  showInfos: boolean,
  showSuccess: boolean,
  showDebug: boolean
}
