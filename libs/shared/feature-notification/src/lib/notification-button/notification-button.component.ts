import { Component } from '@angular/core';
import { NotificationMessageType } from '../models';
import { AsyncPipe,NgForOf, NgIf } from '@angular/common';
import { ButtonGroupModule, ButtonModule } from '@progress/kendo-angular-buttons';
import { WindowModule } from '@progress/kendo-angular-dialog';
import { LabelModule } from '@progress/kendo-angular-label';
import { SwitchModule } from '@progress/kendo-angular-inputs';
import { FormsModule } from '@angular/forms';
import { NotificationInfoComponent } from '../notification-info/notification-info.component';
import { NotificationListComponent } from '../notification-list/notification-list.component';
import { NotificationService } from '../services/notification.service';

@Component({
  standalone: true,
  imports: [
    NgIf,
    ButtonGroupModule,
    WindowModule,
    ButtonModule,
    AsyncPipe,
    LabelModule,
    SwitchModule,
    FormsModule,
    NgForOf,
    NotificationInfoComponent,
    NotificationListComponent,
  ],
  selector: 'itag-berufsbildung-notification-button',
  templateUrl: './notification-button.component.html',
  styleUrls: ['./notification-button.component.scss'],
})
export class NotificationButtonComponent  {

  showDetails = false;

  constructor(
    public readonly notificationService: NotificationService,
  ) {}

  openClose( show: boolean ) {
    this.showDetails = show;
  }

  showDialog(showType: NotificationMessageType) {
    this.notificationService.setFilter(showType);
    this.openClose(true);
  }

}
