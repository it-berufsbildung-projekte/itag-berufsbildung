import { GlobalConstants } from '../../global-constants';

export class MandatoryRoles {
  static api = GlobalConstants.mandatoryRoles.api;
  static admin = GlobalConstants.mandatoryRoles.admin;
  static readOnly = GlobalConstants.mandatoryRoles.readonly;
  static user = GlobalConstants.mandatoryRoles.user;
  [key: string]: string;
}

export class RoutingInfo {
  base = '';
  sub: { [key: string]: string } = {};
}

export class AvailableRightsInfo {
  api: { [key: string]: { [key: string]: string } } = {};
  menu: { [key: string]: string } = {};
  ui: { [key: string]: { [key: string]: string } } = {};
}

export abstract class BaseModuleConstants {
  abstract readonly availableRights: AvailableRightsInfo;
  abstract readonly availableRoles: MandatoryRoles;
  abstract readonly moduleName: string;
  abstract readonly description: string;
  // abstract readonly rights: {
  //   [key: string]: { [key: string]: string};
  // };
  abstract readonly routing: RoutingInfo;
  abstract readonly version: string;
}
