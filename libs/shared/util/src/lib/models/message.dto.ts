export class MessageDto {
  /**
   * always return an object over the rest and not only a simple type
   */
  message!: string;
}
