import { AdminModuleInfoDto } from './admin-module-info.dto';
import { Menu } from './menu-info.dto';

export interface RoleMembershipInfo {
  /**
   * from will have the default value of 0
   */
  from: number; // timestamp
  /**
   * until should be later as the from field. if we do have no until then it should be set to undefined
   */
  until: number | undefined; // timestamp
}

export class RoleHashDto {
  [roleName: string]: RoleMembershipInfo;
}
export class UserRoleHashDto {
  [eMail: string]: RoleHashDto;
}
export class ModuleRoleHashDto {
  [moduleName: string]: RoleHashDto;
}

/**
 * very often we get only an interface back from services. But as we sometimes require the full information we have to create from this "interface" a class
 */
export class ApiUserDtoInstanceCreator {
  static createInstance(userDto: ApiUserDto | undefined): ApiUserDto | undefined {
    if (!userDto) {
      return userDto;
    }
    const newUserDto = new ApiUserDto();
    for (const key of Object.keys(userDto)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      newUserDto[key] = userDto[key];
    }
    return newUserDto;
  }
}

// export class UiUserDtoInstanceCreator {
//   static createInstance(userDto: ApiUserDto | undefined): UiUserDto | undefined {
//     if (!userDto) {
//       return undefined;
//     }
//     const newUserDto = new UiUserDto();
//     for (const key of Object.keys(userDto)) {
//       // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//       // @ts-ignore
//       newUserDto[key] = userDto[key];
//     }
//     return newUserDto;
//   }
// }

export class UserDto {
  /**
   * the key for the user
   */
  eMail!: string;
}

export class ApiUserDto extends UserDto {
  /**
   * a return value of type RoleHash with key of roleName and RoleMembershipInfo
   */
  apiRoles: RoleHashDto = {};
  /**
   * an array with all the rights for a user of an api
   */
  apiRights: string[] = [];

  hasRight(rightName: string): boolean {
    if (!this.apiRights || Object.keys(this.apiRights).length === 0) {
      return false;
    }
    return !!this.apiRights.find((r) => r.toLowerCase() === rightName.toLowerCase());
  }

  hasRole(roleName: string): boolean {
    if (!this.apiRoles || Object.keys(this.apiRoles).length === 0) {
      return false;
    }
    return !! Object.keys(this.apiRoles).find((r) => r.toLowerCase() === roleName.toLowerCase());
  }
}

export class UiUserDto extends UserDto {
  /**
   * the picture is coming from the authentication process and will be not saved on the server
   */
  picture?: string | undefined;

  /**
   * a hash with all the menu per module
   */
  menuHash: { [moduleName: string]: Menu } = {};

  /**
   * a hash with all the roles per module
   */
  uiRoles: ModuleRoleHashDto = {};

  /**
   * a hash with all the rights per module
   */
  uiRights: { [modleName: string]: string[] } = {};

  /**
   *
   */
  moduleInfoArr: Partial<AdminModuleInfoDto>[] = [];

  hasRight(moduleName: string, rightName: string): boolean {
    if (!this.uiRights || Object.keys(this.uiRights).length === 0 || this.uiRights[moduleName]?.length === 0) {
      return false;
    }
    return !!this.uiRights[moduleName]?.find((r) => r.toLowerCase() === rightName.toLowerCase());
  }

  hasRole(moduleName: string, roleName: string): boolean {
    if (!this.uiRoles || Object.keys(this.uiRoles).length === 0 || Object.keys(this.uiRoles[moduleName])?.length === 0) {
      return false;
    }
    return !!Object.keys(this.uiRoles[moduleName])?.find((r) => r.toLowerCase() === roleName.toLowerCase());
  }
}
