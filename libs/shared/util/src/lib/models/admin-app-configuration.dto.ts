import { AvailableRightsInfo } from '../base/base-module-constants';
import { Menu } from './menu-info.dto';

export declare type LogLevel = 'log' | 'error' | 'warn' | 'debug' | 'verbose';

export interface AuthConfig {
  issuer: string;
  aud: string;
  clientId: string;
  clientSecret: string;
}

export type AppEnvironment = 'dev' | 'test' | 'prod';

export class UserAppConfigurationDto {
  description = 'Beschreibung des Modules';
  /**
   * one of dev | test | prod
   */
  environment: AppEnvironment = 'prod';
  isProduction = true;
  moduleName = 'undefined';
  version = '0.0.0';
}

export class AdminAppConfigurationDto extends UserAppConfigurationDto {
  registerApi = true;
  apiHost = 'localhost';
  apiPort = 3330;
  apiPrefix = 'api';
  apiProtocol: 'http' | 'https' = 'https';
  availableRights: AvailableRightsInfo = {
    api: {},
    menu: {},
    ui: {},
  };
  availableRoles: string[] = [];

  auth: AuthConfig = {
    issuer: '',
    aud: '',
    clientId: '',
    clientSecret: '',
  };

  baseApiUrl = '';
  /**
   * array with elements of: verbose, debug, log, warn, error
   */
  logLevels: LogLevel[] = ['verbose', 'debug', 'log', 'warn', 'error'];

  /**
   * die Menu und routing informationen der Anwendung. Der Benutzer selber wird nur ein Subset davon bekommen
   */
  menu!: Menu;
  /**
   * the origin, default *
   */
  origin = '*';

  serverHost = 'localhost';
  serverPort: string | undefined = undefined;
  serverProtocol: 'http' | 'https' = 'https';

}
