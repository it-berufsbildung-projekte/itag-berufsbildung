export class RedisClientSetting {
  database?: number;
  password?: string;
  port?: number;
  url?: string;
  username?: string;
}
