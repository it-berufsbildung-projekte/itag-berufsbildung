export class RoleRightHashDto {
  [roleName: string]: string[];
}

export class RoleRightDto {
  rights: string[] = [];
}
