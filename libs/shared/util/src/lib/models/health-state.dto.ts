export class HealthStateDto {
  /**
   * the message to show
   */
  message = 'Der API Server arbeitet wie erwartet';
  /**
   * one of okay | error | initializing
   */
  state: 'okay' | 'error' | 'initializing' = 'initializing';
}
