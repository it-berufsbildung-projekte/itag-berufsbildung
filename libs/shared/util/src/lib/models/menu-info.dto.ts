/**
 * this datatype is for a menu entry, not for a separator
 */
export interface IMenuItem {
  /**
   * the text we show on the menu
   */
  text: string;
  /**
   * the tooltip title
   */
  title: string;
  /**
   * an kendo-ui-angular icon name
   */
  icon: string;
  /**
   * the url path to the component
   */
  path: string;
  /**
   * level for ident. 0 for top-level
   */
  level: number;
  /**
   * we always have to set a right
   */
  right: string;
  /**
   * on runtime the status to show if selected
   */
  selected?: boolean;
  /**
   * on runtime the status to show if a menu is expanded or not.
   * only possible for menus with children
   */
  expanded?: boolean;
  /**
   * also shows if it's a parent menu
   */
  children?: Menu[];
  /**
   * we don't want to have this property set
   */
  separator?: never;
}

export interface IOrderedMenuItem extends IMenuItem {
  order: number;
  moduleName: string;
}

/**
 * this datatype is only for a separator
 */
export interface IMenuSeparator {
  text?: never;
  title?: never;
  icon?: never;
  path?: never;
  level?: never;
  selected?: never;
  expanded?: never;
  children?: never;

  /**
   * for a separator the right is optional
   */
  right?: string;
  /**
   * only this property has to be set to true
   */
  separator: true;
}
export interface IOrderedMenuSeparator extends IMenuSeparator {
  order: number;
  moduleName: string;
}

export type Menu = IMenuItem | IMenuSeparator;

export type OrderedMenu = IOrderedMenuItem | IOrderedMenuSeparator;
