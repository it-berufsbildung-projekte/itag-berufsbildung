import { v4 } from 'uuid';

export type IBaseUuidDtoHistory = IBaseDocumentDtoPostHistory | IBaseDocumentDtoUpdateHistory;

export type IBaseDocumentDtoPostHistory = {
  method: 'POST';
  email: string;
  timestamp: number;
}

export type IBaseDocumentDtoUpdateHistory = {
  method: 'UPDATE';
  email: string;
  timestamp: number;
  oldValue: unknown;
}

export class BaseDocumentDto {
  isDeleted? = false;
  uuid!: string;
  history: IBaseUuidDtoHistory[] | undefined = [];
  constructor( ) {
    this.uuid = v4();
  }

}

// example to show in swagger
export const baseDocumentExample = new BaseDocumentDto();
export const baseDocumentExampleArr = [
  new BaseDocumentDto(),
  new BaseDocumentDto(),
  new BaseDocumentDto(),
];
