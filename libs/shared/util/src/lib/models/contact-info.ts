export interface ContactInfo {
  name: string;
  url: string;
  eMail: string;
}
