export interface IQueryParams {
  [key: string]: undefined | string | string[];
}
