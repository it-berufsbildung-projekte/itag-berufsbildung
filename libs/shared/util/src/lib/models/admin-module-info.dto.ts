import { AvailableRightsInfo } from '../base/base-module-constants';
import { AdminAppConfigurationDto } from './admin-app-configuration.dto';
import { Menu } from './menu-info.dto';
import { RedisClientSetting } from './redis-client-setting';

export class UserModuleInfoDto {
  apiUrl = '';
  description = 'Module Beschreibung';
  moduleName = 'Unknown';
  version = '0.0.0';
}

export class AdminModuleInfoDto extends UserModuleInfoDto {
  appConfig?: AdminAppConfigurationDto = undefined;
  availableRights?: AvailableRightsInfo = undefined;
  availableRoles?: string[] = undefined;
  menu?: Menu;
  dbSettings?: RedisClientSetting;
}
