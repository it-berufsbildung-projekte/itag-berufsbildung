import { TestBed } from '@angular/core/testing';
import { TypeCheckers } from './type-checkers';

describe('TypeCheckerTools', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('string', () => {

    it( 'with text', () => {
      const val = 'a valid string';
      const result = TypeCheckers.isString( val );
      expect( result ).toEqual(true);
    } );

    it( 'with number', () => {
      const val = 123;
      const result = TypeCheckers.isString( val );
      expect( result ).toEqual(false);
    } );

    it( 'with  bool', () => {
      const val = false;
      const result = TypeCheckers.isString( val );
      expect( result ).toEqual(false);
    } );

    it( 'with undefined', () => {
      const val = undefined;
      const result = TypeCheckers.isString( val );
      expect( result ).toEqual(false);
    } );

    it( 'with null', () => {
      const val = null;
      const result = TypeCheckers.isString( val );
      expect( result ).toEqual(false);
    } );
  });
});
