export class TypeCheckers {
  static isString(val: unknown): boolean {
    return typeof val === 'string' || val instanceof String;
  }
}
