export class DateAndTimeTools {

  static getWeekNumberForDateKey(dateKey: string): number {
    if (!dateKey) {
      return 0;
    }
    const date = this.getDateFromIsoDate(dateKey);
    return this.getWeekNumber( date );
  }

  /***
   * return the iso calendar week for a give date
   * @param date the date to calculate for
   */
  static getWeekNumber(date: Date): number {
    const startDate = new Date(date.getFullYear(), 0, 1);
    const days = Math.floor(
      (+date - (+startDate) ) / (24 * 60 * 60 * 1000)
    );

    // date.setHours(0, 0, 0, 0);
    // // Thursday in current week decides the year.
    // date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // // January 4 is always in week 1.
    // const week1 = new Date(date.getFullYear(), 0, 4);
    // // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    // return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);

    return Math.ceil( days / 7 );
  }

  /***
   * return the YYYY-MM-DD string of a date
   * @param date the date to calculate for
   */
  static getDateKey(date: Date): string {
    // making sure the timezone doesn't give us a day before the Date.
    // todo: gibt manchmal einen Tag zu früh zurück. Passiert auch nur über getDateKeyAndTimeKeyForDate.
    return new Date( date.getTime() - date.getTimezoneOffset() * 600000).toISOString().split('T')[0];
  }

  // /***
  //  * return the ISO week of the current day
  //  */
  // static getTodayWeek(): number {
  //   return DateAndTimeTools.getWeekNumber(new Date());
  // }

  /***
   * return the YYYY-MM-DD string of the current day
   */
  static getTodayDateKey(): string {
    // we give it back directly since calling the method getDateKey will give us a day prior to today
    return new Date().toISOString().split('T')[0];
  }

  /***
   * return a date object
   * @param dateKey: a string in the ISO format YYYY-MM-DD
   * @param timeKey an optional string in the format of hh:mm, we also support h:mm and h for a fully hour. if omit we use 0:00
   */
  static getDateFromIsoDate(dateKey: string, timeKey?: string): Date {
    if ( !dateKey ) {
      dateKey = this.getDateKey( new Date() );
    }
    const year: number = parseInt(dateKey.substring(0,4), 10);
    const month: number = parseInt(dateKey.substring(5,7), 10) - 1; // be carefully the month in JS is 0 based
    const day: number = parseInt(dateKey.substring(8), 10);
    let hours = 0;
    let minutes = 0;
    if (timeKey) {
      // check if there is a : in the key
      if (timeKey.indexOf(':') > -1 ) {
        // split the timeKey in hour and min
        const hm = timeKey.split(':');
        const h: number = parseInt(hm[0], 10);
        const m: number = hm[1] === '' ? 0 :  parseInt(hm[1], 10);
        // check if the h is valid
        if (h >= 0 && h <= 23) {
          hours = h;
        } else {
          const msg = `the used timeKey ${timeKey} is invalid (check h)`;
          console.error(msg);
          throw new Error(msg);
        }
        // check if the h is valid
        if (m >= 0 && m <= 59) {
          minutes = m;
        } else {
          const msg = `the used timeKey ${timeKey} is invalid (check m)`;
          console.error(msg);
          throw new Error(msg);
        }
      } else {
        const h = parseInt(timeKey, 10);
        // check if the time is less than 24 and greater than -1
        if (h >= 0 && h <= 23) {
          hours = h;
        } else {
          const msg = `the used timeKey ${timeKey} is invalid`;
          console.error(msg);
          throw new Error(msg);
        }
      }

    }
    return new Date(year, month, day, hours, minutes);
  }

  /***
   * return a tuple with week: number, dateKey: YYYY-MM-DD and timeKey: hh:mm
   * @param date
   */
  static getDateKeyAndTimeKeyForDate(date: Date): { week: number; dateKey: string; timeKey: string } {
    const h = date.getHours();
    const min = date.getMinutes();
    const timeKey = `${h > 9 ? h : '0' + h}:${min > 9 ? min : '0' + min}`;
    return {
      week: DateAndTimeTools.getWeekNumber(date),
      dateKey: DateAndTimeTools.getDateKey(date),
      timeKey,
    };
  }

  /***
   * returns the new time Key, !!! we do not fix any next day time.
   * in fact the time that you get back on the timeKey 23:55 and adding 10 will be 0:05
   * @param dateKey a date in the yyyy-mm-dd format
   * @param timeKey a time in the hh:mm format. we also support 0:00, 0:0 and 0
   * @param addMinutes the number of minutes to add to the timeKey
   */
  static addMinuteToDateKeyAndTimeKey(dateKey: string, timeKey: string | undefined, addMinutes: number | undefined): string | undefined {
    if (!timeKey) {
      return timeKey;
    }
    const todayDate = DateAndTimeTools.getDateFromIsoDate(dateKey, timeKey);
    const addMin: number = addMinutes ? addMinutes : 0;
    const durationDate: Date = new Date(todayDate.getTime() + ( addMin * 60000));
    const x = DateAndTimeTools.getDateKeyAndTimeKeyForDate(durationDate);
    return x.timeKey;
  }

  /***
   * returns a timestamp from a date and time give by key
   *
   * @param dateKey a date in the yyyy-mm-dd format
   * @param timeKey a time in the hh:mm format. we also support 0:00, 0:0 and 0
   */
  static getTimestampForDateKeyAndTimeKey(dateKey: string, timeKey: string | undefined): number | undefined {
    if (!timeKey) {
      return undefined;
    }
    // hint: ist im Millisekunden Format!
    return +DateAndTimeTools.getDateFromIsoDate(dateKey, timeKey);
  }

  // /***
  //  * returns time in the format hh:mm
  //  *
  //  * @param time a time in the format hh:mm
  //  * @param durationInMinutes of minutes to add
  //  */
  // static addMinutesToTime(time: string, durationInMinutes: number): string | undefined {
  //   if ( !time ) {
  //     return undefined;
  //   }
  //   const hours = time.split(':')[0];
  //   let zero = '';
  //   // check if the hours have no zero in front so we can make add the zero in the date to get it correctly.
  //   if ( hours.length === 1 ) {
  //     zero = '0';
  //   }
  //   // create a new date and add a random year, the fromTimeKey and for the seconds just 00
  //   const date = new Date('1970-01-01T' + zero + time + ':00' );
  //   // add the duration in minutes (as timestamp) to the time (as timestamp)
  //   // and convert it to time string. Split it at the seconds to just return hh:mm
  //   return new Date(date.getTime() + durationInMinutes*60000).toTimeString().split(':00 GMT')[0];
  // }

  /**
   * returns a string with 'year-kw' for example '2022-15'
   *
   * @param date
   */
  static getYearKw(date: Date): string {
    return `${ date.getFullYear() }-${ DateAndTimeTools.getWeekNumber( date ) }`;
  }

  static getMonthNameForDate(date: Date): string {
    // monat ist 0 based in javascript
    switch ( date.getMonth() ) {
      case 0: return 'Januar';
      case 1: return 'Februar';
      case 2: return 'März';
      case 3: return 'April';
      case 4: return 'Mai';
      case 5: return 'Juni';
      case 6: return 'Juli';
      case 7: return 'August';
      case 8: return 'September';
      case 9: return 'Oktober';
      case 10: return 'November';
      default:
        return 'Dezember';
    }
  }
}
