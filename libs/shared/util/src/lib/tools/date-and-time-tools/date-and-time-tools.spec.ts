import { TestBed } from '@angular/core/testing';
import { DateAndTimeTools } from './date-and-time-tools';

describe('dateAndTimeTools', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('getWeekNumberForDateKey', () => {

    it( 'successfully', () => {
      const dateKey = '2022-01-21';
      const weekNumber = 3;
      const result = DateAndTimeTools.getWeekNumberForDateKey( dateKey );
      expect( result ).toEqual( weekNumber );
    } );

    // not possible as the compiler does not allow this call
    // it( 'with dateKey null', () => {
    //   const dateKey = null;
    //   const result = DateAndTimeTools.getWeekNumberForDateKey(dateKey);
    //   expect( result ).toEqual( 0 );
    // });

    // not possible as the compiler does not allow this call
    // it( 'with dateKey undefined', () => {
    //   const dateKey = undefined;
    //   const result = DateAndTimeTools.getWeekNumberForDateKey(dateKey);
    //   expect( result ).toEqual( 0 );
    // });
  });

  describe('getWeekNumber', () => {
    it( 'successfully', () => {
      const date: Date = new Date('2022-01-21');
      const kw = 3;
      const result = DateAndTimeTools.getWeekNumber( date );
      expect( result ).toEqual( kw );
    });
    it( 'not successfully other kw', () => {
      // 2022 is still kw 52
      const date: Date = new Date('2022-01-01');
      const kw = 1;
      const result = DateAndTimeTools.getWeekNumber( date );
      expect( result ).not.toEqual( kw );
    });
  });

  describe('getDateKey', () => {
    it( 'successfully', () => {
      const dateKey = '2022-01-21';
      const date: Date = new Date(dateKey);
      const result = DateAndTimeTools.getDateKey(date);
      expect(result).toEqual(dateKey);
    });
  });

  describe('getTodayDateKey', () => {
    it( 'successfully', () => {
      const result = DateAndTimeTools.getTodayDateKey();
      expect(result.length).toEqual(10);
    });
  });

  describe('getDateFromIsoDate', () => {

    it( 'successfully with dateKey and without timeKey', () => {
      const year = 2022;
      const month = 1;
      const day = 21;
      // to get the same date we need to create it that way. JS months start with 0 so just subtract 1
      const date = new Date(year, month-1, day, 0, 0);
      const result = DateAndTimeTools.getDateFromIsoDate(`${year}-${month}-0${day}`);
      expect( result ).toEqual( date );
    });

    it( 'successfully with dateKey and timeKey as hh:mm', () => {
      const dateKey = '2022-01-21';
      const timeKey = '08:20';
      const date = new Date(`${dateKey}T${timeKey}`);
      const result = DateAndTimeTools.getDateFromIsoDate(dateKey, timeKey);
      expect( result ).toEqual( date );
    });

    it( 'successfully with dateKey and timeKey as h:mm', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8:20';
      const date = new Date(`${dateKey}T0${timeKey}`);
      const result = DateAndTimeTools.getDateFromIsoDate(dateKey, timeKey);
      expect( result ).toEqual( date );
    });

    it( 'successfully with dateKey and timeKey as h (full hour)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8';
      const date = new Date(`${dateKey}T0${timeKey}:00`);
      const result = DateAndTimeTools.getDateFromIsoDate(dateKey, timeKey);
      expect( result ).toEqual( date );
    });

    it( 'unsuccessfully with timeKey hour greater than 24 (h)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '25';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toEqual( msg );
      }
    });

    it( 'unsuccessfully with timeKey hour smaller than 0 (h)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '-1';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toEqual( msg );
      }
    });

    it( 'unsuccessfully with timeKey hour greater than 24 (h:mm)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '25:25';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toContain( msg );
      }
    });

    it( 'unsuccessfully with timeKey hour smaller than 0 (h:mm)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '-1:25';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toContain( msg );
      }
    });

    it( 'unsuccessfully with timeKey minute greater than 59 (h:mm)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '08:60';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toContain( msg );
      }
    });

    it( 'unsuccessfully with timeKey minute smaller than 0 (h:mm)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '08:-1';
      const msg = `the used timeKey ${timeKey} is invalid`;
      try {
        expect( DateAndTimeTools.getDateFromIsoDate( dateKey, timeKey ) ).toThrow();
      } catch (e) {
        expect( (e as Error).message ).toContain( msg );
      }
    });

  });

  // todo: implement
  // describe('getDateKeyAndTimeKeyForDate', () => {
    // it( 'successfully', () => {
    //   const year = 2022;
    //   const month = 1;
    //   const day = 21;
    //   const hour = 23;
    //   const mins = 5;
    //   const date = new Date(`${year}-0${month}-${day}T${hour}:0${mins}`);
    //   const week = 3;
    //   const result = DateAndTimeTools.getDateKeyAndTimeKeyForDate(date);
    //   expect(result.timeKey).toEqual(`${hour}:0${mins}`);
    //   expect(result.week).toEqual(week);
    //   expect(result.dateKey).toEqual(`${year}-0${month}-${day}`);
    // });
  // });

  describe('addMinuteToDateKeyAndTimeKey', () => {

    it('successfully with hh:mm', () => {
      const dateKey = '2022-01-21';
      const timeKey = '08:30';
      const minutesToAdd = 5;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('08:35');
    });

    it('successfully with h:mm', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8:30';
      const minutesToAdd = 5;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('08:35');
    });

    it('successfully with h:m added some minutes', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8:3';
      const minutesToAdd = 60;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('09:03');
    });


    it('successfully with h:m added one hour (60 minutes)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8:3';
      const minutesToAdd = 8;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('08:11');
    });


    it('successfully with h and added some minutes', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8';
      const minutesToAdd = 8;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('08:08');
    });

    it('successfully with h and added one hour (60 minutes)', () => {
      const dateKey = '2022-01-21';
      const timeKey = '8';
      const minutesToAdd = 60;
      const result = DateAndTimeTools.addMinuteToDateKeyAndTimeKey(dateKey, timeKey, minutesToAdd);
      expect(result).toEqual('09:00');
    });

  });

  describe('getTimestampForDateKeyAndTimeKey', () => {

    it('successfully with timeKey', () => {
      const dateKey = '2021-12-19';
      const timeKey = '15:14';
      const result = DateAndTimeTools.getTimestampForDateKeyAndTimeKey( dateKey, timeKey );
      expect( result ).toEqual(1639923240000);
    });

    it('successfully without timeKey', () => {
      const dateKey = '2021-12-19';
      const timeKey = undefined;
      const result = DateAndTimeTools.getTimestampForDateKeyAndTimeKey( dateKey, timeKey );
      expect( result ).toEqual(undefined );
    });
  });

  // hint: evtl. Overkill. War aber grösstenteils nur Copy-Paste also warum nicht.
  describe('getMonthNameForDate', () => {

    it('successfully Januar', () => {
      const dateKey = '2021-01-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Januar' );
    });

    it('successfully Februar', () => {
      const dateKey = '2021-02-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Februar' );
    });

    it('successfully März', () => {
      const dateKey = '2021-03-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'März' );
    });

    it('successfully April', () => {
      const dateKey = '2021-04-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'April' );
    });

    it('successfully Mai', () => {
      const dateKey = '2021-05-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Mai' );
    });

    it('successfully Juni', () => {
      const dateKey = '2021-06-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Juni' );
    });

    it('successfully Juli', () => {
      const dateKey = '2021-07-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Juli' );
    });

    it('successfully August', () => {
      const dateKey = '2021-08-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'August' );
    });

    it('successfully September', () => {
      const dateKey = '2021-09-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'September' );
    });

    it('successfully Oktober', () => {
      const dateKey = '2021-10-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Oktober' );
    });

    it('successfully November', () => {
      const dateKey = '2021-11-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'November' );
    });

    it('successfully Dezember', () => {
      const dateKey = '2021-12-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Dezember' );
    });

    it('successfully Januar', () => {
      const dateKey = '2021-01-01';
      const date = new Date( dateKey );
      const result = DateAndTimeTools.getMonthNameForDate( date );
      expect( result ).toEqual( 'Januar' );
    });

  });

});
