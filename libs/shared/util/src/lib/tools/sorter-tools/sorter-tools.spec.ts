import {TestBed} from '@angular/core/testing';
import {SorterTools} from './sorter-tools';

describe('SorterTools', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('dynamicSort', () => {

    it('successfully number DESC', () => {
      const objArr = [
        {
        id: 5,
        name: 'aaa'
        },
        {
          id: 3,
          name: 'aba'
        },
        {
          id: 1,
          name: 'aab'
        },
        {
          id: 2,
          name: 'baa'
        },
        {
          id: 4,
          name: 'bba',
        }
      ];
      const sortedArrById = [objArr[2], objArr[3], objArr[1], objArr[4], objArr[0]];
      const result = objArr.sort(SorterTools.dynamicSort('id'));
      expect( result[0] ).toEqual( sortedArrById[0] );
      expect( result[1] ).toEqual( sortedArrById[1] );
      expect( result[2] ).toEqual( sortedArrById[2] );
      expect( result[3] ).toEqual( sortedArrById[3] );
      expect( result[4] ).toEqual( sortedArrById[4] );
    });

    it('successfully number ASC', () => {
      const objArr = [
        {
        id: 5,
        name: 'aaa'
        },
        {
          id: 3,
          name: 'aba'
        },
        {
          id: 1,
          name: 'aab'
        },
        {
          id: 2,
          name: 'baa'
        },
        {
          id: 4,
          name: 'bba',
        }
      ];
      const sortedArrById = [objArr[0], objArr[4], objArr[1], objArr[3], objArr[2]];
      const result = objArr.sort(SorterTools.dynamicSort('-id'));
      expect( result[0] ).toEqual( sortedArrById[0] );
      expect( result[1] ).toEqual( sortedArrById[1] );
      expect( result[2] ).toEqual( sortedArrById[2] );
      expect( result[3] ).toEqual( sortedArrById[3] );
      expect( result[4] ).toEqual( sortedArrById[4] );
    });

    it('successfully string DESC', () => {
      const objArr = [
        {
        id: 5,
        name: 'aaa'
        },
        {
          id: 3,
          name: 'aba'
        },
        {
          id: 1,
          name: 'aab'
        },
        {
          id: 2,
          name: 'bba'
        },
        {
          id: 4,
          name: 'baa',
        }
      ];
      const sortedArrById = [objArr[0], objArr[2], objArr[1], objArr[4], objArr[3]];
      const result = objArr.sort(SorterTools.dynamicSort('name'));
      expect( result[0] ).toEqual( sortedArrById[0] );
      expect( result[1] ).toEqual( sortedArrById[1] );
      expect( result[2] ).toEqual( sortedArrById[2] );
      expect( result[3] ).toEqual( sortedArrById[3] );
      expect( result[4] ).toEqual( sortedArrById[4] );
    });

    it('successfully string ASC', () => {
      const objArr = [
        {
        id: 5,
        name: 'aaa'
        },
        {
          id: 3,
          name: 'aba'
        },
        {
          id: 1,
          name: 'aab'
        },
        {
          id: 2,
          name: 'bba'
        },
        {
          id: 4,
          name: 'baa',
        }
      ];
      const sortedArrById = [objArr[3], objArr[4], objArr[1], objArr[2], objArr[0]];
      const result = objArr.sort(SorterTools.dynamicSort('-name'));
      expect( result[0] ).toEqual( sortedArrById[0] );
      expect( result[1] ).toEqual( sortedArrById[1] );
      expect( result[2] ).toEqual( sortedArrById[2] );
      expect( result[3] ).toEqual( sortedArrById[3] );
      expect( result[4] ).toEqual( sortedArrById[4] );
    });

  });

});
