export class SorterTools {
  /***
   * return a function to use in an arr sort call
   * @param propertyName of the property to sort inside the object of the array, if you use a - sign in front we will sort reverse
   */
  static dynamicSort = (propertyName: string) => {
    let sortOrder = 1;
    if (propertyName[0] === '-') {
      sortOrder = -1;
      propertyName = propertyName.substring(1);
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return (a: any, b: any) => {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      const result = (a[propertyName] < b[propertyName]) ? -1 : (a[propertyName] > b[propertyName]) ? 1 : 0;
      return result * sortOrder;
    };
  };
}
