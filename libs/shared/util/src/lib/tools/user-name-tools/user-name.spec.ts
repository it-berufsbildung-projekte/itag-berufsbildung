import { TestBed } from '@angular/core/testing';
import { UserNameTool } from './user-name-tool';

const regularEMail = 'test.muster@test.org';
const regularFullName = 'Test Muster';
const regularFirstName = 'Test';

const eMailWithoutTopLevelDomain = 'test.muster@test';

const eMailWithoutDotInName = 'test-muster@test.org';
const retWithoutDotInName = 'Test-muster';


describe('userNameTools', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('getFullName', () => {

    it( 'regular e-mail', () => {
      const result = UserNameTool.getFullName( regularEMail );
      expect( result ).toEqual(regularFullName);
    } );

    it( 'without top level domain e-mail', () => {
      const result = UserNameTool.getFullName( eMailWithoutTopLevelDomain );
      expect( result ).toEqual(regularFullName);
    } );

    it( 'without dot in name e-mail', () => {
      const result = UserNameTool.getFullName( eMailWithoutDotInName );
      expect( result ).toEqual(retWithoutDotInName);
    } );

  });

  describe('getFirstName', () => {

    it( 'regular e-mail', () => {
      const result = UserNameTool.getFirstName( regularEMail );
      expect( result ).toEqual(regularFirstName);
    } );

    it( 'without top level domain e-mail', () => {
      const result = UserNameTool.getFirstName( eMailWithoutTopLevelDomain );
      expect( result ).toEqual(regularFirstName);
    } );

    it( 'without dot in name e-mail', () => {
      const result = UserNameTool.getFirstName( eMailWithoutDotInName );
      expect( result ).toEqual(retWithoutDotInName);
    } );


  });


});
