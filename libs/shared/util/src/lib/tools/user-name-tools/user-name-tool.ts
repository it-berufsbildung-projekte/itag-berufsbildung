
export class UserNameTool {
  /***
   * return the name from a given e-mail. It's only working for specific case
   *
   * @param eMail, an e-mail that has a dot as delimiter between firstname and lastname.
   * ex: first.last@domain --> First Last
   * In case we could not find a last we do remove the empty space on the end. the result is then the same as with getFirstName method
   *
   * @returns a string with First Last
   */
  static getFullName( eMail: string ): string {
    const eMailSplit = eMail.split('@');
    const nameSplit = eMailSplit[0].split('.');
    const firstName = nameSplit[0] ? ( nameSplit[0].charAt(0).toUpperCase() + nameSplit[0].slice(1) ) : '';
    const lastName = nameSplit[1] ? ( nameSplit[1].charAt(0).toUpperCase() + nameSplit[1].slice(1) ) : '';
    return `${firstName}${lastName ? ' ' + lastName : ''}`;
  }
  /***
   * return the firstname from a given e-mail. It's only working for specific case
   *
   * @param eMail, an e-mail that has a dot as delimiter between firstname and lastname.
   * ex: first.last@domain --> First
   *
   * @returns a string with First
   */
  static getFirstName( eMail: string ): string {
    const eMailSplit = eMail.split('@');
    const nameSplit = eMailSplit[0].split('.');
    return nameSplit[0] ? (nameSplit[0].charAt( 0 ).toUpperCase() + nameSplit[0].slice( 1 )):'';
  }

}
