export class ObjectTools {
  static cloneDeep<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj)) as T;
  }
}
