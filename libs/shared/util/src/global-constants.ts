import { ContactInfo } from './lib/models';

export class GlobalConstants {
  static readonly contactInfo: ContactInfo = {
    name: 'IT AG Berufsbildung',
    url: 'https://it-berufsbildung.ch',
    eMail: 'itag.berufsbildung@ag.ch?subject=Frage%20zum%20Software%20Portal%20der%20IT%20AG%20Berufsbildung',
  };

  static readonly api = {
    generic: {
      base: 'generic',
      sub: {
        appInfo: 'app-info',
        healthState: 'health-state',
        userInfo: 'user-info',
        menuInfo: 'menu-info',
        users: 'users',
        userRoles: 'user-roles',
      },
    },
    modules: {
      base: 'modules',
      sub: {},
    },
  };

  static readonly globalEnvironmentKeys = {
    authIssuer: 'AUTH_ISSUER',
    baseApiUrl: 'BASE_API_URL',
  };

  static readonly mandatoryRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readonly: 'read-only',
  };

  static readonly emailEnvironmentKeys = {
    emailInitActive: 'EMAIL_INIT_ACTIVE',
    emailHost: 'EMAIL_HOST',
    emailPort: 'EMAIL_PORT',
    emailUser: 'EMAIL_USER',
    emailPassword: 'EMAIL_PASSWORD',
    emailSecure: 'EMAIL_SECURE',
    emailTlsRejectUnauthorized: 'EMAIL_TLS_REJECT_UNAUTHORIZED',
  };
}
