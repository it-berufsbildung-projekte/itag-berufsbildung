import { Component } from '@angular/core';
import { ContactInfo, GlobalConstants } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-error',
  templateUrl: 'error.component.html',
})
export class ErrorComponent {
  contactInfo: ContactInfo = GlobalConstants.contactInfo;
}
