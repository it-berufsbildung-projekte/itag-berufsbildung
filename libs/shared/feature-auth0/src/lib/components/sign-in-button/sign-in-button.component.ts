import { Component } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'itag-berufsbildung-login-button',
  templateUrl: 'sign-in-button.component.html',
})
export class SignInButtonComponent {
  // Inject the authentication service into your component through the constructor
  constructor(public auth: AuthService) {}
}
