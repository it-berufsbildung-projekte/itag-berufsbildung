import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'itag-berufsbildung-logout',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.scss'],
})
export class SignOutComponent implements OnInit {

  private redirectUrl: string = window.location.origin;

  constructor(
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.authService.logout({
      returnTo: this.redirectUrl
    });
  }
}
