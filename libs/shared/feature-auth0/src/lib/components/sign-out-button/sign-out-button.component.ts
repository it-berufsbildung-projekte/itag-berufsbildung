import { Component } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'itag-berufsbildung-logout-button',
  templateUrl: 'sign-out-button.component.html',
})
export class SignOutButtonComponent {
  // Inject the authentication service into your component through the constructor
  private redirectUrl: string = window.location.origin;

  constructor(
    private readonly authService: AuthService,
  ) {}

  doSignOut() {
    this.authService.logout({
      returnTo: this.redirectUrl
    });
  }
}
