import { Component, Inject } from '@angular/core';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '../../services/user-service/user.service';

@Component({
  selector: 'itag-berufsbildung-profile-button',
  templateUrl: 'profile-button.component.html',
})
export class ProfileButtonComponent {
  picture = '/assets/dummy-user.png';

  constructor(@Inject(UiConstants.instance.services.user) readonly userService: UserService) {
    userService.userDto$.subscribe((userDto) => {
      this.picture = userDto?.picture ? userDto.picture : '/assets/dummy-user.png';
    });
  }
}
