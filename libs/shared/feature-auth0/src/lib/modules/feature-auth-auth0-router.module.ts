import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';

import { ErrorComponent } from '../components/error/error.component';
import { SignOutComponent } from '../components/sign-out/sign-out.component';

const routes: Routes = [
  {
    path: UiConstants.instance.routing.base,
    children: [
      { path: UiConstants.instance.routing.sub.error, component: ErrorComponent },
      { path: UiConstants.instance.routing.sub.signOut, component: SignOutComponent },
    ],
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class FeatureAuthAuth0RouterModule {}
