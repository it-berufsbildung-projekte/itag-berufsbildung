import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthModule } from '@auth0/auth0-angular';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { IconModule } from '@progress/kendo-angular-icons';
import { AvatarModule, CardModule } from '@progress/kendo-angular-layout';
import { environment } from '../../../../../environments/environment';

import { ErrorComponent } from './components/error/error.component';
import { ProfileButtonComponent } from './components/profile-button/profile-button.component';
import { SignInButtonComponent } from './components/sign-in-button/sign-in-button.component';
import { SignOutComponent } from './components/sign-out/sign-out.component';
import { SignOutButtonComponent } from './components/sign-out-button/sign-out-button.component';
import { FeatureAuthAuth0RouterModule } from './modules/feature-auth-auth0-router.module';
import { AuthenticationInterceptorService } from './services/authentication-interceptor/authentication-interceptor.service';
import { UserService } from './services/user-service/user.service';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    RouterModule,
    FeatureAuthAuth0RouterModule,
    StoreDevtoolsModule.instrument({ maxAge: 50, logOnly: true }),
    AuthModule.forRoot({
      ...environment.auth,
      httpInterceptor: {
        ...environment.httpInterceptor,
      },
    }),
    AvatarModule,
    CardModule,
  ],
  declarations: [ErrorComponent, SignInButtonComponent, SignOutButtonComponent, SignOutComponent, ProfileButtonComponent],
  providers: [UserService, AuthenticationInterceptorService],
  exports: [ErrorComponent, SignInButtonComponent, SignOutButtonComponent, SignOutComponent, ProfileButtonComponent],
})
export class FeatureAuthAuth0Module {}
