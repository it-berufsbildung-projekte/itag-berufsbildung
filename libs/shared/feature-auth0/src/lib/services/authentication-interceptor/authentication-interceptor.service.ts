import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from '@auth0/auth0-angular';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { catchError, Observable, of, tap } from 'rxjs';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';

@Injectable()
export class AuthenticationInterceptorService implements HttpInterceptor {
  // Variable to hold local userToken
  private readonly userToken = this.localStoreService.get('userToken');

  constructor(
    private readonly auth: AuthService,
    private readonly localStoreService: LocalStoreService,
    private readonly notification: NotificationService
  ) {
    this.userToken = this.getToken();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  intercept(req: HttpRequest<undefined>, next: HttpHandler): Observable<HttpEvent<undefined>> {
    const wsId = this.getWsId();

    let wsIdRequest: HttpRequest<undefined> = req.clone();
    // add the websocket id
    if (wsId) {
      wsIdRequest = wsIdRequest.clone({
        headers: wsIdRequest.headers.set('ws-id', wsId),
      });
    }

    // Get Auth0 scaffolded
    // If userToken exists, clone the http request and add the userToken as the Bearer token
    // Otherwise grab the token from Auth0
    if (this.userToken) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const tokenReq: HttpRequest<any> = wsIdRequest.clone({
        setHeaders: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          Authorization: `Bearer ${this.userToken}`,
        },
      });
      return next.handle(tokenReq);
    }
    // catch any errors. Ensure user is logged out if necessary and have them log in
    return next.handle(req).pipe(
      tap((val) => {
        if (val.type === 0) {
          // request
          // this.spinnerService.addRunner(request.url);
        } else {
          // response
          // this.spinnerService.finishRunner(request.url);
        }

      }),
      catchError(this.outerErrorHandlerClosure())
    );
  }

  /**
   * pulls local storage token and sets it in userToken
   */
  getToken(): string | null {
    return this.localStoreService.get(UiConstants.instance.localStore.userToken);
  }

  getWsId(): string | null {
    return this.localStoreService.get(UiConstants.instance.localStore.wsId);
  }

  private outerErrorHandlerClosure() {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return (err: any): Observable<any> => {
      let msg = err.statusText || err.message || JSON.stringify(err) || 'unknown error in the unauthorizedInterceptor';
      if (err.status) {
        msg += `<br>status: ${err.status}`;
      }
      if (err.url) {
        msg += `<br>url: ${err.url}`;
      }
      if (err.error) {
        msg += `<br>message: ${err.error.message}`;
      }
      this.notification.showDebug(msg);

      if (err instanceof HttpErrorResponse && err.status === 401) {
        this.auth.logout();
        this.auth.loginWithPopup();
        return of(null);
      }

      if ((err instanceof HttpErrorResponse && err.status === 401) || (err instanceof HttpErrorResponse && err.status === 440)) {
        this.auth.logout();
        this.auth.loginWithPopup();
        return of(null);
      } else if (err instanceof HttpErrorResponse && err.status === 403) {
        // this.messageService.addError( msg);
      } else {
        // this.messageService.addError( msg);
        throw err;
      }
      return of(null);
    };
  }
}
