import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { BehaviorSubject } from 'rxjs';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { ApiUserDto, GlobalConstants, Menu, MessageDto, UiUserDto, UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { environment } from '../../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  userDto$: BehaviorSubject<UiUserDto | undefined> = new BehaviorSubject<UiUserDto | undefined>( undefined );

  private userDto!: UiUserDto;
  // get userDtoFilled(): UiUserDto {
  //   return this.userDto;
  // }
  private route?: string;

  constructor(
    private readonly auth: AuthService,
    private readonly localStore: LocalStoreService,
    private readonly notification: NotificationService,
    private readonly http: HttpClient,
    private readonly router: Router
  ) {
    this.auth.idTokenClaims$.subscribe( ( token ) => {
      if ( !token ) {
        this.userDto$.next( undefined );
        return;
      }
      const userDto = new UiUserDto();
      userDto.eMail = token.email ? token.email:'';
      userDto.picture = token.picture;

      // eslint-disable-next-line no-underscore-dangle
      this.storeToken( token.__raw );

      // first we get all the modules
      // get the modules
      this.readModules(environment.apiUrl).subscribe( {
        next: ( arr ) => {
          this.notification.showInfo( 'successfully read the modules from the api!' );
          userDto.moduleInfoArr = arr;
          // now read the menus
          for ( const moduleInfo of arr ) {
            // now we refresh the user
            // to ensure we load it not from the cache the first time, try to remove it from the cache first
            this.refreshUser(moduleInfo.apiUrl)
              .subscribe( {
                next: ( data: MessageDto ) => {
                  this.notification.showDebug( `Module: ${moduleInfo.moduleName}, ${data.message} ` );
                  this.readUserInfo(moduleInfo.apiUrl).subscribe( {
                    next: ( apiUserDto: ApiUserDto ) => {
                      this.notification.showDebug( `we get the apiUserDto for Module ${moduleInfo.moduleName}!` );
                      userDto.uiRights[moduleInfo.moduleName] = apiUserDto.apiRights;
                      userDto.uiRoles[moduleInfo.moduleName] = apiUserDto.apiRoles;

                      this.readMenus( moduleInfo.apiUrl ).subscribe( {
                        next: ( menu ) => {
                          this.notification.showInfo( `successfully read the menu for module ${ moduleInfo.moduleName } from the api!` );
                          // store the menu
                          userDto.menuHash[moduleInfo.moduleName] = menu;

                          this.userDto = userDto;
                          this.userDto$.next( userDto );
                          // we have a proper logged in user
                          this.notification.showSuccess( `user ${ token.email } logged in successfully!` );
                        },
                        error: ( err: Error ) => this.notification.showError( err.message ),
                      } );
                    },
                    error: ( err: Error ) => {
                      this.notification.showError( err.message, err );
                    },
                  } );
                },
                error: ( err: Error ) => {
                  this.notification.showError( err.message, err );
                },
              } );
          }
        },
        error: ( err: Error ) => this.notification.showError( err.message ),
      } );



      // if we have a userDto and a route set, we just navigate to this url
      this.userDto$.subscribe( ( userDto ) => {
        if ( userDto && this.route ) {
          this.router
            .navigateByUrl( this.route )
            .then( () =>  this.notification.showDebug(`successfully! navigate to ${ this.route }` ) )
            .catch( ( err ) => this.notification.showError(err.message ) );
        }
      } );
    } );
  }

  refreshUser(apiUrl: string) {
    return this.http.delete<MessageDto>( `${ apiUrl }/${ GlobalConstants.api.generic.base }/${ GlobalConstants.api.generic.sub.userInfo }` );
  }

  setRoute( route: string ) {
    this.route = route;
  }

  private readUserInfo(apiUrl: string) {
    return this.http.get<ApiUserDto>( `${ apiUrl }/${ GlobalConstants.api.generic.base }/${ GlobalConstants.api.generic.sub.userInfo }` );
  }

  private readModules(apiUrl: string) {
    return this.http.get<UserModuleInfoDto[]>( `${ apiUrl }/${ GlobalConstants.api.modules.base }` );
  }

  private readMenus( apiUrl: string ) {
    return this.http.get<Menu>( `${ apiUrl }/${ GlobalConstants.api.generic.base }/${ GlobalConstants.api.generic.sub.menuInfo }` );
  }

  /**
   * Sets the token in local storage
   */
  private storeToken( token: string ) {
    this.localStore.set( UiConstants.instance.localStore.userToken, token );
  }
}
