import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';

import { map, Observable } from 'rxjs';
import { UserService } from '../services/user-service/user.service';

@Injectable({
  providedIn: 'root',
})
export class ModuleRoleGuard implements CanActivate {
  constructor(@Inject(UiConstants.instance.services.user) readonly userService: UserService, readonly notification: NotificationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.userService.userDto$.pipe(
      map((userDto) => {
        if (!userDto) {
          this.userService.setRoute(state.url);
          return false;
        }
        // kein Modul, heisst nur angemeldet.
        const module = route.data['module'] as string;
        if (!module) {
          return true;
        }
        // wenn keine Rollen gesetzt sind, muss der User nur in dem Modul sein
        const roles = route.data['roles'] as Array<string>;
        if (!roles) {
          return true;
        }

        const userRoles = userDto?.uiRoles;
        const moduleRoleHash = userRoles ? userRoles[module] : undefined;
        if (!moduleRoleHash) {
          this.notification.showWarning(`user ${userDto.eMail} has not the right to access the module ${module}`);
          return false;
        }
        for (const role of roles) {
          if (!moduleRoleHash[role]) {
            continue;
          }
          return true;
        }
        this.notification.showWarning(`user ${userDto.eMail} has not the right to access the page ${state.url}`);
        return false;
      })
    );
  }
}
