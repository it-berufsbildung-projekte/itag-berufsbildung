import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';

import { map, Observable } from 'rxjs';
import { UserService } from '../services/user-service/user.service';

@Injectable({
  providedIn: 'root',
})
export class ModuleRightGuard implements CanActivate {
  constructor(@Inject(UiConstants.instance.services.user) readonly userService: UserService, readonly notification: NotificationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.userService.userDto$.pipe(
      map((userDto) => {
        if (!userDto) {
          this.userService.setRoute(state.url);
          return false;
        }
        // kein Modul, heisst nur angemeldet.
        const module = route.data['module'] as string;
        if (!module) {
          this.notification.showWarning(`Please tell your developer he must set the module. requested route was: ${route.url}`);
          return true;
        }
        // wenn keine Rollen gesetzt sind, muss der User nur in dem Modul sein
        const right = route.data['right'] as string;
        if (!right) {
          this.notification.showWarning(`Please tell your developer he must set the right. requested route was: ${route.url} with data: ${route.data}`);
          return true;
        }

        if (!userDto.hasRight(module, right)) {
          this.notification.showWarning(`user ${userDto.eMail} has not the right to access the module ${module}`);
          return false;
        }
        // all good, continue
        return true;
      })
    );
  }
}
