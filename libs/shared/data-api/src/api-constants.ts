export class ApiConstants {
  static readonly authEnvironmentKeys = {
    authAud: 'AUTH_AUD',
    authClientId: 'AUTH_CLIENT_ID',
    authClientSecret: 'AUTH_CLIENT_SECRET',
  };

  static readonly cacheConfiguration = {
    userExpiringInSeconds: 300, // 5min
    moduleIntervalInMs: 59500,
    moduleAuthExpiringInSeconds: 2591000,
    moduleExpiringInSeconds: 61,
  };

  static readonly correlationIds = {
    main: -1,
    bootstrap: 0,
    baseRedisService: 1,
    baseAppConfigService: 2,
    appGateway: 3,
    authService: 4,
  };

  static readonly decoratorMetadataNames = {
    moduleAllowedRoles: 'moduleAllowedRoles',
    allowedRight: 'allowedRight',
  };

  static readonly generalEnvironmentKeys = {
    registerApi: 'REGISTER_API',
    corsOrigin: 'CORS_ORIGIN',
    logLevels: 'LOG_LEVELS',
  };

  static readonly moduleApiEnvironmentKeys = {
    host: 'API_HOST',
    port: 'API_PORT',
    prefix: 'API_PREFIX',
    protocol: 'API_PROTOCOL',
    environment: 'ENVIRONMENT',
  };

  static readonly moduleRedisEnvironmentKeys = {
    url: 'REDIS_URL',
    username: 'REDIS_USERNAME',
    password: 'REDIS_PASSWORD',
    port: 'REDIS_PORT',
    database: 'REDIS_DATABASE',
  };

  // static readonly moduleUiEnvironmentKeys = {
  //   host: 'UI_HOST',
  //   port: 'UI_PORT',
  //   protocol: 'UI_PROTOCOL',
  // };

  static readonly redisKeys = {
    modules: 'modules',
    userRoleKey: 'user-role',
    cache: {
      usersKey: 'cache:users',
      moduleAuthToken: 'cache:auth-token',
    },
    documents: 'documents',
  };

  static readonly rights = {
    generic: {
      canReadAppInfo: 'generic-can-read-app-info',
      canReadUserInfo: 'generic-can-read-user-info',
      canDeleteOwnCache: 'generic-can-delete-own-cache',
      canDeleteOthersCache: 'generic-can-delete-others-cache',
      canReadMenuInfo: 'generic-can-read-menu-info',
      canReadUsers: 'generic-can-read-user-roles',
      canDeleteUser: 'generic-can-read-user',
      canReadUserRoles: 'generic-can-read-user-roles',
      canWriteUserRoles: 'generic-can-write-user-roles',
      canDeleteUserRoles: 'generic-can-delete-user-roles',
    },
    module: {
      canCreate: 'module-can-create',
    },
  };

  static readonly serverEnvironmentKeys = {
    host: 'SERVER_HOST',
    protocol: 'SERVER_PROTOCOL',
    port: 'SERVER_PORT',
  };

  static readonly services = {
    apiCallerService: 'API-CALLER-SERVICE',
    appConfigService: 'APP-CONFIG-SERVICE',
    dbService: 'DB-SERVICE',
    socketService: 'WEB_SOCKET',
  };

  static readonly webSocketEvents = {
    dataChanged: 'dataChanged',
  };
}
