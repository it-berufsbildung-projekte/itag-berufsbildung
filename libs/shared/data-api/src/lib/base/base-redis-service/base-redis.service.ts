import { Injectable } from '@nestjs/common';
import { RedisClientType } from '@redis/client';
import { RedisDefaultModules, RedisFunctions, RedisModules, RedisScripts } from 'redis';
import { BehaviorSubject } from 'rxjs';

import {
  ApiUserDto,
  ApiUserDtoInstanceCreator,
  Menu,
  MessageDto,
  ObjectTools,
  RedisClientSetting,
  RoleHashDto,
  RoleMembershipInfo,
  UserModuleInfoDto,
  UserRoleHashDto,
} from '@itag-berufsbildung/shared/util';

import { ApiConstants } from '../../../api-constants';
import { RedisClient } from '../../static/redis-client/redis-client';
import { BaseWithErrors } from '../base-with-errors/base-with-errors';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
export class BaseRedisService extends BaseWithErrors {
  clientReady$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private _redisClient: RedisClientType<RedisDefaultModules & RedisModules, RedisFunctions, RedisScripts> | undefined;
  // init with empty default
  private _redisSettings: RedisClientSetting = { database: 0, password: '', port: 0, url: '', username: '' };
  get redisSettings(): RedisClientSetting {
    const clone = ObjectTools.cloneDeep<RedisClientSetting>(this._redisSettings);
    // replace the password
    clone.password = '*'.repeat(10);
    return clone;
  }

   private get redisClient(): RedisClientType<RedisDefaultModules & RedisModules, RedisFunctions, RedisScripts> {
    if (!this._redisClient) {
      const msg = `You try to use the redis client but there is current no redis client ready!`;
      StaticLogger.error(msg, -99);
      throw new Error(msg);
    }
    return this._redisClient;
  }

  protected constructor(
    private readonly rights: {
      canResetCacheForOthers: string;
    },
    private readonly roleRights: { [roleName: string]: string[] }
  ) {
    super();
    const correlationId = ApiConstants.correlationIds.baseRedisService;
    StaticLogger.debug('BaseRedisService.constructor, new correlationId', correlationId);
    this.initRedis(correlationId);
  }

  checkRolesAndGetMenu(menu: Menu, userDto: ApiUserDto, correlationId: number | undefined): Menu | undefined {
    StaticLogger.startMethod('checkRolesAndGetMenu', correlationId);

    if (!menu) {
      return undefined;
    }
    // ensure we do not have any side effects
    const moduleMenu = ObjectTools.cloneDeep<Menu>(menu);

    // check if the user has the requested right
    if (!userDto.apiRights.find((r) => r.toLowerCase() === moduleMenu.right?.toLowerCase())) {
      // no right for the top menu. so ignore the rest
      return undefined;
    }

    // allowed
    const newMenu = ObjectTools.cloneDeep<Menu>(moduleMenu);
    // reset the children
    delete newMenu.children;

    // check for children
    if (moduleMenu.children) {
      for (const subMenu of moduleMenu.children) {
        // recursion
        const subMenuChecked = this.checkRolesAndGetMenu(subMenu, userDto, correlationId);
        if (!subMenuChecked) {
          //  no access take next one
          continue;
        }
        // we have to add this now to the moduleMenu.children
        if (!newMenu.children) {
          newMenu.children = [];
        }
        newMenu.children.push(subMenuChecked);
      }
    }
    // return the checked menu
    return StaticLogger.endMethod<Menu | undefined>('checkRolesAndGetMenu', newMenu, correlationId);
  }

  async del<T>(key: string, correlationId: number | undefined, isHash?: boolean ): Promise<T> {
    StaticLogger.startMethod('del', correlationId);

    let oldValue: T | string | undefined | null;
    if ( !isHash ) {
      oldValue = await this.get<T>(key, correlationId);

      if (!oldValue) {
        // uuid does not exist, we just inform the caller
        throw this.createNotFoundException(`You try to delete a non existing document!`, correlationId);
      }
    }

    // try to delete
    await this.redisClient.del(key);
    // delete from database
    return StaticLogger.endMethod<T>('del', oldValue as T, correlationId);
  }

  /**
   * @param key
   * @param correlationId
   * @param expiring -1 no expiring
   */
  async get<V>(key: string, correlationId: number | undefined, expiring = -1): Promise<V> {
    StaticLogger.startMethod('get', correlationId);
    const valueText: string | null = await this.redisClient.get(key);
    let value: V | undefined;
    if (valueText) {
      value = JSON.parse(valueText) as V;
      if (expiring > 0) {
        await this.upSetExpiring(key, correlationId, expiring);
      }
      return StaticLogger.endMethod<V>('get', value, correlationId);
    }
    // todo: besprechen.
    // Problem hier ist der User Cache: da noch nichts im Cache ist, wird der Fehler geschmissen.
    // Lösungsvorschläge: in den Controllern/Services checken ob return null ist und dann Fehler. Ja gibt uns mehr Code aber so sind wir freier und wenn wir null
    // zurückgeben wollen, können wir das einfach machen. (wie beim User)
    return null as unknown as V;
    // throw this.createNotFoundException( `no document for key ${ key } was found!`, correlationId );
  }

  /**
   * @param filter is to where the objects are stored.
   * @param correlationId
   */
  async getAll<V>(filter: string, correlationId: number | undefined): Promise<V[]> {
    StaticLogger.startMethod('getAll', correlationId);
    const keys = await this.redisClient.keys(`${filter}`);
    if (keys.length === 0) {
      return [];
    }
    // get the objects and parse every to an object.
    const value: V[] = ((await this.redisClient.mGet(keys)) as string[]).map((r) => JSON.parse(r));
    return StaticLogger.endMethod<V[]>('getAll', value, correlationId);
  }

  async getModules(correlationId: number | undefined): Promise<UserModuleInfoDto[]> {
    StaticLogger.startMethod('getModules', correlationId);
    // get the modules by the getAll
    const filter = `${ApiConstants.redisKeys.modules}*`;
    const modules: UserModuleInfoDto[] = await this.getAll<UserModuleInfoDto>(filter, correlationId);
    return StaticLogger.endMethod<UserModuleInfoDto[]>('getModules', modules, correlationId);
  }

  async getUser(eMail: string, correlationId: number | undefined): Promise<ApiUserDto | undefined> {
    StaticLogger.startMethod('getUser', correlationId);
    // first we try to get the user from the cache users:[e-Mail], the key will be automatic deleted after n minutes (GlobalConstants.cacheConfiguration.userExpiring)
    const key = `${ApiConstants.redisKeys.cache.usersKey}:${eMail}`;
    StaticLogger.verbose(`we try to get the userDto from the cache for user ${eMail}`, correlationId);
    const userDtoCache = await this.get<ApiUserDto>(key, correlationId, ApiConstants.cacheConfiguration.userExpiringInSeconds);
    // create a new instance if possible
    let userDto = ApiUserDtoInstanceCreator.createInstance(userDtoCache);
    if (userDtoCache) {
      // we found something in the cache, update ttl
      await this.upSetExpiring(key, correlationId, ApiConstants.cacheConfiguration.userExpiringInSeconds);
      StaticLogger.verbose(`We found the user ${eMail} in the cache and reset the ttl!`, correlationId);
    } else {
      // nothing in the cache, try to read all....
      StaticLogger.verbose(`The user ${eMail} was not in the cache, read it from the database`, correlationId);
      userDto = new ApiUserDto();
      userDto.eMail = eMail;
      // // get the modules for this user
      // userDto.moduleInfoArr = await this.getModules(correlationId);
      // fill the roles for this module
      userDto.apiRoles = await this.getUserRolesInfo(eMail, correlationId);
      // fill the rights
      userDto.apiRights = this.getUserRights(userDto.apiRoles, correlationId);

      // save it as cache
      await this.setUser(userDto, correlationId);
    }
    return StaticLogger.endMethod<ApiUserDto | undefined>('getUser', userDto, correlationId);
  }

  async hDel<V>(key: string, field: string, correlationId: number | undefined): Promise<V | undefined> {
    StaticLogger.startMethod('hDel', correlationId);
    const valueText: string | undefined = await this.redisClient.hGet(key, field);
    let oldObj: V | undefined;
    if (valueText) {
      oldObj = JSON.parse(valueText) as V;
    }
    // delete
    await this.redisClient.hDel(key, field);
    return StaticLogger.endMethod<V | undefined>('hDel', oldObj, correlationId);
  }

  async hGet<V>(key: string, field: string, correlationId: number | undefined): Promise<V | undefined> {
    StaticLogger.startMethod('hGet', correlationId);
    const valueText: string | undefined = await this.redisClient.hGet(key, field);
    let value: V | undefined;
    if (valueText) {
      value = JSON.parse(valueText) as V;
    }
    return StaticLogger.endMethod<V | undefined>('hGet', value, correlationId);
  }

  async hGetAll<T, V>(key: string, correlationId: number | undefined): Promise<T> {
    StaticLogger.startMethod('hGetAll', correlationId);
    StaticLogger.verbose(key, correlationId);
    const hash: { [key: string]: string } = await this.redisClient.hGetAll(key);
    const valueHash: { [key: string]: V } = {};
    StaticLogger.verbose(JSON.stringify(hash), correlationId);
    if (hash) {
      for (const hashKey of Object.keys(hash)) {
        try {
          valueHash[hashKey] = JSON.parse(hash[hashKey]) as V;
        } catch (err) {
          valueHash[hashKey] = hash[hashKey] as unknown as V;
        }
      }
    }
    return StaticLogger.endMethod<T>('hGetAll', valueHash as unknown as T, correlationId);
  }

  /**
   * @param key
   * @param field
   * @param obj
   * @param correlationId
   * @param onlyIfNotExisting
   */
  // todo: besprechen ob T oder string zurückgeben und ob obj T und/oder string sein kann
  async hSet<T>(key: string, field: string, obj: T, correlationId: number | undefined, onlyIfNotExisting = false): Promise<T | undefined> {
    StaticLogger.startMethod('hSet', correlationId);
    const objText = JSON.stringify(obj, null, 2);
    if (onlyIfNotExisting) {
      await this.redisClient.hSetNX(key, field, objText);
    } else {
      await this.redisClient.hSet(key, field, objText);
    }
    return StaticLogger.endMethod<T>('hSet', obj, correlationId);
  }

  async keys(key: string, correlationId: number | undefined): Promise<string[]> {
    StaticLogger.startMethod('keys', correlationId);
    StaticLogger.verbose(key, correlationId);
    const keys: string[] = await this.redisClient.keys(key);
    StaticLogger.verbose(JSON.stringify(keys), correlationId);
    return StaticLogger.endMethod<string[]>('keys', keys, correlationId);
  }

  async registerUserRoleForModule(moduleName: string, userRoleHashDto: UserRoleHashDto, correlationId: number | undefined): Promise<string> {
    StaticLogger.startMethod('registerUserRoleForModule', correlationId);
    StaticLogger.verbose(`register UserRoles for moduleName ${moduleName}!`, correlationId);
    for (const eMail in userRoleHashDto) {
      const perUserRoleHash: RoleHashDto = userRoleHashDto[eMail];
      const dbKey = `${ApiConstants.redisKeys.userRoleKey}:${eMail}`;
      // get the roles for the eMail
      const allValues = await this.hGetAll<RoleMembershipInfo, RoleMembershipInfo>(dbKey, correlationId);
      if (allValues && Object.keys(allValues).length > 0) {
        // this eMail is already registered. We just ignore it
        continue;
      }
      // set the hash for this user
      // for each e-mail for this module
      for (const role of Object.keys(perUserRoleHash)) {
        const roleMembershipInfo: RoleMembershipInfo = perUserRoleHash[role];
        if (!roleMembershipInfo.from) {
          roleMembershipInfo.from = 0;
        }
        // save it if not exist, otherwise ignore
        const created = await this.hSet<RoleMembershipInfo>(dbKey, role, roleMembershipInfo, correlationId, true);
        if (created) {
          StaticLogger.verbose(`the role ${role} for user ${eMail} added!`, correlationId);
        } else {
          StaticLogger.verbose(`the role ${role} for user ${eMail} already exist, skip import`, correlationId);
        }
      }
    }
    return StaticLogger.endMethod('registerUserRoleForModule', `UserRole for module ${moduleName} registered successfully`, correlationId);
  }

  async resetUserCache(userDto: ApiUserDto, correlationId: number | undefined): Promise<MessageDto> {
    StaticLogger.startMethod('resetUserCache', correlationId);
    const key = `${ApiConstants.redisKeys.cache.usersKey}:${userDto.eMail}`;
    // delete from database
    const data: MessageDto = { message: `user ${userDto.eMail} successfully removed from cache!` };
    if (!(await this.del(key, correlationId))) {
      data.message = `user ${userDto.eMail} was not found in cache!`;
    }
    return StaticLogger.endMethod<MessageDto>('resetUserCache', data, correlationId);
  }

  async resetUserCacheForOthers(userDto: ApiUserDto, eMailToRemoveFromCache: string, correlationId: number | undefined): Promise<MessageDto> {
    StaticLogger.startMethod('resetUserCacheForOthers', correlationId);
    // check the role of the user
    // todo: where we can find this without hard coding
    // we have to switch that back to the right and then it's a given right that a user has or not. No roles coming from somewhere

    if (!userDto.hasRight(this.rights.canResetCacheForOthers)) {
      throw this.createUnauthorizedException('You try to reset the cache for a different user but you are not have the right for this action!', correlationId);
    }

    const key = `${ApiConstants.redisKeys.cache.usersKey}:${eMailToRemoveFromCache}`;
    // delete from database
    const data: MessageDto = { message: `user ${eMailToRemoveFromCache} successfully removed from cache!` };
    if (!(await this.del(key, correlationId))) {
      data.message = `user ${eMailToRemoveFromCache} was not found in cache!`;
    }
    return StaticLogger.endMethod<MessageDto>('resetUserCacheForOthers', data, correlationId);
  }

  /**
   * @param key
   * @param obj
   * @param correlationId
   * @param expiring -1 no expiring
   */
  async set<T>(key: string, obj: T, correlationId: number | undefined, expiring = -1): Promise<T> {
    StaticLogger.startMethod('set', correlationId);
    const objText = JSON.stringify(obj, null, 2);
    let ret: string | null;
    if (expiring > 0) {
      ret = await this.redisClient.set(key, objText, { EX: expiring });
    } else {
      ret = await this.redisClient.set(key, objText);
    }
    if (ret === 'OK') {
      StaticLogger.verbose(`key ${key} successfully saved!`, correlationId);
      return obj;
    }
    throw this.createInternalServerException(`we could not set the key ${key} to value ${objText}`, correlationId);
  }

  async ttl(key: string, correlationId: number | undefined): Promise<number> {
    StaticLogger.startMethod('ttl', correlationId);
    const ttl = await this.redisClient.ttl(key);
    return StaticLogger.endMethod<number>('ttl', ttl, correlationId);
  }

  async upSetExpiring(key: string, correlationId: number | undefined, expiringInS = 0) {
    StaticLogger.startMethod('upSetExpiring', correlationId);
    if (!this._redisClient) {
      return undefined;
    }
    if (expiringInS > 0) {
      StaticLogger.verbose(`key ${key} will expire in ${expiringInS}s`, correlationId);
      const ret = await this._redisClient.expire(key, expiringInS);
      return StaticLogger.endMethod<boolean>('upSetExpiring', ret, correlationId);
    }
    return undefined;
  }

  protected async setUser(userDto: ApiUserDto, correlationId: number | undefined): Promise<ApiUserDto> {
    StaticLogger.startMethod('setUser', correlationId);
    const key = `${ApiConstants.redisKeys.cache.usersKey}:${userDto.eMail}`;
    return StaticLogger.endMethod<ApiUserDto>(
      'setUser',
      await this.set<ApiUserDto>(key, userDto, correlationId, ApiConstants.cacheConfiguration.userExpiringInSeconds),
      correlationId
    );
  }

  private getUserRights(roles: RoleHashDto, correlationId: number | undefined): string[] {
    StaticLogger.startMethod('getUserRights', correlationId);
    // we create a hash to avoid double entries
    const hash: { [right: string]: boolean } = {};
    for (const key of Object.keys(roles)) {
      if (this.roleRights[key]) {
        // add each role to the has
        for (const role of this.roleRights[key]) {
          hash[role] = true;
        }
      }
    }
    return StaticLogger.endMethod<string[]>('getUserRights', Object.keys(hash), correlationId);
  }

  private async getUserRolesInfo(eMail: string, correlationId: number | undefined): Promise<RoleHashDto> {
    StaticLogger.startMethod('getUserRolesInfo', correlationId);
    // get the user-roles for this module: modules:[moduleName]:user-roles:[e-mail]
    const moduleUserRoleKey = `${ApiConstants.redisKeys.userRoleKey}:${eMail}`;
    StaticLogger.verbose(`read the userRoles for the key ${moduleUserRoleKey}`, correlationId);
    const userRoleDtoHash: RoleHashDto = await this.hGetAll<RoleHashDto, RoleMembershipInfo>(moduleUserRoleKey, correlationId);

    const userRoleDtoHashChecked: RoleHashDto = {};
    if (userRoleDtoHash) {
      StaticLogger.verbose(`found the following userRoles ${JSON.stringify(userRoleDtoHash, null, 2)}`, correlationId);
      const now = +new Date();
      // check the from - until timestamp
      for (const roleName of Object.keys(userRoleDtoHash)) {
        const roleMemberShipInfo: RoleMembershipInfo = userRoleDtoHash[roleName];
        // check if there is an until timestamp
        const from = roleMemberShipInfo.from ? roleMemberShipInfo.from : 0;
        // hint: we take tha 1st january 9999, should be fare enough in the future :-)
        const until = roleMemberShipInfo.until ? roleMemberShipInfo.until : +new Date(9999, 0, 1);
        if (from > now || until < now) {
          // ignore this role
          continue;
        }
        userRoleDtoHashChecked[roleName] = roleMemberShipInfo;
      }
    }
    return StaticLogger.endMethod<RoleHashDto>('getUserRolesInfo', userRoleDtoHashChecked, correlationId);
  }

  private initRedis(correlationId: number | undefined) {
    StaticLogger.startMethod('initRedis', correlationId);
    const redisSettings: RedisClientSetting = {
      // from global env file
      url: process.env[ApiConstants.moduleRedisEnvironmentKeys.url] || '',
      username: process.env[ApiConstants.moduleRedisEnvironmentKeys.username] || '',
      password: process.env[ApiConstants.moduleRedisEnvironmentKeys.password] || '********',
      port: parseInt(process.env[ApiConstants.moduleRedisEnvironmentKeys.port] || '6379', 10),
      database: parseInt(process.env[ApiConstants.moduleRedisEnvironmentKeys.database] || '1', 10),
    };
    this._redisSettings = redisSettings;
    const options = RedisClient.redisOptionsBuilder(redisSettings);

    RedisClient.createRedisClient(options, correlationId)
      .then((client: RedisClientType<RedisDefaultModules & RedisModules, RedisFunctions, RedisScripts>) => {
        if (!client) {
          this._redisClient = undefined;
        }
        this._redisClient = client;
        this.clientReady$.next(true);
        StaticLogger.endMethod('initRedis', undefined, correlationId);
      })
      .catch((err) => {
        StaticLogger.error((err as Error).message, correlationId);
        this.clientReady$.next(false);
      });
  }
}
