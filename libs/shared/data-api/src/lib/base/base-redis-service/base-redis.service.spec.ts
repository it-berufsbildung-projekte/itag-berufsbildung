import { Test, TestingModule } from '@nestjs/testing';
import { BaseRedisService } from './base-redis.service';

describe('RedisService', () => {
  let service: BaseRedisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BaseRedisService
      ],
    }).compile();

    service = module.get<BaseRedisService>(BaseRedisService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
