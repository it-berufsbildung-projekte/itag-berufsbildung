import { ApiResponseOptions } from '@nestjs/swagger';
import { BaseWithErrors } from '../base-with-errors/base-with-errors';

export class BaseApiController extends BaseWithErrors {

  static conflictResponse: ApiResponseOptions = {
    description: 'You try to update values from obj with id not the same as on the uri!',
  };

  static createResponseOption: ApiResponseOptions = {
    description: 'Successfully created!',
  };

  static forbiddenResponseOption: ApiResponseOptions = {
    description: 'You have not the rights to access the requested resource!',
  };

  static internalServerErrorResponseObject: ApiResponseOptions = {
    description: 'Will be set if the server has an internal error. Please contact the developer with your correlationId.',
  };

  static notFoundResponseOption: ApiResponseOptions = {
    description: 'The resource was not found!',
  };

  static okResponseOption: ApiResponseOptions = {
    description: 'Successfully!',
  };

  static unauthorizedResponseOption: ApiResponseOptions = {
    description: 'You must be signed in before you can access the requested resource!',
  };

  constructor() {
    super();
  }
}
