import { Menu } from '@itag-berufsbildung/shared/util';

export abstract class BaseApiConstants {
  abstract readonly menu: Menu;
}
