import { AdminAppConfigurationDto, AdminModuleInfoDto, BaseModuleConstants, GlobalConstants, UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { Injectable } from '@nestjs/common';
import { ApiConstants } from '../../../api-constants';
import { ApiCallerService } from '../../services/api-caller/api-caller.service';

import { StaticLogger } from '../../static/static-logger/static-logger';
import { BaseApiConstants } from '../base-constants/base-api-constants';
import { BaseRedisService } from '../base-redis-service/base-redis.service';

@Injectable()
export abstract class BaseAppConfigService {
  readonly adminAppConfiguration: AdminAppConfigurationDto;

  protected constructor(
    protected readonly baseRedisService: BaseRedisService,
    protected readonly apiCaller: ApiCallerService,
    moduleConstants: BaseModuleConstants,
    constants: BaseApiConstants,
    isProduction: boolean
  ) {
    const correlationId = ApiConstants.correlationIds.baseAppConfigService;
    StaticLogger.startMethod('BaseAppConfigService.constructor', correlationId);

    this.adminAppConfiguration = BaseAppConfigService.readApiEnvVariables(moduleConstants, constants, isProduction);
    StaticLogger.verbose('BaseAppConfigService:constructor, new correlationId', correlationId);

    this.baseRedisService.clientReady$.subscribe(async (isReady) => {
      if (isReady) {
        // now we initialize the database with import-data, if required
        this.initDatabase(correlationId)
          .then(() => {
            StaticLogger.debug(`init Database for module ${this.adminAppConfiguration.moduleName} successfully!`, correlationId);
            // initial register
            this.registerModule(correlationId);
            // now we start an interval
            setInterval(() => {
              StaticLogger.verbose('interval started to register module', correlationId);
              this.registerModule(correlationId);
            }, ApiConstants.cacheConfiguration.moduleIntervalInMs);
          })
          .catch((err) => StaticLogger.error(err, correlationId));
      }
    });
  }

  static readApiEnvVariables(moduleConstants: BaseModuleConstants, constants: BaseApiConstants, isProduction: boolean): AdminAppConfigurationDto {
    const registerApiText = process.env[ApiConstants.generalEnvironmentKeys.registerApi] || 'true';
    return {
      // from module env file
      registerApi: registerApiText ? JSON.parse(registerApiText) : true,
      apiHost: process.env[ApiConstants.moduleApiEnvironmentKeys.host] || 'env-api-host-not-set',
      apiPort: parseInt(process.env[ApiConstants.moduleApiEnvironmentKeys.port] || `0`, 10),
      apiPrefix: process.env[ApiConstants.moduleApiEnvironmentKeys.prefix] || '',
      apiProtocol: (process.env[ApiConstants.moduleApiEnvironmentKeys.protocol] as 'http' | 'https') || 'https',
      auth: {
        aud: process.env[ApiConstants.authEnvironmentKeys.authAud] || '',
        clientId: process.env[ApiConstants.authEnvironmentKeys.authClientId] || '',
        clientSecret: process.env[ApiConstants.authEnvironmentKeys.authClientSecret] || '',
        issuer: process.env[GlobalConstants.globalEnvironmentKeys.authIssuer] || '',
      },
      description: moduleConstants.description,
      environment: (process.env[ApiConstants.moduleApiEnvironmentKeys.environment] as 'dev' | 'test' | 'prod') || 'prod',
      // from global env file
      logLevels: JSON.parse(process.env[ApiConstants.generalEnvironmentKeys.logLevels] || '[]'),
      origin: process.env[ApiConstants.generalEnvironmentKeys.corsOrigin] || '',
      baseApiUrl: process.env[GlobalConstants.globalEnvironmentKeys.baseApiUrl] || '',
      isProduction,
      moduleName: moduleConstants.moduleName,
      menu: constants.menu,
      serverPort: process.env[ApiConstants.serverEnvironmentKeys.port] || undefined,
      serverProtocol: (process.env[ApiConstants.serverEnvironmentKeys.protocol] as 'http' | 'https') || 'http',
      serverHost: process.env[ApiConstants.serverEnvironmentKeys.host] || 'env-server-host-not-set',
      availableRights: moduleConstants.availableRights,
      availableRoles: Object.values(moduleConstants.availableRoles),
      version: moduleConstants.version,
    };
  }

  // getApiUrl(): string {
  //   if (!this.adminAppConfiguration) {
  //     return 'undefined';
  //   }
  //   let url = `${this.adminAppConfiguration.apiProtocol}://${this.adminAppConfiguration.apiHost}`;
  //   // do we have a port?
  //   if (this.adminAppConfiguration.apiPort) {
  //     url += `:${this.adminAppConfiguration.apiPort}`;
  //   }
  //   // do we use an api prefix?
  //   if (this.adminAppConfiguration.apiPrefix) {
  //     url += `/${this.adminAppConfiguration.apiPrefix}`;
  //   }
  //   return url;
  // }

  getModuleInfo(): AdminModuleInfoDto {
    const port = this.adminAppConfiguration.serverPort ? `:${this.adminAppConfiguration.serverPort}` : '';
    return {
      apiUrl: `${this.adminAppConfiguration.serverProtocol}://${this.adminAppConfiguration.serverHost}${port}`,
      availableRights: this.adminAppConfiguration.availableRights,
      availableRoles: this.adminAppConfiguration.availableRoles,
      description: this.adminAppConfiguration.description,
      menu: this.adminAppConfiguration.menu,
      moduleName: this.adminAppConfiguration.moduleName,
      version: this.adminAppConfiguration.version,
      dbSettings: this.baseRedisService.redisSettings,
      appConfig: this.adminAppConfiguration,
    };
  }

  abstract initDatabase(correlationId: number | undefined): Promise<void>;

  private registerModule(correlationId: number | undefined) {
    StaticLogger.startMethod('registerModule', correlationId);
    const port = this.adminAppConfiguration.serverPort ? `:${this.adminAppConfiguration.serverPort}` : '';
    const userModuleInfoDto: UserModuleInfoDto = {
      description: this.adminAppConfiguration.description,
      moduleName: this.adminAppConfiguration.moduleName,
      apiUrl: `${this.adminAppConfiguration.serverProtocol}://${this.adminAppConfiguration.serverHost}${port}`,
      version: this.adminAppConfiguration.version,
    };
    const registerApiUrl = `${this.adminAppConfiguration.baseApiUrl}/${GlobalConstants.api.modules.base}`;
    const authConfig = this.adminAppConfiguration.auth;
    // only register the api in the case we like to do it. This allows us to register a "similar" api without registering it, and it may run independent
    if ( this.adminAppConfiguration.registerApi) {
      this.apiCaller
        .registerModule( registerApiUrl, userModuleInfoDto, authConfig, correlationId )
        .then( () => StaticLogger.verbose( `Module ${ this.adminAppConfiguration.moduleName } registered!`, correlationId ) )
        .catch( ( err ) => StaticLogger.error( err, correlationId ) );
    } else {
      StaticLogger.debug('registerModule: this api will not registered. If this is not on purpose then you have to check the REGISTER_API environment variable!', correlationId);
    }
  }
}
