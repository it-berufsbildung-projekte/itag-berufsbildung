import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  InternalServerErrorException,
  MethodNotAllowedException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { StaticLogger } from '../../../index';

export class BaseWithErrors {

  createBadRequestException(message: string, correlationId: number | undefined): BadRequestException {
    StaticLogger.warn(message, correlationId);
    return new BadRequestException(message);
  }

  createConflictException(message: string, correlationId: number | undefined): ConflictException {
    StaticLogger.warn(message, correlationId);
    return new ConflictException(message);
  }


  createForbiddenException(message: string, correlationId: number | undefined): ForbiddenException {
    StaticLogger.warn(message, correlationId);
    return new ForbiddenException(message);
  }

  createInternalServerException(message: string, correlationId: number | undefined): InternalServerErrorException {
    StaticLogger.error(message, correlationId);
    return new InternalServerErrorException(message);
  }

  createMethodNotAllowedException(message: string, correlationId: number | undefined): MethodNotAllowedException {
    StaticLogger.error(message, correlationId);
    return new MethodNotAllowedException(message);
  }

  createNotFoundException(message: string, correlationId: number | undefined): NotFoundException {
    StaticLogger.warn(message, correlationId);
    return new NotFoundException(message);
  }

  createUnauthorizedException(message: string, correlationId: number | undefined): UnauthorizedException {
    StaticLogger.warn(message, correlationId);
    return new UnauthorizedException(message);
  }

}
