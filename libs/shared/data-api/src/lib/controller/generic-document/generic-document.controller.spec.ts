import { Test, TestingModule } from '@nestjs/testing';
import { GenericDocumentController } from './generic-document.controller';

describe('GenericDocumentController', () => {
  let controller: GenericDocumentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GenericDocumentController],
    }).compile();

    controller = module.get<GenericDocumentController>(GenericDocumentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
