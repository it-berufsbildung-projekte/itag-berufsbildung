import { Body, Delete, Get, Inject, Param, Patch, Post, Put, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Request } from 'express';

import { ApiUserDto, BaseDocumentDto, baseDocumentExample, baseDocumentExampleArr, IBaseUuidDtoHistory, IQueryParams, ObjectTools } from '@itag-berufsbildung/shared/util';

import { ApiConstants } from '../../../api-constants';
import { BaseApiController } from '../../base/base-api-controller/base-api.controller';
import { BaseRedisService } from '../../base/base-redis-service/base-redis.service';
import { GetCorrelationId, GetUser, GetWsId } from '../../decorators';
import { ApiSocketService } from '../../services/socketService/api-socket.service';
import { StaticLogger } from '../../static/static-logger/static-logger';
import { GenericAppController } from '../generic-app/generic-app.controller';

// description for swagger
const genericDataType = 'generic datatype';
const genericDataTypeDescription = 'generic datatype';
const genericDataDescription = `
 * depending on the role of the user we get only a subset back.
 * with the role admin all properties will be get back`;

export enum UpsetMethodType {
  Create,
  Update,
}

export enum CheckAccessMethodType {
  getByUuid,
  patch,
  post,
  put,
  delete,
}

// rights to have the possibility to check them in this controller
export interface IGenericDocumentControllerRights {
  canShowDeleted: string;
  canRead: string;
  canCreate: string;
  canUpdate: string;
  canModifyDelete: string;
  canRemove: string;
  canSeeHistory: string;
}

export abstract class GenericDocumentController<T extends BaseDocumentDto> extends BaseApiController {
  protected constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
    public readonly documentName: string,
    public readonly rights: IGenericDocumentControllerRights
  ) {
    super();
  }

  /**
   *
   * @param newData is in this case either the put/patch or post DTO
   * @param oldValue is the value which is in our Redis Store
   * @param newHistory
   * @param correlationId
   * @private
   */
  protected static sanitizeMethodHistoryWrite<T>(
    newData: Partial<T>,
    oldValue: Partial<T> | undefined,
    newHistory: IBaseUuidDtoHistory,
    correlationId: number | undefined
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeMethodHistoryWrite', correlationId);
    const baseDtoOfOldValue = oldValue as unknown as BaseDocumentDto;
    const baseDtoOfNewData = newData as unknown as BaseDocumentDto;
    // add the history of the oldValue or add an empty array (is needed when creating)
    baseDtoOfNewData.history = baseDtoOfOldValue?.history ? baseDtoOfOldValue.history : [];
    // hack: we want the last change in front, so we use unshift, and we don't have to sort :)
    baseDtoOfNewData?.history?.unshift(newHistory);
    return StaticLogger.endMethod<Partial<T>>('sanitizeMethodHistoryWrite', baseDtoOfNewData as unknown as T, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiOperation({
    description: '',
    summary: 'fully delete a field of the document by its uuid',
  })
  @Delete('/:uuid')
  async delete(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number | undefined,
    @Param('uuid') uuid: string,
    @Req() request: Request,
    @GetWsId() wsId: string,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('delete', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canRemove)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    const oldObj = await this.getOneMethod(userDto, uuid, correlationId);
    // checkAccess
    this.checkAccess(oldObj, userDto, CheckAccessMethodType.delete, queryParams, correlationId);

    // delete
    const obj = await this.deleteMethod<T>(userDto, uuid, queryParams, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    // emit a websocket event
    this.socketService.socket?.emit(ApiConstants.webSocketEvents.dataChanged,{wsId: wsId, payload: {documentName: this.documentName, data: {key: uuid}}});

    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<T>>('delete', finalObj, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      items: {
        type: genericDataType,
      },
      example: baseDocumentExampleArr,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    description: '',
    summary: 'Get all fields of the document',
  })
  @Get()
  async getAll(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    ): Promise<Partial<T>[]> {
    const filter = request.query as IQueryParams;
    StaticLogger.startMethod('getAll', correlationId);
    // check rights
    if (!userDto.hasRight(this.rights.canRead)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    const objArr: Partial<T>[] = await this.getAllMethod<T>(userDto, filter, correlationId);
    // hint: if there is no result we send an empty array.
    if (objArr?.length < 1) {
      return StaticLogger.endMethod<Partial<T>[]>('getAll', [], correlationId);
    }
    // sanitize the array. Might be overridden by the child class
    const sanitizedObjArr = objArr.map((o) => this.sanitizeReadMethod(userDto, o, correlationId));
    // remove the entries if they are deleted and the user can't see the deleted entries or showDeleted is false
    let removedIsDeletedArr = sanitizedObjArr;
    const showDeleted = request?.query.isDeleted ? request.query.isDeleted : false;
    // hint: we filter them first before removing the property since it would return every document.
    if (!userDto.hasRight(this.rights.canShowDeleted) || !showDeleted) {
      // hint: we can't use o.hasOwnProperty() or hasOwn() so the workaround is to convert it to a string and look if the string doesn't contain 'isDeleted' (happens when someone who can't modify the property posts a document)
      removedIsDeletedArr = sanitizedObjArr.filter((o) => !(o as unknown as BaseDocumentDto).isDeleted || !JSON.stringify(o as unknown as BaseDocumentDto).includes('isDeleted'));
    }
    // we remove the isDeleted property if the user doesn't have the right to see it
    const sanitizedShowDeletedObjArr = removedIsDeletedArr.map((s) => this.sanitizeMethodIsDeletedRead(userDto, s, correlationId));
    // we remove the history if the user doesn't habe the right to see it
    const sanitizedShowHistoryObjArr = sanitizedShowDeletedObjArr.map((s) => this.sanitizeMethodHistoryRead(userDto, s, correlationId));

    return StaticLogger.endMethod<Partial<T>[]>('getAll', sanitizedShowHistoryObjArr, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiOperation({
    description: '',
    summary: 'Get a single field of the document by his uuid',
  })
  @Get('/:uuid')
  async getByUuid(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Req() request: Request,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('get', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canRead)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    const obj = await this.getOneMethod(userDto, uuid, correlationId);
    // throw error if the user can't see deleted entries and the entry is deleted
    if (!userDto.hasRight(this.rights.canShowDeleted) && (obj as unknown as BaseDocumentDto).isDeleted) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} doesn't have the right to see a deleted document!`, correlationId);
    }
    // checkAccess
    this.checkAccess(obj, userDto, CheckAccessMethodType.getByUuid, queryParams, correlationId);

    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod(userDto, obj, correlationId);
    // we always remove the property isDeleted if the user doesn't have the right to see it
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // we always remove the property history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<T>>('get', finalObj, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiConflictResponse(GenericAppController.conflictResponse)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiBody({
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Replace on an existing field some values',
  })
  @Patch('/:uuid')
  async patch(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() patchDto: T,
    @Req() request: Request,
    @GetWsId() wsId: string,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('Patch', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canUpdate) && !userDto.hasRight(this.rights.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    // we try to get first the old value
    const oldValue: Partial<T> = await this.getOneMethod<T>(userDto, uuid, correlationId);
    // check if uuid is equal,
    if (patchDto?.uuid && patchDto.uuid !== uuid) {
      // different uuid in the body and in the uri. we do not allow this. the user has to remove the uuid or set it to equal
      throw this.createConflictException(`You try to update a document with a different uuid in the body!`, correlationId);
    }
    // check if isDeleted is already true and the user doesn't have the right to modify a deleted field.
    if (oldValue.isDeleted && !userDto.hasRight(this.rights.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify a deleted field!`, correlationId);
    }
    // checkAccess. Our check will always lose.
    this.checkAccess(oldValue, userDto, CheckAccessMethodType.patch, queryParams, correlationId, patchDto);
    // check if the oldValue isDeleted and the user doesn't have the right to modifyDelete
    const sanitizedIsDeletedPatchDto = this.sanitizeMethodIsDeletedWrite(userDto, patchDto, correlationId);
    const sanitizedWritePatchDto = this.sanitizeUpsetMethod(userDto, sanitizedIsDeletedPatchDto, oldValue, queryParams, UpsetMethodType.Update, correlationId);

    // clone the old value and remove history to prevent circular structure in the newHistory
    const oldValueWithoutHistory = ObjectTools.cloneDeep<Partial<T>>(oldValue);
    delete oldValueWithoutHistory.history;
    const newHistory: IBaseUuidDtoHistory = {
      oldValue: oldValueWithoutHistory,
      method: 'UPDATE',
      email: userDto.eMail,
      timestamp: Date.now(),
    };
    const sanitizedHistoryPatchDto = GenericDocumentController.sanitizeMethodHistoryWrite(sanitizedWritePatchDto, oldValue, newHistory, correlationId);

    // now we update the oldValues with the new values
    for (const prop of Object.keys(sanitizedHistoryPatchDto)) {
      // todo: wir haben gewaltig was falsch mit den Datentypen.. wird auch nur auf diesem File so angezeigt
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      oldValue[prop] = sanitizedWritePatchDto[prop];
    }
    // remove field uuid since it's no longer needed.
    delete oldValue.uuid;
    // save the patched entry
    const obj = await this.upsetMethod<T>(uuid, oldValue, userDto, queryParams, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod<T>(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // we remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    // emit a websocket event
    this.socketService.socket?.emit(ApiConstants.webSocketEvents.dataChanged,{wsId: wsId, payload: {documentName: this.documentName, data: {key: uuid}}});
    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<T>>('Patch', finalObj, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiCreatedResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiBody({
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Create a new field in the document',
  })
  @Post()
  async post(
    @Body() postDto: T,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @GetWsId() wsId: string,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('post', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canCreate)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    // checkAccess
    this.checkAccess(postDto, userDto, CheckAccessMethodType.post, queryParams, correlationId);
    const sanitizedIsDeletedPostDto = this.sanitizeMethodIsDeletedWrite(userDto, postDto, correlationId);
    const sanitizedWritePostDto = this.sanitizeUpsetMethod(userDto, sanitizedIsDeletedPostDto, undefined, queryParams, UpsetMethodType.Create, correlationId);

    // remove the history from the postDto and add create history since we internally control and add it
    const newHistory: IBaseUuidDtoHistory = {
      method: 'POST',
      email: userDto.eMail,
      timestamp: Date.now(),
    };
    const sanitizedHistoryPostDto = GenericDocumentController.sanitizeMethodHistoryWrite(sanitizedWritePostDto, undefined, newHistory, correlationId);

    // hint: if we do that earlier and use the sanitizedHistoryPostDto we have an error in the upsetMethod
    const genericObjOfT = sanitizedHistoryPostDto as unknown as T;
    if (!genericObjOfT.uuid) {
      throw this.createBadRequestException(`uuid has to be set!`, correlationId);
    }

    const obj = await this.upsetMethod<T>(genericObjOfT.uuid, genericObjOfT, userDto, queryParams, correlationId, true);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // we remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    // emit a websocket event
    this.socketService.socket?.emit(ApiConstants.webSocketEvents.dataChanged,{wsId: wsId, payload: {documentName: this.documentName, data: {key: genericObjOfT.uuid}}});

    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<T>>('post', finalObj, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
    description: genericDataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiConflictResponse(GenericAppController.conflictResponse)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiBody({
    schema: {
      description: genericDataTypeDescription,
      type: genericDataType,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Replace an existing field with a new one',
  })
  @Put('/:uuid')
  async put(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() putDto: T,
    @Req() request: Request,
    @GetWsId() wsId: string,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('put', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canUpdate) && !userDto.hasRight(this.rights.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    // we try to get first the old value
    const oldValue = await this.getOneMethod<T>(userDto, uuid, correlationId);
    // check if isDeleted is already true and the user doesn't have the right to modify a deleted field.
    if (oldValue.isDeleted && !userDto.hasRight(this.rights.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify a deleted field!`, correlationId);
    }
    // check if uuid is equal,
    if (putDto?.uuid && putDto.uuid !== uuid) {
      // different uuid in the body and in the uri. we do not allow this. the user has to remove the uuid or set it to equal
      throw this.createConflictException(`You try to update a document with a different uuid in the body!`, correlationId);
    }
    // checkAccess to entry. Our test will always lose.
    this.checkAccess(oldValue, userDto, CheckAccessMethodType.put, queryParams, correlationId, putDto);
    // todo: check if we realy have to remove the uuid
    // remove uuid since its no longer needed
    // delete putDto.uuid;

    // sanitize the putDto
    const sanitizedIsDeletedPutDto = this.sanitizeMethodIsDeletedWrite(userDto, putDto, correlationId);
    const sanitizedWritePutDto = this.sanitizeUpsetMethod(userDto, sanitizedIsDeletedPutDto as Partial<T>, oldValue, queryParams, UpsetMethodType.Update, correlationId);

    // clone the oldVal and remove the history to prevent circular structure in the newHistory
    const oldValueWithoutHistory = ObjectTools.cloneDeep<Partial<T>>(oldValue);
    delete oldValueWithoutHistory.history;
    const newHistory: IBaseUuidDtoHistory = {
      method: 'UPDATE',
      oldValue: oldValueWithoutHistory,
      email: userDto.eMail,
      timestamp: Date.now(),
    };
    // we remove the history from the sanitizedWritePutDto and add our internal one
    const sanitizedHistoryPutDto = GenericDocumentController.sanitizeMethodHistoryWrite(sanitizedWritePutDto as T, oldValue, newHistory, correlationId);

    // save it
    const obj = await this.upsetMethod<T>(uuid, sanitizedHistoryPutDto as unknown as T, userDto, queryParams, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    // emit a websocket event
    this.socketService.socket?.emit(ApiConstants.webSocketEvents.dataChanged,{wsId: wsId, payload: {documentName: this.documentName, data: {key: uuid}}});

    return StaticLogger.endMethod<Partial<T>>('put', finalObj, correlationId);
  }

  // helps us to check the access to certain entries or an entry.
  protected checkAccess(
    // hint: in post it's the new obj.
    oldObj: Partial<T>,
    userDto: ApiUserDto,
    methodName: CheckAccessMethodType,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    newObj?: Partial<T>
  ): void {
    StaticLogger.startMethod('checkAccess', correlationId);
    StaticLogger.endMethod<null>('checkAccess', null, correlationId);
  }

  protected async deleteMethod<T>(
    userDto: ApiUserDto,
    uuid: string,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('deleteMethod', correlationId);
    const obj = await this.baseRedisService.hDel<Partial<T>>(this.getDocumentKey(), uuid, correlationId);
    // tell typescript obj is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<Partial<T>>('deleteMethod', baseObj as unknown as Partial<T>, correlationId);
  }

  protected async getAllMethod<T>(
    userDto: ApiUserDto,
    filter: IQueryParams,
    correlationId: number | undefined,
    ): Promise<Partial<T>[]> {
    StaticLogger.startMethod('getAllMethod', correlationId);
    // any is the key of the field (uuid).
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objWithKeys = await this.baseRedisService.hGetAll<any, Partial<T>>(this.getDocumentKey(), correlationId);
    const obj: Partial<T>[] = [];
    // add the uuid back for each object
    for (const key in objWithKeys) {
      obj.push({ ...objWithKeys[key], uuid: key });
    }
    return StaticLogger.endMethod<Partial<T>[]>('getAllMethod', obj, correlationId);
  }

  protected getDocumentKey(): string {
    return `${ApiConstants.redisKeys.documents}:${this.documentName}`;
  }

  protected async getOneMethod<T>(
    userDto: ApiUserDto,
    uuid: string,
    correlationId: number | undefined,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('getOneMethod', correlationId);
    const obj = await this.baseRedisService.hGet<T>(this.getDocumentKey(), uuid, correlationId);
    if (!obj) {
      throw this.createNotFoundException(`no document with uuid ${uuid} was found!`, correlationId);
    }
    // tell typescript obj is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<Partial<T>>('getOneMethod', baseObj as unknown as T, correlationId);
  }

  protected sanitizeMethodHistoryRead<T>(
    userDto: ApiUserDto,
    data: Partial<T>,
    correlationId: number | undefined,
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeMethodHistoryRead', correlationId);
    if (!userDto.hasRight(this.rights.canSeeHistory)) {
      // remove the field isDeleted from the entry
      delete (data as unknown as BaseDocumentDto).history;
    }
    return StaticLogger.endMethod<Partial<T>>('sanitizeMethodHistoryRead', data, correlationId);
  }

  protected sanitizeMethodIsDeletedRead<T>(
    userDto: ApiUserDto,
    data: Partial<T>,
    correlationId: number | undefined,
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeMethodIsDeletedRead', correlationId);
    // We remove the isDeleted when it's the return value and the user doesn't have the right to see it.
    if (!userDto.hasRight(this.rights.canShowDeleted)) {
      // remove the field isDeleted from the entry
      delete (data as unknown as BaseDocumentDto).isDeleted;
    }
    return StaticLogger.endMethod<Partial<T>>('sanitizeMethodIsDeletedRead', data, correlationId);
  }

  protected sanitizeMethodIsDeletedWrite<T>(
    userDto: ApiUserDto,
    data: T,
    correlationId: number | undefined,
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeMethodIsDeletedWrite', correlationId);
    // We remove the isDeleted when it's the return value and the user doesn't have the right to see it.
    if (!userDto.hasRight(this.rights.canModifyDelete)) {
      // remove the field isDeleted from the entry
      delete (data as unknown as BaseDocumentDto).isDeleted;
    }
    return StaticLogger.endMethod<Partial<T>>('sanitizeMethodIsDeletedWrite', data, correlationId);
  }

  protected sanitizeReadMethod<T>(
    userDto: ApiUserDto,
    data: Partial<T>,
    correlationId: number | undefined,
    ): Partial<T> {
    StaticLogger.startMethod('sanitizeReadMethod', correlationId);
    return StaticLogger.endMethod<Partial<T>>('sanitizeReadMethod', data, correlationId);
  }

  protected sanitizeUpsetMethod<T>(
    userDto: ApiUserDto,
    newData: Partial<T>,
    oldData: Partial<T> | undefined,
    queryParams: IQueryParams,
    methodType: UpsetMethodType,
    correlationId: number | undefined
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeUpsetMethod', correlationId);
    return StaticLogger.endMethod<Partial<T>>('sanitizeUpsetMethod', newData, correlationId);
  }

  protected async upsetMethod<T>(
    uuid: string,
    obj: Partial<T>,
    userDto: ApiUserDto,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    onlyIfNotExisting = false,
    ): Promise<Partial<T>> {
    StaticLogger.startMethod('upsetMethod', correlationId);
    // when this function gets overridden then as T might not be possible.
    const ret = await this.baseRedisService.hSet<Partial<T>>(this.getDocumentKey(), uuid, obj, correlationId, onlyIfNotExisting);
    // tell typescript ret is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = ret as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<T>('upsetMethod', baseObj as unknown as T, correlationId);
  }
}
