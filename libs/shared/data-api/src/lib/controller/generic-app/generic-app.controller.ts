import { ModuleConstants } from '@itag-berufsbildung/portal/data';
import {
  AdminModuleInfoDto,
  ApiUserDto,
  GlobalConstants,
  HealthStateDto,
  Menu,
  MessageDto,
  RoleHashDto,
  UserModuleInfoDto,
  UserRoleHashDto,
} from '@itag-berufsbildung/shared/util';
import { Body, Controller, Delete, Get, Inject, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { ApiConstants } from '../../../api-constants';
import { BaseApiController } from '../../base/base-api-controller/base-api.controller';
import { BaseAppConfigService } from '../../base/base-app-config-service/base-app-config.service';
import { BaseRedisService } from '../../base/base-redis-service/base-redis.service';
import { GetCorrelationId, GetUser, Right } from '../../decorators';
import { RightGuard } from '../../guards';
import { StaticLogger } from '../../static/static-logger/static-logger';

@ApiTags( GlobalConstants.api.generic.base )
@Controller( GlobalConstants.api.generic.base )
export abstract class GenericAppController extends BaseApiController {
  protected constructor(
    @Inject( ApiConstants.services.appConfigService ) protected readonly appConfigService: BaseAppConfigService,
    @Inject( ApiConstants.services.dbService ) protected readonly baseRedisService: BaseRedisService,
  ) {
    super();
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @ApiOkResponse( GenericAppController.okResponseOption )
  @ApiNotFoundResponse( { ...GenericAppController.notFoundResponseOption, type: String } )
  @ApiBearerAuth()
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canDeleteUser )
  @ApiOperation( {
    description: `
## deleteUser

Delete a user entry.
 `,
    summary: 'delete the user by the given eMail',
  } )
  @Delete( `${ GlobalConstants.api.generic.sub.userRoles }/:eMail` )
  async deleteUser(
    @GetCorrelationId() correlationId: number | undefined,
    @GetUser() userDto: ApiUserDto,
    @Param( 'eMail' ) eMail: string,
  ): Promise<Menu | undefined> {
    StaticLogger.startMethod( 'deleteUser', correlationId );
    const key = `${ApiConstants.redisKeys.userRoleKey}:${eMail}`;

    // delete user from module
    await this.baseRedisService.del( key, correlationId, true );

    // return menu
    const obj = await this.baseRedisService.checkRolesAndGetMenu( this.appConfigService.adminAppConfiguration.menu, userDto, correlationId );
    return StaticLogger.endMethod<Menu | undefined>( 'putUserRole', obj, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @ApiOkResponse( GenericAppController.okResponseOption )
  @ApiNotFoundResponse( { ...GenericAppController.notFoundResponseOption, type: String } )
  @ApiBearerAuth()
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canDeleteUserRoles )
  @ApiOperation( {
    description: `
## deleteUserRole

Delete a user-role entry.
 `,
    summary: 'delete the user-role by the given eMail and roleName',
  } )
  @Delete( `${ GlobalConstants.api.generic.sub.userRoles }/:eMail/:roleName` )
  async deleteUserRole(
    @GetCorrelationId() correlationId: number | undefined,
    @GetUser() userDto: ApiUserDto,
    @Param( 'eMail' ) eMail: string,
    @Param( 'roleName' ) roleName: string,
  ): Promise<Menu | undefined> {
    StaticLogger.startMethod( 'deleteUserRole', correlationId );

    const userKey = `${ ApiConstants.redisKeys.userRoleKey }:${eMail}`;
    await this.baseRedisService.hDel( userKey, roleName, correlationId );

    // return menu
    const obj = await this.baseRedisService.checkRolesAndGetMenu( this.appConfigService.adminAppConfiguration.menu, userDto, correlationId );
    return StaticLogger.endMethod<Menu | undefined>( 'deleteUserRole', obj, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( {
    ...GenericAppController.okResponseOption,
    type: AdminModuleInfoDto,
  } )
  @ApiOperation( {
    description: `
## getAppInfo
Depending on the role of the user we get only a subset back.
 - role admin:
   - all properties will be get back

 - all other users just get back a subset of data
 `,
    summary: 'get the information about the application back',
  } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canReadAppInfo )
  @Get( GlobalConstants.api.generic.sub.appInfo )
  async getAppInfo( @GetUser() userDto: ApiUserDto, @GetCorrelationId() correlationId: number | undefined ): Promise<UserModuleInfoDto | AdminModuleInfoDto> {
    StaticLogger.startMethod( 'getAppInfo', correlationId );

    // declare the return type
    const moduleInfo: AdminModuleInfoDto = this.appConfigService.getModuleInfo();
    // replace the authSettings
    if ( moduleInfo.appConfig?.auth?.clientSecret ) {
      moduleInfo.appConfig.auth.clientSecret = '*'.repeat( 10 );
    }

    // check if we have the right, otherwise remove all not admin information
    if ( !userDto.hasRight( ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo ) ) {
      // remove the admin part
      delete moduleInfo.appConfig;
      delete moduleInfo.availableRights;
      delete moduleInfo.availableRoles;
      delete moduleInfo.menu;
      delete moduleInfo.dbSettings;
    }

    return StaticLogger.endMethod<UserModuleInfoDto | AdminModuleInfoDto>( 'getAppInfo', moduleInfo, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( { ...GenericAppController.okResponseOption, type: HealthStateDto } )
  @ApiOperation( {
    description: `
## getHealthState

checks if the user-info for the module is visible and the ttl is set!.
 `,
    summary: 'get health state of the module back',
  } )
  @Get( GlobalConstants.api.generic.sub.healthState )
  async getHealthState( @GetCorrelationId() correlationId: number | undefined ): Promise<HealthStateDto> {
    StaticLogger.startMethod( 'getHealthState', correlationId );
    // some check
    const ret = new HealthStateDto();
    ret.state = 'okay';
    ret.message = `Der API Server <${ this.appConfigService.adminAppConfiguration.moduleName }> arbeitet wie erwartet!`;
    return StaticLogger.endMethod<HealthStateDto>( 'getHealthState', ret, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( { ...GenericAppController.okResponseOption, type: ApiUserDto } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canReadUserInfo )
  @ApiOperation( {
    description: `
## getUserInfo

get the information about the singed-in user back!.
 `,
    summary: 'get information about the signed-in user back',
  } )
  @Get( GlobalConstants.api.generic.sub.userInfo )
  async getUserInfo( @GetCorrelationId() correlationId: number | undefined, @GetUser() userDto: ApiUserDto ): Promise<ApiUserDto> {
    StaticLogger.startMethod( 'getUserInfo', correlationId );
    return StaticLogger.endMethod<ApiUserDto>( 'getUserInfo', userDto, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( { ...GenericAppController.okResponseOption, schema: { type: 'Menu' } } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canReadMenuInfo )
  @ApiOperation( {
    description: `
## getUserInfo

get the menu information about the singed-in use!.
 `,
    summary: 'get information about the signed-in user back',
  } )
  @Get( GlobalConstants.api.generic.sub.menuInfo )
  async getUserMenu( @GetCorrelationId() correlationId: number | undefined, @GetUser() userDto: ApiUserDto ): Promise<Menu | undefined> {
    StaticLogger.startMethod( 'getUserMenu', correlationId );
    // get the menu for this user
    const obj = await this.baseRedisService.checkRolesAndGetMenu( this.appConfigService.adminAppConfiguration.menu, userDto, correlationId );
    return StaticLogger.endMethod<Menu | undefined>( 'getUserMenu', obj, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( {
    ...GenericAppController.okResponseOption,
    type: ApiUserDto,
  } )
  @ApiOperation( {
    description: `
    ## getUserRolesInfo
 `,
    summary: 'get the information all user with roles back',
  } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canReadUserRoles )
  @Get( `${ GlobalConstants.api.generic.sub.userRoles }/:eMail` )
  async getUserRolesInfo( @GetUser() userDto: ApiUserDto, @GetCorrelationId() correlationId: number | undefined, @Param( 'eMail' ) eMail: string ): Promise<RoleHashDto> {
    StaticLogger.startMethod( 'getUserRolesInfo', correlationId );

    // declare the return type
    const key = `${ ApiConstants.redisKeys.userRoleKey }:${ eMail }`;
    const x = await this.baseRedisService.hGetAll<RoleHashDto, string>( key, correlationId );
    StaticLogger.verbose( JSON.stringify( x ), correlationId );
    return StaticLogger.endMethod<RoleHashDto>( 'getUserRolesInfo', x, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( {
    ...GenericAppController.okResponseOption,
    type: String,
    isArray: true,
  } )
  @ApiOperation( {
    description: `
    ## getUsersInfo
 `,
    summary: 'get all users back for this module',
  } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canReadUsers )
  @Get( GlobalConstants.api.generic.sub.users )
  async getUsersInfo( @GetUser() userDto: ApiUserDto, @GetCorrelationId() correlationId: number | undefined ): Promise<string[]> {
    StaticLogger.startMethod( 'getUsersInfo', correlationId );
    // declare the return type
    const key = `${ ApiConstants.redisKeys.userRoleKey }*`;
    const x = await this.baseRedisService.keys( key, correlationId );
    return StaticLogger.endMethod<string[]>(
      'getUsersInfo',
      x.map( ( key ) => key.substring( ApiConstants.redisKeys.userRoleKey.length + 1 ) ),
      correlationId,
    );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( { ...GenericAppController.okResponseOption, schema: { type: 'Menu' } } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canWriteUserRoles )
  @ApiOperation( {
    description: `
## postUserRole

create a new user-role entry.
 `,
    summary: 'post a new user-role',
  } )
  @Post( GlobalConstants.api.generic.sub.userRoles )
  async postUserRole(
    @GetCorrelationId() correlationId: number | undefined,
    @GetUser() userDto: ApiUserDto,
    @Body() userRoleHashDto: UserRoleHashDto,
  ): Promise<Menu | undefined> {
    StaticLogger.startMethod( 'postUserRole', correlationId );
    await this.upsetUserRoles( userRoleHashDto, correlationId );

    // get the menu for this user
    const obj = await this.baseRedisService.checkRolesAndGetMenu( this.appConfigService.adminAppConfiguration.menu, userDto, correlationId );
    return StaticLogger.endMethod<Menu | undefined>( 'postUserRole', obj, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiOkResponse( { ...GenericAppController.okResponseOption, schema: { type: 'Menu' } } )
  @ApiBearerAuth()
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canWriteUserRoles )
  @ApiOperation( {
    description: `
## postUserRole

modify an user-role entry.
 `,
    summary: 'update a user-role',
  } )
  @Put( GlobalConstants.api.generic.sub.userRoles )
  async putUserRole(
    @GetCorrelationId() correlationId: number | undefined,
    @GetUser() userDto: ApiUserDto,
    @Body() userRoleHashDto: UserRoleHashDto,
  ): Promise<Menu | undefined> {
    StaticLogger.startMethod( 'putUserRole', correlationId );
    await this.upsetUserRoles( userRoleHashDto, correlationId );

    const obj = await this.baseRedisService.checkRolesAndGetMenu( this.appConfigService.adminAppConfiguration.menu, userDto, correlationId );
    return StaticLogger.endMethod<Menu | undefined>( 'putUserRole', obj, correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @ApiOkResponse( GenericAppController.okResponseOption )
  @ApiNotFoundResponse( { ...GenericAppController.notFoundResponseOption, type: String } )
  @ApiBearerAuth()
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canDeleteOwnCache )
  @ApiOperation( {
    description: `
## resetUserCache

The user information is cached internally for about ${ ApiConstants.cacheConfiguration.userExpiringInSeconds } seconds.
With this call you can clear the cache for the signed-in user!.
 `,
    summary: 'delete the signed-in user from the cache',
  } )
  @Delete( GlobalConstants.api.generic.sub.userInfo )
  async resetUserCache(
    @GetCorrelationId() correlationId: number | undefined,
    @GetUser() userDto: ApiUserDto,
  ): Promise<MessageDto> {
    StaticLogger.startMethod( 'resetUserCache', correlationId );
    return StaticLogger.endMethod<MessageDto>( 'resetUserCache', await this.baseRedisService.resetUserCache( userDto, correlationId ), correlationId );
  }

  @ApiInternalServerErrorResponse( GenericAppController.internalServerErrorResponseObject )
  @ApiUnauthorizedResponse( GenericAppController.unauthorizedResponseOption )
  @ApiOkResponse( GenericAppController.okResponseOption )
  @ApiNotFoundResponse( { ...GenericAppController.notFoundResponseOption, type: String } )
  @ApiBearerAuth()
  @UseGuards( RightGuard )
  @Right( ApiConstants.rights.generic.canDeleteOthersCache )
  @ApiOperation( {
    description: `
## resetUserCacheAsAdmin

The user information is cached internally for about ${ ApiConstants.cacheConfiguration.userExpiringInSeconds } seconds.

An Portal-Admin can delete an user by his eMail from the cache!.
 `,
    summary: 'delete the user by the given eMail from the cache',
  } )
  @Delete( `${ GlobalConstants.api.generic.sub.userInfo }/:eMail` )
  async resetUserCacheAsAdmin( @GetCorrelationId() correlationId: number | undefined, @GetUser() userDto: ApiUserDto, @Param( 'eMail' ) eMail: string ): Promise<MessageDto> {
    StaticLogger.startMethod( 'resetUserCache', correlationId );
    return StaticLogger.endMethod<MessageDto>( 'resetUserCache', await this.baseRedisService.resetUserCacheForOthers( userDto, eMail, correlationId ), correlationId );
  }

  private async upsetUserRoles(
    userRoleHashDto: UserRoleHashDto,
    correlationId: number | undefined,
  ): Promise<UserRoleHashDto> {
    StaticLogger.startMethod( 'upsetMethod', correlationId );

    for ( const userRoleKey of Object.keys( userRoleHashDto ) ) {
      const roleHash = userRoleHashDto[userRoleKey];
      const userKey = `${ ApiConstants.redisKeys.userRoleKey }:${ userRoleKey }`;

      for ( const roleKey of Object.keys( roleHash ) ) {
        const role = roleHash[roleKey];
        await this.baseRedisService.hSet( userKey, roleKey, role, correlationId );
      }

    }

    return StaticLogger.endMethod<UserRoleHashDto>( 'upsetMethod', userRoleHashDto as unknown as UserRoleHashDto, correlationId );
  }

}
