import { Inject, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { constants } from 'http2';

import { AuthConfig, UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { ApiConstants } from '../../../api-constants';
import { BaseRedisService } from '../../base/base-redis-service/base-redis.service';
import { BaseWithErrors } from '../../base/base-with-errors/base-with-errors';
import { StaticLogger } from '../../static/static-logger/static-logger';

export interface AuthAccessToken {
  access_token: string;
  token_type: string;
}

@Injectable()
export class ApiCallerService extends BaseWithErrors {
  constructor(private readonly http: HttpService, @Inject(ApiConstants.services.dbService) private readonly db: BaseRedisService) {
    super();
  }

  async getToken(correlationId: number | undefined): Promise<string | undefined> {
    StaticLogger.startMethod('getToken', correlationId);
    return StaticLogger.endMethod<string | undefined>('getToken', await this.db.get<string>(ApiConstants.redisKeys.cache.moduleAuthToken, correlationId), correlationId);
  }

  async registerModule(registerApiUrl: string, userModuleInfoDto: UserModuleInfoDto, authConfig: AuthConfig, correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('registerModule', correlationId);

    if (!(await this.getToken(correlationId))) {
      //  first get the token
      await this.authenticate(authConfig, correlationId);
    }

    // check if the token is set
    StaticLogger.verbose(`register module ${userModuleInfoDto.moduleName}:${userModuleInfoDto.version} for ${registerApiUrl}`, correlationId);
    return StaticLogger.endMethod<void>('registerModule', await this.post<void>(registerApiUrl, userModuleInfoDto, authConfig, correlationId), correlationId);
  }

  private authenticate(authConfig: AuthConfig, correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('authenticate', correlationId);
    return new Promise((resolve, reject) => {
      const url = `${authConfig.issuer}/oauth/token`;
      const data = {
        client_id: authConfig.clientId,
        client_secret: authConfig.clientSecret,
        audience: authConfig.aud,
        grant_type: 'client_credentials',
      };
      StaticLogger.verbose('Ask for a token', correlationId);
      StaticLogger.verbose('url:' + url, correlationId);
      StaticLogger.verbose(JSON.stringify(data, null, 2), correlationId);

      this.http.post<AuthAccessToken>(url, data).subscribe({
        next: async (res) => {
          if (res.status === constants.HTTP_STATUS_CREATED || res.status === constants.HTTP_STATUS_OK) {
            await this.db.set(ApiConstants.redisKeys.cache.moduleAuthToken, res.data.access_token, correlationId, ApiConstants.cacheConfiguration.moduleAuthExpiringInSeconds);
            return resolve();
          }
          StaticLogger.error(`We get an error on request token. status code ${res.status}, ${res.statusText}`, correlationId);
          reject();
        },
        error: (err) => {
          StaticLogger.error((err as Error).message, correlationId);
          reject();
        },
      });
    });
  }

  private async post<T>(url: string, data: unknown, authConfig: AuthConfig, correlationId: number | undefined): Promise<T> {
    StaticLogger.startMethod('post', correlationId);
    const token = await this.getToken(correlationId);
    return new Promise((resolve, reject) => {
      this.http
        .post<T>(url, data, {
          headers: {
            authorization: `Bearer ${token}`,
          },
        })
        .subscribe({
          next: async (res) => {
            if (res.status === constants.HTTP_STATUS_UNAUTHORIZED) {
              // may the token is no longer valid
              await this.db.del(ApiConstants.redisKeys.cache.moduleAuthToken, correlationId);
              await this.authenticate(authConfig, correlationId);
              return resolve(await this.post(url, data, authConfig, correlationId));
            }
            resolve(res.data);
          },
          error: (err) => {
            reject(err);
          },
        });
    });
  }
}
