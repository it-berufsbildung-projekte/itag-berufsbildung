import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { ApiUserDto } from '@itag-berufsbildung/shared/util';

import { ApiConstants } from '../../../api-constants';
import { BaseAppConfigService } from '../../base/base-app-config-service/base-app-config.service';
import { BaseRedisService } from '../../base/base-redis-service/base-redis.service';
import { BaseWithErrors } from '../../base/base-with-errors/base-with-errors';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
export class ApiUserService extends BaseWithErrors {
  constructor(
    private readonly jwtService: JwtService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfig: BaseAppConfigService
  ) {
    super();
  }

  async decodeJwtToken(token: string, correlationId: number | undefined): Promise<ApiUserDto> {
    StaticLogger.startMethod('decodeJwtToken', correlationId);
    if (!token || token == 'null') {
      throw this.createMethodNotAllowedException('the token is empty!', correlationId);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const decodeToken: any = this.jwtService.decode(token) || {};
    // check the expiring
    if (!decodeToken) {
      throw this.createMethodNotAllowedException('the token is invalid, No exp!', correlationId);
    }
    // check if the token is still valid
    const exp = decodeToken.exp;
    if (!exp || exp < Date.now() / 1000) {
      throw this.createMethodNotAllowedException('the token is invalid, expired!', correlationId);
    }

    // check the issuer
    const issuer = decodeToken.iss;
    if (!issuer) {
      throw this.createMethodNotAllowedException('the token is invalid, No issuer!', correlationId);
    }

    let eMail = '';
    switch (issuer.toLowerCase()) {
      // auth0
      case this.appConfig.adminAppConfiguration.auth.issuer.toLocaleLowerCase() + '/':
        {
          StaticLogger.debug(`we found a token from auth0 `, correlationId);
          // first we check if there is an aud set. if so it's an api call, and we assume the aud is an eMail
          if ( !decodeToken['email']) {
            // api
            eMail = this.appConfig.adminAppConfiguration.auth.aud;
          } else {
            // user
            eMail = decodeToken['email'].toLocaleLowerCase();
            if (!eMail) {
              // not valid
              throw this.createMethodNotAllowedException('the token is invalid, wrong email!', correlationId);
            }
          }
        }
        break;
      default:
        this.createMethodNotAllowedException('your token is invalid. Check issuer!', correlationId);
    }

    eMail = eMail.toLowerCase();
    StaticLogger.debug(`We found a valid token for eMail ${eMail}`, correlationId);

    // get the user
    const userDto = (await this.baseRedisService.getUser(eMail, correlationId)) as ApiUserDto;
    // read the rights for the roles

    return StaticLogger.endMethod<ApiUserDto>('decodeJwtToken', userDto, correlationId);
  }
}
