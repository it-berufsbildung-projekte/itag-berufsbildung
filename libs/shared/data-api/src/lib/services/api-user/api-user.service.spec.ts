// import { JwtService } from '@nestjs/jwt';
// import { Test } from '@nestjs/testing';
// import { getRepositoryToken } from '@nestjs/typeorm';
// import * as bCrypt from 'bcrypt';
// import { mock } from 'jest-mock-extended';
// import { Repository, SelectQueryBuilder } from 'typeorm';
//
// import { MockType, repositoryMockFactory } from '../../../../../server/utils/generic.mockups';
// import { MailModule } from '../../../../classes';
// import { SocketService } from '../../../../services';
// import { mockedJwtService } from '../../../../utils/jwt.service.mockup';
// import { adminUser, disableEnvLoggerAndReturnOldEnv } from '../../../../utils/mockup.helper';
// import { mockedSocketService } from '../../../../utils/socket.service.mockup';
// import { ApiTokenDto, CreateLongTimeTokenDto,
//   EmailDto,
//   PasswordLostDto,
//   PasswordResetDto,
//   RoleDto,
//   SignInDto,
//   UserCreateDto,
//   UserCreateForUserDto,
//   UserDto,
//   UserUpdateForUserDto,
// } from '../dto';
// import { RoleEntity, UserRoleEntity } from '../entity';
// import { enumUserType, UserEntity } from '../entity/user.entity';
// import { createLongTimeTokenDtoMockup, roleEntityArrayMockup, roleEntityMockup, userDtoMockup, userEntityArrayMockup, userEntityMockup, userRole } from '../utils/userEntryMockup';
// import { UserService } from './user.service';
//
// jest.useFakeTimers();
//
// // https://github.com/typeorm/typeorm/issues/12671
// describe( 'userService with mockup', () => {
//   let env: NodeJS.ProcessEnv;
//   let userService: UserService;
//   let userEntityRepositoryMock: MockType<Repository<UserEntity>>;
//   let roleEntityRepositoryMock: MockType<Repository<RoleEntity>>;
//   // let userRoleEntityRepositoryMock: MockType<Repository<UserRoleEntity>>;
//   let jwtService: JwtService;
//
//   // hint: we prepare also the SelectQueryBuilder just in case someone like to use the QueryBuilder for a select
//   const selectQBuilderMock = mock<SelectQueryBuilder<UserEntity>>();
//   // const insertQBuilderMock = mock<InsertQueryBuilder<UserEntity>>();
//   // const updateQBuilderMock = mock<UpdateQueryBuilder<UserEntity>>();
//   // const deleteQBuilderMock = mock<DeleteQueryBuilder<UserEntity>>();
//
//   beforeEach( async () => {
//     jest.clearAllMocks();
//
//     // read the env:
//     env = disableEnvLoggerAndReturnOldEnv();
//     // create module
//     const module = await Test.createTestingModule( {
//       imports: [],
//       providers: [
//         UserService,
//         { provide: SocketService, useValue: mockedSocketService },
//         { provide: JwtService, useValue: mockedJwtService },
//         {
//           provide: getRepositoryToken( UserEntity ),
//           useFactory: repositoryMockFactory,
//         },
//         {
//           provide: getRepositoryToken( RoleEntity ),
//           useFactory: repositoryMockFactory,
//         },
//         {
//           provide: getRepositoryToken( UserRoleEntity ),
//           useFactory: repositoryMockFactory,
//         },
//       ],
//     } ).compile();
//     // save the import-data from the TestingModule
//     userService = module.get<UserService>( UserService );
//     jwtService = module.get<JwtService>( JwtService );
//     userEntityRepositoryMock = module.get( getRepositoryToken( UserEntity ) );
//     roleEntityRepositoryMock = module.get( getRepositoryToken( RoleEntity ) );
//     // userRoleEntityRepositoryMock = module.get(getRepositoryToken(UserRoleEntity));
//   } );
//
//   afterAll( () => {
//     // reset the environment
//     process.env = env;
//     // clear alle Mockups
//     jest.restoreAllMocks();
//   } );
//
//   it( 'userService should be defined', () => {
//     expect( userService ).toBeDefined();
//   } );
//
//   describe( 'validateUser', () => {
//     it( 'valide response', async () => {
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'user1@test',
//         password: 'test1234',
//       };
//
//       const userEntity: UserEntity = userEntityMockup( enumUserType.form, true );
//       userEntity.salt = await bCrypt.genSalt();
//       userEntity.password = await bCrypt.hash(
//         signInDto.password,
//         userEntity.salt,
//       );
//
//       // const userDto = UserService.convertUserEntityToUserDto(userEntity);
//       const userDto = userDtoMockup();
//
//       userEntityRepositoryMock.findOne.mockReturnValue( userEntity );
//       jest
//         .spyOn( userService, 'getUserWithRolesById' )
//         .mockResolvedValueOnce( userDto );
//
//       const userDtoValidated = await userService.validateUser( signInDto, 0 );
//       expect( userDtoValidated ).toEqual( userDto );
//     } );
//
//     it( 'wrong password', async () => {
//       const userEntity: UserEntity = userEntityMockup( enumUserType.form, true );
//       const msg = `user ${ userEntity.username } could not be validated!`;
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'user1@test',
//         password: 'test1234',
//       };
//       userEntity.salt = await bCrypt.genSalt();
//       userEntity.password = await bCrypt.hash( 'test4321', userEntity.salt );
//       const userDto = UserService.convertUserEntityToUserDto( userEntity );
//       userEntityRepositoryMock.findOne.mockReturnValue( userEntity );
//       jest
//         .spyOn( userService, 'getUserWithRolesById' )
//         .mockResolvedValueOnce( userDto );
//       try {
//         expect( await userService.validateUser( signInDto, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'ADFS user', async () => {
//       const msg = `the requested user is registered as ADFS user. You can't sign-in with this username!`;
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'user1@test',
//         password: 'test1234',
//       };
//       const userEntity: UserEntity = userEntityMockup( enumUserType.adfs, true );
//       userEntity.salt = await bCrypt.genSalt();
//       userEntity.password = await bCrypt.hash(
//         signInDto.password,
//         userEntity.salt,
//       );
//       userEntityRepositoryMock.findOne.mockReturnValue( userEntity );
//       try {
//         expect( await userService.validateUser( signInDto, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'empty SignInDto', async () => {
//       const msg = `SignInDto has to be set!`;
//       try {
//         expect( await userService.validateUser( null, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'returns value null', async () => {
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'abc123',
//         password: 'abc123',
//       };
//       const userEntity: unknown = null;
//       userEntityRepositoryMock.findOne.mockReturnValue( userEntity );
//       expect( await userService.validateUser( signInDto, 1 ) ).toEqual( userEntity );
//     } );
//   } );
//
//   describe( 'createUserEntity', () => {
//     it( 'create successfully Form', async () => {
//       const entity = userEntityMockup( enumUserType.form, false );
//       const dto = UserService.convertUserEntityToUserDto( entity );
//       // check email
//       jest.spyOn( userService, 'getEntitiesFiltered' ).mockReturnValueOnce( null );
//       // check username
//       jest.spyOn( userService, 'getEntitiesFiltered' ).mockReturnValueOnce( null );
//       // create entity
//       jest.spyOn( userService, 'createEntity' ).mockResolvedValueOnce( entity );
//       // save new entity
//       jest.spyOn( entity, 'save' ).mockResolvedValueOnce( entity );
//       // get by name
//       jest
//         .spyOn( userService, 'deleteOldRolesAndSetNewRoles' )
//         .mockResolvedValueOnce( null );
//
//       // send Token
//       jest
//         .spyOn( userService, 'sendEmailValidationTokenByEMail' )
//         .mockReturnValueOnce( null );
//
//       // finally, get the result back
//       jest
//         .spyOn( userService, 'getUserWithRolesById' )
//         .mockResolvedValueOnce( dto );
//       // prepare the creation object, based on the expected return value
//       const createDto: UserCreateForUserDto = {
//         displayName: entity.displayName,
//         email: entity.email,
//         password: entity.password,
//         userTypeName: entity.userType,
//         username: entity.username,
//         fromDate: entity.fromDate,
//         untilDate: entity.untilDate,
//       };
//       // now we are ready to call
//       expect(
//         await userService.createUserEntity( createDto, null, 0, null, false ),
//       ).toEqual( dto );
//     } );
//
//     it( 'with existing email', async () => {
//       const userEntity = userEntityMockup();
//       const userDto = UserService.convertUserEntityToUserDto( userEntity );
//       const userCreateDto: UserCreateForUserDto = {
//         displayName: userDto.displayName,
//         email: userDto.email,
//         password: 'abc123',
//         userTypeName: userDto.userTypeName,
//         username: userDto.username,
//         fromDate: userDto.fromDate,
//         untilDate: userDto.untilDate,
//       };
//       const msg = `the email ${ userDto.email } already exists!`;
//       const userEntityArray: UserEntity[] = userEntityArrayMockup( 10 );
//       // mockup email check
//       jest
//         .spyOn( userService, 'getEntitiesFiltered' )
//         .mockResolvedValueOnce( userEntityArray );
//       try {
//         expect(
//           await userService.createUserEntity( userCreateDto, null, 0, '', false ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with existing username', async () => {
//       const userEntity = userEntityMockup();
//       const userDto = UserService.convertUserEntityToUserDto( userEntity );
//       const userCreateDto: UserCreateForUserDto = {
//         displayName: userDto.displayName,
//         email: userDto.email,
//         password: 'abc123',
//         userTypeName: userDto.userTypeName,
//         username: userDto.username,
//         fromDate: userDto.fromDate,
//         untilDate: userDto.untilDate,
//       };
//       const msg = `the username ${ userDto.username } already exists!`;
//       const userEntityArray: UserEntity[] = userEntityArrayMockup( 10 );
//       // mockup email check
//       jest
//         .spyOn( userService, 'getEntitiesFiltered' )
//         .mockResolvedValueOnce( null );
//       // mockup username check
//       jest
//         .spyOn( userService, 'getEntitiesFiltered' )
//         .mockResolvedValueOnce( userEntityArray );
//       try {
//         expect(
//           await userService.createUserEntity( userCreateDto, null, 0, '', false ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//     it( 'with empty password for form user', async () => {
//       const userEntity = userEntityMockup( enumUserType.form );
//       const userDto = UserService.convertUserEntityToUserDto( userEntity );
//       const userCreateDto: UserCreateForUserDto = {
//         displayName: userDto.displayName,
//         email: userDto.email,
//         password: '',
//         userTypeName: userDto.userTypeName,
//         username: userDto.username,
//         fromDate: userDto.fromDate,
//         untilDate: userDto.untilDate,
//       };
//       const msg = `For form based users, we need a password with a min length of 6 characters!`;
//       // mockup email check
//       jest
//         .spyOn( userService, 'getEntitiesFiltered' )
//         .mockResolvedValueOnce( null );
//       // mockup username check
//       jest
//         .spyOn( userService, 'getEntitiesFiltered' )
//         .mockResolvedValueOnce( null );
//       // mockup the creation of an entity
//       jest.spyOn( userService, 'createEntity' ).mockResolvedValueOnce( userEntity );
//       try {
//         expect(
//           await userService.createUserEntity( userCreateDto, null, 0, '', false ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with UserCreateDto null', async () => {
//       const userCreateDto: UserCreateDto = null;
//       const msg = 'UserCreateDto cant be null!';
//       try {
//         expect(
//           await userService.createUserEntity( userCreateDto, null, 0, '', false ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with UserCreateDto empty', async () => {
//       const userCreateDto: any = {};
//       const msg = 'UserCreateDto cant be empty!';
//       try {
//         expect(
//           await userService.createUserEntity( userCreateDto, null, 0, '', false ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   // WORKING
//
//   describe( 'signIn', () => {
//     it( 'signIn with valid username and password', async () => {
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'user1@test',
//         password: 'test1234',
//       };
//       const token = `fake token from jwt.mockup`;
//       const userDto = userDtoMockup();
//       userDto.username = signInDto.usernameOrEmail;
//
//       const spyValidate = jest
//         .spyOn( userService, 'validateUser' )
//         .mockResolvedValueOnce( userDto );
//       const spySign = jest.spyOn( jwtService, 'sign' ).mockReturnValue( token );
//       const receivedToken = await userService.signIn( signInDto, 0 );
//       expect( receivedToken.accessToken ).toEqual( token );
//       expect( spyValidate ).toHaveBeenCalledTimes( 1 );
//       expect( spySign ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'with user not found', async () => {
//       const msg = 'user not found!';
//       const signInDto: SignInDto = {
//         usernameOrEmail: 'user1@test',
//         password: 'test1234',
//       };
//       jest.spyOn( userService, 'validateUser' ).mockReturnValue( null );
//       try {
//         expect( await userService.signIn( signInDto, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'getRoleEntityByName', () => {
//     it( 'get a valid response', async () => {
//       const roleEntity = roleEntityMockup();
//       roleEntityRepositoryMock.findOne.mockReturnValue( roleEntity );
//       const response = await userService.getRoleEntityByName(
//         roleEntity.name,
//         0,
//       );
//       expect( response ).toEqual( roleEntity );
//     } );
//     it( 'without a name', async () => {
//       const msg = 'property name must be set!';
//       try {
//         expect( await userService.getRoleEntityByName( null, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'getUsersWithRoles', () => {
//     it( 'get all users', async () => {
//       const count = 10;
//       const find = userEntityArrayMockup( count );
//       const dtos: UserDto[] = [];
//       find.forEach( ( entity ) => {
//         dtos.push( UserService.convertUserEntityToUserDto( entity ) );
//       } );
//
//       // mock the queryBuilder
//       selectQBuilderMock.select.mockReturnThis();
//       selectQBuilderMock.leftJoinAndSelect.mockReturnThis();
//       selectQBuilderMock.leftJoinAndSelect.mockReturnThis();
//       selectQBuilderMock.where.mockReturnThis();
//       selectQBuilderMock.getMany.mockResolvedValue( find );
//       // add the queryBuilderMockups to the genericRepositoryMock
//       userEntityRepositoryMock.createQueryBuilder.mockReturnValue(
//         selectQBuilderMock,
//       );
//       const ret: UserDto[] = await userService.getUsersWithRoles( true, 0 );
//       expect( ret ).toEqual( dtos );
//       expect( ret.length ).toEqual( count );
//     } );
//   } );
//
//   describe( 'getUserWithRolesById', () => {
//     it( 'successfully get userDto', async () => {
//       const userEntityObj: UserEntity = userEntityMockup();
//       // mock the queryBuilder
//       selectQBuilderMock.select.mockReturnThis();
//       selectQBuilderMock.leftJoinAndSelect.mockReturnThis();
//       selectQBuilderMock.where.mockReturnThis();
//       selectQBuilderMock.getOne.mockResolvedValue( userEntityObj );
//       // add the queryBuilderMockups to the genericRepositoryMock
//       userEntityRepositoryMock.createQueryBuilder.mockReturnValue(
//         selectQBuilderMock,
//       );
//       // we search for an entity after the creation to get a full entity back
//       userEntityRepositoryMock.findOne.mockReturnValueOnce( userEntityObj );
//       const userDto: UserDto = UserService.convertUserEntityToUserDto(
//         userEntityObj,
//       );
//       expect( await userService.getUserWithRolesById( 1, 0 ) ).toEqual( userDto );
//     } );
//
//     it( 'Check id has to be greater than zero', async () => {
//       const id = 0;
//       const msg = `id has to be greater than zero!`;
//       try {
//         expect( await userService.getUserWithRolesById( id, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'unsuccessfully get id with letters', async () => {
//       const id: any = 'abc';
//       const msg = `id has to be a number!`;
//       try {
//         expect( await userService.getUserWithRolesById( id, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'Check for not existing id', async () => {
//       // mock the queryBuilder
//       selectQBuilderMock.select.mockReturnThis();
//       selectQBuilderMock.leftJoinAndSelect.mockReturnThis();
//       selectQBuilderMock.where.mockReturnThis();
//       selectQBuilderMock.getOne.mockResolvedValue( null );
//       // add the queryBuilderMockups to the genericRepositoryMock
//       userEntityRepositoryMock.createQueryBuilder.mockReturnValue(
//         selectQBuilderMock,
//       );
//       userEntityRepositoryMock.findOne.mockReturnValue( null );
//       const id = 97;
//       const msg = `we could not find the entity with id: ${ id }!`;
//       try {
//         expect( await userService.getUserWithRolesById( id, 0 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'updateUserEntity', () => {
//     it( 'working with id 1', async () => {
//       const entity = userEntityMockup( enumUserType.form );
//       // prepare the creation object, based on the expected return value
//       const userUpdateDto: UserUpdateForUserDto = {
//         password: null,
//         roles: [],
//         userTypeName: entity.userType,
//         displayName: 'newDisplayName',
//         email: entity.email,
//         username: entity.username,
//       };
//       // we search for an entity after the update with the new value
//       const updatedEntity: UserEntity = { ...entity } as UserEntity;
//       updatedEntity.displayName = userUpdateDto.displayName;
//       jest
//         .spyOn( userService, 'getRoleEntityByName' )
//         .mockResolvedValueOnce( userRole() );
//       jest.spyOn( userService, 'createEntity' ).mockResolvedValueOnce( entity );
//       jest.spyOn( userService, 'getEntityById' ).mockResolvedValueOnce( entity );
//       jest.spyOn( entity, 'save' ).mockResolvedValueOnce( entity );
//       jest
//         .spyOn( userService, 'getUserWithRolesById' )
//         .mockResolvedValueOnce(
//           UserService.convertUserEntityToUserDto( updatedEntity ),
//         );
//       // we search for an entity before we update to ensure the record exists
//       userEntityRepositoryMock.findOne.mockReturnValueOnce( entity );
//       // now we are ready to call
//       expect(
//         await userService.updateUserEntity( 1, userUpdateDto, null, 0, null ),
//       ).toEqual( UserService.convertUserEntityToUserDto( updatedEntity ) );
//     } );
//
//     it( 'not working, update not exist id 1', async () => {
//       const id = 999;
//       const msg = `we could not find the entity with id: ${ id }!`;
//       try {
//         // not found the user
//         // userEntityRepositoryMock.findOne.mockReturnValueOnce(null);
//         const userUpdateDto: UserUpdateForUserDto = {
//           password: 'aaaa',
//           roles: [],
//           userTypeName: enumUserType.form,
//           email: 'aaa',
//           username: 'aaaa',
//           displayName: 'newName',
//         };
//         jest.spyOn( userService, 'getEntityById' ).mockResolvedValueOnce( null );
//         userEntityRepositoryMock.findOne.mockReturnValueOnce( null );
//         expect(
//           await userService.updateUserEntity( id, userUpdateDto, null, 0, null ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'updateDto is null', async () => {
//       const msg = `we could not update an user without data!`;
//       try {
//         expect(
//           await userService.updateUserEntity( 1, null, null, 0, null ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'updateDto is empty', async () => {
//       const msg = `update dto can't be empty!`;
//       try {
//         const userUpdateDto: any = {};
//         expect(
//           await userService.updateUserEntity( 1, userUpdateDto, null, 0, null ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'update email to an already existing one', async () => {
//       const oldEntity = userEntityMockup();
//       const resForMail = userEntityMockup();
//       const msg =
//         'we could not save the email as the email new@new.ch already exist!';
//       const email = 'new@new.ch';
//       const userUpdateDto: UserUpdateForUserDto = {
//         password: null,
//         roles: [],
//         userTypeName: oldEntity.userType,
//         email,
//         displayName: oldEntity.displayName,
//         username: oldEntity.username,
//       };
//       resForMail.email = email;
//       jest.spyOn( userService, 'getEntityById' ).mockResolvedValueOnce( oldEntity );
//       // entity with the same email as the email from userUpdateDto
//       jest.spyOn( UserEntity, 'findOne' ).mockResolvedValueOnce( resForMail );
//       try {
//         expect(
//           await userService.updateUserEntity( 1, userUpdateDto, null, 0, null ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'update username to an already existing one', async () => {
//       const oldEntity = userEntityMockup();
//       const resForUsername = userEntityMockup();
//       const msg =
//         'we could not save the username new@new.ch as the username already exist!';
//       const username = 'new@new.ch';
//       const userUpdateDto: UserUpdateForUserDto = {
//         password: null,
//         roles: [],
//         userTypeName: oldEntity.userType,
//         username,
//         displayName: oldEntity.displayName,
//         email: oldEntity.email,
//       };
//       resForUsername.username = username;
//       jest.spyOn( userService, 'getEntityById' ).mockResolvedValueOnce( oldEntity );
//       // entity with the same username as the username from userUpdateDto
//       jest.spyOn( UserEntity, 'findOne' ).mockResolvedValueOnce( resForUsername );
//       try {
//         expect(
//           await userService.updateUserEntity( 1, userUpdateDto, null, 0, null ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'validateTokenAndConfirmUser', () => {
//     it( 'working validate token', async () => {
//       const token = 'token';
//       const entity = userEntityMockup();
//       // we return the token from user in database
//       const userSpy = jest
//         .spyOn( userService, 'validateToken' )
//         .mockResolvedValueOnce( entity );
//       // spy on save
//       const saveSpy = jest.spyOn( entity, 'save' ).mockReturnThis();
//
//       await userService.validateTokenAndConfirmUser( token, 1 );
//
//       expect( userSpy ).toHaveBeenCalledTimes( 1 );
//       expect( saveSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'token null', async () => {
//       const token: string = null;
//       const msg = 'the token was empty. Not allowed!';
//       try {
//         expect( await userService.validateTokenAndConfirmUser( token, 1 ) ).toThrow(
//           msg,
//         );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   // hint: validate User check schon, ob das Passwort übereinstimmt. Deswegen hier nicht getestet.
//   describe( 'passwordReset', () => {
//     it( 'successfully', async () => {
//       const passwordResetDto: PasswordResetDto = {
//         newPassword: 'abc123',
//         oldPassword: '321cba',
//       };
//       const userDto: UserDto = userDtoMockup();
//
//       const validateUserSpy = jest
//         .spyOn( userService, 'validateUser' )
//         .mockReturnThis();
//       const getEntitySpy = jest
//         .spyOn( userService, 'getEntityById' )
//         .mockReturnThis();
//       const changePasswordSpy = jest
//         .spyOn( userService, 'changePassword' )
//         .mockReturnThis();
//
//       await userService.passwordReset(
//         1,
//         passwordResetDto,
//         userDto,
//         null,
//         1,
//       );
//       expect( validateUserSpy ).toHaveBeenCalledTimes( 1 );
//       expect( getEntitySpy ).toHaveBeenCalledTimes( 1 );
//       expect( changePasswordSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'empty passwordResetDto', async () => {
//       const passwordResetDto: any = {};
//       const userDto: UserDto = userDtoMockup();
//       const msg = `passwordResetDto can't be empty!`;
//       try {
//         expect(
//           await userService.passwordReset(
//             userDto.id,
//             passwordResetDto,
//             userDto,
//             null,
//             1,
//           ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'passwordResetDto null', async () => {
//       const passwordResetDto: PasswordResetDto = null;
//       const userDto: UserDto = userDtoMockup();
//       const msg = `passwordResetDto can't be null!`;
//       try {
//         expect(
//           await userService.passwordReset(
//             userDto.id,
//             passwordResetDto,
//             userDto,
//             null,
//             1,
//           ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//   } );
//
//   describe( 'sendEmailValidationTokenByEMail', () => {
//     it( 'successfully', async () => {
//       const emailDto: EmailDto = {
//         email: 'abc@abc.ch',
//       };
//       const link = 'test.ch';
//       const updateUserSpy = jest
//         .spyOn( userService, 'updateUserWithCodeByEmail' )
//         .mockResolvedValueOnce( userEntityMockup() );
//       const createLinkSpy = jest
//         .spyOn( userService, 'createLink' )
//         .mockReturnValue( link );
//       const sendTokenSpy = jest
//         .spyOn( userService, 'sendValidationTokenByEmail' )
//         .mockReturnThis();
//       await userService.sendEmailValidationTokenByEMail( emailDto, 1 );
//       expect( updateUserSpy ).toHaveBeenCalledTimes( 1 );
//       expect( createLinkSpy ).toHaveBeenCalledTimes( 1 );
//       expect( sendTokenSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'empty emailDto', async () => {
//       const emailDto: any = {};
//       const msg = `emailDto can't be empty!`;
//       try {
//         expect(
//           await userService.sendEmailValidationTokenByEMail( emailDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'emailDto null', async () => {
//       const emailDto: EmailDto = null;
//       const msg = `emailDto can't be null!`;
//       try {
//         expect(
//           await userService.sendEmailValidationTokenByEMail( emailDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'sendPasswordValidationTokenByEmail', () => {
//     it( 'successfully', async () => {
//       const emailDto: EmailDto = {
//         email: 'test@test.ch',
//       };
//       // Since we don't have a return value we test if the method has called all functions
//       const updateUserSpy = jest
//         .spyOn( userService, 'updateUserWithCodeByEmail' )
//         .mockReturnThis();
//       const createLinkSpy = jest
//         .spyOn( userService, 'createLink' )
//         .mockReturnThis();
//       const sendTokenSpy = jest
//         .spyOn( userService, 'sendValidationTokenByEmail' )
//         .mockReturnThis();
//
//       await userService.sendEmailValidationTokenByEMail( emailDto, 1 );
//
//       expect( updateUserSpy ).toHaveBeenCalledTimes( 1 );
//       expect( createLinkSpy ).toHaveBeenCalledTimes( 1 );
//       expect( sendTokenSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'empty emailDto', async () => {
//       const emailDto: any = {};
//       const msg = `email dto can't be emtpy!`;
//       try {
//         expect(
//           await userService.sendPasswordValidationTokenByEmail( emailDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'emailDto null', async () => {
//       const emailDto: EmailDto = null;
//       const msg = `emailDto cant be null!`;
//       try {
//         expect(
//           await userService.sendPasswordValidationTokenByEmail( emailDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'validateToken', () => {
//     it( 'successfully with correct return value', async () => {
//       const token = {
//         email: 'test@test.ch',
//         code: 'abcdefg',
//       };
//       jest.spyOn( jwtService, 'verify' ).mockReturnThis();
//       jest.spyOn( jwtService, 'decode' ).mockReturnValue( token );
//
//       const userEntity = userEntityMockup();
//       userEntity.code = token.code;
//       jest.spyOn( UserEntity, 'findOne' ).mockResolvedValueOnce( userEntity );
//
//       expect( await userService.validateToken( 'abcd', 1 ) ).toEqual( userEntity );
//     } );
//
//     it( 'without any user', async () => {
//       const token = {
//         email: 'test@test.ch',
//         code: 'abcdefg',
//       };
//       const msg = `user with email ${ token.email } and code ${ token.code } not found!`;
//       jest.spyOn( jwtService, 'verify' ).mockReturnThis();
//       jest.spyOn( jwtService, 'decode' ).mockReturnValue( token );
//
//       jest.spyOn( UserEntity, 'findOne' ).mockResolvedValueOnce( null );
//       try {
//         expect( await userService.validateToken( 'abcd', 1 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with a token null', async () => {
//       const token: string = null;
//       const msg = `token can't be null!`;
//       try {
//         expect( await userService.validateToken( token, 1 ) ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'validateTokenAndChangePassword', () => {
//     it( 'successfully', async () => {
//       const userEntity = userEntityMockup( enumUserType.form );
//       const passwordLostDto: PasswordLostDto = {
//         validationToken: 'abcdefg',
//         password: 'abc123',
//       };
//       // we spy on each method that is getting called from validateTokenAndChangePassword since the method doesn't have a return value
//       const validateTokenSpy = jest
//         .spyOn( userService, 'validateToken' )
//         .mockResolvedValueOnce( userEntity );
//       const changePasswordSpy = jest
//         .spyOn( userService, 'changePassword' )
//         .mockReturnThis();
//
//       await userService.validateTokenAndChangePassword( passwordLostDto, 1 );
//
//       expect( validateTokenSpy ).toHaveBeenCalledTimes( 1 );
//       expect( changePasswordSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//
//     it( 'unsuccessfully with adfs user', async () => {
//       const userEntity = userEntityMockup( enumUserType.adfs );
//       const passwordLostDto: PasswordLostDto = {
//         validationToken: 'abcdefg',
//         password: 'abc123',
//       };
//       const msg = `user with type ADFS can't change password!`;
//       jest
//         .spyOn( userService, 'validateToken' )
//         .mockResolvedValueOnce( userEntity );
//       try {
//         expect(
//           await userService.validateTokenAndChangePassword( passwordLostDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with empty passwordLostDto', async () => {
//       const passwordLostDto: any = {};
//       const msg = `passwordLostDto can't be empty!`;
//       try {
//         expect(
//           await userService.validateTokenAndChangePassword( passwordLostDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with passwordLostDto null', async () => {
//       const passwordLostDto: PasswordLostDto = null;
//       const msg = `passwordLostDto can't be null!`;
//       try {
//         expect(
//           await userService.validateTokenAndChangePassword( passwordLostDto, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'updateUserWithCodeByEmail', () => {
//     it( 'successfully', async () => {
//       const userEntity = userEntityMockup( enumUserType.form );
//       // mock findOne
//       jest
//         .spyOn( userEntityRepositoryMock, 'findOne' )
//         .mockReturnValue( userEntity );
//       // mock createAccessCode
//       jest
//         .spyOn( UserService, 'createAccessCode' )
//         .mockResolvedValueOnce( userEntity.code );
//       // mock save
//       jest.spyOn( userEntity, 'save' ).mockReturnThis();
//
//       expect(
//         await userService.updateUserWithCodeByEmail( userEntity.email, 1 ),
//       ).toEqual( userEntity );
//     } );
//
//     it( 'with adfs user', async () => {
//       const userEntity = userEntityMockup( enumUserType.adfs );
//       const msg = `user with type ADFS has no validation possibility!`;
//       jest
//         .spyOn( userEntityRepositoryMock, 'findOne' )
//         .mockReturnValue( userEntity );
//       try {
//         expect(
//           await userService.updateUserWithCodeByEmail( userEntity.email, 1 ),
//         ).toThrow( msg );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//
//     it( 'with no email', async () => {
//       const userEntity: unknown = null;
//       const msg = `user with email null not found!`;
//       jest
//         .spyOn( userEntityRepositoryMock, 'findOne' )
//         .mockReturnValue( userEntity );
//       try {
//         expect( await userService.updateUserWithCodeByEmail( null, 1 ) ).toThrow(
//           msg,
//         );
//       } catch ( err ) {
//         expect( err.message ).toContain( msg );
//       }
//     } );
//   } );
//
//   describe( 'createLink', () => {
//     it( 'successfully', async () => {
//       const userEntity = userEntityMockup();
//       const validationToken = 'abcdef';
//       const linkDetail = '123456';
//       const link = `http://localhost:4201/validate/${ linkDetail }/${ validationToken }`;
//       jest.spyOn( jwtService, 'sign' ).mockReturnValue( validationToken );
//
//       expect( await userService.createLink( userEntity, linkDetail, 1 ) ).toEqual(
//         link,
//       );
//     } );
//   } );
//
//   describe( 'sendValidationTokenByEmail', () => {
//     it( 'successfully', async () => {
//       const userEntity = userEntityMockup();
//
//       // the method doesn't have a return value, so we spy on the called ones
//       const mailSpy = jest.spyOn( MailModule, 'sendMail' ).mockReturnThis();
//       const writeDebugSpy = jest
//         .spyOn( userService, 'writeDebugLogEntry' )
//         .mockReturnThis();
//
//       await userService.sendValidationTokenByEmail(
//         userEntity,
//         1,
//         '',
//         '',
//         '',
//       );
//
//       expect( mailSpy ).toHaveBeenCalledTimes( 1 );
//       expect( writeDebugSpy ).toHaveBeenCalledTimes( 1 );
//     } );
//   } );
//
//   describe( 'createLongTimeToken', () => {
//     it( 'successfully', async () => {
//       const createLongTimeTokenDto = createLongTimeTokenDtoMockup();
//       const userEntityMock: UserEntity = userEntityMockup( enumUserType.form );
//       const userDtoMock: UserDto = UserService.convertUserEntityToUserDto(
//         userEntityMock,
//       );
//       const apiTokenDto: ApiTokenDto = {
//         accessToken: 'testToken1',
//       };
//       // mock findOne
//       userEntityRepositoryMock.findOne.mockReturnValue( userEntityMock );
//       // mock validateUser
//       jest
//         .spyOn( userService, 'validateUser' )
//         .mockResolvedValueOnce( userDtoMock );
//       // mock sign
//       jest.spyOn( jwtService, 'sign' ).mockReturnValue( apiTokenDto.accessToken );
//
//       expect(
//         await userService.createLongTimeToken( createLongTimeTokenDto, 1 ),
//       ).toEqual( apiTokenDto );
//     } );
//
//     it( 'unsuccessfully with null createLongTimeTokenDto', async () => {
//       const createLongTimeTokenDto: CreateLongTimeTokenDto = null;
//       const msg = `createLongTimeTokenDto has to be set!`;
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//
//     it('unsuccessfully with empty createLongTimeTokenDto', async () => {
//       const createLongTimeTokenDto: any = {};
//       const msg = `createLongTimeToken can't be empty!`;
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//
//     it('unsuccessfully with admin user found', async () => {
//       const createLongTimeTokenDto = createLongTimeTokenDtoMockup();
//       const userDtoMock: UserDto = adminUser;
//       const msg = `the requested user is member of the admin role. You can't register a longTimeToken with this username!`;
//
//       jest
//         .spyOn(userService, 'validateUser')
//         .mockResolvedValueOnce(userDtoMock);
//
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//
//     it('unsuccessfully with more than 120 duration days', async () => {
//       const createLongTimeTokenDto = createLongTimeTokenDtoMockup();
//       createLongTimeTokenDto.durationInDays = 121;
//       const msg = `the duration days can't be higher than 120 and 0 or lower!`;
//
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//
//     it('unsuccessfully with duration days lower than 1', async () => {
//       const createLongTimeTokenDto = createLongTimeTokenDtoMockup();
//       createLongTimeTokenDto.durationInDays = 0;
//       const msg = `the duration days can't be higher than 120 and 0 or lower!`;
//
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//
//     it('unsuccessfully with invalid user import-data', async () => {
//       const createLongTimeTokenDto = createLongTimeTokenDtoMockup();
//       const msg = `invalid user data!`;
//
//       jest
//         .spyOn(userService, 'validateUser')
//         .mockResolvedValueOnce(null);
//
//       try {
//         expect(
//           await userService.createLongTimeToken(createLongTimeTokenDto, 1),
//         ).toThrow(msg);
//       } catch (err) {
//         expect(err.message).toContain(msg);
//       }
//     });
//   });
//
//   describe( 'getRoles', () => {
//
//     it( 'successfully', async () => {
//       const roles: RoleEntity[] = roleEntityArrayMockup(5);
//       // mock the find
//       roleEntityRepositoryMock.find.mockReturnValueOnce(roles);
//       // convert the role entity to dto
//       const dtoArr: RoleDto[] = [];
//       for ( const key in roles ) {
//         const dto: RoleDto = {
//           name: roles[key].name,
//         }
//         dtoArr.push(dto);
//       }
//
//       expect(await userService.getRoles(0)).toEqual(dtoArr);
//     });
//
//   });
//
// });
