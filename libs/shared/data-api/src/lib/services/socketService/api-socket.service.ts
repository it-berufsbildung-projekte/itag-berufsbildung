import { Inject, Injectable } from '@nestjs/common';
import { Server } from 'socket.io';
import { ApiConstants } from '../../../api-constants';
import { BaseAppConfigService } from '../../base/base-app-config-service/base-app-config.service';

@Injectable()
export class ApiSocketService {
  socket: Server | undefined = undefined;
  readonly url: string;

  constructor(@Inject(ApiConstants.services.appConfigService) appConfig: BaseAppConfigService) {
    const appPath = `${appConfig.adminAppConfiguration.apiProtocol}://${appConfig.adminAppConfiguration.apiHost}${
      appConfig.adminAppConfiguration.apiPort ? '_' + appConfig.adminAppConfiguration.apiPort : ''
    }`;
    this.url = `${appPath}/socket.io/socket.io.js`;
  }

}
