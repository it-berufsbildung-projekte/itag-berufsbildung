import { disableEnvLoggerAndReturnOldEnv } from '../../utils/mockup.helper';
import { Test } from '@nestjs/testing';
import { ApiSocketService } from './api-socket.service';

describe('socketService', () => {
  let env: NodeJS.ProcessEnv;
  let service: ApiSocketService;

  beforeAll(async () => {
    // read the env:
    env = disableEnvLoggerAndReturnOldEnv();
    // create module
    const module = await Test.createTestingModule({
      imports: [],
      providers: [ ApiSocketService],
    }).compile();
    // save the data from the TestingModule
    service = module.get<ApiSocketService>(ApiSocketService);
  });

  beforeEach(async () => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    // reset the environment
    process.env = env;
    // clear alle Mockups
    jest.restoreAllMocks();
  });

  it('userService should be defined', () => {
    expect(service).toBeDefined();
  });
});
