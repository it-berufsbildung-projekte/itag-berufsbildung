import { Inject, Injectable } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

import { ApiConstants } from '../../../api-constants';
import { ApiSocketService } from '../../services/socketService/api-socket.service';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
@WebSocketGateway({cors: true})
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  server?: Server;

  constructor(
    @Inject( ApiConstants.services.socketService ) protected readonly socketService: ApiSocketService,
  ) {}

  afterInit(server: Server): void {
    StaticLogger.debug(`after init SocketIO Server`, ApiConstants.correlationIds.appGateway);
    this.socketService.socket = server;
  }

  handleConnection(client: Socket): void {
    // create a cookie with the clientId to be able to access the client.id later on a rest call
    StaticLogger.log(`Websocket client with id: ${client.id} connected`, ApiConstants.correlationIds.appGateway);
  }

  handleDisconnect(client: Socket): void {
    StaticLogger.debug(`Client disconnected: ${client.id}`, ApiConstants.correlationIds.appGateway);
  }

}
