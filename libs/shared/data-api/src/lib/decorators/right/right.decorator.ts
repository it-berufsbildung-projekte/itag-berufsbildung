import { SetMetadata } from '@nestjs/common';
import { ApiConstants } from '../../../api-constants';

export const Right = ( allowedRight: string ) => SetMetadata( ApiConstants.decoratorMetadataNames.allowedRight, allowedRight);
