import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApiUserDto } from '@itag-berufsbildung/shared/util';

export const GetWsId = createParamDecorator((data: unknown, ctx: ExecutionContext): ApiUserDto => {
  const request = ctx.switchToHttp().getRequest();
  return request.wsId;
});
