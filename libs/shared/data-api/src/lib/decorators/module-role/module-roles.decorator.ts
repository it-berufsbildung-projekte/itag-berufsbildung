import { SetMetadata } from '@nestjs/common';
import { ApiConstants } from '../../../api-constants';


export interface IModuleAllowedRoles {
  moduleName: string,
  allowedRoles: string[]
}
/**
 * @param moduleAllowedRoles Modulname und Rollen die zugreifen zu dürfen. Leeres Roles Array heisst alle Rollen haben Zugriff.
 */
export const ModuleRoles = ( moduleAllowedRoles: IModuleAllowedRoles ) => SetMetadata( ApiConstants.decoratorMetadataNames.moduleAllowedRoles, moduleAllowedRoles);
