import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { CanActivate, ExecutionContext, ForbiddenException, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ApiConstants } from '../../../api-constants';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
export class RightGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    let msg;
    const request = context.switchToHttp().getRequest();
    if (request?.user) {
      // auslesen und check, ob ein Recht gesetzt ist
      const allowedRight = this.reflector.get<string>(ApiConstants.decoratorMetadataNames.allowedRight, context.getHandler());
      if (!allowedRight) {
        msg = 'right is not set for this method, please contact the developer so he can set a right for this method!';
        throw new InternalServerErrorException(msg);
      }

      // check ob user nicht dieses Recht hat und Fehler ausgeben.
      const userDto: ApiUserDto = request.user;
      if (!userDto.apiRights.includes(allowedRight)) {
        msg = `user with email ${userDto.eMail} has no access to this method!`;
        StaticLogger.warn(msg, request.correlationId);
        throw new ForbiddenException(msg);
      }
      return true;
    }

    msg = `user is not authorized!`;
    throw new UnauthorizedException(msg);
  }
}
