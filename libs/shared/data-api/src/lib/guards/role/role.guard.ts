import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { CanActivate, ExecutionContext, ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ApiConstants } from '../../../api-constants';
import { IModuleAllowedRoles } from '../../decorators';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    let msg;
    const request = context.switchToHttp().getRequest();
    if (request?.user) {
      const userDto: ApiUserDto = request.user;

      // erst auslesen und dann check, ob es gesetzt ist
      const moduleRoles = this.reflector.get<IModuleAllowedRoles>(ApiConstants.decoratorMetadataNames.moduleAllowedRoles, context.getHandler());

      // todo: hier muss ja neu nicht mehr der Modulname abgefragt werde,. sondern das Recht.
      // Wir sind immer nur "innerhalb" eines Modules also braucht es eine Stufe weniger bei der Prüfung/
      // und es MUSS IMMER ein Recht gesetzt werden. Das alte "wenn nicht existiert...." bitte nicht mehr zulassen.
      if (!moduleRoles?.moduleName) {
        return true;
      }

      // todo: nicht rollen sondern rechte Rollen des Modules vom User auslesen
      const roleHashDtoElement = userDto.apiRoles;
      if (roleHashDtoElement) {
        // ist das Rollenarray leer, hat der User immer Zugriff
        if (moduleRoles.allowedRoles.length === 0) {
          return true;
        }

        // jede Rolle iterieren um zu schauen ob User in einer korrekten Rolle und from/until date zum jetztigen Zeitpunkt passen.
        for (const allowedRole of moduleRoles.allowedRoles) {
          const now = Date.now();
          if (!(roleHashDtoElement[allowedRole]?.from < now || roleHashDtoElement[allowedRole]?.until || (roleHashDtoElement[allowedRole]?.until as unknown as number) > now)) {
            continue;
          }
          return true;
        }

        msg = `User ist zum jetzigen Zeitpunkt im Modul ${moduleRoles.moduleName} nicht in einer der Rollen: ${moduleRoles.allowedRoles.toString()}!`;
        throw new ForbiddenException(msg);
      }

      msg = `User ${userDto.eMail} hat keinen Zugriff auf das Modul ${moduleRoles.moduleName}!`;
      throw new ForbiddenException(msg);
    }

    msg = `User ist nicht authorisiert!`;
    throw new UnauthorizedException(msg);
  }
}
