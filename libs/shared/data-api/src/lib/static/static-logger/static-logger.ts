import { TypeCheckers } from '@itag-berufsbildung/shared/util';
import { Logger } from '@nestjs/common';
import { ConsoleLogger } from '@nestjs/common';

export class StaticLogger extends ConsoleLogger {
  constructor() {
    super();
  }

  static debug(message: string, correlationId: number | undefined): void {
    Logger.debug(`${StaticLogger.getCallingMethod()} | ${message}`, this.getCorrelationId(correlationId));
  }

  static endMethod<T>(methodName: string, data: T, correlationId: number | undefined): T {
    if (!data) {
      this.debug(`Ending ${methodName} successfully.`, correlationId);
    } else {
      this.debug(`Ending ${methodName} successfully. Data: \n${TypeCheckers.isString(data) ? data : JSON.stringify(data, null, 2)}`, correlationId);
    }
    return data;
  }
  static error(message: string, correlationId: number | undefined): void {
    Logger.error(`${StaticLogger.getCallingMethod()} | ${message}`, this.getCorrelationId(correlationId));
  }

  static log(message: string, correlationId: number | undefined): void {
    Logger.log(`${StaticLogger.getCallingMethod()} | ${message}`, this.getCorrelationId(correlationId));
  }

  static startMethod(methodName: string, correlationId: number | undefined): void {
    this.debug(`Starting ${methodName}`, correlationId);
  }

  static verbose(message: string, correlationId: number | undefined): void {
    Logger.verbose(`${StaticLogger.getCallingMethod()} | ${message}`, this.getCorrelationId(correlationId));
  }

  static warn(message: string, correlationId: number | undefined): void {
    Logger.warn(`${StaticLogger.getCallingMethod()} | ${message}`, this.getCorrelationId(correlationId));
  }

  // static writeDebugLogStartMethodWithTableName(
  //   tableName: string,
  //   correlationId: number,
  // ): void {
  //   this.debug(
  //     `Starting. (tableName: ${tableName})`,
  //     correlationId
  //   );
  // }

  // static writeDebugLogEntryWithTableName(
  //   message: string,
  //   tableName: string,
  //   correlationId: number,
  // ): void {
  //   this.debug(
  //     `${StaticLogger.getCallingMethod()} | ${message} (tableName: ${tableName})`,
  //     correlationId
  //   );
  // }

  // we assuming you are in the BaseController or BaseService class. Both are derived from this BaseClass
  private static getCallingMethod(): string {
    if (process.env['NODE_ENV'] === 'production') {
      return ''; // we just don't give back lines in production
    }
    let methodName: string;
    // hack: I like to get the current stack, for this reason I throw an error to get an execution stack, with this information I can play :-)
    try {
      // noinspection ExceptionCaughtLocallyJS
      throw new Error('dummy');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (err: any) {
      const stack: string = err.stack;
      const stackArr: string[] = stack.split('\n');
      // we ignore now all method with baseController, baseClass or baseService
      const newStackArr = stackArr.filter((e) => {
        const small = e.toLowerCase();
        return (
          small.indexOf('error: dummy') === -1 && // we ignore our own error message
          small.indexOf('processTicksAndRejections'.toLowerCase()) === -1 &&
          small.indexOf('fulfilled'.toLowerCase()) === -1 &&
          small.indexOf('Generator.next'.toLowerCase()) === -1 &&
          small.indexOf('static-logger'.toLowerCase()) === -1
        );
      });
      // if everything is okay now, the index 0 should now contain the right information
      //    at Function.<anonymous> (C:\\Development\\gitlab.com\\itag-berufsbildung\\dist\\apps\\profile\\api\\webpack:\\itag-berufsbildung\\libs\\shared\\data-api\\src\\lib\\static\\nest-app-creator.ts:17:18)

      // we split into the path
      let separator = '\\';
      if (newStackArr[0].indexOf(separator) === -1) {
        // may in mac or linux forwardslash (/)
        separator = '/';
      }
      const infos = newStackArr[0].split(separator);
      let methodFull = infos[0];
      // remove the at
      methodFull = methodFull.substring(methodFull.indexOf('at ') + 3);
      methodFull = methodFull.substring(0, methodFull.indexOf('(') - 1);
      const name = infos[infos.length - 1].slice(0, -1);
      methodName = `${methodFull} ${name}`;
    }
    return methodName;
  }

  private static getCorrelationId(correlationId: number | undefined): string {
    return (correlationId ? correlationId : 0).toString(10);
  }
}
