import { RedisClientSetting } from '@itag-berufsbildung/shared/util';
import { createClient, RedisClientOptions, RedisDefaultModules, RedisFunctions, RedisModules, RedisScripts } from 'redis';

import { RedisClientType } from '@redis/client';
import { StaticLogger } from '../static-logger/static-logger';

export class RedisClient {
  static createRedisClient(
    options: RedisClientOptions,
    correlationId: number | undefined
  ): Promise<RedisClientType<RedisDefaultModules & RedisModules, RedisFunctions, RedisScripts>> {
    StaticLogger.startMethod('createRedisClient', correlationId);
    return new Promise((resolve, reject) => {
      const client: RedisClientType<RedisDefaultModules & RedisModules, RedisFunctions, RedisScripts> = createClient(options);
      client.on('error', (err) => {
        StaticLogger.error(err, correlationId);
      });
      client
        .connect()
        .then(async () => {
          const id = await client.CLIENT_ID();
          StaticLogger.debug(`Redis client ${id} successfully connected!`, correlationId);
          resolve(client);
        })
        .catch((reason) => {
          StaticLogger.error(reason, correlationId);
          reject(reason);
        });
    });
  }

  static redisOptionsBuilder(redisSettings: RedisClientSetting): RedisClientOptions {
    const redisUrl = `redis://${redisSettings.url}:${redisSettings.port}`;
    return {
      url: redisUrl,
      username: redisSettings.username,
      password: redisSettings.password,
      database: redisSettings.database,
    };
  }
}
