import * as smtpTransport from 'nodemailer-smtp-transport';
import { StaticLogger } from '../static-logger/static-logger';
import { GlobalConstants } from '@itag-berufsbildung/shared/util';
import * as nodeMailer from 'nodemailer';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Mail from 'nodemailer/lib/mailer';


interface IEmailConfig {
  // emailInitActive: boolean;
  // emailHost: string;
  // emailPort: number;
  emailUser: string;
  // emailPassword: string;
  // emailSecure: boolean;
  // emailTlsRejectUnauthorized: boolean;

  emailSettings: {
    host: string;
    port: number;
    secure: boolean;
    auth: {
      pass: string;
      user: string;
    },
    tls: {
      rejectUnauthorized: boolean;
    }
  }
}

export class Email {
  static async sendMail(
    to: string,
    subject: string,
    htmlBody: string,
    correlationId: number,
    cc?: string,
    bcc?: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    attachments?: any[],
  ): Promise<boolean> {
    let transport: Mail;
    const emailConfig = this.getEmailConfig();

    try {
      transport = nodeMailer.createTransport(smtpTransport(emailConfig.emailSettings));
    } catch (err) {
      StaticLogger.error(`email-transport verification was unsuccessfully!`, correlationId);
    }

    // verify the email transport settings
    StaticLogger.log(`successfully connect to mail transport (correlationId: ${correlationId})`, correlationId);

    // setup e-mail data with unicode symbols
    const mailOptions = {
      from: `no-reply <${emailConfig.emailUser}>`, // sender address
      to, // list of receivers
      cc,
      bcc,
      subject, // Subject line
      html: htmlBody, // html body
      attachments,
    };
    try {
      // send mail with defined transport object
      await transport.sendMail(mailOptions);
      StaticLogger.log(`e-mail to ${mailOptions.to} send successfully! (correlationId: ${correlationId})`, correlationId);
      return true;
    } catch (err) {
      StaticLogger.error(`e-mail to ${mailOptions.to} was not send successfully!`, correlationId);
      return false;
    }
  }

  private static getEmailConfig(): IEmailConfig {
    return {
      emailUser: process.env[GlobalConstants.emailEnvironmentKeys.emailUser] || 'emailHost',
      emailSettings: {
        auth: {
          pass: process.env[GlobalConstants.emailEnvironmentKeys.emailPassword] || 'emailPassword',
          user: process.env[GlobalConstants.emailEnvironmentKeys.emailUser] || 'emailUser',
        },
        host: process.env[GlobalConstants.emailEnvironmentKeys.emailHost] || 'emailHost',
        port: parseInt(process.env[GlobalConstants.emailEnvironmentKeys.emailPort] || '465'),
        secure: (process.env[GlobalConstants.emailEnvironmentKeys.emailSecure] === 'true'),
        tls: {
          rejectUnauthorized: (process.env[GlobalConstants.emailEnvironmentKeys.emailTlsRejectUnauthorized] === 'true'),
        }
      }
    };
  }
}
