import { AdminAppConfigurationDto, GlobalConstants } from '@itag-berufsbildung/shared/util';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication, MiddlewareConsumer, RequestMethod } from '@nestjs/common';

import { CorrelationGeneratorMiddleware, JwtCheckerMiddleware, WsIdMiddleware } from '../../midlewares';
import { StaticLogger } from '../static-logger/static-logger';

export class NestAppCreator {
  static async addConfigure(consumer: MiddlewareConsumer, correlationId: number | undefined) {
    StaticLogger.startMethod('addConfigure', correlationId);
    StaticLogger.verbose('start setting up the AppModule', correlationId);
    // add all required middlewares like auth etc.
    consumer.apply(WsIdMiddleware).forRoutes({ method: RequestMethod.ALL, path: '*' });
    consumer.apply(CorrelationGeneratorMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
    consumer.apply(JwtCheckerMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });

    StaticLogger.endMethod('addConfigure', undefined, correlationId);
  }

  static async initApp(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    appModule: any,
    appConfiguration: AdminAppConfigurationDto,
    correlationId: number | undefined
  ): Promise<INestApplication> {
    StaticLogger.startMethod('initApp', correlationId);
    const app = await NestFactory.create(appModule, {
      logger: appConfiguration.logLevels,
    });

    app.enableCors({
      origin: appConfiguration.origin,
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
      credentials: true,
    });

    if (!appConfiguration.isProduction) {
      const doc = new DocumentBuilder()
        .setTitle(appConfiguration.moduleName)
        .setDescription(`The ${appConfiguration.moduleName} API description`)
        .setVersion(appConfiguration.version)
        .setContact(GlobalConstants.contactInfo.name, GlobalConstants.contactInfo.url, GlobalConstants.contactInfo.eMail)
        .setLicense('Licence: MIT', 'https://opensource.org/licenses/MIT')
        .addBearerAuth()
        .setExternalDoc('api doc', `${appConfiguration.apiProtocol}://${appConfiguration.apiHost}:${appConfiguration.apiPort}/${appConfiguration.apiPrefix}-json`)
        .build();
      const document = SwaggerModule.createDocument(app, doc);
      SwaggerModule.setup(appConfiguration.apiPrefix, app, document);
    }
    // return the app
    StaticLogger.endMethod('initApp', undefined, correlationId);
    return app;
  }

  static async startApp(app: INestApplication, appConfiguration: AdminAppConfigurationDto, correlationId: number | undefined) {
    StaticLogger.startMethod('startApp', correlationId);
    try {
      await app.listen(appConfiguration.apiPort, appConfiguration.apiHost);
      StaticLogger.log(
        `🚀 Application is running on: ${appConfiguration.apiProtocol}://${appConfiguration.apiHost}:${appConfiguration.apiPort}/${appConfiguration.apiPrefix}`,
        correlationId
      );
      StaticLogger.endMethod('startApp', undefined, correlationId);
    } catch (err) {
      StaticLogger.error('We stopping the process!', correlationId);
      process.exit(-1);
    }
  }
}
