import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ExtendedRequest } from '../../model';
import { ApiUserService } from '../../services/api-user/api-user.service';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: ApiUserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      // todo: vielleicht brauchen wir den secret oder den Key, aber sicher aus dem environment
      // todo: @Michel, ist dies nicht der apiKey aus dem root env file? oder für was ist dieser gedacht?
      secretOrKey: 'momentanNochLeer',
    });
  }

  override async authenticate(req: ExtendedRequest): Promise<void> {
    const correlationId = req.correlationId;
    StaticLogger.startMethod('authenticate', correlationId);

    // read the bearer
    const token = req.headers['authorization'];
    if (token) {
      const key = token.substring('Bearer '.length);
      const userDto: ApiUserDto = await this.userService.decodeJwtToken(key, correlationId);
      console.debug(`found user ${JSON.stringify(userDto, null, 2)}`);
      req.user = userDto;
      this.success(userDto, null);
      // ? may we should add a return here as well to ensure we're not going to the debug line
      return;
    }
    console.debug(`No auth token`, correlationId);
    this.fail(`No auth token`, 403);
  }

  async validate(payload: ApiUserDto): Promise<ApiUserDto> {
    return payload;
  }
}
