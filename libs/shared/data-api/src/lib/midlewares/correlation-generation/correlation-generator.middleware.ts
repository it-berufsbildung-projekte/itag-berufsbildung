import { ExtendedRequest } from '../../model';
import { StaticLogger } from '../../static/static-logger/static-logger';

export function CorrelationGeneratorMiddleware(
  req: ExtendedRequest,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/ban-types
  next: Function
): void {
  const correlationId = Math.floor(Math.random() * 10000) + 1000;
  req.correlationId = correlationId;
  // console.dir(req);
  StaticLogger.log(`Computer with ip ${req.ip} (WsId: ${req.wsId}) open route ${req.method} ${req.url}`, correlationId);
  next();
}
