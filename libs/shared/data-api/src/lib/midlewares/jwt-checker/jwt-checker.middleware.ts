import { Injectable, InternalServerErrorException, NestMiddleware } from '@nestjs/common';
import { ExtendedRequest } from '../../model';
import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { ApiUserService } from '../../services/api-user/api-user.service';
import { StaticLogger } from '../../static/static-logger/static-logger';

@Injectable()
export class JwtCheckerMiddleware implements NestMiddleware {
  constructor(protected readonly userService: ApiUserService) {}

  // eslint-disable-next-line @typescript-eslint/ban-types
  async use(req: ExtendedRequest, res: Response, next: Function): Promise<void> {
    const correlationId = req.correlationId;
    StaticLogger.startMethod('use', correlationId);

    // read the bearer
    const token = req.headers['authorization'];
    if (token) {
      const key = token.substring('Bearer '.length);
      const userDto: ApiUserDto = await this.userService.decodeJwtToken(key, correlationId);
      if (!userDto) {
        throw new InternalServerErrorException('no user was found for this token¨!');
      }
      StaticLogger.debug(`found user ${JSON.stringify(userDto, null, 2)}`, correlationId);
      req.user = userDto;
    }
    StaticLogger.endMethod('use', undefined, correlationId);
    next();
  }
}
