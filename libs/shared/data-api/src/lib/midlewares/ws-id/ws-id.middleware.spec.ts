
// mockup
import { ExtendedRequest } from '../../model';
import { WsIdMiddleware } from './ws-id.middleware';

const req: ExtendedRequest = jest.genMockFromModule('MyRequest');
const res: Response = jest.genMockFromModule('Response');
const next = () => {
  return undefined;
};

describe('WsIdMiddleware', () => {

  it('ws-id undefined', async () => {
    req.headers['ws-id'] = undefined;
    //
    WsIdMiddleware( req, res, next );
    // check if the req has now the right id
    expect(req.wsId).not.toBeDefined();
  });

  it('ws-id 99', async () => {
    req.headers['ws-id'] = '99';
    //
    WsIdMiddleware( req, res, next );
    // check if the req has now the right id
    expect(req.wsId).toEqual('99');
  });

});
