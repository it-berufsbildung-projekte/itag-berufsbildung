import { ExtendedRequest } from '../../model';

// eslint-disable-next-line @typescript-eslint/ban-types
export function WsIdMiddleware( req: ExtendedRequest, res: Response, next: Function): void {
  req.wsId = req.headers['ws-id'] as string;
  next();
}
