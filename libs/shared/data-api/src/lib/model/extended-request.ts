import { Request } from 'express';
import { ApiUserDto } from '@itag-berufsbildung/shared/util';

export interface ExtendedRequest extends Request {
  correlationId?: number;
  // socketIo: any;
  wsId?: string;
  user?: ApiUserDto;
}
