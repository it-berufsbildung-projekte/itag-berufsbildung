# data-api-shared

gemeinsam genutzte Klassen, Services etc für API's (nestjs)

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test data-api-shared` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint data-api-shared` to execute the lint via [ESLint](https://eslint.org/).
