import { Inject, Injectable } from '@angular/core';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { BehaviorSubject, Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';

export interface WebSocketPayload {
  wsId?: string;
  payload: {
    moduleName?: string;
    documentName?: string;

    data?: { key: string }; // example {eventId: number}
  };

}

@Injectable()
export class WebSocketService {
  // eslint-disable-next-line @typescript-eslint/member-ordering
  newDataArrived$: BehaviorSubject<WebSocketPayload | undefined> = new BehaviorSubject<WebSocketPayload | undefined>(undefined);

  protected wsId?: string;

  // hint: socketClient muss hier definiert werden, damit keine Fehler beim Verbinden auftreten
  private socketClient?: Socket;

  constructor(
    private readonly localStore: LocalStoreService,
    protected readonly notification: NotificationService,
    @Inject(String) private apiUrl: string,
    @Inject(String) private readonly apiUrl$?: Observable<string>,
  ) {
    if ( this.apiUrl$ ) {
      this.apiUrl$.subscribe( (apiUrl) => {
        if ( apiUrl.length > 1 ) {
          this.apiUrl = apiUrl;
          this.initWs( apiUrl );
        }
      });
    } else {
      this.initWs(apiUrl);
    }
  }

  // hint: wir machen hier die Methode so, dass sie auf einer abgeleiteten Klasse, z.B. WebSocketReportService überschrieben werden kann
  protected initWsExtend( socketClient: Socket ): void {
    // do nothing special
    this.notification.showInfo(`connect websocket with id ${socketClient.id}`);
    return;
  }

  private initWs(wsUrl: string): void {
    this.socketClient = io(wsUrl).connect();

    // we only accept a valid connection to continue
    this.socketClient.on('connect', () => {
      if (this.socketClient?.id !== undefined) {
        const wsId: string = this.socketClient.id;
        this.wsId = wsId;
        this.notification.showInfo(`connect websocket with id ${wsId}`);
        // save the wsId
        this.localStore.set(UiConstants.instance.localStore.wsId, wsId);
      }
    });

    this.socketClient.on('disconnect', () => {
      // reset it
      this.notification.showInfo(`Disconnect websocket with id ${this.wsId}`);
      this.wsId = undefined;
    });

    this.socketClient.on('dataChanged', (payload: WebSocketPayload) => {
      this.notification.showInfo(`Data has changed for document ${payload.payload.documentName}`);
      if (this.wsId === payload.wsId) {
        // ignore if we are the sender
        return;
      }
      // now we react on the change of the entity...
      this.newDataArrived$.next(payload);
    });

    // should be the last
    this.initWsExtend(this.socketClient);
  }
}
