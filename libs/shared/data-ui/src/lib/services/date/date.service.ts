import { formatDate } from '@angular/common';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor(
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  format(timestamp: number, format = 'fullDate'): string {
    return formatDate(new Date(timestamp), format, this.locale);
  }

  formatIso(isoDate: string, format = 'fullDate'): string {
    return formatDate(new Date(isoDate), format, this.locale);
  }
}
