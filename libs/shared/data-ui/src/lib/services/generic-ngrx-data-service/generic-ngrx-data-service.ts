import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';
import { IQueryParams } from '@itag-berufsbildung/shared/util';
import { DefaultDataService, HttpUrlGenerator, QueryParams } from '@ngrx/data';
import { Observable, of } from 'rxjs';
import { Update } from '@ngrx/entity';

export class GenericNgrxDataService<T> extends DefaultDataService<T> {

  private _apiUrl!: string;

  constructor(
    @Inject( HttpClient ) http: HttpClient,
    @Inject( HttpUrlGenerator ) httpUrlGenerator: HttpUrlGenerator,
    @Inject( String ) readonly documentName: string,
  ) {
    super( documentName, http, httpUrlGenerator );
  }

  override add( entity: T, queryParams?: IQueryParams ): Observable<T> {
    // add query params
    let paramRoute = '';
    if ( queryParams ) {
      for ( const key of Object.keys( queryParams ) ) {
        if ( paramRoute.length < 1 ) {
          paramRoute = `/?${ key }=${ queryParams[key] }`;
        } else {
          paramRoute += `&${ key }=${ queryParams[key] }`;
        }
      }
    }
    const url = `${ this._apiUrl }${ paramRoute }`;

    // hint: if we have query params we have to do this or else it won't fire off a request to the server.
    this.http.post<T>( url, entity ).subscribe();
    return of( entity );
  }

  /**
   * This method REMOVES the entry from the database. It does not set the property isDeleted of the object to true.
   * The method is called delete because in ngrx data delete means remove completely from database.
   *
   * @param key
   * @param queryParams
   */
  override delete( key: number | string, queryParams?: IQueryParams  ): Observable<number | string> {
    // add query params
    let paramRoute = '';
    if ( queryParams ) {
      for ( const key of Object.keys( queryParams ) ) {
        if ( paramRoute.length < 1 ) {
          paramRoute = `/?${ key }=${ queryParams[key] }`;
        } else {
          paramRoute += `&${ key }=${ queryParams[key] }`;
        }
      }
    }
    const url = `${ this._apiUrl }/${ key }${ paramRoute }`;
    // hint: somehow the request does not reach the api without subscribing
    this.http.delete( url ).subscribe();
    return of( key );
  }

  override getAll(): Observable<T[]> {
    const url = `${ this._apiUrl }?showDeleted=false`;
    return this.http.get<T[]>( url );
  }

  override getWithQuery( queryParams: QueryParams ): Observable<T[]> {
    let url = `${ this._apiUrl }`;

    let isFirstKey = true;
    for ( const key of Object.keys( queryParams ) ) {
      if ( isFirstKey ) {
        url += '?';
        isFirstKey = false;
      } else {
        url += '&';
      }
      url += `${ key }=${ queryParams[key] }`;
    }
    return this.http.get<T[]>( url );
  }

  override getById( key: number | string ): Observable<T> {
    const url = `${ this._apiUrl }/${ key }`;
    return this.http.get<T>( url );
  }

  // todo: funktioniert nicht korrekt. Neue Daten werden nicht korrekt angezeigt.
  override update( update: Update<T>, queryParams?: IQueryParams ): Observable<T> {
    // add query params
    let paramRoute = '';
    if ( queryParams ) {
      for ( const key of Object.keys( queryParams ) ) {
        if ( paramRoute.length < 1 ) {
          paramRoute = `/?${ key }=${ queryParams[key] }`;
        } else {
          paramRoute += `&${ key }=${ queryParams[key] }`;
        }
      }
    }
    const url = `${ this._apiUrl }/${ update.id }${ paramRoute }`;
    this.http.patch<T>( url, update.changes ).subscribe();
    return of( update.changes as T);
  }

  set apiUrl( url: string ) {
    this._apiUrl = url;
  }

}
