import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ScreenSize {
  width: number;
  height: number;
}

@Injectable({
  providedIn: 'root'
})
export class ScreenInfoService {

  screenWidth = 0;
  screenHeight = 0;

  screenResize$: BehaviorSubject<ScreenSize> = new BehaviorSubject<ScreenSize>( { width: 0, height: 0 });

  screenResized(width: number, height: number) {
    this.screenWidth = width;
    this.screenHeight = height;
    this.screenResize$.next( { width, height });
  }}
