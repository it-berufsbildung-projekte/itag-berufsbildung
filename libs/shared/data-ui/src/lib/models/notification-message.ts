import { v4 } from 'uuid';

export type NotificationMessageType = 'error' | 'warning' | 'info' | 'success' | 'debug';

export class NotificationMessage {
  readonly uuid: string;
  readonly untilTimestamp: number = 0;
  readonly created: number = 0;
  constructor(
    public readonly message: string,
    public readonly type: NotificationMessageType
  ) {
    this.uuid = v4();
    if (this.type === 'debug' || this.type === 'info' || this.type === 'success') {
      // set the untilTimestamp
      const now = new Date();
      this.created = +(now);
      this.untilTimestamp = +(now.setSeconds( now.getSeconds() + 20));
    }
  }
}

