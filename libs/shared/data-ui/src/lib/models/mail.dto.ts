
export interface MailDto {
  bcc: string;
  cc: string;
  contentHtml: string;
  receiver: string;
  subject: string;
}
