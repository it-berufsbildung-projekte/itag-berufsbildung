import { Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';
import { AddEvent, CancelEvent, GridDataResult, RemoveEvent, SaveEvent, SelectionEvent } from '@progress/kendo-angular-grid';
import { process, SortDescriptor, State } from '@progress/kendo-data-query';
import { map, Observable, of } from 'rxjs';
import { v4 } from 'uuid';
import { BaseGridEditService } from '../base-grid-edit-service/base-grid-edit.service';

export interface IBaseGridRights {
  canShowDeleted: string;
  canCreate: string;
  canUpdate: string;
  canModifyDelete: string;
  canRemove: string;
}

export interface IGridOptions {
  isInlineEdit: boolean;
  showToolbar: boolean;
  showCommandColumn: boolean;
  sortable: boolean;
  pageable: boolean;
  selectable: boolean;
  filterable: boolean;
  maxHeightPx: number;
  showAddButton: boolean;
  pdfExport: boolean;
  excelExport: boolean;
}

export interface IDisplayedColumns {
  columnName: string;
  columnTitle?: string;
  width: number;
  type: 'string' | 'number' | 'date' | 'checkbox' | 'autocompleteComplex' | 'autocompletePrimitive' | 'dropdownComplex';
  readonly: boolean;
  dropdownComplexData?: Observable<object[]>;
  dropdownPrimitiveData?: string[];
  valueField?: string;
  textField?: string;
}

export class BaseGrid<T extends BaseDocumentDto> {
  editDataItem: T | undefined;
  formGroup!: FormGroup;
  gridData: Observable<GridDataResult> = of({} as GridDataResult);
  gridState: State = {
    skip: 0,
    sort: [],
    take: 10,
    filter: {
      logic: 'and',
      filters: [],
    },
  };

  // hint: is used to define whether the remove event from the grid should remove or delete an entry
  isRemoveRemove = false;
  isRemoveOrDeleteConfirmDialogOpen = false;
  isNew!: boolean;
  removeItem!: T;

  protected editedRowIndex: number | undefined;

  protected constructor(
    protected readonly router: Router,
    protected readonly editService: BaseGridEditService<T>,
    protected readonly formBuilder: FormBuilder,
    @Inject('IDisplayedColumns') readonly displayedColumns: IDisplayedColumns[],
    @Inject('IGridOptionsInterface') readonly gridOptions: IGridOptions,
    @Inject(String) readonly moduleName: string,
    @Inject('IBaseGridRights') readonly rights: IBaseGridRights,
    @Inject('SortDescriptorArr') readonly sort: SortDescriptor[],
    @Inject(Number) readonly take: number,
    @Inject(String) readonly externEditBaseRoute?: string,
    @Inject(String) readonly externCreateRoute?: string
  ) {
    this.gridState.take = take;
    this.gridState.sort = sort;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.gridData = this.editService.pipe(map((data: any) => process(data, this.gridState)));

    this.editService.read();
  }

  addHandler(args?: AddEvent): void {
    if (this.gridOptions.isInlineEdit) {
      this.closeEditor(args?.sender);
      this.formGroup = this.getAddFormGroup();
      args?.sender.addRow(this.formGroup);
    } else {
      if (this.externCreateRoute) {
        this.router.navigateByUrl(this.externCreateRoute).then();
      }
    }
  }

  cancelHandler(args?: CancelEvent): void {
    this.closeEditor(args?.sender, args?.rowIndex);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = {} as FormGroup;
  }

  closeRemoveDialog(isRemoveOrDeleteConfirmed: boolean): void {
    if (isRemoveOrDeleteConfirmed) {
      this.editService.remove(this.removeItem, this.isRemoveRemove);
    }
    this.isRemoveOrDeleteConfirmDialogOpen = false;
  }

  editHandler(args: AddEvent): void {
    if (this.gridOptions.isInlineEdit) {
      const { dataItem } = args;
      this.closeEditor(args.sender);
      this.formGroup = this.getEditFormGroup(dataItem);
      this.editedRowIndex = args.rowIndex;
      args.sender.editRow(args.rowIndex, this.formGroup);
    } else {
      if (this.externEditBaseRoute) {
        this.router.navigateByUrl(`${this.externEditBaseRoute}/${args.dataItem.uuid}`).then();
      }
    }
  }

  onStateChange(state: State): void {
    this.gridState = state;
    this.editService.read();
  }

  removeHandler({ dataItem }: RemoveEvent): void {
    if (!this.isRemoveOrDeleteConfirmDialogOpen) {
      this.removeItem = dataItem;
      this.isRemoveOrDeleteConfirmDialogOpen = true;
    }
  }

  saveHandler({ sender, rowIndex, formGroup, isNew }: SaveEvent): void {
    const saveItem = formGroup.value;
    this.editService.save(saveItem, isNew);
    sender.closeRow(rowIndex);
  }

  showOrHideIsDeletedEntries($event: boolean): void {
    this.editService.loadEntriesWithQuery({ showDeleted: String($event) });
  }

  protected getAddFormGroup(): FormGroup {
    const form = this.formBuilder.group({});

    for (const column of this.displayedColumns) {
      form.addControl(column.columnName, new FormControl('', [Validators.required]));

      // hint: if readonly column, then remove the validator
      if (column.readonly) {
        form.get(column.columnName)?.removeValidators([Validators.required]);
      }
    }

    // create uuid for new entry
    form.addControl('uuid', new FormControl(v4()));

    return form;
  }

  protected getEditFormGroup(data: T): FormGroup {
    const form = this.formBuilder.group({});

    for (const column of this.displayedColumns) {
      const columnName = column.columnName;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      let value = (data as any)[columnName];
      if (column.type === 'date') {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        value = new Date((data as any)[columnName]);
      }
      form.addControl(columnName, new FormControl(value, [Validators.required]));

      // hint: if readonly column, then remove the validator
      if (column.readonly) {
        form.get(column.columnName)?.removeValidators([Validators.required]);
      }
    }

    // set uuid in FormGroup, because it acts as a DTO
    form.addControl('uuid', new FormControl(data.uuid));

    return form;
  }

  getAutocompleteTextValue(columnName: string, value: string, valueFieldName: string | undefined, textFieldName: string | undefined): string {
    // find the column with the selectionItems
    const column = this.displayedColumns.find((c) => c.columnName === columnName);
    let selectionItem;

    // find the selectionItem with the value
    column?.dropdownComplexData?.subscribe({
      next: (columns) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        selectionItem = columns.find((v) => (v as any)[valueFieldName as string] === value);
      },
    });

    // return the value of the property wich has the name in textFieldName
    return selectionItem ? selectionItem[textFieldName as string] : '';
  }

  setAutocompleteValueToFormControl(columnName: string, textValue: string, valueFieldName: string | undefined, textFieldName: string | undefined): void {
    // find the column with the selectionItems
    const column = this.displayedColumns.find((c) => c.columnName === columnName);

    column?.dropdownComplexData?.subscribe({
      next: (value) => {
        // find the selectionItem with the textValue
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const selectionItem = value.find((v) => (v as any)[textFieldName as string] === textValue);

        // get the formControl from the formGroup by the columnName and set the value from the valueField to the control
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const formControl = this.formGroup.get(columnName);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        formControl?.setValue((selectionItem as any)[valueFieldName as string]);

        // mark the formControl as touched if a selectionItem was found which makes the formGroup valid (save is enabled)
        // if no selectionItem was found mark the formControl as untouched to disable the save button
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        !(selectionItem as any)[valueFieldName as string] ? formControl?.markAsTouched() : formControl?.markAsUntouched();
      },
    });
  }

  // hint: needs to be public instead of protected, because html template can't resolve protected methods. You may have to override it for special implementation.
  selectionChange(selection: SelectionEvent): void {
    if (selection.selectedRows && this.externEditBaseRoute) {
      this.router.navigateByUrl(`${this.externEditBaseRoute}/${selection.selectedRows[0].dataItem.uuid}`).then();
    }
  }

  protected removeDisplayedColumn(columnName: string): void {
    const index = this.displayedColumns.findIndex((column) => column.columnName === columnName);
    if (index > -1) {
      this.displayedColumns.splice(index, 1);
    }
  }
}
