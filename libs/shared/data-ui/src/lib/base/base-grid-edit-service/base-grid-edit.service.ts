import { Inject } from '@angular/core';
import { EntityCollectionServiceBase, QueryParams } from '@ngrx/data';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

export abstract class BaseGridEditService<T> extends BehaviorSubject<T> {
  entries$: Observable<T[]> = of([]);
  protected data: T[] = [];

  protected constructor(@Inject(EntityCollectionServiceBase) private entityService: EntityCollectionServiceBase<T>) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    super([] as any);
  }

  read(): void {
    if (this.data.length) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return super.next(this.data as any);
    }

    this.fetch()
      .pipe(
        tap((data) => {
          this.data = data;
        })
      )
      .subscribe((data) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        super.next(data as any);
      });
  }

  save(data: T, isNew?: boolean): void {
    const action = isNew ? CREATE_ACTION : UPDATE_ACTION;

    this.reset();

    this.fetch(action, data).subscribe(
      () => this.read(),
      () => this.read()
    );
  }

  remove(data: T, isRemoveRemove?: boolean): void {
    this.reset();

    this.fetch(REMOVE_ACTION, data, isRemoveRemove).subscribe(
      () => this.read(),
      () => this.read()
    );
  }

  // resetItem(dataItem: T): void {
  //   if (!dataItem) {
  //     return;
  //   }
  //
  //   // find orignal data item
  //   const originalDataItem = this.find(dataItem);
  //
  //   // revert changes
  //   Object.assign(originalDataItem, dataItem);
  //
  //   // eslint-disable-next-line @typescript-eslint/no-explicit-any
  //   super.next(this.data as any);
  // }

  private reset() {
    this.data = [];
  }

  private fetch(action = '', data?: T, isRemoveRemove?: boolean): Observable<T[]> {
    if (data) {
      switch (action) {
        case CREATE_ACTION:
          this.create(data);
          break;
        case UPDATE_ACTION:
          this.update(data);
          break;
        case REMOVE_ACTION:
          this.delete(data, isRemoveRemove as boolean);
          break;
      }
    }

    return this.entries$;
  }

  protected create(data: T): void {
    this.entityService.add(data);
  }

  protected update(data: T): void {
    this.entityService.update(data);
  }

  protected delete(data: T, isRemoveRemove: boolean): void {
    if (isRemoveRemove) {
      // hint: for NGRX data delete means that an entry gets removed from the database
      this.entityService.delete(data);
    } else {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const uuid = (data as any).uuid;
      this.entityService.update({ uuid, isDeleted: true } as unknown as Partial<T>);

      // remove entity from cache (store) so it disappears in the grid
      this.entityService.removeOneFromCache(data);
    }
  }

  protected find(data: T): T {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const element = this.data.find((e) => (e as any).uuid === (data as any).uuid);
    return element as T;
  }

  loadEntriesWithQuery(queryParams: QueryParams): void {
    this.entityService.clearCache();
    this.entityService.getWithQuery(queryParams);
  }
}
