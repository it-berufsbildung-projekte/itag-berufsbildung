import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';
import { EntityCollectionServiceBase } from '@ngrx/data';
import { v4 } from 'uuid';

export interface IBaseFormRights {
  canCreate: string;
  canUpdate: string;
  canDelete: string;
  canRemove: string;
}

export interface IDisplayedInputs {
  inputName: string;
  type: 'string' | 'number' | 'date' | 'checkbox';
  readonly: boolean;
}

export abstract class BaseForm<T extends BaseDocumentDto> {
  editForm!: FormGroup;
  protected dto!: T;
  isNew = false;

  isRemoveRemove = false;
  isRemoveOrDeleteConfirmDialogOpen = false;
  setIsDeleted = false;

  protected constructor(
    protected readonly route: ActivatedRoute,
    protected readonly router: Router,
    readonly rights: IBaseFormRights,
    protected readonly formBuilder: FormBuilder,
    readonly displayedInputs: IDisplayedInputs[],
    private readonly entityCollectionService: EntityCollectionServiceBase<T>,
    private readonly cancelRoute: string,
    readonly moduleName: string
  ) {
    this.route.params.subscribe((p) => {
      if (p['uuid']) {
        this.entityCollectionService.entities$.subscribe({
          next: (value) => {
            this.dto = value.find((e) => e.uuid === p['uuid']) as T;
          },
        });
      } else {
        this.isNew = true;
      }
      this.editForm = this.getEditForm();
    });
  }

  onSave(): void {
    const formRecord: Record<string, unknown> = {};
    for (const control of this.displayedInputs) {
      formRecord[control.inputName] = this.editForm.controls[control.inputName].value;
    }
    if (this.isNew) {
      formRecord['uuid'] = v4();
      this.entityCollectionService.add(formRecord as T);
    } else {
      formRecord['uuid'] = this.dto.uuid;
      this.entityCollectionService.update(formRecord as T);
    }

    this.onCancel();
  }

  onCancel(): void {
    this.router.navigateByUrl(this.cancelRoute).then();
  }

  openRemoveDialog(setIsDeleted: boolean): void {
    if (!this.isRemoveOrDeleteConfirmDialogOpen) {
      this.setIsDeleted = setIsDeleted;
      this.isRemoveOrDeleteConfirmDialogOpen = true;
    }
  }

  closeRemoveDialog(isRemoveOrDeleteConfirmed: boolean): void {
    if (isRemoveOrDeleteConfirmed) {
      if (this.setIsDeleted) {
        const formRecord: Record<string, unknown> = {};
        for (const control of this.displayedInputs) {
          formRecord[control.inputName] = this.editForm.controls[control.inputName].value;
        }
        formRecord['uuid'] = this.dto.uuid;
        formRecord['isDeleted'] = true;
      } else {
        this.entityCollectionService.delete(this.dto);
      }
    }
    this.isRemoveOrDeleteConfirmDialogOpen = false;
  }

  protected getEditForm(): FormGroup {
    const form = this.formBuilder.group({});
    // hint: The problem is we can't access this.dto[key]

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const record: Record<string, any> = this.dto as Record<string, any>;
    for (const column of this.displayedInputs) {
      if (this.isNew) {
        form.addControl(column.inputName, new FormControl('', [Validators.required]));
      } else {
        if (column.type === 'date') {
          form.addControl(column.inputName, new FormControl(new Date(record[column.inputName]), [Validators.required]));
        } else {
          form.addControl(column.inputName, new FormControl(record[column.inputName], [Validators.required]));
        }
      }
      // remove the validators if it's readonly
      if (column.readonly) {
        form.get(column.inputName)?.removeValidators(Validators.required);
      }
    }
    return form;
  }
}
