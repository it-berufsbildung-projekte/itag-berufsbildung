import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '@progress/kendo-angular-buttons';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
  ],
})
export class DataUiSharedModule {}
