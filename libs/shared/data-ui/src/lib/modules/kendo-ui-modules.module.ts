import { NgModule } from '@angular/core';
import { ToolBarModule } from '@progress/kendo-angular-toolbar';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { ContextMenuModule, MenusModule } from '@progress/kendo-angular-menu';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { NavigationModule } from '@progress/kendo-angular-navigation';
import { IconsModule } from '@progress/kendo-angular-icons';
import { PagerModule } from '@progress/kendo-angular-pager';
import { ExcelModule, GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { ScrollViewModule } from '@progress/kendo-angular-scrollview';
import { TooltipsModule } from '@progress/kendo-angular-tooltip';
import { EditorModule } from '@progress/kendo-angular-editor';
import { GanttModule } from '@progress/kendo-angular-gantt';
import { ListViewModule } from '@progress/kendo-angular-listview';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ProgressBarModule } from '@progress/kendo-angular-progressbar';
import { RippleModule } from '@progress/kendo-angular-ripple';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';
import { BarcodesModule } from '@progress/kendo-angular-barcodes';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { ChatModule } from '@progress/kendo-angular-conversational-ui';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export';
import { GaugesModule } from '@progress/kendo-angular-gauges';
import { IndicatorsModule } from '@progress/kendo-angular-indicators';
import { FloatingLabelModule, LabelModule } from '@progress/kendo-angular-label';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { UploadsModule } from '@progress/kendo-angular-upload';

const modules = [
  BarcodesModule,
  ButtonsModule,
  ChartsModule,
  ChatModule,
  DateInputsModule,
  DialogsModule,
  DropDownsModule,
  EditorModule,
  ExcelExportModule,
  GanttModule,
  GaugesModule,
  GridModule,
  IconsModule,
  IndicatorsModule,
  InputsModule,
  LabelModule,
  FloatingLabelModule,
  LayoutModule,
  ListViewModule,
  MenusModule,
  ContextMenuModule,
  NavigationModule,
  PagerModule,
  PDFExportModule,
  PopupModule,
  ProgressBarModule,
  RippleModule,
  SchedulerModule,
  ScrollViewModule,
  ToolBarModule,
  TooltipsModule,
  TreeViewModule,
  TreeListModule,
  UploadsModule,
  PDFModule,
  ExcelModule,
];

@NgModule({
  declarations: [],
  imports: [...modules],
  exports: [...modules],
})
export class KendoUiModules {}
