import { Pipe, PipeTransform } from '@angular/core';
import { UserNameTool } from '@itag-berufsbildung/shared/util';

@Pipe({
  standalone: true,
  name: 'fullName',
})
export class FullNameFromEmailPipe implements PipeTransform {
  transform(eMail: string | null): string {
    if ( !eMail ) {
      return '';
    }
    return UserNameTool.getFullName( eMail );
  }
}
