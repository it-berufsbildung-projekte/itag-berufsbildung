import { Pipe, PipeTransform } from '@angular/core';
import { UserNameTool } from '@itag-berufsbildung/shared/util';

@Pipe({
  standalone: true,
  name: 'firstName',
})
export class FirstNameFromEmailPipe implements PipeTransform {
  transform(eMail: string): string {
    return UserNameTool.getFirstName( eMail );
  }
}
