export class UiConstants {
  // singleton
  private static _instance: UiConstants;
  static get instance(): UiConstants {
    return this._instance || (this._instance = new this());
  }

  readonly customWebSocketEvents = {
    onReportDiscussionShowAnswer: 'onReportDiscussionShowAnswer',
    onReportDiscussionStepChange: 'onReportDiscussionStepChange',
  };

  readonly localStore = {
    userToken: 'userToken',
    wsId: 'wsId',
  };

  readonly services = {
    config: 'CONFIG_SERVICE',
    user: 'USER_SERVICE',
    websocket: 'WEB_SOCKET_SERVICE',
  };

  readonly routing = {
    base: 'auth',
    sub: {
      error: 'error',
      signOut: 'sign-out',
    },
  };
}
