import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStoreService {

  private localStore: Storage;

  constructor() {
    this.localStore = window.localStorage;
  }

  get( key: string, defaultValue: string | null = null): string | null {
    const text = this.localStore.getItem(key);
    return text ? text : defaultValue;
  }
  set( key: string, value: string): void {
    this.localStore.setItem(key, value);
  }
  delete( key: string): void {
    this.localStore.removeItem(key);
  }
  clear(): void {
    this.localStore.clear();
  }

  getNumber( key: string, defaultValue: number | undefined = undefined): number | undefined {
    const value = this.get(key, defaultValue ? defaultValue.toString(10) : undefined) as never;
    if ( isNaN(value) ) {
      return 0;
    }
    return parseInt(value, 10);
  }
  setNumber( key: string, value: number): void {
    return this.set(key, value.toString(10));
  }

  getBoolean( key: string, defaultValue: boolean = false) {
    const def = defaultValue ? 'true' : 'false';
    const value = this.get(key, def) as never;
    return !!(parseInt(value) || value === "true");
  }
  setBoolean (key: string, value: boolean): void {
    this.localStore.setItem(key, value ? 'true' : 'false' );
  }

  getObject<T>(key: string, defaultValue: T): T {
    const txt = this.get(key, undefined);
    if (txt) {
      const obj = JSON.parse(txt);
      return obj as T;
    }
    return defaultValue;
  }
  setObject<T>( key: string, obj: T ): void {
    const value = JSON.stringify(obj);
    this.set(key, value);
  }
}
