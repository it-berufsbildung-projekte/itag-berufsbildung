import { Injectable } from '@angular/core';
import { FilterStore } from '../store/filter.store';

@Injectable()
export class PlanningFilterService {
  activeCategories$ = this.filterStore.activeCategories$;
  activeOwners$ = this.filterStore.activeOwners$;
  activeProjectStates$ = this.filterStore.activeProjectStates$;
  activeProjects$ = this.filterStore.activeProjects$;
  owners$ = this.filterStore.owners$;

  constructor( private readonly filterStore: FilterStore ) {}

  reloadProjects(): void {
    this.filterStore.reloadProjects();
  }

  setProjectOwner( ownerEmail: string ): void {
    this.filterStore.projectOwner = ownerEmail;
  }

}
