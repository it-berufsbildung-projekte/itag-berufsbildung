import { Injectable } from '@angular/core';
import { AnnualPlanningEventDto } from '@itag-berufsbildung/planning/data';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { parseDate } from '@progress/kendo-angular-intl';
import { BaseEditService } from '@progress/kendo-angular-scheduler';
import { map, Observable, tap, zip } from 'rxjs';
import { v4 } from 'uuid';
import { AnnualPlanningEventStore } from '../store/annual-planning-event.store';
import { PlanningFilterService } from './planning-filter.service';

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

const fields = {
  id: 'uuid',
  title: 'title',
  description: 'description',
  start: 'start',
  end: 'end',
  isAllDay: 'isAllDay',
  recurrenceRule: 'recurrenceRule',
  recurrenceId: 'recurrenceId',
  recurrenceExceptions: 'recurrenceException',
};

@Injectable()
export class AnnualPlanningSchedulerEditService extends BaseEditService<AnnualPlanningEventDto> {
  private eventStore!: AnnualPlanningEventStore;
  private categoryFilterArr: string[] = [];
  private ownerFilterArr: string[] = [];
  loading = false;

  constructor(private readonly ownerCategoryFilter: PlanningFilterService, private readonly notificationService: NotificationService) {
    super(fields);

    this.ownerCategoryFilter.activeOwners$.subscribe({
      next: (activeOwners) => {
        this.ownerFilterArr = activeOwners;
        if (this.eventStore) {
          this.read();
        }
      },
    });

    this.ownerCategoryFilter.activeCategories$.subscribe({
      next: (activeCategories) => {
        this.categoryFilterArr = activeCategories;
        if (this.eventStore) {
          this.read();
        }
      },
    });
  }

  /**
   * Method creates events for each day between the start and end date for multiple users selected in the create dialog. It also checks if days are occupied by events
   * with a bigger weight and skips them. That means, the next recurrence is placed on the next possible day.
   * Saturdays and Sundays are always excluded.
   *
   * @param newEvent - Events with required properties to create recurrences (if start and end date are not equal)
   * @param userEmailArr - Array of e-mail addresses from these users to whom events are created
   */
  createEventsForUsers(newEvent: AnnualPlanningEventDto, userEmailArr: string[]): void {
    let eventsOfOwner: AnnualPlanningEventDto[] = [];
    for (const eMail of userEmailArr) {
      // filter events that belong to the eMail
      this.eventStore.annualPlanningEvents$
        .subscribe(
          (events) =>
            (eventsOfOwner = events.filter((e) => {
              const ownerCategoryKeySplit = this.splitOwnerCategoryKey(e.ownerCategoryKey);
              return eMail === ownerCategoryKeySplit[0];
            }))
        )
        .unsubscribe();

      // calculate difference between start and end in days (sa/su are excluded)
      const differenceInDays = (newEvent.end.getTime() - newEvent.start.getTime()) / (1000 * 3600 * 24);
      const date = new Date(newEvent.start);

      for (let i = 0; i <= differenceInDays; i++) {
        // ignore saturday and sunday
        if ( !['Ferien/Abwesenheit', 'Feiertage', 'Betrieb'].includes(newEvent.eventCategory.name) && (date.getDay() === 0 || date.getDay() === 6) ) {
          date.setDate(date.getDate() + 1);

          // if it is the first day, notify the user that it is impossible to create an event on the weekend.
          if (i === 0) {
            this.notificationService.showInfo(`Ereignisse können nicht am Wochenende eingeplant werden!`);
          }
          continue;
        }

        // check if day is not occupied by an event with a more important category and create a recurrence if possible
        if (!this.isDayOccupied(eventsOfOwner, newEvent, date)) {
          const newEventCopy = ObjectTools.cloneDeep(newEvent);
          newEventCopy.start = new Date(date);
          newEventCopy.end = new Date(date);
          newEventCopy.ownerCategoryKey = `${eMail}|${newEvent.eventCategoryUuid}`;
          this.save([newEventCopy], [], []);
        } else {
          // notify the user if event can't be created due to another more important event on the first day (start date)
          if (i === 0) {
            this.notificationService.showInfo(`Ereignis kann am ersten Tag nicht eingeplant werden, da der Tag mit einem höher gewichteten Ereignis besetzt ist!`);
          }
          i--;
        }

        date.setDate(date.getDate() + 1);
      }
    }
  }

  read(): void {
    this.fetch().subscribe((data) => {
      this.data = data.map((item) => this.readEvent(item));
      this.source.next(this.data);
    });
  }

  save(created: AnnualPlanningEventDto[], updated: AnnualPlanningEventDto[], deleted: AnnualPlanningEventDto[]): void {
    const completed = [];
    if (deleted.length) {
      completed.push(this.fetch(REMOVE_ACTION, deleted));
    }

    if (updated.length) {
      completed.push(this.fetch(UPDATE_ACTION, updated));
    }

    if (created.length) {
      completed.push(this.fetch(CREATE_ACTION, created));
    }

    zip(...completed).subscribe(() => this.read());
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected fetch(action = '', data?: AnnualPlanningEventDto[]): Observable<any[]> {
    this.loading = true;
    data = data as AnnualPlanningEventDto[];

    if (action === CREATE_ACTION) {
      // generate uuid and add it to the new event before it gets saved
      const newEvent = { ...data[0], uuid: v4() };
      this.eventStore.addEvent(newEvent);
    } else if (action === UPDATE_ACTION) {
      this.eventStore.updateEvent(data[0]);
    } else if (action === REMOVE_ACTION) {
      this.eventStore.deleteEvent(data[0]);
    }

    return this.eventStore.annualPlanningEvents$.pipe(
      map((events) =>
        events.filter((event) => {
          // array with owner eMail and category uuid
          const ownerCategoryKeySplit = this.splitOwnerCategoryKey(event.ownerCategoryKey);
          return this.ownerFilterArr.includes(ownerCategoryKeySplit[0]) && this.categoryFilterArr.includes(ownerCategoryKeySplit[1]);
        })
      ),
      tap(() => (this.loading = false))
    );
  }
// .map( (event) => ( {...event, start: new Date(event.start), end: new Date(event.end) } ) )

  set annualPlanningEventStore(annualPlanningEventStore: AnnualPlanningEventStore) {
    this.eventStore = annualPlanningEventStore;
  }

  private isDayOccupied(eventsOfOwner: AnnualPlanningEventDto[], newEvent: AnnualPlanningEventDto, date: Date): boolean {
    const eventsOfDay = eventsOfOwner.filter((e) => {
      const start = new Date(e.start);
      return start.getDate() === date.getDate() && start.getMonth() === date.getMonth() && start.getFullYear() === date.getFullYear();
    }).map((e) => ({...e, start: new Date(e.start)}));

    // if no events were found or if the weight of the category is -1, then the slot is free for the new event
    if (eventsOfDay.length < 1 || newEvent.eventCategory.weight === -1) {
      return false;
    }

    for (const event of eventsOfDay) {
      if (
        event.start.getDate() === date.getDate() &&
        event.start.getMonth() === date.getMonth() &&
        event.start.getFullYear() === date.getFullYear() &&
        event.eventCategory.weight > newEvent.eventCategory.weight
      ) {
        return true;
      }
    }

    return false;
  }

  private splitOwnerCategoryKey(ownerCategoryKey: string): string[] {
    return ownerCategoryKey.split('|');
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private readEvent(item: any): any {
    return {
      ...item,
      start: parseDate(item.start),
      end: parseDate(item.end),
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      recurrenceException: typeof item.recurrenceException === 'string' ? this.parseExceptions(item.recurrenceException) : item.recurrenceException.map( (e: any) => new Date(e)),
    };
  }
}
