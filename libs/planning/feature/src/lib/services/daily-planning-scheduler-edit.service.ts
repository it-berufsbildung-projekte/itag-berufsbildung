import { Injectable } from '@angular/core';
import { DailyPlanningEventDto } from '@itag-berufsbildung/planning/data';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { parseDate } from '@progress/kendo-angular-intl';
import { BaseEditService } from '@progress/kendo-angular-scheduler';
import { Observable, tap, zip } from 'rxjs';
import { v4 } from 'uuid';
import { DailyPlanningEventStore } from '../store/daily-planning-event.store';

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

const fields = {
  id: 'uuid',
  title: 'title',
  description: 'description',
  start: 'start',
  end: 'end',
  isAllDay: 'isAllDay',
  isTaskFinished: 'isTaskFinished',
  ownerEmail: 'ownerEmail',
  task: 'task',
  taskConclusion: 'taskConclusion',
  fachkompetenz: 'fachkompetenz',
  fachkompetenzBegruendung: 'fachkompetenzBegruendung',
  methodenkompetenz: 'methodenkompetenz',
  methodenkompetenzBegruendung: 'methodenkompetenzBegruendung',
  selbstkompetenz: 'selbstkompetenz',
  selbstkompetenzBegruendung: 'selbstkompetenzBegruendung',
  sozialkompetenz: 'sozialkompetenz',
  sozialkompetenzBegruendung: 'sozialkompetenzBegruendung',
  usedTime: 'usedTime',
};

@Injectable()
export class DailyPlanningSchedulerEditService extends BaseEditService<DailyPlanningEventDto> {
  private store!: DailyPlanningEventStore;
  loading = false;

  constructor(
    private readonly notificationService: NotificationService,
    // private readonly filterService: PlanningFilterService,
  ) {
    super(fields);
  }

  protected fetch(action = '', data?: DailyPlanningEventDto[]): Observable<DailyPlanningEventDto[]> {
    this.loading = true;
    data = data as DailyPlanningEventDto[];

    if (action === CREATE_ACTION) {
      // generate uuid and add it to the new event before it gets saved
      const newEvent = { ...data[0], uuid: v4() };
      this.store.add(newEvent);
    } else if (action === UPDATE_ACTION) {
      this.store.update(data[0]);
    } else if (action === REMOVE_ACTION) {
      this.store.delete(data[0]);
    }

    return this.store.dailyPlanningEvents$.pipe(tap(() => (this.loading = false)));
  }

  read(): void {
    this.fetch().subscribe((data) => {
      setTimeout( () =>  {
        this.data = data.map((item) => this.readEvent(item));
        this.source.next(this.data);
      }, 500);
    });
  }

  save(created: DailyPlanningEventDto[], updated: DailyPlanningEventDto[], deleted: DailyPlanningEventDto[]): void {
    const completed = [];
    if (deleted.length) {
      completed.push(this.fetch(REMOVE_ACTION, deleted));
    }

    if (updated.length) {
      completed.push(this.fetch(UPDATE_ACTION, updated));
    }

    if (created.length) {
      completed.push(this.fetch(CREATE_ACTION, created));
    }

    zip(...completed).subscribe(() => this.read());
  }

  set dailyPlanningEventStore(dailyPlanningEventStore: DailyPlanningEventStore) {
    this.store = dailyPlanningEventStore;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private readEvent(item: any): any {
    return {
      ...item,
      start: parseDate(item.start),
      end: parseDate(item.end),
      recurrenceException: typeof item.recurrenceException === 'string' ? this.parseExceptions(item.recurrenceException) : item.recurrenceException,
    };
  }
}
