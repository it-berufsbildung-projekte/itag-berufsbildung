import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { KendoUiModules, WebSocketService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { map } from 'rxjs';

import { AnnualPlanningPlanningViewComponent } from './components/annual-planning-planning-view/annual-planning-planning-view.component';

import { FeaturePlanningRouterModule } from './modules/feature-planning-router.module';
import { AnyPipe } from './pipes/any.pipe';
import { AnnualPlanningSchedulerEditService } from './services/annual-planning-scheduler-edit.service';
import { FilterComponent } from './components/filter/filter.component';
import { DailyPlanningSchedulerEditService } from './services/daily-planning-scheduler-edit.service';
import { PlanningFilterService } from './services/planning-filter.service';
import { FilterStore } from './store/filter.store';
import { ProjectTaskListComponent } from './components/project-task-list/project-task-list.component';
import { AnnualPlanningOverviewComponent } from './components/annual-planning-overview/annual-planning-overview.component';
import { AnnualPlanningComponent } from './components/annual-planning/annual-planning.component';
import { CustomSchedulerToolbarComponent } from './components/custom-scheduler-toolbar/custom-scheduler-toolbar.component';
import { MonthRangeSelectorComponent } from './components/month-range-selector/month-range-selector.component';
import { DailyPlanningComponent } from './components/daily-planning/daily-planning.component';
import { DailyPlanningPlanningViewComponent } from './components/daily-planning-planning-view/daily-planning-planning-view.component';
import { DailyPlanningLogComponent } from './components/daily-planning-log/daily-planning-log.component';
import { DailyPlanningTaskConclusionDialogComponent } from './components/daily-planning-task-conclusion-dialog/daily-planning-task-conclusion-dialog.component';
import { DailyPlanningTaskCompetenceFormComponent } from './components/daily-planning-task-competence-form/daily-planning-task-competence-form.component';
import { ProjectTaskStore } from './store/project-task-store.service';

const websocketServiceFactory = ( ls: LocalStoreService, ans: NotificationService, user: UserService ) => {
  const apiUrl$ = user.userDto$.pipe(
    map( (user) => {
      if ( user ) {
        const moduleInfo = user.moduleInfoArr.find( ( m ) => m.moduleName === ModuleConstants.instance.moduleName );
        return moduleInfo?.apiUrl as string;
      }
      return '';
    })
  );

  return new WebSocketService( ls, ans, '', apiUrl$ );
};

@NgModule( {
  imports: [
    CommonModule,
    FeaturePlanningRouterModule,
    KendoUiModules,
  ],
  declarations: [
    AnnualPlanningPlanningViewComponent,
    AnyPipe,
    FilterComponent,
    ProjectTaskListComponent,
    AnnualPlanningOverviewComponent,
    AnnualPlanningComponent,
    CustomSchedulerToolbarComponent,
    MonthRangeSelectorComponent,
    DailyPlanningComponent,
    DailyPlanningPlanningViewComponent,
    DailyPlanningLogComponent,
    DailyPlanningTaskConclusionDialogComponent,
    DailyPlanningTaskCompetenceFormComponent,
  ],
  providers: [
    AnnualPlanningSchedulerEditService,
    DailyPlanningSchedulerEditService,
    {
      deps: [ HttpClient, UserService, LocalStoreService ],
      provide: FilterStore,
      useFactory: ( http: HttpClient, user: UserService, localStore: LocalStoreService, websocket: WebSocketService ) => () => new FilterStore( http, user, localStore, websocket ),
    },
    {
      deps: [ HttpClient, UserService, NotificationService ],
      provide: ProjectTaskStore,
      useFactory: ( http: HttpClient, user: UserService, notificationService: NotificationService ) => () => new ProjectTaskStore( http, user, notificationService ),
    },
    FilterStore,
    ProjectTaskStore,
    PlanningFilterService,
    WebSocketService,
    {
      deps: [LocalStoreService, NotificationService, UserService],
      provide: WebSocketService,
      useFactory: websocketServiceFactory,
    },
  ],
} )
export class FeaturePlanningModule {
}
