import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { AnnualPlanningComponent } from '../components/annual-planning/annual-planning.component';
import { DailyPlanningComponent } from '../components/daily-planning/daily-planning.component';

const routes: Routes = [
  {
    path: ModuleConstants.instance.routing.sub.annualPlanning,
    component: AnnualPlanningComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
    },
  },
  {
    path: ModuleConstants.instance.routing.sub.dailyPlanning,
    component: DailyPlanningComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,
    },
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeaturePlanningRouterModule {}
