import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toAny',
})
export class AnyPipe implements PipeTransform {
  /**
   * Returns an any[] array. If value is null, it returns an empty any[] array.
   * @param value
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  transform(value: any): any[] {
    if (!value) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return [] as any[];
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return value.map((a: any) => a as any);
  }
}
