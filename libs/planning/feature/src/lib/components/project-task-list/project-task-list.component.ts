import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModuleConstants, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { map, Observable, of } from 'rxjs';
import { v4 } from 'uuid';
import { PlanningFilterService } from '../../services/planning-filter.service';
import { ProjectTaskStore } from '../../store/project-task-store.service';

@Component( {
  selector: 'itag-berufsbildung-project-task-list',
  templateUrl: './project-task-list.component.html',
  styleUrls: [ './project-task-list.component.scss' ],
  encapsulation: ViewEncapsulation.Emulated,
} )
export class ProjectTaskListComponent implements OnInit {
  moduleConstants = ModuleConstants.instance;
  projects$ = this.moduleProjectStore.projects$;
  projectsForAnnualPlanning$: Observable<ProjectTaskDto[]> = of([]);
  projectsAndTasks$!: Observable<ProjectTaskDto[]>;
  user: UiUserDto = new UiUserDto();
  owners$ = this.filterService.owners$;
  expandedNodes: string[] = [];
  isCreateProjectTaskDialogOpen = false;
  parentProjectName = '';
  formGroup!: FormGroup;

  @Input()
  isForAnnualPlanning = false;

  @Input()
  isForDailyPlanning = false;

  @Input()
  showFilter!: boolean;

  constructor(
    private readonly moduleProjectStore: ProjectTaskStore,
    private readonly filterService: PlanningFilterService,
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
  ) {


    this.userService.userDto$.subscribe( {
      next: ( user ) => {
        if ( user ) {
          this.user = user;

          // set owner email in the filter store if the current user has the right to only see his own projects and tasks (AB) so project/tasks can be filtered
          if ( user.hasRight( this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.projectTask.canReadOnlyOwnProjectTasks ) ) {
            this.filterService.setProjectOwner( user.eMail );
          }
        }
      },
    } );
  }

  ngOnInit(): void {
    // load projects and tasks whenever the active owner observable emits a new value
    this.filterService.activeOwners$.subscribe( {
      next: () => this.loadProjectAndTasks(),
    } );

    if ( this.isForAnnualPlanning ) {
      this.projectsForAnnualPlanning$ = this.projects$.pipe(
        map( (projects) => {
          return projects.filter( project => project.name !== 'Allgemein' );
        })
      );
    }
  }

  closeCreateDialog(save?: boolean): void {
    if (save) {
      const newProjectTaskDto: ProjectTaskDto = this.formGroup.value;
      newProjectTaskDto.uuid = v4();

      // set the users eMail address as ownerEmail if the user does not have the right to create project/tasks for others (role ab)
      if (!this.user.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.projectTask.canCreateForOthers)) {
        newProjectTaskDto.ownerEmail = [this.user.eMail];
        this.moduleProjectStore.addProjectOrTask(newProjectTaskDto);
      } else {
        if (!newProjectTaskDto.isTask) {
          // in the case of a project, there can be multiple owners, because several people can be involved in one project
          this.moduleProjectStore.addProjectOrTask(newProjectTaskDto);
        } else {
          // for tasks, each owner needs an own task. That's why we loop over each selected ownerEmail.
          newProjectTaskDto.ownerEmail.forEach((email) => {
            const newProjectTaskDtoCopy = ObjectTools.cloneDeep(newProjectTaskDto);
            newProjectTaskDtoCopy.ownerEmail = [email];
            newProjectTaskDtoCopy.uuid = v4();
            this.moduleProjectStore.addProjectOrTask(newProjectTaskDtoCopy);
          });
        }
      }

      // reload the list of projects in the filter, so they can be selected
      this.filterService.reloadProjects();
    }
    this.isCreateProjectTaskDialogOpen = false;
  }

  /**
   * Opens the dialog to create a new project or task. To create a project, no value for the param parentProjectUuid has to be passed. It is
   * only required for tasks to make a reference to the project to whom it belongs.
   * @param parentProject - the uuid of the project to make a reference for tasks
   * @param createGeneralTask - whether a new task for project 'Allgemein' should be created or not
   */
  openCreateDialog(parentProject?: ProjectTaskDto, createGeneralTask?: boolean) {
    this.formGroup = this.getFormGroup();
    this.parentProjectName = parentProject ? parentProject.name : '';

    // remove the form control for the parentProjectUuid, since it isn't needed for projects
    if (!parentProject) {
      if ( createGeneralTask ) {
        // todo: search project allgemein and assign it to the control
        this.projects$.subscribe( {
          next: (projects) => {
            const generalProject = projects.find( (p) => p.name === 'Allgemein' );
            this.formGroup.controls['parentProjectUuid'].setValue(generalProject?.uuid);
          }
        } );
      } else {
        this.formGroup.removeControl('parentProjectUuid');
      }
    } else {
      this.formGroup.controls['parentProjectUuid'].setValue(parentProject.uuid);
    }
    this.formGroup.controls['isTask'].setValue(!!parentProject || createGeneralTask);

    this.isCreateProjectTaskDialogOpen = true;
  }

  dragStart(event: DragEvent, dataItem: ProjectTaskDto): void {
    if (event.dataTransfer) {
      event.dataTransfer.effectAllowed = 'move';
      event.dataTransfer.setData('text/json', JSON.stringify(dataItem));
    }
    event.stopPropagation();
  }

  private loadProjectAndTasks(): void {
    if (this.showFilter) {
      // filter projects and tasks
      this.moduleProjectStore.projectsAndTasks$.subscribe({
        next: (projectsAndTasks) => {
          // reset expanded treeView Nodes
          this.expandedNodes = [];

          this.filterService.activeProjects$.subscribe({
            next: (activeProjects) => {
              this.filterService.activeProjectStates$.subscribe({
                next: (activeProjectStates) => {

                  this.projectsAndTasks$ = of(
                    projectsAndTasks
                      .filter((pt) => {
                        // push uuid of project to expand the node in the treeView
                        if (!pt.isTask) {
                          this.expandedNodes.push(pt.uuid as string);
                        }
                        // if the obj is a task the parentProjectUuid must be in the list of all active projects and if not it is the uuid
                        return (pt.isTask ? activeProjects.includes(pt.parentProjectUuid) : activeProjects.includes(pt.uuid as string)) && activeProjectStates.includes(pt.state);
                      })
                      .filter((pt) => {
                        if (this.user.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.projectTask.canCreateForOthers)) {
                          return this.checkOwner(pt.ownerEmail) || pt.name === 'Allgemein';
                        } else {
                          return pt.ownerEmail.includes(this.user.eMail) || pt.name === 'Allgemein';
                        }
                      })
                  );

                },
              });
            },
          });
        },
      });
    }
  }

  private checkOwner(ownerEmails: string[]): boolean {
    let isOwnerActive = false;
    this.filterService.activeOwners$
      .subscribe({
        next: (activeOwners) => {
          if (activeOwners.some((ao) => ownerEmails.includes(ao))) {
            isOwnerActive = true;
          }
        },
      })
      .unsubscribe();
    return isOwnerActive;
  }

  private getFormGroup(): FormGroup {
    return this.formBuilder.group({
      isDeleted: [false],
      isTask: [false],
      name: ['', Validators.required],
      ownerEmail: [[]],
      parentProjectUuid: [''],
      state: [ModuleConstants.instance.moduleProjectStates.backlog],
      timeBudgetHours: [0, [Validators.required, Validators.min(0.1)]],
    });
  }
}
