import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { AnnualPlanningEventStore } from '../../store/annual-planning-event.store';

@Component({
  selector: 'itag-berufsbildung-annual-planning',
  templateUrl: './annual-planning.component.html',
  styleUrls: ['./annual-planning.component.scss'],
  providers: [AnnualPlanningEventStore],
  encapsulation: ViewEncapsulation.Emulated,
})
export class AnnualPlanningComponent implements OnInit {
  height = 600;

  constructor(
    readonly annualPlanningEventStore: AnnualPlanningEventStore,
    private readonly screenInfoService: ScreenInfoService,
  ) {}

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe( {
      next: screenSize => this.height = screenSize.height,
    } );
  }
}
