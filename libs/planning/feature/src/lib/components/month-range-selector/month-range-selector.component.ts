import { AfterViewInit, Component, EventEmitter, Output, ViewEncapsulation } from '@angular/core';

export interface ISelectedMonthRange {
  fromMonth: Date;
  untilMonth: Date;
  numberOfMonths: number;
}

@Component({
  selector: 'itag-berufsbildung-month-range-selector',
  templateUrl: './month-range-selector.component.html',
  styleUrls: ['./month-range-selector.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class MonthRangeSelectorComponent implements AfterViewInit {
  fromMonth: Date = new Date();
  untilMonth: Date;

  @Output()
  fromUntilMonthChange: EventEmitter<ISelectedMonthRange> = new EventEmitter();

  constructor() {
    const untilMonth = new Date();
    untilMonth.setMonth( untilMonth.getMonth() + 2);
    this.untilMonth = untilMonth;
  }

  ngAfterViewInit(): void {
    // emit values after initialization
    this.onValueChange();
  }

  onValueChange() {
    this.fromUntilMonthChange.emit( {
      fromMonth: this.fromMonth,
      untilMonth: this.untilMonth,
      numberOfMonths: this.calculateMonthDifference(),
    } );
  }

  private calculateMonthDifference(): number {
    let months;
    months = (this.untilMonth.getFullYear() - this.fromMonth.getFullYear()) * 12;
    months -= this.fromMonth.getMonth();
    months += this.untilMonth.getMonth();
    return months <= 0 ? 0 : months;
  }

}
