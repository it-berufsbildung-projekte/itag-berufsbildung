import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthRangeSelectorComponent } from './month-range-selector.component';

describe('MonthRangeSelectorComponent', () => {
  let component: MonthRangeSelectorComponent;
  let fixture: ComponentFixture<MonthRangeSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MonthRangeSelectorComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MonthRangeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
