import { Component, Input, OnChanges, QueryList, SimpleChanges, ViewChildren, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DailyPlanningEventDto, DailyPlanningLogDto, ModuleConstants } from '@itag-berufsbildung/planning/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { ExpansionPanelComponent } from '@progress/kendo-angular-layout';
import { v4 } from 'uuid';
import { DailyPlanningEventStore } from '../../store/daily-planning-event.store';
import { DailyPlanningLogStore } from '../../store/daily-planning-log.store';

@Component({
  selector: 'itag-berufsbildung-daily-planning-log',
  templateUrl: './daily-planning-log.component.html',
  styleUrls: ['./daily-planning-log.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DailyPlanningLogComponent implements OnChanges {
  @ViewChildren(ExpansionPanelComponent)
  panels!: QueryList<ExpansionPanelComponent>;

  @Input()
  dailyPlanningLogStore!: DailyPlanningLogStore;

  @Input()
  dailyPlanningEventStore!: DailyPlanningEventStore;

  @Input()
  dailyPlanningLog!: DailyPlanningLogDto | undefined;

  @Input()
  selectedOwner!: string;

  @Input()
  selectedDate!: Date;

  @Input()
  isReadonly = true;

  moduleConstants = ModuleConstants.instance;
  user!: UiUserDto;
  isDayConclusionDialogOpen = false;
  formGroup!: FormGroup;


  constructor(
    private readonly userService: UserService,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService,
  ) {
    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;
        }
      },
    });
  }

  ngOnChanges( changes: SimpleChanges ): void {
    // listen to changes in the input properties and set their values to the current (new) value
    for ( const key of Object.keys( changes ) ) {
      if ( key === 'dailyPlanningLog' ) {
        this.dailyPlanningLog = changes[key].currentValue;
      }
      if ( key === 'selectedOwner' ) {
        this.selectedOwner = changes[key].currentValue;
      }
      if ( key === 'selectedDate' ) {
        this.selectedDate = changes[key].currentValue;
      }
    }
  }

  closeDialog( save: boolean, close: boolean ): void {
    if ( save && this.dailyPlanningLog ) {
      this.dailyPlanningLog.conclusion = this.formGroup.value.dayConclusion;
      this.dailyPlanningLog.submissionDateConclusion = new Date();
      this.dailyPlanningLog.closed = close;
      this.dailyPlanningLogStore.update( this.dailyPlanningLog );
    }

    this.isDayConclusionDialogOpen = false;
  }

  submitPlan(): void {
    const currentDate = new Date();

    // only create a log if the current date is equal to the selected date in the scheduler
    if ( this.selectedDate.getFullYear() === currentDate.getFullYear()
      && this.selectedDate.getMonth() === currentDate.getMonth()
      && this.selectedDate.getDate() === currentDate.getDate()
    ) {
      const newLog = {
        ownerEmail: this.selectedOwner,
        submissionDatePlan: new Date(),
        uuid: v4(),
      };

      this.dailyPlanningLogStore.add( newLog as DailyPlanningLogDto );
    } else {
      this.notificationService.showWarning('Tagespläne können nur für den aktuellen Tag freigegeben werden!');
    }
  }

  openDayConclusionDialog(): void {
    this.createFormGroup();
    this.isDayConclusionDialogOpen = true;
  }

  createFormGroup(): void {
    this.formGroup = this.formBuilder.group( {
      dayConclusion: [this.dailyPlanningLog?.conclusion, Validators.required],
    } );
  }

  onExpandPanel( index: number ): void {
    this.panels.forEach((panel, idx) => {
      if (idx !== index && panel.expanded) {
        panel.toggle();
      }
    });
  }

  getTimeArrow( item: DailyPlanningEventDto ): string {
    const estimatedDurationInMillis = item.end.getTime() - item.start.getTime();
    const effectiveDurationInMillis = item.usedTime * 60 * 1000;
    const difference = estimatedDurationInMillis - effectiveDurationInMillis;

    if ( difference > 0 ) {
      return '↓';
    } else if ( difference < 0 ) {
      return '↑';
    } else {
      return '→';
    }
  }
}
