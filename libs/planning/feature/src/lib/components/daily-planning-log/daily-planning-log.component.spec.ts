import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPlanningLogComponent } from './daily-planning-log.component';

describe('DailyPlanningLogComponent', () => {
  let component: DailyPlanningLogComponent;
  let fixture: ComponentFixture<DailyPlanningLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DailyPlanningLogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DailyPlanningLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
