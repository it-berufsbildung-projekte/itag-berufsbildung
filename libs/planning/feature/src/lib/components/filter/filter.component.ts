import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { EventCategoryDto, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { FilterStore } from '../../store/filter.store';

@Component({
  selector: 'itag-berufsbildung-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  providers: [],
  encapsulation: ViewEncapsulation.Emulated,
})
export class FilterComponent implements OnInit{
  categories$ = this.filterStore.categories$;
  owners$ = this.filterStore.owners$;
  projects$ = this.filterStore.projects$;
  filteredProjects$ = this.filterStore.filteredProjects$;
  projectStates$ = this.filterStore.projectStates$;
  activeCategories$ = this.filterStore.activeCategories$;
  activeOwners$ = this.filterStore.activeOwners$;
  activeProjects$ = this.filterStore.activeProjects$;
  activeProjectStates$ = this.filterStore.activeProjectStates$;
  selectedCategories: EventCategoryDto[] = [];
  selectedOwners: string[] = [];
  selectedProjects: ProjectTaskDto[] = [];
  selectedProjectStates: string[] = [];
  showFilter = false;
  activeFilters: { [key: string]: boolean } = {};

  @Input()
  showOwnerFilterMultiple = false;

  @Input()
  showOwnerFilterSingle = false;

  @Input()
  showEventCategoryFilter = false;

  @Input()
  showProjectStateFilter = false;

  @Input()
  showProjectFilter = false;

  @Input()
  numberOfColumns = 0;

  constructor(
    private readonly filterStore: FilterStore,
  ) {}

  ngOnInit(): void {
    if ( this.showEventCategoryFilter ) {
      this.activeCategories$.subscribe( {
        next: activeCategoryUuids => {
          // save active/inactive state of category filter
          this.activeFilters['category'] = activeCategoryUuids.length!==0;

          // find selected categories and take their names to display in multiselect
          this.categories$.subscribe( {
            next: categories => {
              this.selectedCategories = categories.filter( ( c ) => activeCategoryUuids.includes( c.uuid as string ) );
            }
          } ).unsubscribe();
        }
      } );
    }

    if ( this.showOwnerFilterMultiple || this.showOwnerFilterSingle ) {
      this.activeOwners$.subscribe( {
        next: ( activeOwnerEmails ) => {
          this.activeFilters['owner'] = activeOwnerEmails.length!==0;

          // find selected owners and take their eMail to display in multiselect
          this.owners$.subscribe( {
            next: ( owners ) => {
              this.selectedOwners = owners.filter( ( ownerEmail ) => activeOwnerEmails.includes( ownerEmail ) );
            }
          } ).unsubscribe();
        }
      } );
    }

    if ( this.showProjectFilter ) {
      this.activeProjects$.subscribe( {
        next: ( activeProjectUuids ) => {
          this.activeFilters['project'] = activeProjectUuids.length!==0;

          // find selected projects
          this.projects$.subscribe( {
            next: ( projects ) => {
              this.selectedProjects = projects.filter( ( project ) => activeProjectUuids.includes( project.uuid as string ) );
            }
          } ).unsubscribe();
        }
      } );
    }

    if ( this.showProjectStateFilter ) {
      this.activeProjectStates$.subscribe( {
        next: ( activeProjectStates ) => {
          this.activeFilters['projectStates'] = activeProjectStates.length!==0;

          // find selected project states
          this.projectStates$.subscribe( {
            next: ( projectStates ) => {
              this.selectedProjectStates = projectStates.filter( ( projectState ) => activeProjectStates.includes( projectState ) );
            }
          } ).unsubscribe();
        }
      } );
    }

    // hint: if other filters are added, repeat the same for the new filters
  }

  getSelectedSingleOwner(): string {
    let activeOwner = '';

    this.activeOwners$.subscribe( {
      next: (activeOwners) => {
        if ( activeOwners.length > 0 ){
          activeOwner = activeOwners[0];
        }
      }
    } ).unsubscribe();

    return activeOwner;
  }

  isFilterApplied(): boolean {
    for ( const filterActive of Object.values(this.activeFilters) ) {
      if ( filterActive ) {
        return true;
      }
    }
    return false;
  }

  saveSelectedCategories( selectedCategories: EventCategoryDto[] ): void {
    this.filterStore.saveActiveCategories( selectedCategories );
  }

  saveSelectedOwners( selectedOwners: string[], changeFilter?: boolean ): void {
    this.filterStore.saveActiveOwners( selectedOwners );

    if ( changeFilter ) {
      this.filterStore.projectOwner = selectedOwners[0];
      this.filteredProjects$ = this.filterStore.filteredProjects$;
    }
  }

  saveSelectedProjects( selectedProjects: ProjectTaskDto[] ): void {
    this.filterStore.saveActiveProjects( selectedProjects );
  }

  saveSelectedProjectStates( selectedProjectStates: string[] ): void {
    this.filterStore.saveActiveProjectStates( selectedProjectStates );
  }
}
