import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualPlanningOverviewComponent } from './annual-planning-overview.component';

describe('AnnualPlanningOverviewComponent', () => {
  let component: AnnualPlanningOverviewComponent;
  let fixture: ComponentFixture<AnnualPlanningOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnnualPlanningOverviewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AnnualPlanningOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
