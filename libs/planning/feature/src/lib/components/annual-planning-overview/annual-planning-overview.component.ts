import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AnnualPlanningEventDto } from '@itag-berufsbildung/planning/data';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { GridComponent } from '@progress/kendo-angular-grid';
import { SchedulerComponent } from '@progress/kendo-angular-scheduler';
import { State } from '@progress/kendo-data-query';
import { AnnualPlanningSchedulerEditService } from '../../services/annual-planning-scheduler-edit.service';
import { PlanningFilterService } from '../../services/planning-filter.service';
import { AnnualPlanningEventStore } from '../../store/annual-planning-event.store';
import { CustomSchedulerToolbarComponent } from '../custom-scheduler-toolbar/custom-scheduler-toolbar.component';
import { ISelectedMonthRange } from '../month-range-selector/month-range-selector.component';

interface IDayEventHash { [key: string]: string }

@Component({
  selector: 'itag-berufsbildung-annual-planning-overview',
  templateUrl: './annual-planning-overview.component.html',
  styleUrls: ['./annual-planning-overview.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class AnnualPlanningOverviewComponent implements OnInit {
  @ViewChild('scheduler') private schedulerComponent!: SchedulerComponent;
  @ViewChild('toolbar') private toolbarComponent!: CustomSchedulerToolbarComponent;
  @ViewChild('grid') private gridComponent!: GridComponent;

  private ownerEventsHash: { [ key: string ]: AnnualPlanningEventDto[] } = {};
  private events: AnnualPlanningEventDto[] = [];
  private untilMonth!: Date;
  private numberOfMonths!: number;

  @Input()
  store!: AnnualPlanningEventStore;

  owners: string[] = [];
  activeOwners: string[] = [];
  ownerNames: string[] = [];
  dateEvents: IDayEventHash[] = [];
  fromMonth!: Date;
  height = 600;

  gridState: State = {
    skip: 0,
    sort: [],
    take: 10,
    filter: {
      logic: 'and',
      filters: [],
    },
  };

  constructor(
    readonly editService: AnnualPlanningSchedulerEditService,
    private readonly filterService: PlanningFilterService,
    private readonly screenInfoService: ScreenInfoService,
  ) {
    this.filterService.activeOwners$.subscribe( (activeOwners) => {
      this.activeOwners = activeOwners;
      this.ownerNames = activeOwners.map( owner => owner.split('@')[0].replace( '.', '') );
    } );

    // boolean is used to skip the first value change of the activeCategories$ observable, since the filter is already set with the values from the local store
    let isFirstCategoryChange = true;
    this.filterService.activeCategories$.subscribe( () => {
      if ( isFirstCategoryChange ) {
        isFirstCategoryChange = false;
        return;
      }

      setTimeout( () => {
        // go back to the initially selected month, because the scheduler stays at the last selected month
        this.toolbarComponent.setSelectedDate( this.fromMonth );


        this.events = [];
        this.dateEvents = [];
        this.events = this.loadEventsForMonths();
        this.processEvents();
      }, 10 );

    } );


  }

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });

    this.editService.annualPlanningEventStore = this.store;
    this.editService.read();

    this.store.users$.subscribe( {
      next: (value) => {
        this.owners = value;
      }
    } );
  }

  onStateChange(state: State): void {
    this.gridState = state;
  }

  setFromAndUntilMonth( $event: ISelectedMonthRange ): void {
    this.fromMonth = $event.fromMonth;
    this.untilMonth = $event.untilMonth;
    this.numberOfMonths = $event.numberOfMonths;

    // set new selected fromMonth to the scheduler and precess events
    this.toolbarComponent.setSelectedDate( this.fromMonth );
    this.events = this.loadEventsForMonths();
    this.processEvents();
  }

  private processEvents(): void {
    this.events = this.loadEventsForMonths();
    this.filterEventsOfOwners();

    // reset dateEvents if filter has changed
    this.dateEvents = [];

    const date = new Date( this.fromMonth );
    date.setDate( 1 );

    const numberOfDays = this.numberOfMonths * 31;
    for( let i = 0; i < numberOfDays; i++ ) {
      if ( i !== 0 ) {
        date.setDate( date.getDate() + 1 );
      }

      //skip saturday and sunday
      if ( [0, 6].includes( date.getDay() ) ) {
        continue;
      }

      // create object with property day with the value of date. Properties for owners are added later
      const dayAndOwnerEvents: IDayEventHash = {};
      dayAndOwnerEvents['date'] = String( date.getTime() );
      dayAndOwnerEvents['dayName'] = this.getDayName( date );

      // add property with the name of each owner and his events as string
      for ( const owner of this.activeOwners ) {
        const eventsOfCurrentDate = this.getEventsOfCurrentDay( this.ownerEventsHash[owner], date );
        let eventsString = '';
        eventsOfCurrentDate.forEach( ev => eventsString += `${ev.title} | ` );

        // ownerName is the same name displayed in the column header
        const ownerName = owner.split('@')[0].replace('.', '');
        dayAndOwnerEvents[ownerName] = eventsString;
      }

      this.dateEvents.push(dayAndOwnerEvents);
    }
  }

  private filterEventsOfOwners(): void {
    let eventsOfOwner: AnnualPlanningEventDto[] = [];

    for ( const owner of this.activeOwners ) {
      eventsOfOwner = this.events.filter( (ev) => {
        ev = ev as AnnualPlanningEventDto;
        const categoryOwnerKeySplit = ev.ownerCategoryKey.split('|');
        return categoryOwnerKeySplit[0] === owner;
      } );

      this.ownerEventsHash[owner] = eventsOfOwner;
    }
  }

  private getDayName( date: Date ): string {
    switch (date.getDay()) {
      case 1:
        return 'Mo';
      case 2:
        return 'Di';
      case 3:
        return 'Mi';
      case 4:
        return 'Do';
      case 5:
        return 'Fr';
      default:
        return '';
    }
  }

  private getEventsOfCurrentDay( events: AnnualPlanningEventDto[], date: Date ): AnnualPlanningEventDto[] {
    return events.filter(e =>
      e.start.getDate() === date.getDate() && e.start.getMonth() === date.getMonth() && e.start.getFullYear() === date.getFullYear(),
    );
  }

  private loadEventsForMonths(): AnnualPlanningEventDto[] {
    this.toolbarComponent.setSelectedDate( this.fromMonth );
    let events: AnnualPlanningEventDto[] = [];

    for ( let i = 0; i < this.numberOfMonths; i++ ) {
      // events are retrieved from the scheduler component, since it parses the recurrence rules of recurring events, so we don't have to do it
      const newEvents = this.schedulerComponent.events.filter( event => {
        // if the event has a recurrence rule, then we must check if it isn't the recurrence master or doesn't exist in the events array
        // otherwise we would have some occurrences that are redundant
        if ( event.recurrenceRule ) {
          if ( !event.recurrenceId ) {
            return false;
          }
          if ( events.find( e => e.recurrenceRule === event.recurrenceRule && e.start.getTime() === event.start.getTime() ) ) {
            return false;
          }
        }
        return !events.includes( event );
      } );

      // array with the new events is concatenated with the events array
      events = events.concat( newEvents );

      // select the next month in the scheduler component to load the recurring events for the next period
      this.toolbarComponent.next();
    }

    return events;
  }

}
