import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';

@Component({
  selector: 'itag-berufsbildung-daily-planning',
  templateUrl: './daily-planning.component.html',
  styleUrls: ['./daily-planning.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class DailyPlanningComponent implements OnInit{
  height = 600;

  constructor(
    private readonly screenInfoService: ScreenInfoService,
  ) {}

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe( {
      next: screenSize => this.height = screenSize.height,
    } );
  }

}
