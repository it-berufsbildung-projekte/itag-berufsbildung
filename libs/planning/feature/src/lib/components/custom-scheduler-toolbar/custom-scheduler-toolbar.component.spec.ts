import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSchedulerToolbarComponent } from './custom-scheduler-toolbar.component';

describe('CustomSchedulerToolbarComponent', () => {
  let component: CustomSchedulerToolbarComponent;
  let fixture: ComponentFixture<CustomSchedulerToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomSchedulerToolbarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomSchedulerToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
