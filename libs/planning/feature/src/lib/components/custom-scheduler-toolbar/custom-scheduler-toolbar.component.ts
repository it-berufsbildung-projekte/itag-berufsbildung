import { Component, ViewEncapsulation } from '@angular/core';
import { ToolbarService } from '@progress/kendo-angular-scheduler';

@Component({
  selector: 'itag-berufsbildung-custom-scheduler-toolbar',
  templateUrl: './custom-scheduler-toolbar.component.html',
  styleUrls: ['./custom-scheduler-toolbar.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class CustomSchedulerToolbarComponent {

  constructor(
    private readonly toolbarService: ToolbarService
  ) { }

  next(): void {
    this.toolbarService.navigate({
      type: 'next'
    });
  }

  previous(): void {
    this.toolbarService.navigate( {
      type: 'prev'
    } );
  }

  setSelectedDate( date: Date ) {
    this.toolbarService.navigate( {
      type: 'select-date',
      date: date,
    } );
  }

}
