import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AnnualPlanningEventDto, EventCategoryDto, ModuleConstants, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools, SorterTools, UiUserDto, UserNameTool } from '@itag-berufsbildung/shared/util';
import { CancelEvent, CrudOperation, EditMode, EventClickEvent, RemoveEvent, SaveEvent, SchedulerComponent, SlotClickEvent } from '@progress/kendo-angular-scheduler';
import { filter, Observable, of } from 'rxjs';
import { AnnualPlanningSchedulerEditService } from '../../services/annual-planning-scheduler-edit.service';
import { PlanningFilterService } from '../../services/planning-filter.service';
import { AnnualPlanningEventStore } from '../../store/annual-planning-event.store';
import { ProjectTaskStore } from '../../store/project-task-store.service';

@Component({
  selector: 'itag-berufsbildung-annual-planning-planning-view',
  templateUrl: './annual-planning-planning-view.component.html',
  styleUrls: ['./annual-planning-planning-view.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class AnnualPlanningPlanningViewComponent implements OnInit, AfterViewInit {
  @ViewChild('scheduler') schedulerComponent!: SchedulerComponent;

  @Input()
  store!: AnnualPlanningEventStore;

  users$: Observable<string[]> = of([]);
  categories$: Observable<EventCategoryDto[]> = of([]);
  selectedDate: Date = new Date();
  formGroup: FormGroup | undefined;
  userDto!: UiUserDto;
  moduleConstants = ModuleConstants.instance;
  height = 600;

  private targetDate: Date | undefined = undefined;
  private droppedProject: ProjectTaskDto | undefined = undefined;

  constructor(
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly user: UserService,
    private readonly notification: NotificationService,
    private readonly screenInfo: ScreenInfoService,
    private readonly projectTaskStore: ProjectTaskStore,
    readonly ownerCategoryFilter: PlanningFilterService,
    readonly annualPlanningSchedulerEditService: AnnualPlanningSchedulerEditService
  ) {
    this.user.userDto$.subscribe({
      next: (u) => {
        if (u) {
          this.userDto = u;
        }
      },
    });
  }

  ngOnInit(): void {
    this.screenInfo.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });

    this.annualPlanningSchedulerEditService.annualPlanningEventStore = this.store;
    this.annualPlanningSchedulerEditService.read();
  }

  ngAfterViewInit(): void {
    this.users$ = this.store.users$;
    this.categories$ = this.store.categories$;
  }

  cancelHandler({ sender }: CancelEvent): void {
    this.closeEditor(sender);
  }

  canUserSeeModuleProjectList(): boolean {
    return this.userDto?.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.annualPlanningEvent.canSeeProjectTaskList);
  }

  canUserCreateEventsForOthers(): boolean {
    return this.userDto?.hasRight(this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.annualPlanningEvent.canCreateEventForOthers);
  }

  eventDblClickHandler({ sender, event }: EventClickEvent): void {
    if ( !this.userDto.hasRight( this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.annualPlanningEvent.canUpdate ) ) {
      this.notification.showWarning( 'Kein Recht zum Bearbeiten von Ereignissen!');
      return;
    }

    this.closeEditor(sender);
    let dataItem = event.dataItem;

    // check if the user is the owner of the event (only the owner can update an event)
    const ownerCategoryKeySplit = dataItem.ownerCategoryKey.split('|');
    if (ownerCategoryKeySplit[0] !== this.userDto.eMail) {
      this.notification.showWarning(`Sie haben nur das Recht Ihre eigenen Ereignisse zu bearbeiten!`);
      return;
    }

    if (this.annualPlanningSchedulerEditService.isRecurring(dataItem)) {
      sender
        .openRecurringConfirmationDialog(CrudOperation.Edit)
        // The result will be undefined if the dialog was closed.
        .pipe(filter((editMode) => editMode !== undefined))
        .subscribe((editMode: EditMode) => {
          if (editMode === EditMode.Series) {
            dataItem = this.annualPlanningSchedulerEditService.findRecurrenceMaster(dataItem);
            this.createFormGroup(EditMode.Series, dataItem);
          } else {
            this.createFormGroup(EditMode.Occurrence, dataItem);
          }
          sender.editEvent(dataItem, { group: this.formGroup, mode: editMode });
        });
    } else {
      this.createFormGroup(EditMode.Event, dataItem);
      sender.editEvent(dataItem, { group: this.formGroup });
    }
  }

  getDayFromDate(date: string): number {
    return new Date(date).getDate();
  }

  isSelectedDateEqualToSlotDate(slotDate: string): boolean {
    const _slotDate = new Date(slotDate);
    return (
      _slotDate.getDate() === this.selectedDate.getDate() && _slotDate.getMonth() === this.selectedDate.getMonth() && _slotDate.getFullYear() === this.selectedDate.getFullYear()
    );
  }

  isEditingSeries(editMode: EditMode): boolean {
    return editMode === EditMode.Series;
  }

  onDrop(event: DragEvent): void {
    if (event.dataTransfer) {
      const droppedProject = JSON.parse(event.dataTransfer.getData('text/json'));
      this.createFormGroup(EditMode.Event, undefined, this.targetDate, this.targetDate, droppedProject.name);
      this.schedulerComponent.addEvent(this.formGroup);

      // the project will be updated while saving the new event
      this.droppedProject = droppedProject;
    }
  }

  onDragOver(event: DragEvent): void {
    event.preventDefault();
    // get the target slot to extract its date
    const slot = this.schedulerComponent.slotByPosition(event.pageX, event.pageY);

    // assign the targetDate only if a slot has been found
    this.targetDate = slot ? slot.start : undefined;
  }

  removeHandler({ sender, dataItem }: RemoveEvent): void {
    if ( !this.userDto.hasRight( this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.annualPlanningEvent.canRemove ) ) {
      this.notification.showWarning( 'Kein Recht zum Löschen von Ereignissen' );
      return;
    }
    // check if the user is the creator of the event and prevent deletion if not
    if ( dataItem.creatorEmail !== this.userDto.eMail ) {
      this.notification.showWarning(`Sie haben nur das Recht die von Ihnen erstellten Ereignisse zu löschen!`);
      return;
    }

    if (this.annualPlanningSchedulerEditService.isRecurring(dataItem)) {
      sender
        .openRecurringConfirmationDialog(CrudOperation.Remove)
        // The result will be undefined if the dialog was closed.
        .pipe(filter((editMode) => editMode !== undefined))
        .subscribe((editMode) => {
          this.handleRemove(dataItem, editMode);
        });
    } else {
      sender.openRemoveConfirmationDialog().subscribe((shouldRemove) => {
        if (shouldRemove) {
          this.handleRemove(dataItem, EditMode.Event);
        }
      });
    }
  }

  slotDblClickHandler(event: SlotClickEvent): void {
    if ( !this.userDto.hasRight( this.moduleConstants.moduleName, this.moduleConstants.availableRights.ui.annualPlanningEvent.canCreate) ) {
      this.notification.showWarning('Kein Recht zum Erstellen von Ereignissen!');
      return;
    }

    this.closeEditor(event.sender);
    this.createFormGroup(EditMode.Event, undefined, event.start, event.end, '');
    event.sender.addEvent(this.formGroup);
  }

  saveHandler({ sender, formGroup, isNew, dataItem, mode }: SaveEvent): void {
    if (formGroup.valid) {
      let formValue = { ...formGroup.value };
      formValue = { ...formValue, eventCategoryUuid: formValue.eventCategory.uuid };

      if (isNew) {
        const ownerEmailArr: string[] = formValue.owners;

        // selected owners are added to the list of owners in the dropped project
        if ( this.droppedProject ) {
          ownerEmailArr.forEach( owner => {
            if ( !this.droppedProject?.ownerEmail.find( e => e === owner ) ) {
              this.droppedProject?.ownerEmail.push( owner );
            }
          } );
          this.projectTaskStore.updateProjectOrTask( this.droppedProject );

          formValue.projectTaskUuid = this.droppedProject.uuid;

          // clear the dropped project
          this.droppedProject = undefined;
        }

        // to avoid displaying two events for the first occurrence, the start date of the series is added to the recurrenceException array.
        if ( formValue.recurrenceRule.length > 1 ) {
          formValue.recurrenceException.push(formValue.start);
        }

        // delete property owners from formValue, since it is passed separately to the editService
        delete formValue.owners;
        this.annualPlanningSchedulerEditService.createEventsForUsers(formValue, ownerEmailArr);
      } else {
        this.handleUpdate(dataItem, formValue, mode);
      }

      this.closeEditor(sender);
    }
  }

  getOwnerName( event: AnnualPlanningEventDto ): string {
    const ownerCategoryKeySplit = event.ownerCategoryKey.split('|');
    return UserNameTool.getFullName( ownerCategoryKeySplit[0]);
  }

  /**
   * Creates the formGroup for the edit dialog. Parameter dataItem is only required if an existing event is getting updated.
   * In this case, all other params don't need to be passed to the method.
   * @param mode
   * @param dataItem - Event to update
   * @param start - start date
   * @param end - end date
   * @param moduleProjectName - module or project name (only used with drag/drop)
   * @private
   */
  private createFormGroup(mode: EditMode, dataItem?: AnnualPlanningEventDto, start?: Date | undefined, end?: Date | undefined, moduleProjectName?: string): void {
    let standardCategory;
    this.store.categories$.subscribe((value) => {
      const sortedCategories = [ ...value.sort( SorterTools.dynamicSort('-weight') ) ];
      standardCategory = sortedCategories[sortedCategories.length - 2];
    }).unsubscribe();

    this.formGroup = this.formBuilder.group({
      creatorEmail: [dataItem ? dataItem.creatorEmail : this.userDto.eMail],
      start: [dataItem ? dataItem.start : start ? start : new Date(), Validators.required],
      end: [dataItem ? dataItem.end : end ? start : new Date(), Validators.required],
      isAllDay: true,
      title: new FormControl(dataItem ? dataItem.title : `${moduleProjectName ? moduleProjectName : ''}`, Validators.required),
      description: new FormControl(dataItem ? dataItem.description : ''),
      eventCategory: new FormControl(dataItem ? dataItem.eventCategory : standardCategory),
      recurrenceException: new FormControl(dataItem ? dataItem.recurrenceException : []),
      recurrenceRule: new FormControl(dataItem ? dataItem.recurrenceRule : ''),
      recurrenceId: new FormControl(dataItem ? dataItem.recurrenceId : ''),
    });

    // if a new event is created the owners can be set. Therefore, a control is added for the selection.
    if (!dataItem) {
      this.formGroup.addControl('owners', new FormControl([this.userDto.eMail], Validators.required));

      // set all active owners from the filter as event owners
      this.ownerCategoryFilter.activeOwners$.subscribe( (owners) => this.formGroup?.controls['owners'].setValue( owners ) );
    }
  }

  private closeEditor(scheduler: SchedulerComponent): void {
    scheduler.closeEvent();
    this.formGroup = undefined;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private handleUpdate(item: AnnualPlanningEventDto, value: any, mode?: EditMode): void {
    if (mode === EditMode.Occurrence) {
      if (this.annualPlanningSchedulerEditService.isException(item)) {
        this.annualPlanningSchedulerEditService.update(item, value);
      } else {
        this.annualPlanningSchedulerEditService.createException(item, value);
      }
    } else {
      // The item is non-recurring, or we are editing the entire series.
      this.annualPlanningSchedulerEditService.update(item, value);
    }
  }

  private handleRemove(item: AnnualPlanningEventDto, mode: EditMode): void {
    if (mode === EditMode.Series) {
      const itemCopy = ObjectTools.cloneDeep(item);

      if ( item.recurrenceId && item.recurrenceId.length > 1 ) {
        itemCopy.uuid = item.recurrenceId;
      }

      this.annualPlanningSchedulerEditService.save([], [], [itemCopy] );
    } else if (mode === EditMode.Occurrence) {
      if (this.annualPlanningSchedulerEditService.isException(item)) {
        this.annualPlanningSchedulerEditService.remove(item);
      } else {
        const recurrenceMaster = this.annualPlanningSchedulerEditService.findRecurrenceMaster(item);
        recurrenceMaster.recurrenceException.push(item.start);
        this.annualPlanningSchedulerEditService.save([], [recurrenceMaster], [] );
        // this.annualPlanningSchedulerEditService.removeOccurrence(item);
      }
    } else {
      this.annualPlanningSchedulerEditService.remove(item);
    }

    if ( item.projectTaskUuid ) {
      // check if there is another event with the same project to avoid removing the ownerEmail in the project if it's still assigned to an owner
      let eventWithSameProject;
      this.annualPlanningSchedulerEditService.events.subscribe( {
        next: (events) => eventWithSameProject = events.find( (e) => e.projectTaskUuid === item.projectTaskUuid && e.ownerCategoryKey === item.ownerCategoryKey ),
      } );

      // if an event with the same project was found, then the rest of this method is skipped, because the project is still assigned to an owner
      if ( eventWithSameProject ){
        return;
      }

      // find the project which is referenced in the event
      let updatedProject: ProjectTaskDto | undefined;
      this.projectTaskStore.projects$.subscribe( {
        next: (projects) => {
          updatedProject = projects.find( (p) => p.uuid === item.projectTaskUuid ) as ProjectTaskDto;
        }
      } );

      // if the project was found, then the ownerEmail can be removed from the project
      if ( updatedProject ) {
        const ownerCategoryKeySplit = item.ownerCategoryKey.split( '|' );
        updatedProject.ownerEmail = updatedProject.ownerEmail.filter( ( o: string ) => o!==ownerCategoryKeySplit[0] );
        this.projectTaskStore.updateProjectOrTask( updatedProject );
      }
    }

  }
}
