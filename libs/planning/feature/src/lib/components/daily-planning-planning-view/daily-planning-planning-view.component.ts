import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DailyPlanningEventDto, DailyPlanningLogDto, ModuleConstants, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { CancelEvent, CrudOperation, DragEndEvent, EditMode, EventClickEvent, RemoveEvent, ResizeEndEvent, SaveEvent, SchedulerComponent } from '@progress/kendo-angular-scheduler';
import { filter } from 'rxjs';
import { DailyPlanningSchedulerEditService } from '../../services/daily-planning-scheduler-edit.service';
import { PlanningFilterService } from '../../services/planning-filter.service';
import { DailyPlanningEventStore } from '../../store/daily-planning-event.store';
import { DailyPlanningLogStore } from '../../store/daily-planning-log.store';
import { ProjectTaskStore } from '../../store/project-task-store.service';
import { DailyPlanningTaskConclusionDialogComponent } from '../daily-planning-task-conclusion-dialog/daily-planning-task-conclusion-dialog.component';

// specify timezone
import '@progress/kendo-date-math/tz/Europe/Berlin';

@Component({
  selector: 'itag-berufsbildung-daily-planning-planning-view',
  templateUrl: './daily-planning-planning-view.component.html',
  styleUrls: ['./daily-planning-planning-view.component.scss'],
  providers: [DailyPlanningEventStore, DailyPlanningLogStore],
  encapsulation: ViewEncapsulation.Emulated,
})
export class DailyPlanningPlanningViewComponent implements OnInit, AfterViewInit {
  @ViewChild('scheduler') scheduler!: SchedulerComponent;
  @ViewChild('conclusionDialog') conclusionDialog!: DailyPlanningTaskConclusionDialogComponent;
  formGroup: FormGroup | undefined;
  selectedDate = new Date();
  height = 600;
  user!: UiUserDto;
  selectedOwner = '';
  dailyPlanningLog: DailyPlanningLogDto | undefined;
  moduleConstants = ModuleConstants.instance;

  constructor(
    private readonly screenInfoService: ScreenInfoService,
    private readonly formBuilder: FormBuilder,
    private readonly filterService: PlanningFilterService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    readonly editService: DailyPlanningSchedulerEditService,
    readonly logStore: DailyPlanningLogStore,
    readonly taskStore: ProjectTaskStore,
    readonly store: DailyPlanningEventStore,
  ) {
    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;
        }
      },
    });
  }

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });

    this.editService.dailyPlanningEventStore = this.store;

    // set this selected owner from the filter and reload the events for the selected date and owner
    this.filterService.activeOwners$.subscribe( {
      next: (owner) => {
        if ( owner[0] ) {
          // load events. If the user can only read his own events, then his eMail is set as the selected owner otherwise the selected owner comes from the filter service
          if ( this.user.hasRight( ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canReadOnlyOwnEvents ) ) {
            this.selectedOwner = this.user.eMail;
            this.store.setSelectedDateAndOwner( this.selectedDate, this.user.eMail );
          } else {
            this.selectedOwner = owner[0];
            this.store.setSelectedDateAndOwner( this.selectedDate, this.selectedOwner );
          }

          // find and set the dailyPlanningLog for the selected date
          this.findDailyPlanningLog();
        }
      }
    } );

    this.editService.read();


  }

  ngAfterViewInit(): void {
    // always reload all events for the selected date if the user changes it in the scheduler
    this.scheduler.selectedDateStream.subscribe({
      next: (date) => {
        if (this.user) {
          this.selectedDate = date;
          // reload all events for the selected date
          this.store.setSelectedDateAndOwner( date, this.selectedOwner );

          // reload the log for the current date
          this.findDailyPlanningLog();
        }
      },
    });
  }

  eventDblClickHandler({ sender, event }: EventClickEvent): void {
    // if a dailyPlanningLog was found the user can't update an event anymore. Instead, a dialog is opened to write the conclusion.
    if ( this.dailyPlanningLog ) {
      // the conclusion dialog is readonly if either the user has no update right or the day was already closed
      const isDayClosed = this.dailyPlanningLog.closed ? this.dailyPlanningLog.closed : false;
      // open the dialog
      this.conclusionDialog.openConclusionDialog(
        event.dataItem,
        !this.user.hasRight( ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate ) || isDayClosed,
        this.user,
      ).subscribe( {
        next: (event) => {
          // only update the event if the user has written his conclusion. If no changes were made, then the event object has no properties.
          if ( Object.keys( event ).length > 0 ) {
            // update the state of the task if finished
            if ( event.isTaskFinished && event.task ) {
              event.task.state = this.moduleConstants.moduleProjectStates.finished;
              this.taskStore.updateProjectOrTask( event.task );
            }

            // update the state of the task if closed
            if ( event.isTaskClosed && event.task ) {
              event.task.state = this.moduleConstants.moduleProjectStates.closed;
              this.taskStore.updateProjectOrTask( event.task );
            }

            // eventually update the event
            this.editService.save( [], [event], [] );
          }
        },
      } );
      return;
    }

    if (!this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate)) {
      this.notificationService.showWarning('Kein Recht zum Bearbeiten von Tagesplanereignissen!');
      return;
    }

    this.closeEditor(sender);
    let dataItem = event.dataItem;

    if (this.editService.isRecurring(dataItem)) {
      sender
        .openRecurringConfirmationDialog(CrudOperation.Edit)
        // The result will be undefined if the dialog was closed.
        .pipe(filter((editMode) => editMode !== undefined))
        .subscribe((editMode: EditMode) => {
          if (editMode === EditMode.Series) {
            dataItem = this.editService.findRecurrenceMaster(dataItem);
            this.createFormGroup(dataItem);
          } else {
            this.createFormGroup(dataItem);
          }
          sender.editEvent(dataItem, { group: this.formGroup, mode: editMode });
        });
    } else {
      this.createFormGroup(dataItem);
      sender.editEvent(dataItem, { group: this.formGroup });
    }
  }

  cancelHandler({ sender }: CancelEvent): void {
    this.closeEditor(sender);
  }

  onDragOver(event: DragEvent): void {
    // if user doesn't have the right to create events or a log for this selected date was found (plan submitted), then the default behaviour is expected
    if (this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canCreate) || this.dailyPlanningLog ) {
      event.preventDefault();
    }
  }

  onDrop(event: DragEvent): void {
    // only allow to drop a task if the user has the right to create events or no log for this selected date was found
    if ( this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canCreate) && event.dataTransfer && !this.dailyPlanningLog ) {
      // get the target slot by x and y position from drop event
      const slot = this.scheduler.slotByPosition(event.pageX, event.pageY);
      const task = JSON.parse(event.dataTransfer.getData('text/json'));

      // to simplify everything, a new form group gets created with all properties for the new event, since there is already exists a method for that and the saveHandler
      // can further process it
      this.createFormGroup(undefined, slot.start, task);
      this.saveHandler({ sender: this.scheduler, formGroup: this.formGroup, isNew: true } as SaveEvent);
    }
  }

  removeHandler({ sender, dataItem }: RemoveEvent): void {
    if (!this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRemove)) {
      this.notificationService.showWarning('Kein Recht zum Löschen von Tagesplanereignissen!');
      return;
    }

    // if a log was found, then events can't be removed
    if ( this.dailyPlanningLog ) {
      this.notificationService.showWarning( 'Freigegebener Tagesplan kann nicht mehr bearbeitet werden!' );
      return;
    }

    sender.openRemoveConfirmationDialog().subscribe((shouldRemove) => {
      if (shouldRemove) {
        this.editService.remove(dataItem);
      }
    });
  }

  resizeEndHandler({ event, start, end }: ResizeEndEvent): void {
    if (!this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate)) {
      this.notificationService.showWarning('Kein Recht zum Bearbeiten von Tagesplanereignissen!');
      return;
    }

    // if a log was found, then events can't be removed
    if ( this.dailyPlanningLog ) {
      this.notificationService.showWarning( 'Freigegebener Tagesplan kann nicht mehr bearbeitet werden!' );
      return;
    }

    // calculate the duration of the event
    const eventDurationHours = (end.getTime() - start.getTime()) / 1000 / 60 / 60;

    // if the event's duration is greater than 8h, the event is shortened to 8h
    if ( eventDurationHours > 8 ) {
      this.notificationService.showWarning('Task darf an einem Tag nicht länger als 8h dauern');
      end.setHours( start.getHours() + 8 );
    }

    const value = { start: start, end: end };
    const dataItem = event.dataItem;
    this.handleUpdate(dataItem, value);
  }

  dragEndHandler({ event, start, end }: DragEndEvent): void {
    if (!this.user.hasRight(ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate)) {
      this.notificationService.showWarning('Kein Recht zum Bearbeiten von Tagesplanereignissen!');
      return;
    }

    // if a log was found, then events can't be removed
    if ( this.dailyPlanningLog ) {
      this.notificationService.showWarning( 'Freigegebener Tagesplan kann nicht mehr bearbeitet werden!' );
      return;
    }

    const value = { start: start, end: end, isAllDay: false };
    const dataItem = event.dataItem;
    this.handleUpdate(dataItem, value);
  }

  getPlanningLogFormReadonly(): boolean {
    if ( this.user ) {
      return !this.user.hasRight( ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canUpdate);
    }
    return false;
  }

  saveHandler({ sender, formGroup, isNew, dataItem }: SaveEvent): void {
    if (formGroup.valid) {
      const event = { ...formGroup.value };

      if (isNew) {
        this.editService.save([event], [], []);
      } else {
        this.handleUpdate(dataItem, event);
      }

      this.closeEditor(sender);
    }
  }

  private closeEditor(scheduler: SchedulerComponent): void {
    scheduler.closeEvent();
    this.formGroup = undefined;
  }

  private createFormGroup(dataItem?: DailyPlanningEventDto, start?: Date | undefined, task?: ProjectTaskDto): void {
    const endDate = new Date(start as Date);

    if (task) {
      let taskDurationHours = task?.timeBudgetHours.valueOf();

      // the duration of a task on one day should not be greater than 8h. Therefore, events with such tasks are shortened to 8h.
      if ( task?.timeBudgetHours > 8 ) {
        this.notificationService.showWarning('Task dauert länger als 8h und wird deshalb auf 8h verkürzt');
        taskDurationHours = 8;
      }

      // calculate the endDate with the duration of the task
      endDate.setMinutes(endDate.getMinutes() + taskDurationHours * 60);
    }

    this.formGroup = this.formBuilder.group({
      start: [dataItem ? dataItem.start : start ? start : new Date(), Validators.required],
      end: [dataItem ? dataItem.end : endDate, Validators.required],
      isAllDay: false,
      title: [dataItem ? dataItem.title : `${task?.name}`, Validators.required],
      description: [dataItem ? dataItem.description : ''],
      task: [dataItem ? dataItem.task : task],
      ownerEmail: [this.user.eMail],
    });
  }

  private findDailyPlanningLog(): void {
    this.logStore.dailyPlanningLogs$.subscribe( {
      next: (logs) => this.dailyPlanningLog = logs.find(
        (l) =>
          l.submissionDatePlan ? l.submissionDatePlan.getDate() === this.selectedDate.getDate()
            && l.submissionDatePlan.getMonth() === this.selectedDate.getMonth()
            && l.submissionDatePlan.getFullYear() === this.selectedDate.getFullYear()
            && l.ownerEmail === this.selectedOwner : false
      )
    } );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private handleUpdate(item: DailyPlanningEventDto, value: any): void {
    this.editService.update(item, value);
  }
}
