import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPlanningPlanningViewComponent } from './daily-planning-planning-view.component';

describe('DailyPlanningPlanningViewComponent', () => {
  let component: DailyPlanningPlanningViewComponent;
  let fixture: ComponentFixture<DailyPlanningPlanningViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DailyPlanningPlanningViewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DailyPlanningPlanningViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
