import { Component, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DailyPlanningEventDto, ModuleConstants } from '@itag-berufsbildung/planning/data';
import { ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'itag-berufsbildung-daily-planning-task-conclusion-dialog',
  templateUrl: './daily-planning-task-conclusion-dialog.component.html',
  styleUrls: ['./daily-planning-task-conclusion-dialog.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class DailyPlanningTaskConclusionDialogComponent {
  moduleConstants = ModuleConstants.instance;
  isDialogOpen = false;
  isReadonly = false;
  formGroup!: FormGroup;
  changedEvent$: BehaviorSubject<DailyPlanningEventDto> = new BehaviorSubject( {} as DailyPlanningEventDto );
  dailyPlanningEvent!: DailyPlanningEventDto;
  user!: UiUserDto;

  constructor(
    private readonly formBuilder: FormBuilder,
  ) {}

  closeDialog( save?: boolean ): void {
    if ( save ) {
      const formValues = this.formGroup.value;

      // hint: dailyPlanningEvent has to be cast to any to access its properties dynamically in a loop
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const eventAsAny = this.dailyPlanningEvent as any;

      for ( const key of Object.keys( formValues ) ) {
        eventAsAny[key] = formValues[key];
      }

      // since the dailyEventDto was cloned by the cloneDeep method, it no longer has date objects for start and end.
      // Instead, it has strings which must be converted back to date objects again to avoid errors.
      this.changedEvent$.next( { ...eventAsAny as DailyPlanningEventDto, start: new Date( eventAsAny.start ), end: new Date( eventAsAny.end ) } );
    }

    this.isDialogOpen = false;
  }

  acceptFinishedTask( withNote?: boolean ): void {
    // set property isTaskClosed to true and emit new value in changedEvent$
    const updatedEvent: DailyPlanningEventDto = {
      ...this.dailyPlanningEvent,
      isTaskClosed: true,
      start: new Date( this.dailyPlanningEvent.start ),
      end: new Date( this.dailyPlanningEvent.end ),
    };

    this.changedEvent$.next( updatedEvent );
    this.isDialogOpen = false;

    if ( withNote ) {
      // todo: open form to write notes
    }
  }

  openConclusionDialog( event: DailyPlanningEventDto, readonly: boolean, user: UiUserDto ): Observable<DailyPlanningEventDto> {
    this.dailyPlanningEvent = ObjectTools.cloneDeep( event );
    this.isReadonly = readonly;
    this.user = user;
    this.createFormGroup();
    this.isDialogOpen = true;
    return this.changedEvent$;
  }

  private createFormGroup(): void {
    this.formGroup = this.formBuilder.group( {
      taskConclusion: [this.dailyPlanningEvent?.taskConclusion ? this.dailyPlanningEvent.taskConclusion : '', Validators.required],
      fachkompetenz: [this.dailyPlanningEvent?.fachkompetenz ? this.dailyPlanningEvent.fachkompetenz : ''],
      fachkompetenzBegruendung: [this.dailyPlanningEvent?.fachkompetenzBegruendung ? this.dailyPlanningEvent.fachkompetenzBegruendung : ''],
      methodenkompetenz: [this.dailyPlanningEvent?.methodenkompetenz ? this.dailyPlanningEvent.methodenkompetenz : ''],
      methodenkompetenzBegruendung: [this.dailyPlanningEvent?.methodenkompetenzBegruendung ? this.dailyPlanningEvent.methodenkompetenzBegruendung : ''],
      selbstkompetenz: [this.dailyPlanningEvent?.selbstkompetenz ? this.dailyPlanningEvent.selbstkompetenz : ''],
      selbstkompetenzBegruendung: [this.dailyPlanningEvent?.selbstkompetenzBegruendung ? this.dailyPlanningEvent.selbstkompetenzBegruendung : ''],
      sozialkompetenz: [this.dailyPlanningEvent?.sozialkompetenz ? this.dailyPlanningEvent.sozialkompetenz : ''],
      sozialkompetenzBegruendung: [this.dailyPlanningEvent?.sozialkompetenzBegruendung ? this.dailyPlanningEvent.sozialkompetenzBegruendung : ''],
      usedTime: [this.dailyPlanningEvent?.usedTime ? this.dailyPlanningEvent. usedTime : 0, Validators.required],
      isTaskFinished: [{value: this.dailyPlanningEvent?.isTaskFinished ? this.dailyPlanningEvent.isTaskFinished : false, disabled: this.isReadonly}, Validators.required],
    } );
  }

}
