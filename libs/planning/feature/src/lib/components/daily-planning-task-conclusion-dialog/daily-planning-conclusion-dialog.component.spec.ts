import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPlanningTaskConclusionDialogComponent } from './daily-planning-task-conclusion-dialog.component';

describe('DailyPlanningConclusionDialogComponent', () => {
  let component: DailyPlanningTaskConclusionDialogComponent;
  let fixture: ComponentFixture<DailyPlanningTaskConclusionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DailyPlanningTaskConclusionDialogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DailyPlanningTaskConclusionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
