import { Component, Input, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'itag-berufsbildung-daily-planning-task-competence-form',
  templateUrl: './daily-planning-task-competence-form.component.html',
  styleUrls: ['./daily-planning-task-competence-form.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class DailyPlanningTaskCompetenceFormComponent {
  _reason!: FormControl;
  _competence!: FormControl;

  @Input()
  set reasonControl( value: AbstractControl | null ) {
    if ( value ) {
      this._reason = value as FormControl;
    }
  }

  @Input()
  set competenceControl( value: AbstractControl | null ) {
    if ( value ) {
      this._competence = value as FormControl;
    }
  }

  @Input()
  title!: string;

  @Input()
  competences!: string[];

  @Input()
  isReadonly!: boolean;

}
