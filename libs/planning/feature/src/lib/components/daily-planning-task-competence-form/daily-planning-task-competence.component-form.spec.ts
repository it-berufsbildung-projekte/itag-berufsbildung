import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPlanningTaskCompetenceFormComponent } from './daily-planning-task-competence-form.component';

describe('DailyPlanningTaskCompetenceComponent', () => {
  let component: DailyPlanningTaskCompetenceFormComponent;
  let fixture: ComponentFixture<DailyPlanningTaskCompetenceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DailyPlanningTaskCompetenceFormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DailyPlanningTaskCompetenceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
