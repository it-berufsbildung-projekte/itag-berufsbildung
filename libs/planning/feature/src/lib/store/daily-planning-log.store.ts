import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DailyPlanningLogDto, ModuleConstants } from '@itag-berufsbildung/planning/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { map, Observable, switchMap } from 'rxjs';

export interface IDailyPlanningLogState {
  dailyPlanningLogs: DailyPlanningLogDto[];
}

@Injectable()
export class DailyPlanningLogStore extends ComponentStore<IDailyPlanningLogState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: IDailyPlanningLogState;

  constructor(private readonly http: HttpClient, private readonly notificationService: NotificationService, private readonly userService: UserService) {
    super({
      dailyPlanningLogs: [],
    });

    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          const moduleInfo = user.moduleInfoArr.find((module) => module.moduleName === ModuleConstants.instance.moduleName);
          this.apiUrl = moduleInfo?.apiUrl as string;

          this.load();
        }
      },
    });
  }

  readonly dailyPlanningLogs$: Observable<DailyPlanningLogDto[]> = this.select((state) => state.dailyPlanningLogs);

  private load = this.effect((origin$: Observable<void>) =>
    origin$.pipe(
      switchMap(() => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningLog}`;
        return this.http.get<DailyPlanningLogDto[]>(url).pipe(
          map( (logs) => logs.map( (log) => {
            return {
              ...log,
              submissionDatePlan: log.submissionDatePlan ? new Date( log.submissionDatePlan ) : undefined,
              submissionDateConclusion: log.submissionDateConclusion ? new Date( log.submissionDateConclusion ) : undefined,
            };
          } ) )
        );
      }),
      tapResponse((dailyPlanningLogs: DailyPlanningLogDto[]) => this.patchState(() => ({ dailyPlanningLogs })), console.error)
    )
  );

  add(dailyPlanningLogDto: DailyPlanningLogDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningLog}`;
      this.http.post<DailyPlanningLogDto>(url, dailyPlanningLogDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      return { dailyPlanningLogs: [...state.dailyPlanningLogs, dailyPlanningLogDto] };
    });
  }

  update(dailyPlanningLogDto: DailyPlanningLogDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningLog}/${dailyPlanningLogDto.uuid}`;
      this.http.patch(url, dailyPlanningLogDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      const index = state.dailyPlanningLogs.findIndex((e) => e.uuid === dailyPlanningLogDto.uuid);
      const dailyPlanningLogs = [...state.dailyPlanningLogs];
      dailyPlanningLogs[index] = dailyPlanningLogDto;
      return { dailyPlanningLogs };
    });
  }

  delete(dailyPlanningLogDto: DailyPlanningLogDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningLog}/${dailyPlanningLogDto.uuid}`;
      this.http.delete(url).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });
      return { dailyPlanningLogs: state.dailyPlanningLogs.filter((e) => e.uuid !== dailyPlanningLogDto.uuid) };
    });
  }

  private revertChangesAndShowError(error: HttpErrorResponse): void {
    this.patchState(() => {
      this.notificationService.showError(error.message);
      return { ...this.oldState };
    });
  }
}
