import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnnualPlanningEventDto, EventCategoryDto, ModuleConstants } from '@itag-berufsbildung/planning/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { GlobalConstants, ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';
import { WebSocketService } from '@itag-berufsbildung/shared/data-ui';

export interface AnnualPlanningEventState {
  annualPlanningEvents: AnnualPlanningEventDto[];
  users: string[];
  categories: EventCategoryDto[];
}

@Injectable()
export class AnnualPlanningEventStore extends ComponentStore<AnnualPlanningEventState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: AnnualPlanningEventState;

  constructor(
    private readonly http: HttpClient,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    private readonly websocket: WebSocketService,
  ) {
    super(
      {
        annualPlanningEvents: [],
        users: [],
        categories: [],
      }
    );

    this.userService.userDto$.subscribe( {
      next: (user) => {
        if ( user ) {
          const moduleInfo = user.moduleInfoArr.find( module => module.moduleName === ModuleConstants.instance.moduleName );
          this.apiUrl = moduleInfo?.apiUrl as string;

          this.load();
        }
      }
    } );

    this.websocket.newDataArrived$.subscribe( {
      next: (payload) => {
        if (
          !payload
          || this.apiUrl.length < 1
          || (ModuleConstants.instance.documents.annualPlanningEvent !== payload.payload.documentName as string
          && ModuleConstants.instance.documents.projectTask !== payload.payload.documentName as string)
        ) {
          return;
        }
        this.load();
      }
    } );

  }

  readonly annualPlanningEvents$: Observable<AnnualPlanningEventDto[]> = this.select( state => state.annualPlanningEvents );
  readonly users$: Observable<string[]> = this.select( state => state.users );
  readonly categories$: Observable<EventCategoryDto[]> = this.select( state => state.categories );

  private load = this.effect( (origin$: Observable<void>) =>

    origin$.pipe(
      //events
      switchMap( () => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.annualPlanningEvent}`;
        return this.http.get<AnnualPlanningEventDto[]>( url );
      } ),
      tapResponse(
        (annualPlanningEvents: AnnualPlanningEventDto[]) => this.patchState( () => ( { annualPlanningEvents } ) ),
        console.error,
      ),
      // users/owners
      switchMap( () =>
        {
          const url = `${this.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.users}`;
          return this.http.get<string[]>(url);
        }
      ),
      tapResponse(
        (users: string[]) => this.patchState( () => ( { users } ) ),
        console.error,
      ),
      // categories
      switchMap( () =>
        this.http.get<EventCategoryDto[]>( `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.eventCategory}`),
      ),
      tapResponse(
        (categories: EventCategoryDto[])  => this.patchState( () => ( { categories } ) ),
        console.error,
      ),
    ),
  );

  addEvent( newEvent: AnnualPlanningEventDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.annualPlanningEvent}`;
      this.http.post<AnnualPlanningEventDto>( url, newEvent ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      return { annualPlanningEvents: [  ...state.annualPlanningEvents, newEvent ] };
    } );
  }

  updateEvent( updatedEvent: AnnualPlanningEventDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.annualPlanningEvent}/${updatedEvent.uuid}`;
      this.http.patch( url, updatedEvent ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      const index = state.annualPlanningEvents.findIndex( e => e.uuid === updatedEvent.uuid );
      const annualPlanningEvents = [  ...state.annualPlanningEvents ];
      annualPlanningEvents[index] = updatedEvent;
      return { annualPlanningEvents };
    } );
  }

  deleteEvent( deletedEvent: AnnualPlanningEventDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.annualPlanningEvent}/${deletedEvent.uuid}`;
      this.http.delete( url ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );
      return { annualPlanningEvents: state.annualPlanningEvents.filter( e => e.uuid !== deletedEvent.uuid ) };
    } );
  }

  private revertChangesAndShowError( error: HttpErrorResponse ): void {
    this.patchState( () => {
      this.notificationService.showError( error.message );
      return {...this.oldState};
    } );
  }

}
