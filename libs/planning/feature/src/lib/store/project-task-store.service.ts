import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';

export interface ProjectTaskState {
  modulesAndProjects: ProjectTaskDto[];
}

@Injectable()
export class ProjectTaskStore extends ComponentStore<ProjectTaskState> {
  private user!: UiUserDto;
  private apiUrl = '';
  private oldState!: ProjectTaskState;

  constructor(
    private readonly http: HttpClient,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    ) {
    super({
      modulesAndProjects: [],
    });

    this.userService.userDto$.subscribe( {
      next: value => {
        if ( value ) {
          this.user = value as UiUserDto;

          // set apiUrl to fetch modules and projects
          const moduleInfo = this.user.moduleInfoArr.find( module => module.moduleName===ModuleConstants.instance.moduleName );
          this.apiUrl = `${ moduleInfo?.apiUrl }/${ ModuleConstants.instance.documentNames.single.projectTask }`;

          this.load();
        }
      }
    } );


  }

  private load = this.effect( (origin$) =>
    origin$.pipe(
      switchMap( () => {
        return this.http.get<ProjectTaskDto[]>( this.apiUrl );
      } ),
      tapResponse(
        (modulesAndProjects: ProjectTaskDto[]) => this.patchState( () => ( { modulesAndProjects } ) ),
        console.error,
      ),
    ),
  );

  readonly projects$: Observable<ProjectTaskDto[]> = this.select( state => state.modulesAndProjects.filter( el => !el.isTask ) );
  readonly projectsAndTasks$: Observable<ProjectTaskDto[]> = this.select(
    state => state.modulesAndProjects.sort( (a, b) => new Date(a.deadlineDate).getTime() - new Date(b.deadlineDate).getTime() )
  );

  addProjectOrTask( projectTaskDto: ProjectTaskDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );
      this.http.post<ProjectTaskDto>( this.apiUrl, projectTaskDto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      return { modulesAndProjects: [ ...state.modulesAndProjects, projectTaskDto ] };
    } );
  }

  updateProjectOrTask( projectTaskDto: ProjectTaskDto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );
      const url = `${this.apiUrl}/${projectTaskDto.uuid}`;
      this.http.patch<ProjectTaskDto>( url, projectTaskDto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      const index = state.modulesAndProjects.findIndex( (p) => p.uuid === projectTaskDto.uuid);
      const modulesAndProjects = [...state.modulesAndProjects];
      modulesAndProjects[index] = projectTaskDto;

      return { modulesAndProjects };
    } );
  }

  private revertChangesAndShowError(error: HttpErrorResponse): void {
    this.patchState(() => {
      this.notificationService.showError(error.message);
      return { ...this.oldState };
    });
  }
}
