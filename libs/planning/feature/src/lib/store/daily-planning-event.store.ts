import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DailyPlanningEventDto, ModuleConstants } from '@itag-berufsbildung/planning/data';
import { WebSocketService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { map, Observable, switchMap } from 'rxjs';

export interface IDailyPlanningEventState {
  dailyPlanningEvents: DailyPlanningEventDto[];
}

@Injectable()
export class DailyPlanningEventStore extends ComponentStore<IDailyPlanningEventState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: IDailyPlanningEventState;
  user!: UiUserDto;
  private _selectedDate = new Date();
  private _selectedOwnerEmail = '';

  constructor(
    private readonly http: HttpClient,
    private readonly notificationService: NotificationService,
    private readonly userService: UserService,
    private readonly websocket: WebSocketService,
    ) {
    super({
      dailyPlanningEvents: [],
    });

    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          this.user = user;
          const moduleInfo = user.moduleInfoArr.find((module) => module.moduleName === ModuleConstants.instance.moduleName);
          this.apiUrl = moduleInfo?.apiUrl as string;
        }
      },
    });

    this.websocket.newDataArrived$.subscribe( {
      next: (payload) => {
        if (
          !payload
          || this.apiUrl.length < 1
          || (ModuleConstants.instance.documents.dailyPlanningEvent !== payload.payload.documentName as string
            && ModuleConstants.instance.documents.projectTask !== payload.payload.documentName as string
            && ModuleConstants.instance.documents.dailyPlanningLog !== payload.payload.documentName as string)
        ) {
          return;
        }
        this.load();
      }
    } );
  }

  readonly dailyPlanningEvents$: Observable<DailyPlanningEventDto[]> = this.select((state) => state.dailyPlanningEvents);

  private load = this.effect((origin$: Observable<void>) =>
    origin$.pipe(
      switchMap(() => {
        return this.loadEventsForDay();
      }),
      // tapResponse((dailyPlanningEvents: DailyPlanningEventDto[]) => this.patchState(() => ({ dailyPlanningEvents })), console.error)
      tapResponse((dailyPlanningEvents: DailyPlanningEventDto[]) => this.patchState(() => {
        return {
          dailyPlanningEvents,
        };
      }), console.error)
    )
  );

  add(dailyPlanningEventDto: DailyPlanningEventDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningEvent}`;
      this.http.post<DailyPlanningEventDto>(url, dailyPlanningEventDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      return { dailyPlanningEvents: [...state.dailyPlanningEvents, dailyPlanningEventDto] };
    });
  }

  update(dailyPlanningEventDto: DailyPlanningEventDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningEvent}/${dailyPlanningEventDto.uuid}/${dailyPlanningEventDto.start.toISOString()}/${dailyPlanningEventDto.ownerEmail}`;
      this.http.patch(url, dailyPlanningEventDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      const index = state.dailyPlanningEvents.findIndex((e) => e.uuid === dailyPlanningEventDto.uuid);
      const dailyPlanningEvents = [...state.dailyPlanningEvents];
      dailyPlanningEvents[index] = dailyPlanningEventDto;

      return { dailyPlanningEvents };
    });
  }

  delete(dailyPlanningEventDto: DailyPlanningEventDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningEvent}/${dailyPlanningEventDto.uuid}/${dailyPlanningEventDto.start.toISOString()}/${dailyPlanningEventDto.ownerEmail}`;
      this.http.delete(url).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });
      return { dailyPlanningEvents: state.dailyPlanningEvents.filter((e) => e.uuid !== dailyPlanningEventDto.uuid) };
    });
  }

  setSelectedDateAndOwner( selectedDate: Date, selectedOwnerEmail: string ): void {
    this._selectedDate = selectedDate;
    this._selectedOwnerEmail = selectedOwnerEmail;
    this.load();
  }

  private revertChangesAndShowError(error: HttpErrorResponse): void {
    this.patchState(() => {
      this.notificationService.showError(error.message);
      return { ...this.oldState };
    });
  }

  private loadEventsForDay(): Observable<DailyPlanningEventDto[]> {
    const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.dailyPlanningEvent}/${this._selectedDate.getFullYear()}-${this._selectedDate.getMonth()+1}-${this._selectedDate.getDate()}/${this._selectedOwnerEmail}`;

    return this.http.get<DailyPlanningEventDto[]>(url).pipe(
      map( (events) => {
        return events.map( (event) => {
          return {
            ... event,
            start: new Date( event.start ),
            end: new Date( event.end ),
          };
        } );
      } )
    );
  }

}
