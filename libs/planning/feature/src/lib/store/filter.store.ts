import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EventCategoryDto, ModuleConstants, ProjectTaskDto } from '@itag-berufsbildung/planning/data';
import { WebSocketService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { GlobalConstants } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, of, switchMap, tap } from 'rxjs';

export interface CategoryFilterState {
  categories: EventCategoryDto[];
  activeCategories: string[];
  owners: string[];
  activeOwners: string[];
  projectStates: string[];
  activeProjectStates: string[];
  projects: ProjectTaskDto[];
  activeProjects: string[];
}

@Injectable()
export class FilterStore extends ComponentStore<CategoryFilterState> {
  private apiUrl = '';
  private categories: EventCategoryDto[] = [];
  private projects: ProjectTaskDto[] = [];
  private projectStates: string[] = [];
  private _projectOwner: string | undefined;
  owners: string[] = [];

  constructor(
    private readonly http: HttpClient,
    private readonly userService: UserService,
    private readonly localStore: LocalStoreService,
    private readonly websocket: WebSocketService,
  ) {
    super({
      categories: [],
      activeCategories: [],
      owners: [],
      activeOwners: [],
      projectStates: [],
      activeProjectStates: [],
      projects: [],
      activeProjects: [],
    });

    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          const moduleInfo = user.moduleInfoArr.find((module) => module.moduleName === ModuleConstants.instance.moduleName);
          this.apiUrl = `${moduleInfo?.apiUrl}`;

          this.load();
        }
      },
    });

    this.websocket.newDataArrived$.subscribe( {
      next: (payload) => {
        if (
          !payload
          || this.apiUrl.length < 1
          || ModuleConstants.instance.documents.projectTask !== payload.payload.documentName as string
        ) {
          return;
        }
        this.load();
      }
    } );
  }

  private load = this.effect((origin$) =>
    origin$.pipe(
      // event categories
      switchMap(() => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.eventCategory}`;
        return this.http.get<EventCategoryDto[]>(url);
      }),
      tapResponse(
        (categories: EventCategoryDto[]) =>
          this.patchState(() => {
            this.categories = categories;
            return { categories };
          }),
        console.error
      ),
      // active event categories from local store
      switchMap(() => {
        const activeCategories = this.localStore.get(ModuleConstants.instance.localStoreKeys.annualPlanningActiveCategories);
        if (!activeCategories) {
          const categoryUuids = this.categories.map((c) => c.uuid);
          this.localStore.set(ModuleConstants.instance.localStoreKeys.annualPlanningActiveCategories, JSON.stringify(categoryUuids));
          return of(categoryUuids);
        }
        return of(JSON.parse(activeCategories));
      }),
      tapResponse((activeCategories: string[]) => this.patchState(() => ({ activeCategories })), console.error),
      // owners (owners are users)
      switchMap(() => {
        const url = `${this.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.users}`;
        return this.http.get<string[]>(url);
      }),
      tapResponse(
        (owners: string[]) =>
          this.patchState(() => {
            this.owners = owners;
            return { owners };
          }),
        console.error
      ),
      // active owners from local store
      switchMap(() => {
        const activeOwners = this.localStore.get(ModuleConstants.instance.localStoreKeys.annualPlanningActiveOwners);
        if (!activeOwners) {
          this.localStore.set(ModuleConstants.instance.localStoreKeys.annualPlanningActiveOwners, JSON.stringify(this.owners));
          return of(this.owners);
        }
        return of(JSON.parse(activeOwners));
      }),
      tapResponse((activeOwners: string[]) => this.patchState(() => ({ activeOwners })), console.error),
      // project states
      switchMap(() => {
        return of(Object.values(ModuleConstants.instance.moduleProjectStates));
      }),
      tapResponse(
        (projectStates: string[]) =>
          this.patchState(() => {
            this.projectStates = projectStates;
            return { projectStates };
          }),
        console.error
      ),

      // activeProjectStates
      switchMap(() => {
        const activeProjectStates = this.localStore.get(ModuleConstants.instance.localStoreKeys.moduleProjectActiveStates);
        if (!activeProjectStates) {
          this.localStore.set(ModuleConstants.instance.localStoreKeys.moduleProjectActiveStates, JSON.stringify(this.projectStates));
          return of(this.projectStates);
        }
        return of(JSON.parse(activeProjectStates));
      }),
      tapResponse((activeProjectStates: string[]) => this.patchState(() => ({ activeProjectStates })), console.error),
      // projects
      tap(() => this.loadProjects())
    )
  );

  // hint: Part project and active project is outsourced because they must get fetched again after a new project has been added to show it in the filter.
  private loadProjects = this.effect((origin$) =>
    origin$.pipe(
      // projects
      switchMap(() => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.projectTask}`;
        return this.http.get<ProjectTaskDto[]>(url);
      }),
      tapResponse(
        (projects: ProjectTaskDto[]) =>
          this.patchState(() => {
            this.projects = projects;
            return { projects };
          }),
        console.error
      ),
      switchMap(() => {
        const activeProjects = this.localStore.get(ModuleConstants.instance.localStoreKeys.moduleProjectActiveProjects);
        if (!activeProjects) {
          const projectNames = this.projects.map((p) => p.name);
          this.localStore.set(ModuleConstants.instance.localStoreKeys.moduleProjectActiveProjects, JSON.stringify(projectNames));
          return of(projectNames);
        }
        return of(JSON.parse(activeProjects));
      }),
      tapResponse((activeProjects: string[]) => this.patchState(() => ({ activeProjects })), console.error)
    )
  );

  readonly categories$: Observable<EventCategoryDto[]> = this.select((state) => state.categories);
  readonly owners$: Observable<string[]> = this.select((state) => state.owners);
  readonly projects$: Observable<ProjectTaskDto[]> = this.select((state) => state.projects);
  readonly filteredProjects$: Observable<ProjectTaskDto[]> = this.select((state) => {
    const projects = state.projects.filter( (p) => !p.isTask );

    // if the ownerEmail is set
    return this._projectOwner ? projects.filter( p => p.ownerEmail.includes( this._projectOwner as string ) || p.name === 'Allgemein' ) : projects;
  } );
  readonly projectStates$: Observable<string[]> = this.select((state) => state.projectStates);
  readonly activeCategories$: Observable<string[]> = this.select((state) => state.activeCategories);
  readonly activeOwners$: Observable<string[]> = this.select((state) => state.activeOwners);
  readonly activeProjects$: Observable<string[]> = this.select((state) => state.activeProjects);
  readonly activeProjectStates$: Observable<string[]> = this.select((state) => state.activeProjectStates);

  set projectOwner(projectOwner: string) {
    this._projectOwner = projectOwner;
  }

  reloadProjects(): void {
    this.loadProjects();
  }

  /**
   * Overwrites all selected categories in the local store and state. If an empty array is passed as the argument, all selected categories will be removed.
   * @param selectedCategories
   */
  saveActiveCategories(selectedCategories: EventCategoryDto[]): void {
    this.patchState(() => {
      const activeCategories = selectedCategories.map((c) => c.uuid as string);
      this.localStore.set(ModuleConstants.instance.localStoreKeys.annualPlanningActiveCategories, JSON.stringify(activeCategories));
      return { activeCategories };
    });
  }

  /**
   * Overwrites all selected owners in the local store and state. If an empty array is passed as the argument, all selected owners will be removed.
   * @param selectedOwners
   */
  saveActiveOwners(selectedOwners: string[]): void {
    this.patchState(() => {
      this.localStore.set(ModuleConstants.instance.localStoreKeys.annualPlanningActiveOwners, JSON.stringify(selectedOwners));
      return { activeOwners: selectedOwners };
    });
  }

  saveActiveProjects(selectedProjects: ProjectTaskDto[]): void {
    this.patchState(() => {
      const activeProjectUuids = selectedProjects.map((p) => p.uuid as string);
      this.localStore.set(ModuleConstants.instance.localStoreKeys.moduleProjectActiveProjects, JSON.stringify(activeProjectUuids));
      return { activeProjects: activeProjectUuids };
    });
  }

  saveActiveProjectStates(selectedProjectStates: string[]): void {
    this.patchState(() => {
      this.localStore.set(ModuleConstants.instance.localStoreKeys.moduleProjectActiveStates, JSON.stringify(selectedProjectStates));
      return { activeProjectStates: selectedProjectStates };
    });
  }
}
