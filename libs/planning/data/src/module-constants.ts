import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
        canCreate: 'api-generic-can-create',
      },
      annualPlanningEvent: {
        canCreate: 'api-annual-planning-can-create',
        canModifyDelete: 'api-annual-planning-can-modify-delete',
        canRead: 'api-annual-planning-can-read',
        canRemove: 'api-annual-planning-can-remove',
        canShowDeleted: 'api-annual-planning-can-show-deleted',
        canUpdate: 'api-annual-planning-can-update',
        canSeeHistory: 'api-annual-planning-can-see-history',
      },
      dailyPlanningEvent: {
        canCreate: 'api-daily-planning-event-can-create',
        canModifyDelete: 'api-daily-planning-event-can-modify-delete',
        canRead: 'api-daily-planning-event-can-read',
        canRemove: 'api-daily-planning-event-can-remove',
        canShowDeleted: 'api-daily-planning-event-can-show-deleted',
        canUpdate: 'api-daily-planning-event-can-update',
        canSeeHistory: 'api-daily-planning-event-can-see-history',
      },
      dailyPlanningLog: {
        canCreate: 'api-daily-planning-log-event-can-create',
        canModifyDelete: 'api-daily-planning-log-event-can-modify-delete',
        canRead: 'api-daily-planning-log-event-can-read',
        canRemove: 'api-daily-planning-log-event-can-remove',
        canShowDeleted: 'api-daily-planning-log-event-can-show-deleted',
        canUpdate: 'api-daily-planning-log-event-can-update',
        canSeeHistory: 'api-daily-planning-log-event-can-see-history',
      },
      projectTask: {
        canCreate: 'api-project-task-can-create',
        canModifyDelete: 'api-project-task-can-modify-delete',
        canRead: 'api-project-task-can-read',
        canReadOnlyOwnProjectTasks: 'api-project-task-can-read_only-own-project-tasks',
        canRemove: 'api-project-task-can-remove',
        canShowDeleted: 'api-project-task-can-show-deleted',
        canUpdate: 'api-project-task-can-update',
        canSeeHistory: 'api-project-task-can-see-history',
      },
      eventCategory: {
        canCreate: 'api-event-category-can-create',
        canModifyDelete: 'api-event-category-can-modify-delete',
        canRead: 'api-event-category-can-read',
        canRemove: 'api-event-category-can-remove',
        canShowDeleted: 'api-event-category-can-show-deleted',
        canUpdate: 'api-event-category-can-update',
        canSeeHistory: 'api-event-category-can-see-history',
      },
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
      canShowAnnualPlanning: 'menu-can-show-annual-planning',
      canShowProjectTask: 'menu-can-show-project-task',
      canShowEventCategory: 'menu-can-show-event-category',
      canShowDailyPlanning: 'menu-can-show-daily-planning',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
      annualPlanningEvent: {
        canCreate: 'ui-annual-planning-can-create',
        canCreateOwnEvent: 'ui-annual-planning-can-create-own-event',
        canCreateEventForOthers: 'ui-annual-planning-can-create-event-for-others',
        canModifyDelete: 'ui-annual-planning-can-modify-delete',
        canDeleteOwnEvent: 'ui-annual-planning-can-delete-own-event',
        canDeleteEventForOthers: 'ui-annual-planning-can-delete-event-for-others',
        canRead: 'ui-annual-planning-can-read',
        canRemove: 'ui-annual-planning-can-remove',
        canShowDeleted: 'ui-annual-planning-can-show-deleted',
        canUpdate: 'ui-annual-planning-can-update',
        canUpdateOwnEvent: 'ui-annual-planning-can-update-own-event',
        canUpdateEventForOthers: 'ui-annual-planning-can-update-event-for-others',
        canSeeHistory: 'ui-annual-planning-can-see-history',
        canSeeProjectTaskList: 'ui-annual-planning-can-see-project-task-list',
      },
      dailyPlanningEvent: {
        canCreate: 'ui-daily-planning-event-can-create',
        canUpdateState: 'ui-daily-planning-event-can-update-state',
        canModifyDelete: 'ui-daily-planning-event-can-modify-delete',
        canRead: 'ui-daily-planning-event-can-read',
        canReadOnlyOwnEvents: 'ui-daily-planning-event-can-read-only-own-events',
        canRemove: 'ui-daily-planning-event-can-remove',
        canShowDeleted: 'ui-daily-planning-event-can-show-deleted',
        canUpdate: 'ui-daily-planning-event-can-update',
        canSeeHistory: 'ui-daily-planning-event-can-see-history',
      },
      dailyPlanningLog: {
        canCreate: 'ui-daily-planning-log-event-can-create',
        canModifyDelete: 'ui-daily-planning-log-event-can-modify-delete',
        canRead: 'ui-daily-planning-log-event-can-read',
        canRemove: 'ui-daily-planning-log-event-can-remove',
        canShowDeleted: 'ui-daily-planning-log-event-can-show-deleted',
        canUpdate: 'ui-daily-planning-log-event-can-update',
        canSeeHistory: 'ui-daily-planning-log-event-can-see-history',
      },
      projectTask: {
        canCreate: 'ui-project-task-can-create',
        canCreateForOthers: 'ui-project-task-can-create-for-others',
        canModifyDelete: 'ui-project-task-can-modify-delete',
        canRead: 'ui-project-task-can-read',
        canReadOnlyOwnProjectTasks: 'ui-project-task-can-read_only-own-project-tasks',
        canRemove: 'ui-project-task-can-remove',
        canShowDeleted: 'ui-project-task-can-show-deleted',
        canUpdate: 'ui-project-task-can-update',
        canSeeHistory: 'ui-project-task-can-see-history',
      },
      eventCategory: {
        canCreate: 'ui-event-category-can-create',
        canModifyDelete: 'ui-event-category-can-modify-delete',
        canRead: 'ui-event-category-can-read',
        canRemove: 'ui-event-category-can-remove',
        canShowDeleted: 'ui-event-category-can-show-deleted',
        canUpdate: 'ui-event-category-can-update',
        canSeeHistory: 'ui-event-category-can-see-history',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
    bb: 'bb',
    pb: 'pb',
    ab: 'ab',
  };

  readonly competences = {
    fachkompetenz: {
      name: 'Fachkompetenz',
      competences: [
        'Ausbildungsstand',
        'Arbeitsqualität',
        'Arbeitsmenge, Arbeitstempo',
        'Umsetzung der Berufskenntnisse',
      ],
    },
    methodenkompetenz: {
      name: 'Methodenkompetenz',
      competences: [
        'Arbeitstechnik',
        'Vernetztes Denken und Handeln',
        'Umgang mit Mitteln, Betriebseinrichtungen',
        'Lern- und Arbeitsstrategie',
      ],
    },
    sozialkompetenz: {
      name: 'Sozialkompetenz',
      competences: [
        'Teamfähigkeit, Konfliktfähigkeit',
        'Zusammenarbeit',
        'Information und Kommunikation',
        'Kundenorientiertes Handeln',
      ],
    },
    selbstkompetenz: {
      name: 'Selbstkompetenz',
      competences: [
        'Selbstständigkeit, Eigenverantwortung',
        'Zuverlässigkeit, Belastbarkeit',
        'Umgangsformen',
        'Motivation',
      ],
    },
  };

  readonly description = 'Beschreibung des planning Modules';

  readonly documentNames = {
    single: {
      annualPlanningEvent: 'annualPlanningEvent',
      dailyPlanningEvent: 'dailyPlanningEvent',
      dailyPlanningLog: 'dailyPlanningLog',
      projectTask: 'projectTask',
      eventCategory: 'eventCategory',
    },
    plural: {},
  };

  readonly documents = {
    annualPlanningEvent: 'annualPlanningEvent',
    dailyPlanningEvent: 'dailyPlanningEvent',
    dailyPlanningLog: 'dailyPlanningLog',
    projectTask: 'projectTask',
    eventCategory: 'eventCategory',
  };

  readonly localStoreKeys = {
    annualPlanningActiveCategories: 'annualPlanningActiveCategories',
    annualPlanningActiveOwners: 'annualPlanningActiveOwners',
    moduleProjectActiveStates: 'moduleProjectActiveStates',
    moduleProjectActiveProjects: 'moduleProjectActiveProjects',
  };

  readonly moduleName = 'Planning';

  readonly moduleProjectStates = {
    active:  'active',
    backlog: 'backlog',
    finished: 'finished',
    closed: 'closed',
  };

  readonly routing = {
    base: 'planning',
    sub: {
      annualPlanning: 'annual',
      dailyPlanning: 'daily',
    },
  };

  readonly version = '1.0.0';
}
