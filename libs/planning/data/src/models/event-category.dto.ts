import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class EventCategoryDto extends BaseDocumentDto {
  backgroundColor = 'lightgreen';
  isAbCategory = false;
  isBusinessCategory = false;
  isGroupCategory = false;
  name = 'name of category';
  textColor = 'black';
  weight = 1000;

  constructor() {
    super();
  }
}
