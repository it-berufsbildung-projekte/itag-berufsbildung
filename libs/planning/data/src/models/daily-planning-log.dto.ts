import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class DailyPlanningLogDto extends BaseDocumentDto {
  closed = false;
  conclusion = 'Der Tagesabschluss';
  feedback = 'Feedback zum Tagesabschluss vom PB';
  ownerEmail = 'owner@email.ch';
  submissionDateConclusion: Date | undefined = new Date();
  submissionDatePlan: Date | undefined = new Date();

  constructor() {
    super();
  }
}
