import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ProjectTaskDto extends BaseDocumentDto {
  deadlineDate = new Date();
  isTask = true;
  name = 'ModuleProject example Name';
  ownerEmail: string[] = ['example@ag.ch'];
  parentProjectUuid = 'abc';
  state = 'backlog';
  timeBudgetHours = 40;

  constructor() {
    super();
  }
}
