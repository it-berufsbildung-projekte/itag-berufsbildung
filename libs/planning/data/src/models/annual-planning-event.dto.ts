import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';
import { EventCategoryDto } from './event-category.dto';

export class AnnualPlanningEventDto extends BaseDocumentDto {
  creatorEmail = 'email@email.ch';
  description = 'AnnualPlanningEvent example description';
  end: Date = new Date();
  eventCategory: EventCategoryDto = {
    backgroundColor: 'lightgreen',
    history: [],
    isAbCategory: false,
    isBusinessCategory: true,
    isGroupCategory: false,
    isDeleted: false,
    name: 'Betrieb',
    textColor: 'black',
    uuid: 'id1',
    weight: 3000,
  };
  eventCategoryUuid = '1';
  isAllDay = true;
  ownerCategoryKey = 'email@email.ch|categoryUuid';
  projectTaskUuid: string | undefined = 'abc';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  recurrenceException?: any;
  recurrenceId?: string;
  recurrenceRule?: string;
  start: Date = new Date();
  title = 'AnnualPlanningEvent example title';

  constructor() {
    super();
  }
}
