import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';
import { ProjectTaskDto } from './project-task.dto';

export class DailyPlanningEventDto extends BaseDocumentDto {
  description = 'DailyPlanningEvent example description';
  end: Date = new Date();
  fachkompetenz = 'Ausbildungsstand';
  fachkompetenzBegruendung = 'Begründung, wie die Fachkompetenz erreicht wurde.';
  isAllDay = false;
  isTaskClosed = false;
  isTaskFinished = false;
  methodenkompetenz = 'Arbeitstechnik';
  methodenkompetenzBegruendung = 'Begründung, wie die Methodenkompetenz erreicht wurde.';
  ownerEmail = 'email@email.ch';
  selbstkompetenz = 'Motivation';
  selbstkompetenzBegruendung = 'Begründung, wie die Selbstkompetenz erreicht wurde.';
  sozialkompetenz = 'Zusammenarbeit';
  sozialkompetenzBegruendung = 'Begründung, wie die Sozialkompetenz erreicht wurde.';
  start: Date = new Date();
  task: ProjectTaskDto | undefined = undefined;
  taskConclusion = 'taskConclusion';
  title = 'DailyPlanningEvent example title';
  usedTime = 60;

  constructor() {
    super();
  }
}
