import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { FullNameFromEmailPipe } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { DatePickerModule, DateTimePickerModule } from '@progress/kendo-angular-dateinputs';
import { DialogModule, WindowModule } from '@progress/kendo-angular-dialog';
import { DropDownListModule } from '@progress/kendo-angular-dropdowns';
import { EditorModule } from '@progress/kendo-angular-editor';
import { IconModule } from '@progress/kendo-angular-icons';
import { LabelModule } from '@progress/kendo-angular-label';
import { ListViewModule } from '@progress/kendo-angular-listview';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { map } from 'rxjs/operators';
import { AbListComponent } from './components/ab-list/ab-list.component';
import { AnswerListComponent } from './components/answer-list/answer-list.component';
import { AnswerComponent } from './components/answer/answer.component';
import { FreeTextComponent } from './components/free-text/free-text.component';
import { GoalReviewComponent } from './components/goal-review/goal-review.component';
import { GoalComponent } from './components/goal/goal.component';
import { NoteFeedbackListComponent } from './components/notes-and-feedback/note-feedback-list/note-feedback-list.component';
import { NoteFeedbackComponent } from './components/notes-and-feedback/note-feedback/note-feedback.component';
import { ReportDetailComponent } from './components/report-detail/report-detail.component';
import { ReportsListComponent } from './components/reports-list/reports-list.component';
import { ReportsPdfModule } from './components/reports-pdf/reports-pdf.module';
import { ReportsEffects } from './components/reports.effects';
import { reportsReducer } from './components/reports.reducers';
import { TrialPeriodEndComponent } from './components/trial-period-end/trial-period-end.component';

import { FeatureReportRouterModule } from './modules/feature-report-router.module';
import { ReportsComponent } from './components/reports/reports.component';
import { RadioButtonModule, TextAreaModule, TextBoxModule } from '@progress/kendo-angular-inputs';
import { ReactiveFormsModule } from '@angular/forms';
import { CardModule, ExpansionPanelModule, StepperModule, TileLayoutModule } from '@progress/kendo-angular-layout';
import { ReportsResolver } from './resolver/reports.resolver';
import { MailService } from './service/mail.service';
import { ReportAnswerService } from './service/report-answer.service';
import { ReportTypeService } from './service/report-type.service';
import { ReportService } from './service/report.service';
import { WebSocketReportService } from './service/web-socket-report.service';

const websocketReportServiceFactory = (ls: LocalStoreService, ans: NotificationService, user: UserService) => {
  const apiUrl$ = user.userDto$.pipe(
    map( (user) => {
      if (user) {
        const moduleInfo = user.moduleInfoArr.find( ( m ) => m.moduleName === ModuleConstants.instance.moduleName );
        return moduleInfo?.apiUrl as string;
      }
      return '';
    })
  );

  return new WebSocketReportService(ls, ans, '', apiUrl$);
};

@NgModule({
  imports: [
    CommonModule,
    FeatureReportRouterModule,
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
    StoreModule.forFeature( 'reports', reportsReducer ),
    EffectsModule.forFeature([ReportsEffects]),
    TextBoxModule,
    ListViewModule,
    ReactiveFormsModule,
    LabelModule,
    TextAreaModule,
    DialogModule,
    ButtonModule,
    TooltipModule,
    RadioButtonModule,
    CardModule,
    StepperModule,
    TileLayoutModule,
    EditorModule,
    ExpansionPanelModule,
    DropDownListModule,
    IconModule,
    DateTimePickerModule,
    WindowModule,
    DatePickerModule,
    ReportsPdfModule,
    NoteFeedbackComponent,
    NoteFeedbackListComponent,
    FullNameFromEmailPipe,
  ],
  declarations: [
    ReportsComponent,
    ReportsListComponent,
    AbListComponent,
    AnswerListComponent,
    AnswerComponent,
    GoalComponent,
    TrialPeriodEndComponent,
    ReportDetailComponent,
    FreeTextComponent,
    GoalReviewComponent,
  ],
  providers: [
    ReportService,
    ReportTypeService,
    ReportAnswerService,
    MailService,
    ReportsResolver,
    WebSocketReportService,
    {
      deps: [LocalStoreService, NotificationService, UserService],
      provide: WebSocketReportService,
      useFactory: websocketReportServiceFactory,
    },
  ],
})
export class FeatureReportModule {}
