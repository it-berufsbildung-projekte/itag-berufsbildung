import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { WebSocketPayload } from '@itag-berufsbildung/shared/feature-web-socket';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ReportsActions } from '../action-types-reports';
import { getReportsInfoStateReports } from '../components/reports.selectors';
import { WebSocketReportService } from '../service/web-socket-report.service';

@Injectable()
export class ReportsResolver implements Resolve<boolean> {

  constructor(
    private readonly store: Store,
    private readonly userService: UserService,
    private readonly websocket: WebSocketReportService,
  ) {
    this.websocket.newDataArrived$.subscribe( (payload) => {
      if ( !payload
        || (payload.payload.documentName !== ModuleConstants.instance.documents.report && payload.payload.documentName !== ModuleConstants.instance.documents.reportAnswer)
      ) {
        return;
      }

      this.reloadData( payload );
    });
  }

  resolve(): Observable<boolean> | Promise<boolean> | boolean {
    this.loadData();
    return true;
  }

  private reloadData( wsPayload: WebSocketPayload ): void {
      if ( wsPayload.payload.documentName === ModuleConstants.instance.documents.report && wsPayload.payload?.data?.key ) {
        if ( this.isReportInStore( wsPayload.payload?.data?.key ) ) {
          this.store.dispatch( ReportsActions.loadOneReport( {id: wsPayload.payload?.data?.key } ) );
        }
      }
      // if the payload.data property is undefined, it means that all reports need to be loaded. This is the case when the bb creates all reports for an apprenticeship.
      else if ( wsPayload.payload.documentName === ModuleConstants.instance.documents.report ) {
        this.loadData();
      }
      else if ( wsPayload.payload.documentName === ModuleConstants.instance.documents.reportAnswer && wsPayload.payload.data ) {
        this.store.dispatch( ReportsActions.loadOneAnswer( {uuid: wsPayload.payload.data.key as string } ) );
      }
  }

  private loadData(): void {
    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {

        // extract the role names the user has
        const roleNames = Object.keys( user.uiRoles[ModuleConstants.instance.moduleName] );

        // load all reports for roles admin, bb and pb
        if ( roleNames.includes( ModuleConstants.instance.availableRoles.admin )
          || roleNames.includes( ModuleConstants.instance.availableRoles.bb )
          || roleNames.includes( ModuleConstants.instance.availableRoles.pb ) ) {
          this.store.dispatch( ReportsActions.loadReports() );
        }

        // only load the user's reports if the user has the role ab
        else if ( roleNames.includes( ModuleConstants.instance.availableRoles.ab ) ) {
          this.store.dispatch( ReportsActions.loadReportsPerUser( { userId: user.eMail, fieldName: 'abUserEmail' } ) );
          // this.store.dispatch( ReportsActions.loadReports() );
        }

        // always load the reportTypeDetailView
        this.store.dispatch( ReportsActions.loadReportTypeDetails() );
      }

    });
  }

  private isReportInStore( uuid: string ): boolean {
    let report;
    this.store.pipe( select( getReportsInfoStateReports ) ).subscribe({
      next: (reports) => {
        report = reports.find( r => r.uuid === uuid );
      }
    });

    return !!report;
  }

}
