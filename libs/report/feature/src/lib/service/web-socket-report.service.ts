import { Injectable } from '@angular/core';
import { WebSocketSyncDto } from '@itag-berufsbildung/report/data';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { WebSocketService } from '@itag-berufsbildung/shared/feature-web-socket';
import { BehaviorSubject } from 'rxjs';
import { Socket } from 'socket.io-client';

export interface WebSocketReportPayload {
  wsId: string;
  websocketSyncDto: WebSocketSyncDto;
}

@Injectable()
export class WebSocketReportService extends WebSocketService {

  reportStepChanged$: BehaviorSubject<WebSocketReportPayload | undefined> = new BehaviorSubject<WebSocketReportPayload | undefined>(undefined);
  reportShowAnswerChanged$: BehaviorSubject<WebSocketReportPayload | undefined> = new BehaviorSubject<WebSocketReportPayload | undefined>(undefined);

  override initWsExtend( socketClient: Socket ): void {
    socketClient.on( UiConstants.instance.customWebSocketEvents.onReportDiscussionStepChange, (payload: WebSocketReportPayload) => {
      this.notification.showInfo(UiConstants.instance.customWebSocketEvents.onReportDiscussionStepChange);
      if (this.wsId === payload.wsId) {
        // ignore if we are the sender
        return;
      }
      // now we react on the change of the entity...
      this.reportStepChanged$.next(payload);
    });

    socketClient.on( UiConstants.instance.customWebSocketEvents.onReportDiscussionShowAnswer, (payload: WebSocketReportPayload) => {
      this.notification.showInfo(UiConstants.instance.customWebSocketEvents.onReportDiscussionShowAnswer);
      if (this.wsId === payload.wsId) {
        // ignore if we are the sender
        return;
      }
      // now we react on the change of the entity...
      this.reportShowAnswerChanged$.next(payload);
    });
  }

}
