import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, ReportAnswerDto } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { Observable } from 'rxjs';

@Injectable()
export class ReportAnswerService {

  private _apiUrl = '';

  constructor(
    private readonly userService: UserService,
    private readonly http: HttpClient,
  ) {
    this.userService.userDto$.subscribe( ( user ) => {
      if ( user ) {
        const moduleInfo = user.moduleInfoArr.find( ( module ) => module.moduleName===ModuleConstants.instance.moduleName );
        this._apiUrl = moduleInfo?.apiUrl as string;
      }
    } );
  }

  loadOneAnswerDto( uuid: string ): Observable<ReportAnswerDto> {
    return this.http.get<ReportAnswerDto>( `${this._apiUrl}/reportAnswer/${uuid}` );
  }

  createReportAnswerDto( body: ReportAnswerDto ): Observable<ReportAnswerDto> {
    return this.http.post<ReportAnswerDto>( `${this._apiUrl}/reportAnswer`, body );
  }

  updateReportAnswerDto( uuid: string, body: ReportAnswerDto ): Observable<ReportAnswerDto> {
    return this.http.patch<ReportAnswerDto>( `${this._apiUrl}/reportAnswer/${uuid}`, body );
  }

}
