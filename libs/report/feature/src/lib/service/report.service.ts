import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, ReportDto, ReportsForApprenticeshipCreateDto, ReportTypeDetailViewDto, WebSocketSyncDto } from '@itag-berufsbildung/report/data';
import { MailDto } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ReportService {

  private _apiUrl = '';

  constructor(
    private readonly userService: UserService,
    private readonly http: HttpClient,
  ) {
    this.userService.userDto$.subscribe( (user) => {
      if( user ) {
        const moduleInfo = user.moduleInfoArr.find( (module) => module.moduleName === ModuleConstants.instance.moduleName );
        this._apiUrl = moduleInfo?.apiUrl as string;
      }
    });
  }

  createReportsForApprenticeship( body: ReportsForApprenticeshipCreateDto ): Observable<ReportDto[]> {
    return this.http.post<ReportDto[]>( `${this._apiUrl}/report/reportsForApprenticeship`, body ).pipe(
      map( (reports) => reports.map( (report) => (
        {
          ...report,
          answers: report.answers.map( (a) => (
            {...a, created: new Date( a.created ) }
          ) ) }
      ) ) ),
    );
  }

  createWebSocketSyncEvent( body: WebSocketSyncDto ): void {
    // hack: interestingly, the request doesn't reach the api without subscription...
    this.http.post( `${this._apiUrl}/report/socket`, body ).subscribe();
  }

  deleteReportDto( uuid: string ): Observable<ReportDto> {
    return this.http.delete<ReportDto>( `${this._apiUrl}/report/${uuid}` );
  }

  getReportDtoArrayFiltered( abUserEmail?: string ): Observable<ReportDto[]> {
    let url = `${this._apiUrl}/report`;
    if ( abUserEmail ) {
      url += `?abUserId=${abUserEmail}`;
    }

    return this.http.get<ReportDto[]>( url ).pipe(
      map( (reports) => reports.map( (report) => (
        {
          ...report,
          answers: report.answers.map( (a) => (
            {...a, created: new Date( a.created ) }
          ) ) }
      ) ) ),
    );
  }

  getReportDtoById( uuid: string ): Observable<ReportDto> {
    return this.http.get<ReportDto>( `${this._apiUrl}/report/${uuid}` ).pipe(
      map( (report) => (
        {
          ...report,
          answers: report.answers.map( (a) => (
            {...a, created: new Date( a.created ) }
          ) ) }
      ) ),
    );
  }

  reportTypeDetailViewDtoArr(): Observable<ReportTypeDetailViewDto[]> {
    return this.http.get<ReportTypeDetailViewDto[]>( `${this._apiUrl}/report/reportTypeDetailView` );
  }

  sendMail( body: MailDto ) {
    return this.http.post<void>( `${this._apiUrl}/report/sendMail`, body ).subscribe();
  }

  updateReportDto( uuid: string, body: ReportDto ): Observable<ReportDto> {
    // clear the answers array to avoid getting a massive history
    body = {...body, answers: []};
    // body.answers = [];

    return this.http.patch<ReportDto>( `${this._apiUrl}/report/${uuid}`, body ).pipe(
      map( (report) => (
        {
          ...report,
          answers: report.answers.map( (a) => (
            {...a, created: new Date( a.created ) }
          ) ) }
      ) ),
    );
  }

}
