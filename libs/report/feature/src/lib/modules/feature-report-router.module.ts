import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { AnswerListComponent } from '../components/answer-list/answer-list.component';
import { NoteFeedbackPageComponent } from '../components/notes-and-feedback/note-feedback-page/note-feedback-page.component';
import { ReportDetailComponent } from '../components/report-detail/report-detail.component';
import { ReportPdfComponent } from '../components/reports-pdf/report-pdf.component';
import { ReportsComponent } from '../components/reports/reports.component';
import { ReportsResolver } from '../resolver/reports.resolver';

const routes: Routes = [
  {
    canActivate: [ ModuleRightGuard ],
    component: ReportsComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    resolve: {
      reports: ReportsResolver,
    },
    path: 'reports',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: ReportsComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/ab/:abId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: ReportsComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/ab/:abId/report/:reportId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: ReportDetailComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/ab/:abId/report-detail/:reportId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: AnswerListComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/abOrPb/:role/report/:reportId/answers',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: AnswerListComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/abOrPb/:role/report/:reportId/answers/:answerId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: AnswerListComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/report/:reportId/meeting',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: AnswerListComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'reports/report/:reportId/meeting/answer/:answerId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: ReportPdfComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'pdf/report/:reportId',
  },
  {
    canActivate: [ ModuleRightGuard ],
    component: NoteFeedbackPageComponent,
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    path: 'notes/:period/ab/:abEmail',
  },
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureReportRouterModule {}
