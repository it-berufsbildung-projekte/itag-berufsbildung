import { NgIf } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NoteFeedbackDto } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { WindowModule } from '@progress/kendo-angular-dialog';
import { DropDownListModule } from '@progress/kendo-angular-dropdowns';
import { FormFieldModule, TextAreaModule, TextBoxModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { v4 } from 'uuid';
import { NoteFeedbackStore } from '../../../store/note-feedback.store';

@Component( {
  standalone: true,
  selector: 'itag-berufsbildung-note-feedback',
  templateUrl: './note-feedback.component.html',
  styleUrls: [ './note-feedback.component.scss' ],
  encapsulation: ViewEncapsulation.Emulated,
  imports: [
    WindowModule,
    NgIf,
    TextBoxModule,
    ButtonModule,
    FormFieldModule,
    DropDownListModule,
    FormsModule,
    LabelModule,
    TextAreaModule,
  ],
})
export class NoteFeedbackComponent {

  @Input()
  abEmail!: string | null;

  pbEmail!: string;
  ratings: Array<string> = ["positiv", "neutral", "negativ"];
  isWindowOpen = false;

  rating = '';
  note = '';

  constructor(
    readonly noteFeedbackStore: NoteFeedbackStore,
    private readonly userService: UserService
  ) {
    this.userService.userDto$.subscribe( {
      next: (user) => {
        if ( user ) {
          this.pbEmail = user.eMail;
        }
      }
    } );
  }

  close( save?: boolean ): void {
    if ( save && this.abEmail ) {
      const newNote: NoteFeedbackDto = {
        abEmail: this.abEmail,
        date: new Date(),
        history: [],
        isDeleted: false,
        note: this.note,
        pbEmail: this.pbEmail,
        rating: this.rating,
        uuid: v4(),
      };

      this.noteFeedbackStore.add( newNote );
    }

    this.isWindowOpen = false;
  }

  openDialog(): void {
    this.isWindowOpen = true;
  }
}
