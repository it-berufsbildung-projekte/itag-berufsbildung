import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteFeedbackComponent } from './note-feedback.component';

describe('NoteFeedbackComponent', () => {
  let component: NoteFeedbackComponent;
  let fixture: ComponentFixture<NoteFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoteFeedbackComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NoteFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
