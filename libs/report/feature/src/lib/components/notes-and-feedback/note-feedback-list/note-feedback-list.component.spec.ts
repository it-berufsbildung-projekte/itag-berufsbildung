import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteFeedbackListComponent } from './note-feedback-list.component';

describe('NoteFeedbackListComponent', () => {
  let component: NoteFeedbackListComponent;
  let fixture: ComponentFixture<NoteFeedbackListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoteFeedbackListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NoteFeedbackListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
