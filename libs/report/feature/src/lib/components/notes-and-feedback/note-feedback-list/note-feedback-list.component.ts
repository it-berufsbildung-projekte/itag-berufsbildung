import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { NoteFeedbackDto } from '@itag-berufsbildung/report/data';
import { GridModule } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs';
import { NoteFeedbackStore } from '../../../store/note-feedback.store';

@Component( {
  standalone: true,
  selector: 'itag-berufsbildung-note-feedback-list',
  templateUrl: './note-feedback-list.component.html',
  styleUrls: [ './note-feedback-list.component.css' ],
  encapsulation: ViewEncapsulation.Emulated,
  imports: [
    GridModule,
    DatePipe,
  ],
})
export class NoteFeedbackListComponent implements OnInit {
  private notes$: Observable<NoteFeedbackDto[]> = this.noteFeedbackStore.noteFeedbacks$;

  @Input()
  period!: string;

  @Input()
  abEmail!: string | null;

  gridData: unknown[] = [];
  gridView!: unknown[];
  mySelection: string[] = [];

  constructor(
    private readonly noteFeedbackStore: NoteFeedbackStore,
  ) {}

  ngOnInit(): void {
    this.gridData = [];
    this.notes$.subscribe( {
      next: (notes) => {
        // convert the property date to a date object for each note
        notes = notes.map( n => ( {...n, date: new Date( n.date ) } ) );

        // filter notes from the injected period
        const period = this.getPeriodStartAndEnd();
        notes = notes.filter( (n) => n.abEmail === this.abEmail && (n.date as Date).getTime() >= period.start.getTime() && (n.date as Date).getTime() <= period.end.getTime() );

        this.gridData = notes;
        this.gridView = this.gridData;
      }
    } );
  }

  private getPeriodStartAndEnd(): {start: Date, end: Date} {
    // the period string is split here
    const periodSplit = this.period.split('-');

    // split and reverse both date strings to match the format 'yyyy-mm-dd'
    const startDateArr = periodSplit[0].split('.').reverse();
    const endDateArr = periodSplit[1].split('.').reverse();

    return { start: new Date( startDateArr.join('-') ), end: new Date( endDateArr.join('-') ) };
  }

}
