import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteFeedbackPageComponent } from './note-feedback-page.component';

describe('NoteFeedbackPageComponent', () => {
  let component: NoteFeedbackPageComponent;
  let fixture: ComponentFixture<NoteFeedbackPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoteFeedbackPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NoteFeedbackPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
