import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FullNameFromEmailPipe, ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { CardModule } from '@progress/kendo-angular-layout';
import { NoteFeedbackListComponent } from '../note-feedback-list/note-feedback-list.component';
import { NoteFeedbackComponent } from '../note-feedback/note-feedback.component';

@Component( {
  standalone: true,
  selector: 'itag-berufsbildung-note-feedback-page',
  templateUrl: './note-feedback-page.component.html',
  styleUrls: [ './note-feedback-page.component.css' ],
  encapsulation: ViewEncapsulation.Emulated,
  imports: [
    CardModule,
    ButtonModule,
    NoteFeedbackListComponent,
    NoteFeedbackComponent,
    FullNameFromEmailPipe,
  ],
})
export class NoteFeedbackPageComponent implements OnInit {

  @ViewChild('noteFeedback')
  noteFeedbackComponent!: NoteFeedbackComponent;

  abUserEmail!: string;
  period!: string;
  height = 600;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly screenInfoService: ScreenInfoService,
  ) {
    this.route.paramMap.subscribe( params => {
      this.period = params.get('period') || '';
      this.abUserEmail = params.get('abEmail') || '';
    });
  }

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });
  }

  onNavigateBack(): void {
    this.router.navigateByUrl(`/reports/reports/ab/${this.abUserEmail}`).then();
  }

  onWriteNote(): void {
    this.noteFeedbackComponent.openDialog();
  }

}
