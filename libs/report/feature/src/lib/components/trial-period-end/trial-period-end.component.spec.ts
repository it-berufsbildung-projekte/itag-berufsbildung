import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialPeriodEndComponent } from './trial-period-end.component';

describe('TrialPeriodEndComponent', () => {
  let component: TrialPeriodEndComponent;
  let fixture: ComponentFixture<TrialPeriodEndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrialPeriodEndComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialPeriodEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
