import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ReportAnswerDto, ReportDto } from '@itag-berufsbildung/report/data';
import { select, Store } from '@ngrx/store';
import { FormatSettings } from '@progress/kendo-angular-dateinputs';
import { getReportsInfoStateReports } from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-trial-period-end',
  styleUrls: ['./trial-period-end.component.scss'],
  templateUrl: './trial-period-end.component.html',
})
export class TrialPeriodEndComponent implements OnInit {

  @Input()
  reportUuid!: string;

  @Input()
  answerUuid!: string | undefined;

  @Input()
  readonly!: boolean;

  @Input()
  navigationActive!: boolean;

  @Input()
  isFirstStep!: boolean;

  @Input()
  isLastStep!: boolean;

  @Input()
  isDiscussion!: boolean;

  @Input()
  showAnswerToOtherRoleActive!: boolean;

  @Output()
  saveAnswerAndNext = new EventEmitter<{ updateDto: ReportAnswerDto, uuid: string }>();

  @Output()
  back = new EventEmitter();

  @Output()
  writeNote = new EventEmitter();

  @Output()
  navigateToReportsPage = new EventEmitter();

  @Output()
  showAnswerToOtherRole = new EventEmitter();

  answer!: ReportAnswerDto;
  report!: ReportDto;
  isWindowOpen = false;

  chosenOption = '';
  previousChosenOption!: string;
  reason = '';
  date: Date = new Date();

  endText = `Die Probezeit wird per {DATUM} erfolgreich abgeschlossen.`;
  extensionText = `Die Probezeit wird aufgrund {GRUND} bis zum {DATUM} verlängert (max. 3 Monate)`;
  dissolveText = `Der Lehrvertrag wird innerhalb oder am Ende der Probezeit durch Kündigung oder Vereinbarung aufgelöst
    (Siehe Handbuch, der Berufsbildung, Kapitel 3 / Anstellungsbedingungen. Detaillierte Arbeitsschritte siehe Prozess Lehrvertragsauflösung.)`;

  dateFormat: FormatSettings = {
    displayFormat: 'dd.MM.yyyy',
    inputFormat: 'dd.MM.yyyy',
  };

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
  ) { }

  ngOnInit(): void {
    this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
      this.report = reports.find( r => r.uuid === this.reportUuid ) as ReportDto;
      this.answer = this.report.answers.find( a => a.uuid === this.answerUuid ) as ReportAnswerDto;
    });

    // die Beschreibung in ihre Bestandteile auseinendernehmen
    let explanationSplit: string[] = [];
    if ( this.answer.pbExplanation ) {
      explanationSplit = this.answer.pbExplanation.split('¦');
    }

    if ( explanationSplit ) {
      // 0 = Abschluss, 1 = Verlängerung, 2 = Auflösung
      switch ( explanationSplit[0] ) {
        case '0':
          this.chosenOption = 'endText';
          this.endText = explanationSplit[1];
          break;
        case '1':
          this.chosenOption = 'extensionText';
          this.extensionText = explanationSplit[1];
          break;
        case '2':
          this.chosenOption = 'dissolveText';
          this.dissolveText = explanationSplit[1];
          break;
      }
    }

    this.previousChosenOption = this.chosenOption;
  }

  onSaveAnswerAndNext(): void {
    if ( !this.readonly ) {
      // 0 = Abschluss, 1 = Verlängerung, 2 = Auflösung
      let explanation;

      if ( this.chosenOption === 'endText' ) {
        explanation = '0¦' + this.endText;
      } else if ( this.chosenOption === 'extensionText' ) {
        explanation = '1¦' + this.extensionText;
      } else {
        // Wenn keine der beiden vorherigen Antworten gemacht wurde, dann den Text für die Auflösung nehmen
        explanation = '2¦' + this.dissolveText;
      }

      const updateDto: ReportAnswerDto = {
        uuid: this.answer.uuid,
        isPbAnswerComplete: true,
        pbExplanation: explanation,
      } as ReportAnswerDto;

      this.saveAnswerAndNext.emit( { updateDto, uuid: updateDto.uuid } );
    } else {
      // answerUpdate null übergeben, weil nicht gespeichert werden muss, sondern nur ein Step weiter
      this.saveAnswerAndNext.emit( undefined );
    }

    // Wenn es die letzte Frage ist, soll zurück auf die Seite Berichte navigiert werden, ausser es wird besprochen, dann soll nur gespeichert werden
    if ( this.isLastStep && !this.isDiscussion ) {
      this.navigateToReportsPage.emit();
    }

  }

  onBack(): void {
    this.back.emit();
  }

  onShowAnswerToOtherRole(): void {
    this.showAnswerToOtherRole.emit();
  }

  closeWindow(): void {
    this.chosenOption = this.previousChosenOption;
    this.isWindowOpen = false;
  }

  openWindow(): void {
    this.isWindowOpen = true;
  }

  onWriteNote(): void {
    this.writeNote.emit();
  }

  isFormValid(): boolean {
    return this.chosenOption !== '';
  }

  setDateAndReason(): void {
    if ( this.chosenOption === 'extensionText' ) {
      this.extensionText = `Die Probezeit wird aufgrund ${this.reason} bis zum ${this.date.toLocaleDateString()} verlängert (max. 3 Monate)`;
    } else {
      this.endText = `Die Probezeit wird per ${this.date.toLocaleDateString()} erfolgreich abgeschlossen.`;
    }

    this.previousChosenOption = this.chosenOption;
    this.isWindowOpen = false;
  }

}
