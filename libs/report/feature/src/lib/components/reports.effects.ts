import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { UiUserDto } from '@itag-berufsbildung/shared/util';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { concatMap, map, tap } from 'rxjs/operators';
import { ReportsActions } from '../action-types-reports';
import { MailService } from '../service/mail.service';
import { ReportAnswerService } from '../service/report-answer.service';
import { ReportTypeService } from '../service/report-type.service';
import { ReportService } from '../service/report.service';

@Injectable()
export class ReportsEffects {

  loadReportsPerUser$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.loadReportsPerUser ),
      concatMap( action => this.reportReportService.getReportDtoArrayFiltered( action.userId  )
        .pipe(
          map( reports => {
            // als Auszubildender direkt zu seinen eigenen Berichten navigieren damit seine user ID im URL ist
            // wenn in der URL 'answers' vorkommt, soll nicht auf die Übersichtsseite navigiert werden, weil der AB dann den Link in der Einladung geöffnet hat, um den
            // Bericht zu beantworten
            if ( this.user.hasRole( ModuleConstants.instance.moduleName, ModuleConstants.instance.availableRoles.ab ) && !this.router.url.includes('answers') ) {
              this.router.navigateByUrl( `reports/reports/ab/${ this.user.eMail }`).then();
            }
            return ReportsActions.loadReportsSuccess( { reports } );
          }),
        )),
    ),
  );

  loadReports$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.loadReports ),
      concatMap( () => this.reportReportService.getReportDtoArrayFiltered()
        .pipe(
          map( reports => ReportsActions.loadReportsSuccess( { reports } ) ),
        )),
    ),
  );

  loadOneReport$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.loadOneReport ),
      concatMap( action => this.reportReportService.getReportDtoById( action.id )
        .pipe(
          map( reportDto => ReportsActions.updateOrLoadOneReportSuccess( { reportDto } ) ),
        )),
    ),
  );

  loadReportTypeDetails$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.loadReportTypeDetails ),
      concatMap( () => this.reportReportService.reportTypeDetailViewDtoArr()
        .pipe(
          map( reportTypeDetails => ReportsActions.loadReportTypeDetailsSuccess( { reportTypeDetails } ) ),
        )),
    ),
  );

  updateReport$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.updateReport ),
      concatMap( action => this.reportReportService.updateReportDto( action.id, action.reportUpdateDto )
        .pipe(
          map( reportDto => ReportsActions.updateOrLoadOneReportSuccess( { reportDto } ) ),
        )),
    ),
  );

  loadOneAnswer$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.loadOneAnswer ),
      concatMap( action => this.reportAnswerService.loadOneAnswerDto( action.uuid )
        .pipe(
          map( reportAnswerDto => ReportsActions.updateAnswerSuccess( { reportAnswerDto } ) ),
        )),
    ),
  );

  updateAnswer$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.updateAnswer ),
      concatMap( action => this.reportAnswerService.updateReportAnswerDto( action.id, action.reportAnswerUpdateDto )
        .pipe(
          map( reportAnswerDto => ReportsActions.updateAnswerSuccess({reportAnswerDto})),
        )),
    ),
  );

  createReportsForApprenticeship$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.createReportsForApprenticeship ),
      concatMap( action => this.reportReportService.createReportsForApprenticeship( action.reportsForApprenticeshipCreateDto )
        .pipe(
          map( reportDtoArr => ReportsActions.createReportsForApprenticeshipSuccess( { reportDtoArr } ) ),
        )),
    ),
  );

  createAnswer$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.createAnswer ),
      concatMap( action => this.reportAnswerService.createReportAnswerDto( action.reportAnswerCreateDto )
        .pipe(
          map( reportAnswerDto => ReportsActions.createAnswerSuccess( { reportAnswerDto } ) ),
        )),
    ),
  );

  deleteReport$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.deleteReport ),
      concatMap( action => this.reportReportService.deleteReportDto( action.id )
        .pipe(
          map( reportDto => ReportsActions.deleteReportSuccess( { reportDto } ) ),
        )),
    ),
  );

  createWebsocketSyncEvent$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.createWebsocketSyncEvent ),
      // hack: aus irgendeinem Grund müssen wir das so lösen, damit die Anfrage zum Server kommt. Gibt bestimmt schönere Lösungen.
      tap( action => this.reportReportService.createWebSocketSyncEvent( action.websocket ) ),
    ),
    { dispatch: false },
  );

  sendMail$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.sendMail ),
      tap( action => this.reportReportService.sendMail( action.mailDto ) ),
    ),
    { dispatch: false },
  );

  saveShowAllReports$ = createEffect(
    () => this.actions$.pipe(
      ofType( ReportsActions.saveShowAllReports ),
      tap( action => {
        this.localStoreService.set( ModuleConstants.instance.localStoreKeys.showAllReports, String( action.showAllReports ) );
      }),
    ),
    { dispatch: false },
  );

  user!: UiUserDto;

  constructor(
    private actions$: Actions,
    private store: Store,
    private reportReportService: ReportService,
    private reportReportTypeService: ReportTypeService,
    private reportAnswerService: ReportAnswerService,
    private readonly localStoreService: LocalStoreService,
    private readonly mailService: MailService,
    // private readonly noteService: ReportNoteService,
    private router: Router,
    private userService: UserService,
  ) {
    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.user = user;
      }
    } );
  }

}
