import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportDto } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiUserDto, UserNameTool } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { ReportsActions } from '../../action-types-reports';
import { getReportsInfoStateReports, getReportsInfoStateReportTypeDetails } from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-detail',
  styleUrls: ['./report-detail.component.scss'],
  templateUrl: './report-detail.component.html',
})
export class ReportDetailComponent {

  profile!: UiUserDto;
  apprentice!: string;
  reportUuid!: string;
  report!: ReportDto;
  reportTypeName!: string;
  selectedMeetingDate!: Date;
  selectedDueDateAnswers!: Date;

  constructor(
    private readonly store: Store,
    private route: ActivatedRoute,
    private readonly router: Router,
    private readonly http: HttpClient,
    private readonly userService: UserService,
  ) {
    this.route.paramMap.subscribe( params => {
      const abId = params.get('abId');
      const reportId = params.get('reportId');


      if ( abId ) {
        this.apprentice = abId;
      }
      if ( reportId ) {
        this.reportUuid = reportId;
      }
    });

    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.profile = user;
      }
    });

    this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
      // Bericht suchen, damit vorhandene Werte in die Formularfelder eingesetzt werden können
      if ( this.reportUuid ) {
        const report = reports.find( r => r.uuid === this.reportUuid );

        if ( report ) {
          this.report = report;

          // Wenn der Besprechungstermin nicht gesetzt ist, dann auf dueDateReport 10:00 Uhr setzen
          if ( this.report.meetingDate ) {
            this.selectedMeetingDate = new Date( this.report.meetingDate );
          } else {
            this.selectedMeetingDate = new Date( this.report.dueDateReport );
            this.selectedMeetingDate.setHours(10);
            this.selectedMeetingDate.setDate( this.selectedMeetingDate.getDate() - 1 );
          }

          // Wenn der Termin für Antworten AB nicht gesetzt ist, dann auf dueDateReport 10:00 Uhr setzen
          if ( this.report.dueDateAnswers ) {
            this.selectedDueDateAnswers = new Date( this.report.dueDateAnswers );
          } else {
            this.selectedDueDateAnswers = new Date( this.report.dueDateReport );
            this.selectedDueDateAnswers.setHours(10);
            this.selectedDueDateAnswers.setDate( this.selectedDueDateAnswers.getDate() - 8 );
          }

        }
      }
    } );

    // ReportTypeDetails aus dem Store holen um den Namen des Berichtstyps zu finden für den Titel der Seite
    this.store.pipe( select( getReportsInfoStateReportTypeDetails ) ).subscribe( rtd => {
      const reportTypeDetailsOfCurrentReport = rtd.find( r => r.reportTypeUuid === this.report.reportTypeUuid );
      this.reportTypeName = reportTypeDetailsOfCurrentReport ? reportTypeDetailsOfCurrentReport.reportTypeName : '';
    });

  }

  get getSelectedMeetingDate(): Date {
    return this.selectedMeetingDate;
  }

  get getSelectedDueDateAnswers(): Date {
    return this.selectedDueDateAnswers;
  }

  getDueDateReportAsString(): string {
    return new Date( this.report.dueDateReport ).toLocaleDateString();
  }

  getPageTitle(): string {
    return `${this.reportTypeName} ${this.report.period} von ${this.apprentice} bearbeiten`;
  }

  onSaveReport(): void {

    const reportUpdateDto: ReportDto = {
      dueDateAnswers: this.selectedDueDateAnswers.toISOString(),
      meetingDate: this.selectedMeetingDate.toISOString(),
      pbUserEmail: this.profile.eMail,
      state: this.report.state + 1,
    } as ReportDto;

    this.store.dispatch( ReportsActions.updateReport({ id: this.report.uuid as string, reportUpdateDto } ) );

    // Einladung an AB senden
    this.sendInvitation();

    // wieder zur Seite mit der Anzeige der Berichte navigieren
    this.router.navigateByUrl( `reports/reports/ab/${this.apprentice}` ).then();
  }

  onCancelClick(): void {
    this.router.navigateByUrl( `reports/reports/ab/${this.apprentice}` ).then();
  }

  meetingDateValueChanged( date: Date ): void {
    this.selectedMeetingDate = date;
  }

  dueDateAnswersValueChanged( date: Date ): void {
    this.selectedDueDateAnswers = date;
  }

  isSaveDisabled(): boolean {
    return !(this.selectedMeetingDate && this.selectedDueDateAnswers);
  }

  /**
   * Sendet dem Auszubildenden des Berichts eine Einladung zum Ausfüllen der Antworten.
   * Praxisbildner werden mit CC auch informiert.
   *
   * @private
   */
  private sendInvitation(): void {
    let url;
    // URL je nach Umgebung zusammensetzen für das Mail
    switch ( environment.title ) {
      case 'Dev ':
        url = `http://localhost:4201/reports/reports/abOrPb/ab/report/${this.reportUuid}/answers`;
        break;
      case 'Int ':
        url = `https://ui.int.it-berufsbildung.ch/reports/reports/abOrPb/ab/report/${this.reportUuid}/answers`;
        break;
      default:
        url = `https//ui.it-berufsbildung.ch/reports/reports/abOrPb/ab/report/${this.reportUuid}/answers`;
    }

    const selectedDueDateAnswers = new Date(this.selectedDueDateAnswers);
    const selectedMeetingDate = new Date(this.selectedMeetingDate);
    const contentHtml = `
      <p>Hallo ${UserNameTool.getFirstName(this.apprentice)}</p>
      <br/>
      <p>
        Dein Praxisbildner hat für den nächsten Bericht die Termine für die Besprechung und dem Datum, an dem du die Antworten geschrieben und freigegeben haben musst, festgelegt.
      </p>
      <p>Datum für deine Antworten: ${selectedDueDateAnswers.toLocaleString()} Uhr</p>
      <p>Datum der Besprechung: ${selectedMeetingDate.toLocaleString()} Uhr</p>
      <p>Hier kommst du zum Bericht: <a href="${url}">${url}</a></p>
      <br/>
      <p>Gruss ${UserNameTool.getFirstName(this.profile.eMail)}</p>
    `;

    const mailDto = {
      bcc: '',
      cc: this.profile.eMail,
      contentHtml,
      receiver: this.apprentice,
      subject: `Einladung zum nächsten Bericht`,
    };

    // Mail wird über Action versendet
    this.store.dispatch( ReportsActions.sendMail( { mailDto } ) );
  }
}
