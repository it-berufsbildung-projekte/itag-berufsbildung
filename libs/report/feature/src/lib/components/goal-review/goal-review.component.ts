import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportAnswerDto, ReportDto } from '@itag-berufsbildung/report/data';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { ExpansionPanelActionEvent, ExpansionPanelComponent } from '@progress/kendo-angular-layout';
import { getReportsInfoStateReports } from '../reports.selectors';

interface IGoalReview {
  goalUuid: string;
  mark: string;
  explanation: string;
}

@Component({
  selector: 'itag-berufsbildung-report-goal-review',
  styleUrls: ['./goal-review.component.scss'],
  templateUrl: './goal-review.component.html',
})
export class GoalReviewComponent implements OnInit {

  @ViewChildren(ExpansionPanelComponent)
  panels!: QueryList<ExpansionPanelComponent>;

  @Input()
  reportUuid!: string;

  @Input()
  answerUuid!: string | undefined;

  @Input()
  goalsFromLastPeriod!: ReportAnswerDto[];

  @Input()
  role!: string;

  @Input()
  readonly!: boolean;

  @Input()
  navigationActive!: boolean;

  @Input()
  isFirstStep!: boolean;

  @Input()
  isLastStep!: boolean;

  @Input()
  canCreateNotes = false;

  @Input()
  showAnswerToOtherRoleActive!: boolean;

  @Output()
  saveAnswerAndNext = new EventEmitter<{ updateDto: ReportAnswerDto, uuid: string } | undefined>();

  @Output()
  back = new EventEmitter();

  @Output()
  navigateToReportsPage = new EventEmitter();

  @Output()
  writeNote = new EventEmitter();

  @Output()
  showAnswerToOtherRole = new EventEmitter();

  @Output()
  publishAnswers = new EventEmitter();

  goalReviews: IGoalReview[] = [];

  // wird nur auf D gesetzt, wenn ein Ziel negativ bewertet wird, damit der api-server das Flag hasMarkD auf true stellen kann
  mark!: string;
  // hier werden alle Zielbewertungen eingefügt
  explanation!: string;

  formGroups: FormGroup[] = [];
  isPublishDialogOpen = false;
  answer!: ReportAnswerDto;
  answersToPublish = false;
  answerUpdateDto!: ReportAnswerDto;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly store: Store,
  ) {

  }

  ngOnInit(): void {
    this.loadMarkAndExplanation();
    let goalReviewsFromExplanation: string[];

    if ( this.explanation ) {
      // Bewertung für jedes Ziel aus explanation der Antwort trennen mit split
      goalReviewsFromExplanation = this.explanation.split( '¦' );

      for ( const goalReview of goalReviewsFromExplanation ) {
        // weil das erste und letzte Element im Array goalReviewsFromExplanation leere Strings sind,
        // werden diese weggelassen, damit beim ersten Ziel keine leere Bewertung steht,
        // obwohl eine Bewertung vorhanden ist
        if ( goalReview.length === 0 ) {
          continue;
        }

        // jetzt werden die Teile einer einzelnen Zielbewertung getrennt und in goalReviews gepusht
        const goalReviewSplit = goalReview.split( ';' );
          this.goalReviews.push(
            {
              explanation: goalReviewSplit[2],
              goalUuid: goalReviewSplit[0],
              mark: goalReviewSplit[1],
            },
          );
      }
    }

    // für jedes Ziel der letzten Periode ein Formular erstellen
    for ( const goal of this.goalsFromLastPeriod ) {
      let mark = '';
      let explanation = '';

      // die Zielbewertung mit der ID des Ziels suchen und wenn gefunden, mark und explanation aus dieser Bewertung nehmen,
      // sonst bleiben sie leer (noch keine Bewertung geschrieben)
      const goalReview = this.goalReviews.find( (gr) => gr.goalUuid === goal.uuid );
      if ( goalReview ) {
        mark = goalReview.mark;
        explanation = goalReview.explanation;
      }

      const formGroup = this.formBuilder.group( {
        explanation: [ explanation, [Validators.required, Validators.minLength(10), Validators.pattern('[^¦;]+')]],
        goalId: [ goal.uuid, Validators.nullValidator ],
        mark: [ {disabled: this.readonly, value: mark }, Validators.required ],
      } );

      this.formGroups.push( formGroup );
    }
  }

  onNext(): void {
    if ( !this.readonly ) {
      let explanation = '¦';
      let mark = this.mark;

      // jedes Formular für eine Zielbewertung durchgehen und seine Werte verketten. Dann zu explanation hinzufügen
      for ( const formGroup of this.formGroups ) {
        explanation += `${ formGroup.controls['goalId'].value };${ formGroup.controls['mark'].value };${ formGroup.controls['explanation'].value }¦`;

        if ( formGroup.controls['mark'].value === 'D' ) {
          // wenn ein Ziel negativ bewertet wurde, wird mark bei der Antwort auch auf D gesetzt, damit der api-server
          // das Flag hasMarkD richtig setzen kann nach der Freigabe des Berichts
          mark = 'D';
        }
      }

      let answerUpdateDto: ReportAnswerDto;
      if ( this.role === 'ab' ) {
        answerUpdateDto = {
          ...this.answer,
          abExplanation: explanation,
          abMark: mark,
          isAbAnswerComplete: true,
        };

        // delete all properties which belong to the pb to avoid overwriting them when the ab saves his changes
        delete answerUpdateDto.pbExplanation;
        delete answerUpdateDto.pbMark;
        delete answerUpdateDto.isPbAnswerComplete;

      } else {
        answerUpdateDto = {
          ...this.answer,
          isPbAnswerComplete: true,
          pbExplanation: explanation,
          pbMark: mark,
        };

        // delete all properties which belong t the ab to avoid overwriting them when the pb saves his changes
        delete answerUpdateDto.abExplanation;
        delete answerUpdateDto.abMark;
        delete answerUpdateDto.isAbAnswerComplete;

      }

      // Speichern der Antwort in der übergeordneten Komponente
      this.saveAnswerAndNext.emit( { updateDto: answerUpdateDto, uuid: answerUpdateDto.uuid } );

      // letzte Frage und AB, dann Dialog öffnen, um Antworten freizugeben
      if ( this.isLastStep && this.role === 'ab' ) {
        this.isPublishDialogOpen = true;
        this.answerUpdateDto = answerUpdateDto;
      }

    } else {
      this.saveAnswerAndNext.emit( undefined );
    }

    // zurück auf die Übersichtsseite Berichte navigieren
    if ( this.isLastStep && this.role === 'pb' ) {
      this.navigateToReportsPage.emit();
    }

  }

  onSaveAndPublish(): void {
    this.publishAnswers.emit();
  }

  onBack(): void {
    this.back.emit();
  }

  onShowAnswerToOtherRole(): void {
    this.showAnswerToOtherRole.emit();
  }

  onExpansionPanelAction( ev: ExpansionPanelActionEvent, index: number ): void {
    this.panels.forEach((panel, idx) => {
      if (idx !== index && panel.expanded) {
        panel.toggle();
      }
    });
  }

  onSkipGoalReview( formGroup: FormGroup ): void {
    formGroup.controls['explanation'].setValue('Dieses Ziel wurde nicht bewertet');

    // hack: mark auf X setzen, damit die formGroup valid ist und zur nächsten Frage gewechselt werden kann
    formGroup.controls['mark'].setValue('X');
  }

  onWriteNote(): void {
    this.writeNote.emit();
  }

  areFormsInvalid(): boolean {
    for ( const form of this.formGroups ) {
      if ( !form.valid ) {
        return true;
      }
    }
    return false;
  }

  getExpansionPanelTitle( formGroup: FormGroup ): string {
    const goalFromLastPeriod = this.goalsFromLastPeriod.find( (g) => g.uuid === formGroup.controls['goalId'].value ) as ReportAnswerDto;

    if ( goalFromLastPeriod && goalFromLastPeriod.pbExplanation ) {
      // Bestandteile des Ziels auseinander trennen
      const goalExplanationSplit = goalFromLastPeriod.pbExplanation.split('¦' ) as string[];
      const info = !formGroup.valid ? 'Bewertung offen' : 'bewertet';
      return `${goalExplanationSplit[0]}: ${goalExplanationSplit[1]} (${info})`;
    }

    return '';
  }

  getGoalById( uuid: string ): IGoalReview {
    return this.goalReviews.find( (g) => g.goalUuid === uuid ) as IGoalReview;
  }

  private loadMarkAndExplanation(): void {
    this.store.pipe( select(getReportsInfoStateReports) ).subscribe( reports => {
      const report = reports.find( r => r.uuid === this.reportUuid ) as ReportDto;
      const answer = report.answers.find( a => a.uuid === this.answerUuid ) as ReportAnswerDto;
      this.answer = ObjectTools.cloneDeep( answer );
      this.mark = this.role === 'ab' ? answer.abMark as string : answer.pbMark as string;
      this.explanation = this.role === 'ab' ? answer.abExplanation as string : answer.pbExplanation as string;
    } );
  }


}
