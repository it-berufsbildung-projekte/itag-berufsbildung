import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ReportsState } from './reports.reducers';

export const getReportsFeatureState = createFeatureSelector<ReportsState>('reports');

export const getReportsInfoStateReports = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.reports,
);

export const getReportsInfoStateReportTypeDetails = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.reportTypeDetails,
);

export const getReportsInfoStateCurrentAnswerIndexForStepper = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.reportStep,
);

export const getReportsInfoStateLastMeetingStep = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.lastMeetingStep,
);

export const getReportsInfoStateAnswerIdsShowedToOtherRole = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.answerIdsShowedToOtherRole,
);

export const getReportsInfoStateShowAllReports = createSelector(
  getReportsFeatureState,
  (state: ReportsState) => state.showAllReports,
);
