import { ReportAnswerDto, ReportDto, ReportsForApprenticeshipCreateDto, ReportTypeDetailViewDto, WebSocketSyncDto } from '@itag-berufsbildung/report/data';
import { MailDto } from '@itag-berufsbildung/shared/data-ui';
import { createAction, props } from '@ngrx/store';

export const loadReports = createAction(
  '[Reports Service] Load reports',
);

export const loadReportsSuccess = createAction(
  '[Reports Service] Load reports success',
  props<{ reports: ReportDto[] }>(),
);

export const loadOneReport = createAction(
  '[Reports Service] Load one report',
  props<{ id: string }>(),
);

export const loadOneAnswer = createAction(
  '[Reports Service] Load one answer',
  props<{ uuid: string }>(),
);

export const loadReportsPerUser = createAction(
  '[Reports Service] Load reports per user',
  props<{ userId: string; fieldName: string }>(),
);

export const loadReportTypeDetails = createAction(
  '[Reports Service] Load reportsTypeDetails',
);

export const loadReportTypeDetailsSuccess = createAction(
  '[Reports Service] Load reportsTypeDetails success',
  props<{ reportTypeDetails: ReportTypeDetailViewDto[] }>(),
);

export const updateReport = createAction(
  '[Reports Service] Update report',
  props<{ id: string; reportUpdateDto: ReportDto }>(),
);

export const updateOrLoadOneReportSuccess = createAction(
  '[Reports Service] Update report success',
  props<{ reportDto: ReportDto }>(),
);

export const createReportsForApprenticeship = createAction(
  '[Reports Service] Create all reports for apprenticeship',
  props<{ reportsForApprenticeshipCreateDto: ReportsForApprenticeshipCreateDto }>(),
);

export const createReportsForApprenticeshipSuccess = createAction(
  '[Reports Service] Create all reports for apprenticeship success',
  props<{ reportDtoArr: ReportDto[] }>(),
);

export const deleteReport = createAction(
  '[Reports Service] Delete report',
  props<{ id: string }>(),
);

export const deleteReportSuccess = createAction(
  '[Reports Service] Delete report success',
  props<{ reportDto: ReportDto }>(),
);

export const updateAnswer = createAction(
  '[Reports Service] Update answer',
  props<{ id: string; reportAnswerUpdateDto: ReportAnswerDto }>(),
);

export const updateAnswerSuccess = createAction(
  '[Reports Service] Update answer success',
  props<{ reportAnswerDto: ReportAnswerDto }>(),
);

export const createAnswer = createAction(
  '[Reports Service] Create answer',
  props<{ reportAnswerCreateDto: ReportAnswerDto }>(),
);

export const createAnswerSuccess = createAction(
  '[Reports Service] Create answer success',
  props<{ reportAnswerDto: ReportAnswerDto }>(),
);

export const sendMail = createAction(
  '[Reports Service] Send email',
  props<{ mailDto: MailDto }>(),
);

// hint: werden nur von der reports-list und answer-list Komponente gebraucht
export const setCurrentStep = createAction(
  '[Reports Service] Set current step for stepper',
  props<{ currentStep: number }>(),
);

export const setLastMeetingStep = createAction(
  '[Reports Service] Set last step of meeting',
  props<{ lastMeetingStep: number }>(),
);

export const createWebsocketSyncEvent = createAction(
  '[Reports Service] Create websocket sync event with new currentStep for page discussion',
  props<{ websocket: WebSocketSyncDto }>(),
);

// für das Speichern der bereits freigegebenen Antworten an der Besprechung
export const addShowedAnswerId = createAction(
  '[Reports Service] Add id of showed answer to other role',
  props<{ answerId: string }>(),
);

export const saveShowAllReports = createAction(
  '[Reports Service] Save show all reports',
  props<{ showAllReports: number }>(),
);
