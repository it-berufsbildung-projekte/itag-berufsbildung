import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportAnswerDto, ReportDto } from '@itag-berufsbildung/report/data';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { getReportsInfoStateReports } from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-answer',
  styleUrls: ['./answer.component.scss'],
  templateUrl: './answer.component.html',
})
export class AnswerComponent implements OnInit, OnChanges {

  @Input()
  reportUuid!: string;

  @Input()
  answerUuid!: string | undefined;

  @Input()
  readonly!: boolean;

  @Input()
  role!: string;

  @Input()
  navigationActive!: boolean;

  @Input()
  isFirstStep!: boolean;

  @Input()
  isLastStep!: boolean;

  @Input()
  showAnswerToOtherRoleActive!: boolean;

  @Input()
  canCreateNotes = false;

  @Output()
  saveAnswerAndNext = new EventEmitter<{ updateDto: ReportAnswerDto, uuid: string } | undefined>();

  @Output()
  back = new EventEmitter();

  @Output()
  navigateToReportsPage = new EventEmitter();

  @Output()
  writeNote = new EventEmitter();

  @Output()
  showAnswerToOtherRole = new EventEmitter();

  @Output()
  publishAnswers = new EventEmitter();

  answer!: ReportAnswerDto;
  mark!: string;
  explanation!: string;
  formGroup!: FormGroup;
  isPublishDialogOpen = false;

  constructor(private readonly formBuilder: FormBuilder, private readonly store: Store) { }

  ngOnInit(): void {
    this.loadMarkAndExplanation();

    this.formGroup = this.formBuilder.group( {
      explanation: [ this.explanation, [Validators.required, Validators.minLength(10)]],
      mark: [ {disabled: this.readonly, value: this.mark }, Validators.required ],
    } );

  }

  // wenn Input answerId geändert wird, dann werden mark und explanation neu geladen (Werte der aktuellen Antwort in das Formular setzen)
  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes['answerUuid'] ) {
      this.answerUuid = changes['answerUuid'].currentValue;
      this.loadMarkAndExplanation();
    }

    if ( this.formGroup ) {
      if ( !this.readonly ) {
        this.formGroup.controls['mark'].enable();
      }
      this.formGroup.controls['mark'].setValue( this.mark );
      this.formGroup.controls['explanation'].setValue( this.explanation );
    }
  }

  onSaveAnswerAndNext(): void {
    // Antwort nur speichern, wenn nicht readonly und das Formular bearbeitet wurde (dirty = bearbeitet)
    if ( !this.readonly && this.formGroup.dirty ) {
      let answerUpdateDto: ReportAnswerDto;

      if ( this.role === 'ab' ) {

        answerUpdateDto = {
          ...this.answer,
          abExplanation: this.formGroup.controls['explanation'].value,
          abMark: this.formGroup.controls['mark'].value,
          isAbAnswerComplete: true,
        };

        // delete all properties which belong to the pb to avoid overwriting them when the ab saves his changes
        delete answerUpdateDto.pbExplanation;
        delete answerUpdateDto.pbMark;
        delete answerUpdateDto.isPbAnswerComplete;

      } else {

        answerUpdateDto = {
          ...this.answer,
          isPbAnswerComplete: true,
          pbExplanation: this.formGroup.controls['explanation'].value,
          pbMark: this.formGroup.controls['mark'].value,
        };

        // delete all properties which belong t the ab to avoid overwriting them when the pb saves his changes
        delete answerUpdateDto.abExplanation;
        delete answerUpdateDto.abMark;
        delete answerUpdateDto.isAbAnswerComplete;

      }
      this.saveAnswerAndNext.emit( { updateDto: answerUpdateDto, uuid: answerUpdateDto.uuid as string } );
    } else {
      // answerUpdate null übergeben, weil nicht gespeichert werden muss, sondern nur ein Step weiter
      this.saveAnswerAndNext.emit( undefined );
    }

    // Wenn es die letzte Frage ist, soll zurück auf die Seite Berichte navigiert werden, wenn der PB Antworten schreibt
    if ( this.isLastStep && this.role === 'pb' ) {
      this.navigateToReportsPage.emit();
    }

    // letzte Frage und AB, dann Dialog öffnen, um Antworten freizugeben
    if ( this.isLastStep && this.role === 'ab' && !this.readonly ) {
      this.isPublishDialogOpen = true;
    }
  }

  onBack(): void {
    this.back.emit();
  }

  onWriteNote(): void {
    this.writeNote.emit();
  }

  onShowAnswerToOtherRole(): void {
    this.showAnswerToOtherRole.emit();
  }

  isRadioButtonInvalid(): boolean {
    return this.formGroup.controls['mark'].value === null && !this.readonly;
  }

  onPublishAnswers(): void {
    this.publishAnswers.emit();
  }

  private loadMarkAndExplanation(): void {
    this.store.pipe( select(getReportsInfoStateReports) ).subscribe( reports => {
      const report = reports.find( (r) => r.uuid === this.reportUuid ) as ReportDto;
      const answer = report.answers.find( a => a.uuid === this.answerUuid ) as ReportAnswerDto;
      this.answer = ObjectTools.cloneDeep<ReportAnswerDto>( answer );
      this.mark = this.role === 'ab' ? answer.abMark as string : answer.pbMark as string;
      this.explanation = this.role === 'ab' ? answer.abExplanation as string : answer.pbExplanation as string;
    } );
  }

}
