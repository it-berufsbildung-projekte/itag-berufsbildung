import { ModuleConstants, ReportAnswerDto, ReportDto, ReportTypeDetailViewDto } from '@itag-berufsbildung/report/data';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { createReducer, on } from '@ngrx/store';
import { ReportsActions } from '../action-types-reports';

export interface ReportsState {
  reports: ReportDto[];
  reportTypeDetails: ReportTypeDetailViewDto[];
  reportStep: number;
  lastMeetingStep: number;
  answerIdsShowedToOtherRole: string[];
  showAllReports: number;
}

export const initializeReportsInfo = (): ReportsState => {
  const localStoreService = new LocalStoreService();

  // Local Store nach showAllReports auslesen
  let showAllReports = localStoreService.getNumber( ModuleConstants.instance.localStoreKeys.showAllReports );
  showAllReports = showAllReports ? showAllReports : 0;

  return {
    answerIdsShowedToOtherRole: [],
    lastMeetingStep: 0,
    reportStep: 0,
    reportTypeDetails: [],
    reports: [],
    showAllReports,
  };
};

export const reportsReducer = createReducer(
  initializeReportsInfo(),
  // auf erfolgreiche Actions hören und State aktualisieren
  on(ReportsActions.loadReportsSuccess, (state, action) => ({
    ...state,
    reports: action.reports,
  })),

  on(ReportsActions.loadReportTypeDetailsSuccess, (state, action) => ({
    ...state,
    reportTypeDetails: action.reportTypeDetails,
  })),

  on(ReportsActions.updateOrLoadOneReportSuccess, ( state, action) => {
    const reportIndex = state.reports.findIndex( r => r.uuid === action.reportDto.uuid );
    const arr: ReportDto[] = [ ...state.reports ];
    arr[reportIndex] = action.reportDto;
    return {
      ...state,
      reports: arr,
    };
  }),

  on(ReportsActions.updateAnswerSuccess, ( state, action) => {
    const report = state.reports.find( r => r.uuid === action.reportAnswerDto.reportUuid ) as ReportDto;
    const reportIndex = state.reports.findIndex( r => r.uuid === report.uuid );
    const answerIndex = report.answers.findIndex( a => a.uuid === action.reportAnswerDto.uuid );

    const reportCopy = { ... report };
    const reportsArr: ReportDto[] = [ ...state.reports ];

    // if the answer is not in the answers array, then add it. Otherwise, only a copy of the answers array is made.
    const answersArr: ReportAnswerDto[] = answerIndex > -1 ? [ ...report.answers ] : [ ...report.answers, action.reportAnswerDto ];

    // if the answer is the answers array, then replace it
    if ( answerIndex > -1 ) {
      answersArr[answerIndex] = action.reportAnswerDto;
    }

    reportCopy.answers = answersArr;
    reportsArr[reportIndex] = reportCopy as ReportDto;

    return {
      ...state,
      reports: reportsArr,
    };
  }),

  on(ReportsActions.createAnswerSuccess, ( state, action) => {
    const report = state.reports.find( r => r.uuid === action.reportAnswerDto.reportUuid ) as ReportDto;
    const reportIndex = state.reports.findIndex( r => r.uuid === report.uuid );

    const reportCopy = { ... report };
    const reportsArr: ReportDto[] = [ ...state.reports ];
    const answersArr: ReportAnswerDto[] = [ ...report.answers, action.reportAnswerDto ];
    reportCopy.answers = answersArr;
    reportsArr[reportIndex] = reportCopy as ReportDto;

    return {
      ...state,
      reports: reportsArr,
    };
  }),

  // todo: note/feedback
  // on(ReportsActions.createNoteSuccess, ( state, action) => {
  //   const report = state.reports.find( r => r.id === action.noteDto.reportId );
  //   const reportIndex = state.reports.findIndex( r => r.id === report.id );
  //
  //   const reportsCopy = { ... report };
  //   const reportsArr: ReportDto[] = [ ...state.reports ];
  //   reportsCopy.notes = [ ...report.notes, action.noteDto ];
  //   reportsArr[reportIndex] = reportsCopy;
  //
  //   return {
  //     ...state,
  //     reports: reportsArr,
  //   };
  // }),

  // todo: note/feedback
  // on(ReportsActions.updateNoteSuccess, ( state, action) => {
  //   const report = state.reports.find( r => r.id === action.noteDto.reportId );
  //   const reportIndex = state.reports.findIndex( r => r.id === report.id );
  //   const noteIndex = report.notes.findIndex( n => n.id === action.noteDto.id );
  //
  //   const reportCopy = { ... report };
  //   const reportsArr: ReportDto[] = [ ...state.reports ];
  //   const notesArr: NoteDto[] = [ ...report.notes ];
  //   notesArr[noteIndex] = action.noteDto;
  //   reportCopy.notes = notesArr;
  //   reportsArr[reportIndex] = reportCopy;
  //
  //   return {
  //     ...state,
  //     reports: reportsArr,
  //   };
  // }),

  on(ReportsActions.createReportsForApprenticeshipSuccess, ( state, action) => {
    const arr: ReportDto[] = [ ...state.reports, ...action.reportDtoArr ];
    return {
      ...state,
      reports: arr,
    };
  }),

  on(ReportsActions.deleteReportSuccess, (state, action) => {
    const reportIndex = state.reports.findIndex( r => r.uuid === action.reportDto.uuid );
    const arr: ReportDto[] = [ ...state.reports ];
    arr.splice(reportIndex, 1);
    return {
      ...state,
      reports: arr,
    };
  }),

  // hint: wird nur von der answer-list Komponente gebraucht
  on(ReportsActions.setCurrentStep, ( state, action) => ({
    ...state,
    reportStep: action.currentStep,
  })),

  on(ReportsActions.setLastMeetingStep, ( state, action) => ({
    ...state,
    lastMeetingStep: action.lastMeetingStep,
  })),

  on(ReportsActions.addShowedAnswerId, ( state, action) => ({
    ...state,
    answerIdsShowedToOtherRole: [ ...state.answerIdsShowedToOtherRole, action.answerId ],
  })),

  on(ReportsActions.saveShowAllReports, ( state, action) => ({
    ...state,
    showAllReports: action.showAllReports,
  })),
);
