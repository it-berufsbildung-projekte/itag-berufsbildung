import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModuleConstants, ReportsForApprenticeshipCreateDto } from '@itag-berufsbildung/report/data';
import { ScreenInfoService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UserNameTool } from '@itag-berufsbildung/shared/util';
import { Store } from '@ngrx/store';
import { ReportsActions } from '../../action-types-reports';

@Component({
  selector: 'itag-berufsbildung-reports',
  styleUrls: ['./reports.component.scss'],
  templateUrl: './reports.component.html',
})
export class ReportsComponent implements OnInit {

  profileUserRoles!: string[];
  isBb!: boolean;
  showAbList = false;
  apprentice!: string;
  height = 600;

  constructor(
    private readonly store: Store,
    private readonly route: ActivatedRoute,
    private readonly userService: UserService,
    private readonly screenInfoService: ScreenInfoService,
  ) {

    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.profileUserRoles = Object.keys( user.uiRoles[ModuleConstants.instance.moduleName] );

        if ( this.profileUserRoles.includes(ModuleConstants.instance.availableRoles.bb)
          || this.profileUserRoles.includes(ModuleConstants.instance.availableRoles.pb)
          || this.profileUserRoles.includes(ModuleConstants.instance.availableRoles.admin) ) {
          this.showAbList = true;
        }

        this.isBb = this.profileUserRoles.includes(ModuleConstants.instance.availableRoles.bb);
      }
    });

    this.route.paramMap.subscribe( params => {
      const abId = params.get( 'abId' );
      if ( abId ) {
        this.apprentice = abId;
      }

    });
  }

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });
  }

  onCreateReportsForApprenticeship(): void {
    const reportsForApprenticeshipCreateDto: ReportsForApprenticeshipCreateDto = {
      abUserId: this.apprentice,
    };

    this.store.dispatch( ReportsActions.createReportsForApprenticeship( { reportsForApprenticeshipCreateDto } ) );
  }

  getTitle(): string {
    return this.apprentice ? `Berichte von ${UserNameTool.getFullName( this.apprentice )}` : `Kein Auszubildender ausgewählt`;
  }

}
