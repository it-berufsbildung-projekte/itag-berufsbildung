import { Component, EventEmitter, Input, OnChanges, OnInit, Output, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportAnswerDto } from '@itag-berufsbildung/report/data';
import { Store } from '@ngrx/store';
import { ExpansionPanelActionEvent, ExpansionPanelComponent } from '@progress/kendo-angular-layout';
import { v4 } from 'uuid';
import { ReportsActions } from '../../action-types-reports';

@Component({
  selector: 'itag-berufsbildung-report-goal',
  styleUrls: ['./goal.component.scss'],
  templateUrl: './goal.component.html',
})
export class GoalComponent implements OnInit, OnChanges {

  @ViewChildren(ExpansionPanelComponent)
  panels!: QueryList<ExpansionPanelComponent>;

  @Input()
  reportUuid!: string;

  @Input()
  goals!: ReportAnswerDto[];

  @Input()
  role!: string;

  @Input()
  readonly!: boolean;

  @Input()
  navigationActive!: boolean;

  @Input()
  isFirstStep!: boolean;

  @Input()
  isLastStep!: boolean;

  @Input()
  isDiscussion!: boolean;

  @Input()
  showAnswerToOtherRoleActive!: boolean;

  @Input()
  canCreateNotes = false;

  @Output()
  saveAnswerAndNext = new EventEmitter<{ answerUpdate: { updateDto: ReportAnswerDto; uuid: string } | undefined; saveOnly: boolean }>();

  @Output()
  back = new EventEmitter();

  @Output()
  navigateToReportsPage = new EventEmitter();

  @Output()
  writeNote = new EventEmitter();

  @Output()
  showAnswerToOtherRole = new EventEmitter();

  formGroups: FormGroup[] = [];

  competences: Array<string> = ['Betriebliche Leistungsziele', 'Fachkompetenz', 'Methodenkompetenz', 'Sozialkompetenz', 'Selbstkompetenz'];

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
  ) { }

  ngOnInit(): void {
    this.createFormGroups();
  }

  onSaveGoal( goalIndex: number ): void {
    if ( !this.readonly ) {
      // das Formular zum bearbeiteten Ziel mit dem Index holen
      const form = this.formGroups[goalIndex];

      // die Beschreibung der Antwort (Text) mit den Bestandteilen aus dem Formular verketten
      const explanation = `${form.controls['competence'].value}¦${form.controls['goal'].value}¦${form.controls['until'].value}¦${form.controls['measure'].value}`;
      const answerUpdateDto: ReportAnswerDto = {
        ...this.goals[goalIndex],
        isPbAnswerComplete: true,
        pbExplanation: explanation,
      };

      // Antwort von der übergeordneten Komponente speichern lassen
      this.saveAnswerAndNext.emit( { answerUpdate: { uuid: this.goals[goalIndex].uuid as string, updateDto: answerUpdateDto }, saveOnly: true } );
    }
  }

  onNext(): void {
    // Wenn es die letzte Frage ist, soll zurück auf die Seite Berichte navigiert werden
    if ( this.isLastStep && !this.isDiscussion ) {
      this.navigateToReportsPage.emit();
    } else {
      // answerUpdate null übergeben, weil nur zum nächsten Schritt gewechselt werden muss
      this.saveAnswerAndNext.emit( { answerUpdate: undefined, saveOnly: false } );
    }
  }

  onBack(): void {
    this.back.emit();
  }

  onAddGoal(): void {
    const reportAnswerCreateDto: ReportAnswerDto = {
      created: new Date(),
      history: [],
      isAbAnswerComplete: false,
      isDeleted: false,
      isPbAnswerComplete: false,
      pbMark: `neues Ziel: ${new Date()}`,
      uuid: v4(),
      questionUuid: this.goals[0].questionUuid,
      reportUuid: this.reportUuid
    };

    this.store.dispatch( ReportsActions.createAnswer({ reportAnswerCreateDto } ) );
  }

  onShowAnswerToOtherRole(): void {
    this.showAnswerToOtherRole.emit();
  }

  onWriteNote(): void {
    this.writeNote.emit();
  }

  onExpansionPanelAction( ev: ExpansionPanelActionEvent, index: number ): void {
    this.panels.forEach((panel, idx) => {
      if (idx !== index && panel.expanded) {
        panel.toggle();
      }
    });
  }

  areFormsInvalid(): boolean {
    for ( const form of this.formGroups ) {
      if ( !form.valid ) {
        return true;
      }
    }
    return false;
  }

  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes['goals'] && !changes['goals'].firstChange ) {
      // only set goals and formGroups again if currentValue and previousValue are different
      if ( JSON.stringify( changes['goals'].currentValue ) !== JSON.stringify( changes['goals'].previousValue ) ) {
        this.goals = changes['goals'].currentValue;
        this.createFormGroups();
      }
    }
  }

  private createFormGroups(): void {
    // clear all forms
    this.formGroups = [];

    for ( const goal of this.goals ) {
      // die Beschreibung in ihre Bestandteile auseinandernehmen. Weil Ziele nur von PB geschrieben werden kann immer der PB-Teil der Antwort genommen werden
      let explanationSplit: string[] = [];

      if ( goal.pbExplanation ) {
        explanationSplit = goal.pbExplanation.split('¦');
      }

      // Kompetenz, Ziel, bis und Massnahme setzen für Vorausgefüllte Felder
      let competence = '';
      let goalDefinition = '';
      let until = '';
      let measure = '';

      if ( explanationSplit ) {
        competence = explanationSplit[0];
        goalDefinition = explanationSplit[1];
        until = explanationSplit[2];
        measure = explanationSplit[3];
      }

      const formGroup = this.formBuilder.group({
        competence: [competence, [Validators.required]],
        goal: [goalDefinition, [Validators.required, Validators.pattern('[^¦]+')]],
        measure: [measure, [Validators.required, Validators.pattern('[^¦]+')]],
        until: [until, [Validators.required, Validators.pattern('[^¦]+')]],
      } );

      this.formGroups.push(formGroup);
    }
  }

}
