import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbListComponent } from './ab-list.component';

describe('AbListComponent', () => {
  let component: AbListComponent;
  let fixture: ComponentFixture<AbListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbListComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
