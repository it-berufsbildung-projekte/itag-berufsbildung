import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { RoleHashDto, UiUserDto } from '@itag-berufsbildung/shared/util';
import { Store } from '@ngrx/store';
import { lastValueFrom, Observable } from 'rxjs';

@Component({
  selector: 'itag-berufsbildung-report-ab-list',
  styleUrls: ['./ab-list.component.scss'],
  templateUrl: './ab-list.component.html',
})
export class AbListComponent {

  user!: UiUserDto;
  users!: string[];
  filteredUsers: string[] = [];
  selectedApprenticeId = '';

  constructor(
    private readonly store: Store,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly http: HttpClient,
  ) {
    this.loadUsers().then();
    this.route.paramMap.subscribe( params => this.selectedApprenticeId = params.get('abId') ?  params.get('abId') as string : '' );
  }

  async loadUsers(): Promise<void> {
    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.user = user;

        // get all users and filter them
        this.getUsers().subscribe( async (users) => {

          // a rather complicated way of filtering the users array to get only those users with the ab role.
          // Further explanation: https://stackoverflow.com/questions/64770970/array-filter-with-async-arrow-function
          this.filteredUsers = (
            await Promise.all(
              users.map( async (user) => {
                const userRole = await this.getUserRole(user);
                let keep = false;
                if ( Object.keys( userRole ).find( (r) => r === ModuleConstants.instance.availableRoles.ab ) ) {
                  keep = true;
                }

                return { user, keep };
              })
            )
          ).filter( (user) => user.keep )
            .map( (user) => user.user );
        });

        // assign the filteredUsers to users to show all ab again after the user cleared the search bar
        this.users = [...this.filteredUsers];

      }
    });
  }

  applyFilter( event: KeyboardEvent ) {
    const filter = (event.target as HTMLInputElement).value;
    this.filteredUsers = this.users.filter( (u) => u.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  onAbSelect( email: string ) {
    // this.selectedApprenticeId = apprentice.id;
    this.router.navigateByUrl(`/reports/reports/ab/${email}`).then();
  }

  private getUsers(): Observable<string[]> {
    const moduleInfo = this.user.moduleInfoArr.find( (module) => module.moduleName === ModuleConstants.instance.moduleName );
    return this.http.get<string[]>( `${moduleInfo?.apiUrl}/generic/users`);
  }

  private getUserRole( email: string ): Promise<RoleHashDto> {
    const moduleInfo = this.user.moduleInfoArr.find( (module) => module.moduleName === ModuleConstants.instance.moduleName );
    return lastValueFrom( this.http.get<RoleHashDto>( `${moduleInfo?.apiUrl}/generic/user-roles/${email}`) );
  }

}
