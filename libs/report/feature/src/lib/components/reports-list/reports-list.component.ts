import { HttpBackend, HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModuleConstants, ReportDto, ReportTypeDetailViewDto } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { SorterTools, UiUserDto, UserNameTool } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ReportsActions } from '../../action-types-reports';
import { getReportsInfoStateReports, getReportsInfoStateReportTypeDetails, getReportsInfoStateShowAllReports } from '../reports.selectors';

interface IReportingInfo {
  forwardEmail: string;
  forwardEmailText: string;
}

@Component({
  selector: 'itag-berufsbildung-reports-list',
  styleUrls: ['./reports-list.component.scss'],
  templateUrl: './reports-list.component.html',
})
export class ReportsListComponent {

  isAdmin!: boolean;
  isPb!: boolean;
  isBb!: boolean;
  isAb!: boolean;
  isRejectDialogOpen = false;
  isAcceptDialogOpen = false;
  profile!: UiUserDto;
  apprentice!: string;
  reports!: ReportDto[];
  reportsFiltered: ReportDto[] = [];
  reportTypeDetails!: ReportTypeDetailViewDto[];
  reportId!: number;
  showAllReports = false;
  httpClient: HttpClient;

  // wird für Zurückweisen der Antworten des AB gebraucht
  reportToReject!: ReportDto;

  // wird für das Akzeptieren des Berichts durch den AB gebraucht
  reportToAccept!: ReportDto;

  constructor(
    private readonly store: Store,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly handler: HttpBackend,
    private readonly userService: UserService,
    ) {
    this.httpClient = new HttpClient(handler);

    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.profile = user;
        const userRoles = Object.keys( user.uiRoles[ModuleConstants.instance.moduleName] );
        this.isAdmin = userRoles.includes(ModuleConstants.instance.availableRoles.admin);
        this.isPb = userRoles.includes(ModuleConstants.instance.availableRoles.pb);
        this.isBb = userRoles.includes(ModuleConstants.instance.availableRoles.bb);
        this.isAb = userRoles.includes(ModuleConstants.instance.availableRoles.ab);
      }
    });

    this.store.pipe( select( getReportsInfoStateShowAllReports ) ).subscribe( showAllReports => {
      this.showAllReports = showAllReports === 1;
    });

    // Parameter aus der URL lesen
    this.route.paramMap.subscribe( params => {
      const abId = params.get('abId');
      const reportId = params.get('reportId');

      // reportId setzen, wenn in URL gesetzt, sonst -1, damit kein report Expansion Panel geöffnet wird
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      reportId ? this.reportId = parseInt( reportId, 10 ) : -1;
      if ( abId ) {
        this.apprentice = abId;
        // this.apprentice = users.find( u => u.id === parseInt( abId, 10 ) );

        // nur dann Berichte holen, wenn ein Auszubildender gewählt wurde und gleich nach seiner ID filtern
        this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
          const reportsFiltered = reports.filter( r => r.abUserEmail === this.apprentice );

          // Berichte nach Erstelldatum sortieren
          this.reports = [ ...reportsFiltered.sort( SorterTools.dynamicSort( 'sortOrder' ) ) ];

          this.filterReports();
        });
      }

      this.store.pipe( select( getReportsInfoStateReportTypeDetails ) ).subscribe( reportTypeDetails => {
          this.reportTypeDetails = reportTypeDetails;
        },
      );

      },
    );
  }

  getReportName( reportTypeUuid: string ): string {
    const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.reportTypeUuid === reportTypeUuid );
    return reportTypeDetail?.reportTypeName ? reportTypeDetail.reportTypeName : '';
  }

  /**
   * Gibt true zurück, wenn ein Benutzer zu einem Bericht gehört. Für Praxisbildner nur report übergeben, für Auszubildender roleAb true übergeben.
   *
   * @param report
   * @param roleAb
   */
  isUserAbOrPbOfReport( report: ReportDto, roleAb?: boolean ): boolean {
    if ( roleAb ) {
      return report.abUserEmail === this.profile.eMail;
    } else {
      return report.pbUserEmail === this.profile.eMail;
    }
  }

  arePbAnswersComplete( report: ReportDto ): boolean {
    let areAnswersComplete = true;
    for ( const answer of report.answers ) {
      const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.questionUuid === answer.questionUuid );

      if ( reportTypeDetail?.reportBlockPbReadWrite && !answer.isPbAnswerComplete ) {
        areAnswersComplete = false;
        break;
      }
    }
    return areAnswersComplete;
  }

  onCreateReport(): void {
    this.router.navigateByUrl(`reports/reports/ab/${this.apprentice}/report-detail/0`).then();
  }

  onEditReportClick( report: ReportDto ): void {
    this.router.navigateByUrl(`reports/reports/ab/${this.apprentice}/report-detail/${report.uuid}`).then();
  }

  onAnswersAbClicked( report: ReportDto ): void {
    // den Index der im Stepper auf der Seite der Antworten geöffneten Frage auf 0 zurückstellen, dann auf Antwortseite weiterleiten
    this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: 0 } ) );
    this.router.navigateByUrl(`reports/reports/abOrPb/ab/report/${report.uuid}/answers`).then();
  }

  onAnswersPbClicked( report: ReportDto ): void {
    // den Index der im Stepper auf der Seite der Antworten geöffneten Frage auf 0 zurückstellen, dann auf Antwortseite weiterleiten
    this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: 0 } ) );
    this.router.navigateByUrl(`reports/reports/abOrPb/pb/report/${report.uuid}/answers`).then();
  }

  onDiscussionClicked( report: ReportDto ): void {
    // den Index der im Stepper auf der Seite der Besprechung (Antworten) geöffneten Frage auf 0 zurückstellen, dann auf Seite der Besprechung weiterleiten
    this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: 0 } ) );
    this.store.dispatch( ReportsActions.setLastMeetingStep( { lastMeetingStep: 0 } ) );

    // Bericht auf Status 2 (Besprechung fregegeben) setzen, damit AB auch beitreten kann
    this.store.dispatch( ReportsActions.updateReport( {id: report.uuid as string, reportUpdateDto: { state: 2 } as ReportDto } ) );
    this.router.navigateByUrl(`reports/reports/report/${report.uuid}/meeting`).then();
  }

  onResetClicked( report: ReportDto ): void {
    // status um eins verkleinern, falls dieser unter 0 kommt, wird 0 eingesetzt
    const state = report.state - 1;

    const reportUpdateDto: ReportDto = {
      state: state === -1 ? 0 : state,
    } as ReportDto;

    this.store.dispatch( ReportsActions.updateReport( { id: report.uuid as string, reportUpdateDto } ) );
  }

  onAcceptReport( report: ReportDto ): void {
    // acceptedDate auf jetzt und Status auf 4 (akzeptiert) ändern
    const reportUpdateDto: ReportDto = {
      acceptedDate: new Date().toISOString(),
      state: report.state + 1,
    } as unknown as ReportDto;

    this.store.dispatch( ReportsActions.updateReport( { id: report.uuid as string, reportUpdateDto } ) );
    this.isAcceptDialogOpen = false;
  }

  onPrintPdf( report: ReportDto ) {
    this.router.navigateByUrl(`/reports/pdf/report/${report.uuid}`).then();
  }

  onSendReminder( report: ReportDto ): void {
    const dueDateAnswers = new Date( report.dueDateReport );
    const contentHtml = `
      <p>Hallo ${UserNameTool.getFirstName( this.apprentice )}</p>
      <br/>
      <p>
        Leider hast du die Antworten zum ${this.getReportName( report.reportTypeUuid )} für die Periode ${report.period} noch nicht geschrieben oder freigegeben.
        Bitte erledige das noch bis zum ${dueDateAnswers.toLocaleDateString('de-CH')}!
      </p>
      <p>Danke.</p>
      <br/>
      <p>Gruss ${UserNameTool.getFirstName( this.profile.eMail )}</p>
    `;

    const mailDto = {
      bcc: '',
      cc: this.apprentice,
      contentHtml,
      receiver: this.profile.eMail,
      subject: `${report.sentReminders + 1}. Erinnerung an das Ausfüllen.`,
    };

    // Mail wird über Action versendet
    this.store.dispatch( ReportsActions.sendMail( { mailDto } ) );

    // Gesendete Reminders aktualisieren
    this.store.dispatch( ReportsActions.updateReport( { id: report.uuid as string, reportUpdateDto: { sentReminders: report.sentReminders + 1 } as ReportDto } ) );
  }

  onNotesClicked( report: ReportDto ): void {
    let period = '';

    if (report.period.includes('/') ) {
      const periodSplit = report.period.split('-');
      const startDate = new Date( periodSplit[0] );
      const endDate = new Date( periodSplit[1] );
      period = `${startDate.toLocaleDateString('de-CH')}-${endDate.toLocaleDateString('de-CH')}`;
    } else {
      period = report.period;
    }

    this.router.navigateByUrl(`/reports/notes/${period}/ab/${this.apprentice}`).then();
  }

  onSignaturesReceived( report: ReportDto ): void {
    // Status wird um eins erhöht
    this.store.dispatch( ReportsActions.updateReport( { id: report.uuid as string, reportUpdateDto: { state: report.state + 1 } as ReportDto } ) );
  }

  async onSendToHr( report: ReportDto ): Promise<void> {
    this.loadReportingInfo().subscribe( (reportInfo) => {
      if ( reportInfo ) {
        // Mailprogramm öffnen
        window.location.href = `mailto:${ reportInfo.forwardEmail }
        ?Subject=${ this.getReportName( report.reportTypeUuid ) } von ${ this.apprentice }
        &body=${ encodeURIComponent( reportInfo.forwardEmailText ) }`;
      }
    });

    this.store.dispatch( ReportsActions.updateReport( { id: report.uuid as string, reportUpdateDto: { state: report.state + 1 } as ReportDto } ) );
  }

  filterReports(): void {
    this.reportsFiltered = [];

    if ( this.showAllReports ) {
      this.reportsFiltered = [...this.reports];
    } else {
      let isActiveReport = false;
      for ( const report of this.reports ) {

        // Wenn Bericht nicht abgeschlossen und noch kein aktiver Bericht angezeigt wird, Bericht anzeigen und isActiveReport auf true setzen,
        // damit nur noch der nächste Bericht zusätzlich angezeigt wird
        if ( report.state < 6 && !isActiveReport) {
          this.reportsFiltered.push( report );
          isActiveReport = true;
        }
        else if ( isActiveReport ) {
          this.reportsFiltered.push( report );
          break;
        }

      }
    }

    // Array umkehren, damit der neuste Bericht zuoberst ist
    this.reportsFiltered.reverse();

    this.store.dispatch( ReportsActions.saveShowAllReports( { showAllReports: this.showAllReports ? 1 : 0 } ) );
  }

  private loadReportingInfo(): Observable<IReportingInfo> {
    return this.httpClient.get<IReportingInfo>('/assets/config/reporting-info.json');
  }

}
