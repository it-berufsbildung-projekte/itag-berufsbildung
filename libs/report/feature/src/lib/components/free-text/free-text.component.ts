import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportAnswerDto, ReportDto } from '@itag-berufsbildung/report/data';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { getReportsInfoStateReports } from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-free-text',
  styleUrls: ['./free-text.component.scss'],
  templateUrl: './free-text.component.html',
})
export class FreeTextComponent implements OnInit, OnChanges {

  @Input()
  reportUuid!: string;

  @Input()
  answerUuid!: string | undefined;

  @Input()
  readonly!: boolean;

  @Input()
  role!: string;

  @Input()
  navigationActive!: boolean;

  @Input()
  isFirstStep!: boolean;

  @Input()
  isLastStep!: boolean;

  @Input()
  isDiscussion!: boolean;

  @Input()
  hasNote!: boolean;

  @Input()
  showAnswerToOtherRoleActive!: boolean;

  @Output()
  saveAnswerAndNext = new EventEmitter<{ updateDto: ReportAnswerDto, uuid: string } | undefined>();

  @Output()
  back = new EventEmitter();

  @Output()
  navigateToReportsPage = new EventEmitter();

  @Output()
  writeNote = new EventEmitter();

  @Output()
  showAnswerToOtherRole = new EventEmitter();

  answer!: ReportAnswerDto;
  explanation!: string;
  formGroup!: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly store: Store,
  ) { }

  ngOnInit(): void {
    this.loadExplanation();

    this.formGroup = this.formBuilder.group( {
      explanation: [ this.explanation, [Validators.required, Validators.minLength(10)]],
    } );
  }

  // Um den Text der Antwort richtig zu laden, wenn die ID der Antwort (Input-Property dieser Komponente) in der übergeordneten Komponente ändert
  ngOnChanges( changes: SimpleChanges ): void {
    this.answerUuid = changes['answerId'].currentValue;
    this.loadExplanation();

    // Text (explanation) in das Formular setzen.
    if ( this.formGroup ) {
      this.formGroup.controls['explanation'].setValue( this.explanation );
    }
  }

  onSaveAnswerAndNext(): void {
    // Antwort nur speichern, wenn nicht readonly und das Formular bearbeitet wurde (dirty = bearbeitet)
    if ( !this.readonly && this.formGroup.dirty ) {
      let updateDto: ReportAnswerDto;

      // Wenn es die Antwort eines AB ist, dann seinen Teil setzen, sonst den des PB
      if ( this.role === 'ab' ) {
        updateDto = {
          ...this.answer,
          abExplanation: this.formGroup.controls['explanation'].value,
          isAbAnswerComplete: true,
        };
      } else {
        updateDto = {
          ...this.answer,
          isPbAnswerComplete: true,
          pbExplanation: this.formGroup.controls['explanation'].value,
        };
      }

      // Antwort über die übergeordnete Komponente (answer-list) speichern lassen mit Event
      this.saveAnswerAndNext.emit( { updateDto, uuid: updateDto.uuid } );
    } else {
      // answerUpdate null übergeben, damit die answer-list Komponente nicht speichert und nur zur nächsten Frage weitergeht
      this.saveAnswerAndNext.emit( undefined );
    }

    // Wenn es die letzte Frage ist, soll zurück auf die Seite Berichte navigiert werden, wenn der PB Antworten schreibt
    if ( this.isLastStep && !this.isDiscussion && this.role === 'pb' ) {
      this.navigateToReportsPage.emit();
    }
  }

  onBack(): void {
    // lässt in der übergeordneten Komponente zur vorherigen Frage gehen
    this.back.emit();
  }

  onWriteNote(): void {
    this.writeNote.emit();
  }

  onShowAnswerToOtherRole(): void {
    this.showAnswerToOtherRole.emit();
  }

  private loadExplanation(): void {
    this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
      // den aktuellen Bericht aus dem Store filtern
      const report = reports.find( (r) => r.uuid === this.reportUuid ) as ReportDto;
      const answer = report.answers.find( a => a.uuid === this.answerUuid ) as ReportAnswerDto;
      this.answer = ObjectTools.cloneDeep( answer );
      // den Text der Antwort der explanation (Begründung wird hier für den Text genutzt)
      // setzen, je nach Rolle, die die Frage beantwortet/anschaut
      this.explanation = this.role === 'ab' ? answer.abExplanation as string : answer.pbExplanation as string;
    });
  }



}
