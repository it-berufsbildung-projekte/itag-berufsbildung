import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModuleConstants, ReportAnswerDto, ReportDto, ReportTypeDetailViewDto, WebSocketSyncDto } from '@itag-berufsbildung/report/data';
import { ScreenInfoService, UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { SorterTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { StepperActivateEvent } from '@progress/kendo-angular-layout';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ReportsActions } from '../../action-types-reports';
import { WebSocketReportService } from '../../service/web-socket-report.service';
import { NoteFeedbackComponent } from '../notes-and-feedback/note-feedback/note-feedback.component';
import {
  getReportsInfoStateAnswerIdsShowedToOtherRole,
  getReportsInfoStateCurrentAnswerIndexForStepper,
  getReportsInfoStateLastMeetingStep,
  getReportsInfoStateReports,
  getReportsInfoStateReportTypeDetails,
} from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-answer-list',
  styleUrls: ['./answer-list.component.scss'],
  templateUrl: './answer-list.component.html',
})
export class AnswerListComponent implements OnInit {

  @ViewChild('noteFeedbackComponent')
  noteFeedbackComponent!: NoteFeedbackComponent;

  profile!: UiUserDto;
  isUserPb!: boolean;
  isUserAb!: boolean;
  role!: string;
  reportUuid!: string;
  report!: ReportDto;
  reportFromLastPeriod!: ReportDto;
  reportTypeDetails!: ReportTypeDetailViewDto[];
  answers: ReportAnswerDto[] = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  steps: any[] = [];
  currentStep = 0;
  goalStep!: number;
  currentAnswer: ReportAnswerDto | undefined;
  showInfo = false;
  isDataLoaded = false;
  apprentice$: Observable<string> = of('');
  height = 600;

  constructor(
    private readonly store: Store,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly socketService: WebSocketReportService,
    private readonly messageService: NotificationService,
    private readonly userService: UserService,
    private readonly http: HttpClient,
    private readonly screenInfoService: ScreenInfoService,
  ) {

    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.profile = user;
        this.isUserAb = !!Object.keys( user.uiRoles[ModuleConstants.instance.moduleName] ).find( (r) => r === ModuleConstants.instance.availableRoles.ab );
        this.isUserPb = !!Object.keys( user.uiRoles[ModuleConstants.instance.moduleName] ).find( (r) => r === ModuleConstants.instance.availableRoles.pb );
      }
    });

    this.route.paramMap.subscribe( params => {
      const role = params.get('role');
      const reportId = params.get('reportId');

      if ( role ) {
        this.role = role;
      }
      if ( reportId ) {
        this.reportUuid = reportId;
      }

      // load the report and the report from the last period
        this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
          this.report = reports.find( r => r.uuid === this.reportUuid ) as ReportDto;

          // load apprentice (it's his eMail which is shown in the title)
          this.apprentice$ = this.getAbOfReport();

          // load the report from the last period after the apprentice was found
          this.apprentice$.subscribe( (abUserEmail) => {
            if ( abUserEmail ) {
              this.reportFromLastPeriod = reports.find( (r) => r.abUserEmail === abUserEmail && r.sortOrder === (this.report.sortOrder - 1) ) as ReportDto;
            }
          });



          // Wenn über den Link im Einladungsmail die Seite Antworten geöffnet wird, müssen zuerst die Daten geladen werden, bevor die Fragen usw. Angezeigt werden können
          this.isDataLoaded = !!this.report;
        });

      // den currentStep im Stepper aus dem Store holen
      this.store.pipe( select( getReportsInfoStateCurrentAnswerIndexForStepper ) ).subscribe( indexForStepper => {
        this.currentStep = indexForStepper ? indexForStepper : 0;
      });

      // load reportTypeDetails
      this.store.pipe( select( getReportsInfoStateReportTypeDetails ) ).subscribe( reportTypeDetails => {
        // Berichtstypdetails nach der Berichtstyp ID aus dem Bericht filtern
        reportTypeDetails = reportTypeDetails.filter( rtd => rtd.reportTypeUuid === this.report?.reportTypeUuid );

        // Details weiter filtern, dass nur Blöcke und Fragen, die auf dem Bildschirm angezeigt werden enthalten sind
        reportTypeDetails = reportTypeDetails.filter( rtd => rtd.reportBlockOnScreen && !rtd.reportBlockReadonly );
        this.reportTypeDetails = reportTypeDetails;
      });

      // wenn Rolle gesetzt ist, bedeutet es, dass nicht besprochen wird. Antworten müssen nach Rolle herausgesucht werden
      if ( this.role ) {
        this.filterAnswers();
      }

      // keine Rolle gesetzt bedeutet, dass besprochen wird. Alle Antworten von beiden Rollen werden gebraucht, also nicht filtern
      else {
        this.answers = this.report.answers;

        // hint: Antworten nach questionId sortieren, weil sie vom Server nach created/updated sortiert kommen und sie in der Reihenfolge sein sollen
        const answersCopy = [ ...this.report.answers ];
        this.answers = answersCopy.sort( SorterTools.dynamicSort( 'created' ) );

        // Steps erstellen
        this.makeSteps();

      }

      this.findCurrentAnswer();
    });

    // Websocket Event abfangen und neuen currentStep daraus setzen (nur bei Besprechung)
    this.socketService.reportStepChanged$.subscribe( next => {
      if ( next && (next.websocketSyncDto.recordUuid === this.report.uuid) && !this.role ) {
        this.currentStep = next.websocketSyncDto.nextStep as number;
        this.store.dispatch( ReportsActions.setLastMeetingStep( { lastMeetingStep: this.currentStep } ) );
        this.makeSteps();
        this.findCurrentAnswer();
      }
    });

    // Websocket Event für Freigabe von Antworten abfangen und Antwort anzeigen (nur bei Besprechung)
    this.socketService.reportShowAnswerChanged$.subscribe( next => {
      // prüfen, ob im websocketSyncDto die gleiche ID des Berichts ist, wie an der Besprechung und ob die ID der Antwort auch die gleiche ist, wie aktuell angezeigt
      // mit !this.role wird geschaut, ob es sich um die Besprechung handelt, weil dort die Rolle in der URL nicht gesetzt ist
      if ( this.currentAnswer && next && ( next.websocketSyncDto.recordUuid === this.reportUuid && next.websocketSyncDto.answerUuid === this.currentAnswer.uuid ) && !this.role ) {
        let answerIdsShowedToOtherRole: string[] = [];
        this.store.pipe( select( getReportsInfoStateAnswerIdsShowedToOtherRole ) ).subscribe( (answerIds) => answerIdsShowedToOtherRole = answerIds ).unsubscribe();

        // prüfen, ob ID der Antwort aus dem Event schon angezeigt wurde und wenn nicht, wird sie im Store zu den angezeigten Antworten hinzugefügt
        // dient dazu, dass Antworten die schon angezeigt wurden, beim Wechsel im Stepper angezeigt bleiben
        if ( !answerIdsShowedToOtherRole.includes( next.websocketSyncDto.answerUuid as string ) ) {
          this.store.dispatch( ReportsActions.addShowedAnswerId( { answerId: next.websocketSyncDto.answerUuid as string } ) );
        }
      }
    });

  }

  ngOnInit(): void {
    this.screenInfoService.screenResize$.subscribe({
      next: (screenSize) => (this.height = screenSize.height),
    });
  }

  // hack: da answers zuerst immer undefiniert ist gab es nur "false". Workaround: einfach die id der Antwort mitgeben und dann damit den Check erledigen
  // vielleicht ist es anders möglich (wie Offiziell dokumentiert), aber ich habe es nicht zum laufen gebracht.
  isStepValid( uuid: string ): boolean {
    const answer = this.answers?.find( a => a.uuid === uuid) as ReportAnswerDto;

    // bei Besprechung alle Steps als valid markieren, die kleiner oder gleich des letzten Steps an der Besprechung sind.
    if ( !this.role ) {
      let lastMeetingStep;
      this.store.pipe( select( getReportsInfoStateLastMeetingStep ) ).subscribe( (lastStep) => lastMeetingStep = lastStep ).unsubscribe();

      if ( lastMeetingStep && this.answers.indexOf( answer ) <= lastMeetingStep ) {
        return true;
      } else {
        return false;
      }
    }

    if ( this.getQuestionField( answer, 3 ) === 'nq' ) {
      if ( this.isUserPb ) {
        // step als valid markieren, wenn PB Antworten von AB anschaut
        if ( this.role === 'ab' ) {
          return true;
        } else {
          return answer?.pbMark !== undefined;
        }
      } else if ( this.isUserAb ) {
        return answer?.abMark !== undefined;
      }
    } else if ( this.getQuestionField( answer, 3 ) === 'g' ) {
      if ( this.isUserPb ) {
        return answer?.pbExplanation !== undefined;
      } else if ( this.isUserAb ) {
        return answer?.abExplanation !== undefined;
      }
    } else if ( this.getQuestionField( answer, 3 ) === 'tpe' || this.getQuestionField( answer, 3 ) === 'ft' ) {
      return answer.isPbAnswerComplete as boolean;
    } else if ( this.getQuestionField( answer, 3 ) === 'gr' ) {
      // beim Typ freier Text oder Zielbewertung ist der Step valid, wenn die Antwort entweder vom PB oder AB vollständig ist
      return this.role === 'pb' ? answer.isPbAnswerComplete as boolean : answer.isAbAnswerComplete as boolean;
    }

    return false;
  }

  /**
   * Gibt true zurück, wenn die Antwort an der Besprechung freigegeben wurde.
   * Wenn es sich um die Spalte des AB handelt, dann wird isAbColumn auf true gesetzt. Für die PB Spalte auf false.
   *
   * @param isAbColumn
   */
  isAnswerOfOtherRoleDisplayed( isAbColumn: boolean ): boolean {
    let answerIdsShowedToOtherRole: string[] = [];
    this.store.pipe( select( getReportsInfoStateAnswerIdsShowedToOtherRole ) )
      .subscribe( (answerIds) => answerIdsShowedToOtherRole = answerIds ).unsubscribe();

    if ( this.isUserPb ) {
      // Wenn der Benutzer ein PB ist, muss seine Spalte immer angezeigt werden
      if ( !isAbColumn ) {
        return true;
      }
      // Wenn es die Spalte des AB ist, darf sie erst angezeigt werden, wenn sie freigegeben wurde
      else if ( this.currentAnswer && isAbColumn && answerIdsShowedToOtherRole.includes( this.currentAnswer.uuid as string ) ) {
        return true;
      }
    } else if ( this.isUserAb ) {
      // Wenn der Benutzer ein AB ist, muss seine Spalte auch immer angezeigt werden
      if ( isAbColumn ) {
        return true;
      }
      // Wenn es die Spalte des PB ist, wird sie nur angezeigt, wenn er sie freigegeben hat
      else if ( this.currentAnswer && !isAbColumn && answerIdsShowedToOtherRole.includes( this.currentAnswer.uuid as string ) ) {
        return true;
      }
    }

    return false;
  }

  getQuestionField( answer: ReportAnswerDto | undefined, field: 0 | 1 | 2 | 3 ): string {
    if ( !answer ) return '';

    const reportTypeDetail = this.reportTypeDetails?.find( rtd => rtd.questionUuid === answer.questionUuid );

    // 0 = Titel, 1 = Beschreibung, 2 = Hilfetext, 3 = ContentTypeKey
    switch ( field ) {
      case 0:
        return reportTypeDetail?.questionTitle as string;
      case 1:
        return reportTypeDetail?.questionDescription as string;
      case 2:
        return reportTypeDetail?.questionHelpText as string;
      case 3:
        return reportTypeDetail?.contentTypeKey as string;
    }
  }

  getReadonlyMode( isAbColumn?: boolean ): boolean {
    // Antworten nur noch anschauen, wenn AB freigegeben hat
    if ( this.role === 'ab' && this.isUserAb && this.report.state > 0 ) {
      return true;
    }
    // Antworten nur noch zum Anschauen, wenn PB bei Besprechung freigegeben hat
    else if ( this.role === 'pb' && this.isUserPb && this.report.state >= 2  ) {
      return true;
    }
    // PB kann immer nur Antworten von AB anschauen
    else if ( this.role === 'ab' && this.isUserPb ) {
      return true;
    }
    // Bei Besprechung
    else if ( !this.role ) {
      if ( this.isUserAb ) {
        // false (bearbeitbar) zurückgeben, wenn die Frage nur von AB beantwortet werden kann, wie Bewertung Ausbildungsbetrieb
        return !( this.isAnswerForRole( this.currentAnswer as ReportAnswerDto, 'ab' ) && !this.isAnswerForRole( this.currentAnswer as ReportAnswerDto, 'pb' ) );
      } else if ( this.isUserPb ) {
        // PB darf nicht Antwort des AB bearbeiten deshalb kann gleich isAbColumn zurückgegeben werden, weil es bei der AB Spalte immer true ist
        return isAbColumn as boolean;
      }
    }

    return false;
  }

  getNavigationActive( isAbColumn?: boolean ): boolean {
    if ( !this.role ) {
      // Knöpfe für weiter und zurück nur für PB und in seiner Spalte einblenden
      if ( this.isUserPb ) {
        return !isAbColumn;
      } else if ( this.isUserAb ) {
        return false;
      }
    } else {
      // wenn nicht Besprochen wird Navigation immer einblenden zum Beantworten der Fragen
      return true;
    }

    return false;
  }

  /**
   * Gibt alle Ziele eines Berichts zurück. Parameter isLastPeriod muss nicht gesetzt werden, wenn Ziele des aktuellen Berichts geholt werden.
   * Für Ziele der Letzten Periode true für isLastPeriod übergeben.
   *
   * @param isLastPeriod
   */
  getAllGoals( isLastPeriod?: boolean ): ReportAnswerDto[] {
    const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.contentTypeKey === 'g' ) as ReportTypeDetailViewDto;
    const goalQuestionId = reportTypeDetail.questionUuid;

    // hint: this.answers sind die Antworten des aktuellen Berichts
    const answers = isLastPeriod ? this.reportFromLastPeriod.answers : this.report.answers;

    let goals = answers.filter( a => a.questionUuid === goalQuestionId);
    goals = [ ...goals.sort( SorterTools.dynamicSort( 'created' ) ) ];
    return goals;
  }

  getTitle(): string {
    const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.reportTypeUuid === this.report.reportTypeUuid );
    const reportTypeName = reportTypeDetail?.reportTypeName;
    const pageName = this.role ? 'Antworten' : 'Besprechung';

    return `${pageName} ${this.role ? this.role.toUpperCase() : ''}
      des ${reportTypeName} ${this.report.period}
      von `;
  }

  isAnswerForRole( answer: ReportAnswerDto | undefined, role?: string ): boolean {
    if ( !answer ) return false;

    const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.questionUuid === answer.questionUuid ) as ReportTypeDetailViewDto;

    if ( ( role && role === 'ab' ) || this.role === 'ab' ) {
      return reportTypeDetail.reportBlockAbReadWrite;
    } else if ( ( role && role === 'pb' ) || this.role === 'pb' ) {
      return reportTypeDetail.reportBlockPbReadWrite;
    }

    return false;
  }

  isLastStep(): boolean {
    return this.currentStep === (this.steps.length - 1);
  }

  isFirstStep(): boolean {
    return this.currentStep === 0;
  }

  hasQuestionNote(): boolean {
    // todo: note/feedback
    // if ( this.isUserAb && this.singleNoteComponent ) {
    //   return this.singleNoteComponent.note !== undefined;
    // }
    return false;
  }

  onNavigateBack(): void {
    this.router.navigateByUrl(`/reports/reports/ab/${this.report.abUserEmail}/report/${this.report.uuid}`).then();
  }

  onPublishAnswers() {
    // prüfen, ob es nicht vollständige Antworten gibt
    let areAnswersIncomplete = false;

    for ( const answer of this.report.answers ) {
      if ( this.role === 'ab' ) {
        if ( this.isAnswerForRole( answer, 'ab' ) && !answer.isAbAnswerComplete ){
          areAnswersIncomplete = true;
          break;
        }
      } else {
        if ( this.isAnswerForRole( answer, 'pb' ) && !answer.isPbAnswerComplete ){
          areAnswersIncomplete = true;
          break;
        }
      }
    }

    // wenn die Antworten komplett sind, wird der Status erhöht, sonst eine Meldung ausgegeben.
    if ( !areAnswersIncomplete ) {
      const reportUpdateDto: ReportDto = {
        publishDatePb: this.isUserPb ? new Date().toISOString() : this.report.publishDatePb,
        state: this.report.state + 1,
      } as ReportDto;

      this.store.dispatch( ReportsActions.updateReport( { id: this.report.uuid as string, reportUpdateDto } ) );
      this.onNavigateBack();
    } else {
      this.messageService.showWarning( 'Nicht alle Antworten wurden geschrieben.' );
    }
  }

  onCurrentStepChange( newStep: number ): void {
    this.currentStep = newStep;
    this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: this.currentStep } ) );
    this.findCurrentAnswer();

    // Websocket Event auslösen für Screensync an Besprechung
    if ( !this.role ) {
      this.createWebsocketSyncEvent();
    }
  }

  onStepActivate( ev: StepperActivateEvent ): void {
    // Mit Rolle erkennen, ob es eine Besprechung ist
    if ( !this.role ) {
      // Verhindert, dass der AB an der Besprechung im Stepper die Frage wechseln kann.
      if ( this.isUserAb ) {
        ev.preventDefault();
        return;
      }

      let lastMeetingStep;
      this.store.pipe( select( getReportsInfoStateLastMeetingStep ) ).subscribe( lastStep => {
        lastMeetingStep = lastStep;
      }).unsubscribe();

      if ( lastMeetingStep ) {
        // Step nur verhindern, wenn index grösser als der letzte Step + 1 ist, damit auch auf alle vorherigen Schritte gewechselt werden kann.
        if ( ev.index <= lastMeetingStep || ev.index === lastMeetingStep + 1 ) {
          // letzter Step im Store nur speichern, wenn der neue Step grösser, als der im Store ist.
          if ( ev.index > lastMeetingStep ) {
            this.store.dispatch( ReportsActions.setLastMeetingStep( { lastMeetingStep: ev.index } ) );
          }
        } else {
          ev.preventDefault();
        }
      }

    }
    // Wenn keine Besprechung
    else {
      // Sorgt dafür, dass der Stepper nur Schritte anzeigt, die valid sind oder der vorherige Step valid ist.
      if ( this.steps[ev.index - 1] && !this.steps[ev.index - 1].isValid ) {
        ev.preventDefault();
      }
    }
  }

  /**
   * Speichert Antwort und geht zum nächsten Schritt im Stepper, wenn saveOnly false oder nicht gesetzt ist.
   * Wenn saveOnly true ist, wird die Antwort nur gespeichert und der Stepper bleibt beim aktuellen Schritt stehen.
   *
   * @param answerUpdate
   * @param saveOnly?
   */
  onSaveAndNext( answerUpdate: { updateDto: ReportAnswerDto; uuid: string } | undefined, saveOnly?: boolean ): void {
    if ( answerUpdate && this.currentAnswer ) {
      this.store.dispatch( ReportsActions.updateAnswer(
        {
          // id aus answerUpdate wird nur bei Zielen gebraucht, sonst id von currentAnswer
          id: answerUpdate.uuid ? answerUpdate.uuid as string : this.currentAnswer.uuid as string,
          reportAnswerUpdateDto: answerUpdate.updateDto,
        },
      ) );
      // mark the current step as done
      this.steps[this.currentStep].icon = 'check';
      this.steps[this.currentStep].isValid = true;
    }
    // saveOnly auf true bedeutet, dass nur gespeichert wird und der Stepper nicht weitergeht. Nicht gesetzt oder false geht zum nächsten Schritt im Stepper.
    if ( !saveOnly && this.currentStep < this.steps.length - 1 ) {
      // mark the current step as done
      this.steps[this.currentStep].icon = 'check';
      this.steps[this.currentStep].isValid = true;

      this.currentStep++;

      // currentStep im Store aktualisieren
      this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: this.currentStep } ) );

      // Websocket Event von Server auslösen lassen, um nächste Frage dem AB anzeigen zu lassen. Letzter Step an Besprechung in Store speichern, damit
      // die Überprüfungen gemacht werden können, sodass nur ein Schritt vor geblättert werden kann.
      if ( !this.role ) {
        this.createWebsocketSyncEvent();
        this.store.dispatch( ReportsActions.setLastMeetingStep( { lastMeetingStep: this.currentStep } ) );
      }
    }
    this.findCurrentAnswer();
  }

  onBack(): void {
    this.currentStep--;

    // currentStep im Store aktualisieren
    this.store.dispatch( ReportsActions.setCurrentStep( { currentStep: this.currentStep } ) );

    if ( !this.role ) {
      this.createWebsocketSyncEvent();
    }
    this.findCurrentAnswer();
  }

  onBackOrNext( answer: ReportAnswerDto ): void {
    if ( answer ) {
      this.currentStep++;
    } else {
      this.currentStep--;
    }
    const reportTypeDetail = this.reportTypeDetails.find(rtd => rtd.questionTitle === this.steps[this.currentStep].label) as ReportTypeDetailViewDto;
    this.currentAnswer = this.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid) as ReportAnswerDto;

  }

  onWriteNote(): void {
    // todo: note/feedback (ab can no longer write notes)
    this.noteFeedbackComponent.openDialog();
    // this.singleNoteComponent.isWindowOpen = true;
  }

  onEditAbNote(): void {
    // todo: note/feedback
    // this.singleNoteComponent.onEditAbNote();
  }

  /**
   * Löst ein Websocket Event für das Anzeigenlassen einer Antwort der anderen Partei aus.
   */
  onShowAnswerToOtherRole(): void {
    const websocketSyncDto: WebSocketSyncDto = {
      answerUuid: this.currentAnswer?.uuid as string,
      eventName: UiConstants.instance.customWebSocketEvents.onReportDiscussionShowAnswer,
      recordUuid: this.report.uuid as string,
      nextStep: undefined,
    };

    this.store.dispatch( ReportsActions.createWebsocketSyncEvent({ websocket: websocketSyncDto } ) );
  }

  private filterAnswers(): void {
    // Hier werden die Antworten herausgefiltert, die die in der URL gesetzte Rolle beantworten darf
    if ( !this.report?.answers ) {
      return;
    }

    for ( const answer of this.report.answers ) {
      // jump over an answer if it was found in this.answers
      // if ( this.answers.find( a => a.uuid === answer.uuid ) ) {
      //   continue;
      // }
      const reportTypeDetail = this.reportTypeDetails.find( rtd => rtd.questionUuid === answer.questionUuid );

      if ( reportTypeDetail?.reportBlockAbReadWrite && this.role === 'ab' ) {
        this.answers.push( answer );
      } else if ( reportTypeDetail?.reportBlockPbReadWrite && this.role === 'pb' ) {
        this.answers.push( answer );
      }
    }

    // hint: Antworten nach questionId sortieren, weil sie vom Server nach created/updated sortiert kommen und sie in der Reihenfolge sein sollen
    this.answers.sort( SorterTools.dynamicSort( 'created' ) );

    // name der steps reinpushen
    this.makeSteps();

  }

  private makeSteps(): void {
    this.steps = [];
    let isGoalInSteps = false;

    for ( const answer of this.answers ) {
      // Wenn schon ein Schritt für Ziele vorhanden ist, überspringen
      if ( this.getQuestionField( answer, 3 ) === 'g' && isGoalInSteps ) {
        continue;
      } else if ( this.getQuestionField( answer, 3 ) === 'g' ) {
        isGoalInSteps = true;
        this.goalStep = this.steps.length;
      }

      const isValid = this.isStepValid(answer.uuid as string);
      this.steps.push( {
          contentType: this.getQuestionField(answer, 3),
          icon: isValid ? 'check' : 'warning',
          isValid,
          label: this.getQuestionField(answer, 0),
          themeColor: 'secondary',
        } );
    }
  }

  private findCurrentAnswer(): void {
    // use the steps label with the current step to get the reportTypeDetail.
    const reportTypeDetail = this.reportTypeDetails.find(rtd => rtd.questionTitle === this.steps[this.currentStep].label) as ReportTypeDetailViewDto;
    this.currentAnswer = this.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid);
  }

  private createWebsocketSyncEvent(): void {
    const websocketSyncDto: WebSocketSyncDto = {
      eventName: UiConstants.instance.customWebSocketEvents.onReportDiscussionStepChange,
      nextStep: this.currentStep,
      recordUuid: this.report.uuid as string,
      answerUuid: undefined,
    };

    this.store.dispatch( ReportsActions.createWebsocketSyncEvent({ websocket: websocketSyncDto } ) );
  }

  private getAbOfReport(): Observable<string> {
    if ( !this.profile ) {
      // just return an empty name if the profile is undefined
      return of('');
    }
    const moduleInfo = this.profile.moduleInfoArr.find( (module) => module.moduleName === ModuleConstants.instance.moduleName );
    return this.http.get<string[]>( `${moduleInfo?.apiUrl}/generic/users`).pipe(
      map( (users) => users.find( u => u === this.report.abUserEmail ) as string ),
    );
  }

}
