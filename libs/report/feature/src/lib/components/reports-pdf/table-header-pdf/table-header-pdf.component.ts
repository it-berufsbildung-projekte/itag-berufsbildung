import { Component } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-table-header-pdf',
  styleUrls: ['./table-header-pdf.component.scss'],
  templateUrl: './table-header-pdf.component.html',
})
export class TableHeaderPdfComponent {}
