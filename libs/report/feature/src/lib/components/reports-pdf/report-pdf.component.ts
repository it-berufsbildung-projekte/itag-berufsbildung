import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportAnswerDto, ReportDto, ReportTypeDetailViewDto } from '@itag-berufsbildung/report/data';
import { UserNameTool } from '@itag-berufsbildung/shared/util';
import { select, Store } from '@ngrx/store';
import { getReportsInfoStateReports, getReportsInfoStateReportTypeDetails } from '../reports.selectors';

@Component({
  selector: 'itag-berufsbildung-report-pdf',
  styleUrls: ['./report-pdf.component.scss'],
  templateUrl: './report-pdf.component.html',
})
export class ReportPdfComponent {

  currentDate = new Date();
  reportUuid!: string;
  report!: ReportDto;
  reportFromLastPeriod!: ReportDto;
  reportTypeDetailsForCurrentReportType: ReportTypeDetailViewDto[] = [];
  reportTypeDetails: ReportTypeDetailViewDto[] = [];
  reportTypeName!: string;
  abUser!: string;
  pbUser!: string;
  pageTitle!: string;

  reportBlocks: string[] = [];

  constructor(
    private readonly store: Store,
    private route: ActivatedRoute,
    private readonly router: Router,
  ) {
    this.route.paramMap.subscribe( params => {
      const reportId = params.get('reportId');
      if ( reportId ) {
        this.reportUuid = reportId;

        this.store.pipe( select( getReportsInfoStateReports ) ).subscribe( reports => {
          const report = reports.find( r => r.uuid === this.reportUuid ) as ReportDto;

          if ( report ) {
            this.report = report;
            this.abUser = report.abUserEmail;
            this.pbUser = report.pbUserEmail;

            // find the report from last period
            this.reportFromLastPeriod = reports.find( (r) => r.abUserEmail === this.abUser && r.sortOrder === (this.report.sortOrder - 1) ) as ReportDto;

            this.store.pipe( select( getReportsInfoStateReportTypeDetails ) ).subscribe( rtd => {
              this.reportTypeDetails = rtd;
              this.reportTypeDetailsForCurrentReportType = rtd.filter( r => r.reportTypeUuid === this.report.reportTypeUuid );
            });

            this.reportBlocks = this.getReportBlocks();
            this.reportTypeName = this.getReportTypeName();
            this.pageTitle = this.getPageTitle();
          }

        });

      }
    });

  }

  onNavigateBack(): void {
    this.router.navigateByUrl(`/reports/reports/ab/${this.report.abUserEmail}/report/${this.report.uuid}`).then();
  }

  getReportTypeName(): string {
    return this.reportTypeDetailsForCurrentReportType[0].reportTypeName;
  }

  getReportTypeDetailsOfBlock( reportBlockTitle: string ): ReportTypeDetailViewDto[] {
    return this.reportTypeDetailsForCurrentReportType.filter( rtd => rtd.reportBlockTitle === reportBlockTitle);
  }

  getMarkOfAnswer( reportTypeDetail: ReportTypeDetailViewDto ): string {
    const answer = this.report.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid ) as ReportAnswerDto;

    if ( reportTypeDetail.reportBlockAbReadWrite && !reportTypeDetail.reportBlockPbReadWrite ) {
      return answer.abMark as string;
    } else {
      return answer.pbMark as string;
    }
  }

  getExplanationOfAnswer( reportTypeDetail: ReportTypeDetailViewDto ): string {
    const answer = this.report.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid ) as ReportAnswerDto;

    if ( reportTypeDetail.reportBlockAbReadWrite && !reportTypeDetail.reportBlockPbReadWrite ) {
      return answer.abExplanation as string;
    } else {
      return answer.pbExplanation as string;
    }
  }

  getGoals( reportTypeDetail: ReportTypeDetailViewDto ): ReportAnswerDto[] {
    return this.report.answers.filter( a => a.questionUuid === reportTypeDetail.questionUuid );
  }

  getGoalsFromLastPeriod(): ReportAnswerDto[] {
    const goalDetails = this.reportTypeDetails
      .find( ( rtd) => rtd.contentTypeKey === 'g' && rtd.reportTypeUuid === this.reportFromLastPeriod.reportTypeUuid );

    if ( goalDetails ) {
      return this.reportFromLastPeriod.answers.filter( a => a.questionUuid === goalDetails.questionUuid );
    }

    return [];
  }

  getAnswerToQuestion( reportTypeDetail: ReportTypeDetailViewDto ): ReportAnswerDto {
    return this.report.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid ) as ReportAnswerDto;
  }

  getTrialPeriodEndText( reportTypeDetail: ReportTypeDetailViewDto ): string {
    const answer = this.report.answers.find( a => a.questionUuid === reportTypeDetail.questionUuid );
    if ( answer && answer.pbExplanation ) {
      const explanationSplit = answer.pbExplanation.split('¦');
      return explanationSplit[1];
    }
    return '';
  }

  getPageTitle(): string {
    return `Vorschau ${this.reportTypeName} ${this.report.period} von ${UserNameTool.getFullName( this.abUser )}`;
  }

  private getReportBlocks(): string[] {
    const reportBlocks = [];
    let reportBlockTitle = '';

    for ( const rtd of this.reportTypeDetailsForCurrentReportType ) {
      if ( rtd.reportBlockTitle !== reportBlockTitle ) {
        reportBlockTitle = rtd.reportBlockTitle;
        reportBlocks.push( reportBlockTitle );
      }
    }

    return reportBlocks;
  }

}
