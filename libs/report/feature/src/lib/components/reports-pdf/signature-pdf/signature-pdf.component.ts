import { Component, Input } from '@angular/core';
import { ReportDto } from '@itag-berufsbildung/report/data';
import { UserNameTool } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-signature-pdf',
  styleUrls: ['./signature-pdf.component.scss'],
  templateUrl: './signature-pdf.component.html',
})
export class SignaturePdfComponent {

  @Input()
  report!: ReportDto;

  @Input()
  abUser!: string;

  @Input()
  pbUser!: string;

  getSignatureText( role: 'pb' | 'ab' ): string {
    return `${ role === 'pb' ? 'Praxisbildner' : 'Auszubildender'} ${ role === 'pb' ? UserNameTool.getFullName(this.pbUser) : UserNameTool.getFullName(this.abUser)}
    hat den Bericht elektronisch am ${role === 'pb' ? this.getSignatureDate( 'pb' ) : this.getSignatureDate( 'ab' )} signiert`;
  }

  private getSignatureDate( role: 'pb' | 'ab' ): string {
    if ( role === 'ab' && !this.report.acceptedDate ) {
      return '(noch offen)';
    }
    const date = new Date( role === 'pb' ? this.report.publishDatePb : this.report.acceptedDate );
    return date.toLocaleDateString();
  }

}
