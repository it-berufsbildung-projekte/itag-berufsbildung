import { Component, Input } from '@angular/core';
import { ReportAnswerDto } from '@itag-berufsbildung/report/data';

@Component({
  selector: 'itag-berufsbildung-goal-pdf',
  styleUrls: ['./goal-pdf.component.scss'],
  templateUrl: './goal-pdf.component.html',
})
export class GoalPdfComponent {

  @Input()
  goals!: ReportAnswerDto[];

  getGoalPart( goal: ReportAnswerDto, indexOfPart: 0 | 1 | 2 | 3 ): string {
    const explanationSplit = goal?.pbExplanation?.split('¦');

    // indexOfPart: 0 = Kompetenz, 1 = Ziel, 2 = Bis, 3 = Massnahme
    return explanationSplit && explanationSplit.length > 0 ? explanationSplit[indexOfPart] : '';
  }

}
