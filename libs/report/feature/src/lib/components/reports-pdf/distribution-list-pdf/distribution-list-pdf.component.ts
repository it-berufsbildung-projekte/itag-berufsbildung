import { Component, Input } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-distribution-list-pdf',
  styleUrls: ['./distribution-list-pdf.component.scss'],
  templateUrl: './distribution-list-pdf.component.html',
})
export class DistributionListPdfComponent {

  @Input()
  abUser!: string;

}
