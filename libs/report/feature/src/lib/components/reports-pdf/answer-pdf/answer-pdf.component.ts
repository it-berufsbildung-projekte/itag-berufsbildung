import { Component, Input } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-answer-pdf',
  styleUrls: ['./answer-pdf.component.scss'],
  templateUrl: './answer-pdf.component.html',
})
export class AnswerPdfComponent {

  @Input()
  questionDescription!: string;

  @Input()
  mark!: string;

  @Input()
  explanation!: string;

}
