import { Component, Input } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-pdf-header',
  styleUrls: ['./pdf-header.component.scss'],
  templateUrl: './pdf-header.component.html',
})
export class PdfHeaderComponent {

  @Input()
  reportTypeName!: string;

}
