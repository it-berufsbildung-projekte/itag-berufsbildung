import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { AdminModuleInfoDto, RoleHashDto, UiUserDto } from '@itag-berufsbildung/shared/util';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'itag-berufsbildung-details-pdf',
  styleUrls: ['./details-pdf.component.scss'],
  templateUrl: './details-pdf.component.html',
})
export class DetailsPdfComponent implements OnInit {

  @Input()
  abUser!: string;

  @Input()
  pbUser!: string;

  @Input()
  period!: string;

  year$: Observable<number> = of(0);
  private user!: UiUserDto;

  constructor(
    private readonly userService: UserService,
    private readonly http: HttpClient,
    ) {
    this.userService.userDto$.subscribe( (user) => {
      if ( user ) {
        this.user = user;
      }
    });
  }

  ngOnInit(): void {
    const moduleInfo: AdminModuleInfoDto = this.user.moduleInfoArr.find( (module) => module.moduleName === ModuleConstants.instance.moduleName ) as AdminModuleInfoDto;
    const currentTime = new Date().getTime();

    this.http.get<RoleHashDto>( `${moduleInfo.apiUrl}/generic/user-roles/${this.abUser}`).subscribe( (roleHash) => {
      if ( roleHash ) {
        const abFromDateTime = new Date( roleHash[ModuleConstants.instance.availableRoles.ab].from);
        this.year$ = of(Math.ceil((currentTime - abFromDateTime.getTime()) / 1000 / 60 / 60 / 24 / 31 / 12));
      }
    });
  }

}
