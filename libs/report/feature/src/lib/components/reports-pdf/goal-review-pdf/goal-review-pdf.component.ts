import { Component, Input, OnInit } from '@angular/core';
import { ReportAnswerDto } from '@itag-berufsbildung/report/data';

interface IGoalReview {
  competence: string;
  explanation: string;
  mark: string;
  textFromLastGoal: string;
}

@Component({
  selector: 'itag-berufsbildung-goal-review-pdf',
  styleUrls: ['./goal-review-pdf.component.scss'],
  templateUrl: './goal-review-pdf.component.html',
})
export class GoalReviewPdfComponent implements OnInit{

  @Input()
  goalReviewAnswer!: ReportAnswerDto;

  @Input()
  goalsFromLastPeriod: ReportAnswerDto[] = [];

  goalReviews: IGoalReview[] = [];

  ngOnInit(): void {
    const goalReviewsFromExplanation = this.goalReviewAnswer?.pbExplanation?.split('¦');

    if ( goalReviewsFromExplanation ) {
      for ( const goalReview of goalReviewsFromExplanation ) {
        if ( goalReview.length === 0 ) {
          continue;
        }

        const goalReviewSplit = goalReview.split(';');
        const goalIdFromLastPeriod = goalReviewSplit[0];
        const goalFromLastPeriod = this.goalsFromLastPeriod.find( (g) => g.uuid === goalIdFromLastPeriod) as ReportAnswerDto;
        const goalFromLastPeriodSplit = goalFromLastPeriod?.pbExplanation?.split('¦');

        if ( goalFromLastPeriodSplit && goalFromLastPeriodSplit.length > 0 ) {
          this.goalReviews.push( {
            competence: goalFromLastPeriodSplit[0],
            explanation: goalReviewSplit[2],
            mark: goalReviewSplit[1],
            textFromLastGoal: goalFromLastPeriodSplit[1] as string,
          } );
        }
      }
    }
  }

}
