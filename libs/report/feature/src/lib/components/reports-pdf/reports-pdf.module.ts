import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FullNameFromEmailPipe, KendoUiModules } from "@itag-berufsbildung/shared/data-ui";
import { AnswerPdfComponent } from './answer-pdf/answer-pdf.component';
import { DetailsPdfComponent } from './details-pdf/details-pdf.component';
import { DistributionListPdfComponent } from './distribution-list-pdf/distribution-list-pdf.component';
import { GoalPdfComponent } from './goal-pdf/goal-pdf.component';
import { ReportPdfComponent } from './report-pdf.component';
import { SignaturePdfComponent } from './signature-pdf/signature-pdf.component';
import { TableHeaderPdfComponent } from './table-header-pdf/table-header-pdf.component';
import { TrialPeriodEndPdfComponent } from './trial-period-end-pdf/trial-period-end-pdf.component';
import { FreeTextPdfComponent } from './free-text-pdf/free-text-pdf.component';
import { GoalReviewPdfComponent } from './goal-review-pdf/goal-review-pdf.component';
import { PdfHeaderComponent } from './pdf-header/pdf-header.component';

@NgModule({
  declarations: [
    ReportPdfComponent,
    AnswerPdfComponent,
    GoalPdfComponent,
    TrialPeriodEndPdfComponent,
    DetailsPdfComponent,
    SignaturePdfComponent,
    DistributionListPdfComponent,
    TableHeaderPdfComponent,
    FreeTextPdfComponent,
    GoalReviewPdfComponent,
    PdfHeaderComponent,
  ],
  imports: [
    CommonModule,
    KendoUiModules,
    FullNameFromEmailPipe
  ]
})
export class ReportsPdfModule { }
