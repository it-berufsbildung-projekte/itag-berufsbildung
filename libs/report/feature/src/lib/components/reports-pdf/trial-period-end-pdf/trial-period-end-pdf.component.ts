import { Component, Input } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-trial-period-end-pdf',
  styleUrls: ['./trial-period-end-pdf.component.scss'],
  templateUrl: './trial-period-end-pdf.component.html',
})
export class TrialPeriodEndPdfComponent {

  @Input()
  trialPeriodEndText!: string;

}
