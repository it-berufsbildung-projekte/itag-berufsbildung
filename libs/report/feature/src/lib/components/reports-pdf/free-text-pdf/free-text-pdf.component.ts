import { Component, Input } from '@angular/core';

@Component({
  selector: 'itag-berufsbildung-free-text-pdf',
  styleUrls: ['./free-text-pdf.component.scss'],
  templateUrl: './free-text-pdf.component.html',
})
export class FreeTextPdfComponent {

  @Input()
  text!: string;

}
