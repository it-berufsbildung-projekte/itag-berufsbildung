import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, NoteFeedbackDto } from '@itag-berufsbildung/report/data';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';

export interface INoteFeedbackState {
  noteFeedbacks: NoteFeedbackDto[];
}

@Injectable({
  providedIn: "root",
})
export class NoteFeedbackStore extends ComponentStore<INoteFeedbackState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: INoteFeedbackState;

  constructor(private readonly http: HttpClient, private readonly notificationService: NotificationService, private readonly userService: UserService) {
    super({
      noteFeedbacks: [],
    });

    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          const moduleInfo = user.moduleInfoArr.find((module) => module.moduleName === ModuleConstants.instance.moduleName);
          this.apiUrl = moduleInfo?.apiUrl as string;

          this.load();
        }
      },
    });
  }

  readonly noteFeedbacks$: Observable<NoteFeedbackDto[]> = this.select((state) => state.noteFeedbacks);

  private load = this.effect((origin$: Observable<void>) =>
    origin$.pipe(
      switchMap(() => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.noteFeedback}`;
        return this.http.get<NoteFeedbackDto[]>(url);
      }),
      tapResponse((noteFeedbacks: NoteFeedbackDto[]) => this.patchState(() => ({ noteFeedbacks })), console.error)
    )
  );

  add(noteFeedbackDto: NoteFeedbackDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.noteFeedback}`;
      this.http.post<NoteFeedbackDto>(url, noteFeedbackDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      return { noteFeedbacks: [...state.noteFeedbacks, noteFeedbackDto] };
    });
  }

  update(noteFeedbackDto: NoteFeedbackDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.noteFeedback}/${noteFeedbackDto}`;
      this.http.patch(url, noteFeedbackDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      const index = state.noteFeedbacks.findIndex((e) => e.uuid === noteFeedbackDto.uuid);
      const noteFeedbacks = [...state.noteFeedbacks];
      noteFeedbacks[index] = noteFeedbackDto;
      return { noteFeedbacks };
    });
  }

  delete(noteFeedbackDto: NoteFeedbackDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.noteFeedback}/${noteFeedbackDto.uuid}`;
      this.http.delete(url).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });
      return { noteFeedbacks: state.noteFeedbacks.filter((e) => e.uuid !== noteFeedbackDto.uuid) };
    });
  }

  private revertChangesAndShowError(error: HttpErrorResponse): void {
    this.patchState(() => {
      this.notificationService.showError(error.message);
      return { ...this.oldState };
    });
  }
}
