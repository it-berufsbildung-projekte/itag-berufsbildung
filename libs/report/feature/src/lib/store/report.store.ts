import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants, ReportDto } from '@itag-berufsbildung/report/data';
import { AppNotificationService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';

export interface IReportState {
  reports: ReportDto[];
}

@Injectable()
export class ReportStore extends ComponentStore<IReportState> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: IReportState;

  constructor(private readonly http: HttpClient, private readonly notificationService: AppNotificationService, private readonly userService: UserService) {
    super({
      reports: [],
    });

    this.userService.userDto$.subscribe({
      next: (user) => {
        if (user) {
          const moduleInfo = user.moduleInfoArr.find((module) => module.moduleName === ModuleConstants.instance.moduleName);
          this.apiUrl = moduleInfo?.apiUrl as string;

          this.load();
        }
      },
    });
  }

  readonly reports$: Observable<ReportDto[]> = this.select((state) => state.reports);

  private load = this.effect((origin$: Observable<void>) =>
    origin$.pipe(
      switchMap(() => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.report}`;
        return this.http.get<ReportDto[]>(url);
      }),
      tapResponse((reports: ReportDto[]) => this.patchState(() => ({ reports })), console.error)
    )
  );

  add(reportDto: ReportDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.report}`;
      this.http.post<ReportDto>(url, reportDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      return { reports: [...state.reports, reportDto] };
    });
  }

  update(reportDto: ReportDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.report}/${reportDto}`;
      this.http.patch(url, reportDto).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });

      const index = state.reports.findIndex((e) => e.uuid === reportDto.uuid);
      const reports = [...state.reports];
      reports[index] = reportDto;
      return { reports };
    });
  }

  delete(reportDto: ReportDto): void {
    this.patchState((state) => {
      this.oldState = ObjectTools.cloneDeep(state);

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.report}/${reportDto.uuid}`;
      this.http.delete(url).subscribe({
        error: (err) => {
          this.revertChangesAndShowError(err);
        },
      });
      return { reports: state.reports.filter((e) => e.uuid !== reportDto.uuid) };
    });
  }

  private revertChangesAndShowError(error: HttpErrorResponse): void {
    this.patchState(() => {
      this.notificationService.showError(error.message);
      return { ...this.oldState };
    });
  }
}
