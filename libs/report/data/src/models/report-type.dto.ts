import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ReportTypeDto extends BaseDocumentDto {
  sortOrder = 100;
  type = 'ReportType example type';
  version = '2021-1';

  constructor() {
    super();
  }
}
