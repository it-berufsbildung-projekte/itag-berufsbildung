
export class ReportTypeDetailViewDto {
  contentTypeKey = 'example content type key';
  contentTypeVersion = 'example version';
  questionDescription = 'example description';
  questionHelpText = 'example help text';
  questionSortOrder = 10;
  questionTitle = 'example title';
  questionUuid = '2';
  reportBlockAbReadWrite = true;
  reportBlockOnPdf = true;
  reportBlockOnScreen = true;
  reportBlockPbReadWrite = true;
  reportBlockReadonly = false;
  reportBlockSortOrder = 10;
  reportBlockTitle = 'example title';
  reportTypeName = 'example';
  reportTypeUuid =  '1';
  reportTypeVersion = 'example version';
}
