
export class WebSocketSyncDto {
  answerUuid: string | undefined = 'abc';
  eventName = 'reportEvent';
  nextStep: number | undefined = 3;
  recordUuid = 'abc';
}
