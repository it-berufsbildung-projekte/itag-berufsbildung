import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ReportTypeReportBlockDto extends BaseDocumentDto {
  reportBlockUuid = 'abc';
  reportTypeUuid = 'abc';
  sortOrder = 10;

  constructor() {
    super();
  }
}
