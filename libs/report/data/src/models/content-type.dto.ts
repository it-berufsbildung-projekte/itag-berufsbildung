import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ContentTypeDto extends BaseDocumentDto {
  key = 'nq';
  typeName = 'name';
  version = 'ContentType example version';

  constructor() {
    super();
  }
}
