import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ReportBlockQuestionDto extends BaseDocumentDto {
  questionUuid = 'abc';
  reportBlockUuid = 'abc';
  sortOrder = 10;

  constructor() {
    super();
  }
}
