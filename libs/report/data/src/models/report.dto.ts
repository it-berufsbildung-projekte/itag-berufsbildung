import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';
import { ReportAnswerDto } from './report-answer.dto';

// todo: add sort order for sorting
export class ReportDto extends BaseDocumentDto {
  abUserEmail = 'ab@email.ch';
  acceptedDate: Date | string = new Date();
  answers: ReportAnswerDto[] = [];
  bbUserEmail = 'bb@email.ch';
  // created = new Date();
  dueDateAnswers: Date | string = new Date();
  dueDateReport = new Date();
  hasMarkD = false;
  meetingDate: Date | string = new Date();
  pbUserEmail = 'pb@email.ch';
  period = '1.8.2022 - 31.10.2022';
  publishDatePb: Date | string = new Date();
  reportTypeUuid = '2';
  sentReminders = 0;
  sortOrder = 0;
  state = 0;

  constructor() {
    super();
  }
}
