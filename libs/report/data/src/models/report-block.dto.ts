import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ReportBlockDto extends BaseDocumentDto {
  abReadWrite = true;
  onPdf = true;
  onScreen = true;
  pbReadWrite = true;
  readonly = false;
  title = 'ReportBlock example title';

  constructor() {
    super();
  }
}
