import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class QuestionDto extends BaseDocumentDto {
  contentTypeUuid = 'exampleUuid';
  description = 'example description';
  helpText = 'example help text';
  title = 'Question example title';

  constructor() {
    super();
  }
}
