import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class NoteFeedbackDto extends BaseDocumentDto {
  abEmail = 'ab@email.ch';
  date: string | Date = new Date();
  note = 'abc';
  pbEmail = 'pb@email.ch';
  rating = 'neutral';

  constructor() {
    super();
  }
}
