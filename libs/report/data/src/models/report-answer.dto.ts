import { BaseDocumentDto } from '@itag-berufsbildung/shared/util';

export class ReportAnswerDto extends BaseDocumentDto {
  abExplanation?: string = 'explanation';
  abMark?: string = 'mark';
  created: string | Date = new Date();
  isAbAnswerComplete?: boolean = false;
  isPbAnswerComplete?: boolean = false;
  pbExplanation?: string = 'explanation';
  pbMark?: string = 'mark';
  questionUuid = 'abc';
  reportUuid = 'abc';

  constructor() {
    super();
  }
}
