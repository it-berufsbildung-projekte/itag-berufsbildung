import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
      report: {
        canCreate: 'api-report-can-create',
        canCreateForApprenticeship: 'âpi-report-can-create-for-apprenticeship',
        canModifyDelete: 'api-report-can-modify-delete',
        canRead: 'api-report-can-read',
        canRemove: 'api-report-can-remove',
        canShowDeleted: 'api-report-can-show-deleted',
        canUpdate: 'api-report-can-update',
        canSeeHistory: 'api-report-can-see-history',
      },
      reportAnswer: {
        canCreate: 'api-report-answer-can-create',
        canModifyDelete: 'api-report-answer-can-modify-delete',
        canRead: 'api-report-answer-can-read',
        canRemove: 'api-report-answer-can-remove',
        canShowDeleted: 'api-report-answer-can-show-deleted',
        canUpdate: 'api-report-answer-can-update',
        canSeeHistory: 'api-report-answer-can-see-history',
      },
      reportType: {
        canCreate: 'api-report-type-can-create',
        canModifyDelete: 'api-report-type-can-modify-delete',
        canRead: 'api-report-type-can-read',
        canRemove: 'api-report-type-can-remove',
        canShowDeleted: 'api-report-type-can-show-deleted',
        canUpdate: 'api-report-type-can-update',
        canSeeHistory: 'api-report-type-can-see-history',
      },
      reportBlock: {
        canCreate: 'api-report-block-can-create',
        canModifyDelete: 'api-report-block-can-modify-delete',
        canRead: 'api-report-block-can-read',
        canRemove: 'api-report-block-can-remove',
        canShowDeleted: 'api-report-block-can-show-deleted',
        canUpdate: 'api-report-block-can-update',
        canSeeHistory: 'api-report-block-can-see-history',
      },
      contentType: {
        canCreate: 'api-content-type-can-create',
        canModifyDelete: 'api-content-type-can-modify-delete',
        canRead: 'api-content-type-can-read',
        canRemove: 'api-content-type-can-remove',
        canShowDeleted: 'api-content-type-can-show-deleted',
        canUpdate: 'api-content-type-can-update',
        canSeeHistory: 'api-content-type-can-see-history',
      },
      question: {
        canCreate: 'api-question-can-create',
        canModifyDelete: 'api-question-can-modify-delete',
        canRead: 'api-question-can-read',
        canRemove: 'api-question-can-remove',
        canShowDeleted: 'api-question-can-show-deleted',
        canUpdate: 'api-question-can-update',
        canSeeHistory: 'api-question-can-see-history',
      },
      reportBlockQuestion: {
        canCreate: 'api-report-block-question-can-create',
        canModifyDelete: 'api-report-block-question-can-modify-delete',
        canRead: 'api-report-block-question-can-read',
        canRemove: 'api-report-block-question-can-remove',
        canShowDeleted: 'api-report-block-question-can-show-deleted',
        canUpdate: 'api-report-block-question-can-update',
        canSeeHistory: 'api-report-block-question-can-see-history',
      },
      reportTypeReportBlock: {
        canCreate: 'api-report-type-report-block-can-create',
        canModifyDelete: 'api-report-type-report-block-can-modify-delete',
        canRead: 'api-report-type-report-block-can-read',
        canRemove: 'api-report-type-report-block-can-remove',
        canShowDeleted: 'api-report-type-report-block-can-show-deleted',
        canUpdate: 'api-report-type-report-block-can-update',
        canSeeHistory: 'api-report-type-report-block-can-see-history',
      },
      noteFeedback: {
        canCreate: 'api-note-feedback-can-create',
        canModifyDelete: 'api-note-feedback-can-modify-delete',
        canRead: 'api-note-feedback-can-read',
        canRemove: 'api-note-feedback-can-remove',
        canShowDeleted: 'api-note-feedback-can-show-deleted',
        canUpdate: 'api-note-feedback-can-update',
        canSeeHistory: 'api-note-feedback-can-see-history',
      },
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
      canShowReportMenu: 'menu-can-show-report-menu',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
    },
  };

  readonly availableRoles = {
    api: 'api',
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
    bb: 'bb',
    pb: 'pb',
    ab: 'ab',
  };

  readonly description = 'Beschreibung des report Modules';

  readonly documentNames = {
    single: {
      report: 'report',
      reportAnswer: 'reportAnswer',
      reportType: 'reportType',
      reportBlock: 'reportBlock',
      contentType: 'contentType',
      question: 'question',
      reportBlockQuestion: 'reportBlockQuestion',
      reportTypeReportBlock: 'reportTypeReportBlock',
      noteFeedback: 'noteFeedback',
    },
    plural: {
      report: 'report',
      reportAnswer: 'reportAnswer',
      reportType: 'reportType',
      reportBlock: 'reportBlock',
      contentType: 'contentType',
      question: 'question',
      reportBlockQuestion: 'reportBlockQuestion',
      reportTypeReportBlock: 'reportTypeReportBlock',
      noteFeedback: 'noteFeedback',
    },
  };

  readonly documents = {
    report: 'report',
    reportAnswer: 'reportAnswer',
    reportType: 'reportType',
    reportBlock: 'reportBlock',
    contentType: 'contentType',
    question: 'question',
    reportBlockQuestion: 'reportBlockQuestion',
    reportTypeReportBlock: 'reportTypeReportBlock',
    noteFeedback: 'noteFeedback',
  };

  readonly localStoreKeys = {
    showAllReports: 'showAllReports',
  };

  readonly moduleName = 'Report';

  readonly routing = {
    base: 'reports',
    sub: {
      reports: 'reports',
    },
  };

  readonly version = '1.0.0';
}
