import { ModuleConstants } from '@itag-berufsbildung/portal/data';
import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    level: 0,
    path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.information}`,
    right: `${ModuleConstants.instance.availableRights.menu.canShowMainMenuInfo}`,
    text: ModuleConstants.instance.routing.sub.information,
    title: 'Information über die Anwendung!',
    icon: 'information',
    selected: false,
    expanded: false,
  };
}
