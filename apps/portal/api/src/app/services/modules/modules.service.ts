import { ApiConstants, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import { ApiUserDto, UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { Inject, Injectable } from '@nestjs/common';
import { DbService } from '../db/db.service';

@Injectable()
export class ModulesService {
  constructor(@Inject(ApiConstants.services.dbService) protected readonly redisService: DbService) {}

  async getAllModules(userDto: ApiUserDto, correlationId: number | undefined): Promise<UserModuleInfoDto[]> {
    // get all module information
    StaticLogger.startMethod('getAllModules', correlationId);
    const modules = await this.redisService.getModules(correlationId);
    return StaticLogger.endMethod<UserModuleInfoDto[]>('getAllModules', modules, correlationId);
  }

  async setModule(userDto: ApiUserDto, moduleInfo: UserModuleInfoDto, correlationId: number): Promise<UserModuleInfoDto> {
    // set modul information
    StaticLogger.startMethod('setModule', correlationId);
    const key = `${ApiConstants.redisKeys.modules}:${moduleInfo.moduleName}`;
    const objSet = await this.redisService.set<UserModuleInfoDto>(key, moduleInfo, correlationId, ApiConstants.cacheConfiguration.moduleExpiringInSeconds);
    return StaticLogger.endMethod<UserModuleInfoDto>('setModule', objSet, correlationId);
  }
}
