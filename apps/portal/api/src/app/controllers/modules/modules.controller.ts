import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { ApiConstants, GenericAppController, GetCorrelationId, GetUser, Right, RightGuard, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import { ApiUserDto, UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { BaseApiController } from '@itag-berufsbildung/shared/data-api';

import { ModulesService } from '../../services/modules/modules.service';
import { ModuleConstants } from '@itag-berufsbildung/portal/data';

@ApiTags(`${ModuleConstants.instance.api.sub.modules}`)
@Controller(`${ModuleConstants.instance.api.sub.modules}`)
export class ModulesController extends BaseApiController {
  constructor(private readonly moduleService: ModulesService) {
    super();
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    type: UserModuleInfoDto,
    description: `depending on the role of the user we get only a subset back.`,
  })
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiBearerAuth()
  @UseGuards(RightGuard)
  @Right(ModuleConstants.instance.availableRights.api.module.canRead)
  @ApiOperation({
    description: `
## getModules

Get the information about the running modules back.

Depending of the roles you will get different information!.
 `,
    summary: 'get information about running modules',
  })
  @Get()
  async getModules(@GetUser() userDto: ApiUserDto, @GetCorrelationId() correlationId: number): Promise<UserModuleInfoDto[]> {
    StaticLogger.startMethod('getModules', correlationId);
    const modules: UserModuleInfoDto[] = await this.moduleService.getAllModules(userDto, correlationId);
    return StaticLogger.endMethod<UserModuleInfoDto[]>('getModules', modules, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiCreatedResponse({
    ...GenericAppController.createResponseOption,
    type: UserModuleInfoDto,
    isArray: true,
    description: `depending on the role of the user we get only a subset back.`,
  })
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiBearerAuth()
  @UseGuards(RightGuard)
  @Right(ApiConstants.rights.module.canCreate)
  @ApiOperation({
    description: `
##   async postModule(

write the modules information.

each module will have to send a special jwt token that we have to create for it!!!!
 `,
    summary: 'get information about running modules',
  })
  @Post()
  async postModule(@GetUser() userDto: ApiUserDto, @GetCorrelationId() correlationId: number, @Body() moduleInfo: UserModuleInfoDto): Promise<UserModuleInfoDto> {
    StaticLogger.startMethod('postModule', correlationId);
    // todo: check the jwt token if it's a legal one, otherwise don't save it
    const obj: UserModuleInfoDto = await this.moduleService.setModule(userDto, moduleInfo, correlationId);
    return StaticLogger.endMethod<UserModuleInfoDto>('postModule', obj, correlationId);
  }
}
