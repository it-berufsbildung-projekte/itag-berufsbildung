import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavigationStart, Router } from '@angular/router';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ModuleConstants } from '@itag-berufsbildung/portal/data';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { WebSocketPayload, WebSocketService } from '@itag-berufsbildung/shared/feature-web-socket';
import { IMenuItem, Menu, ObjectTools, OrderedMenu, SorterTools, UiUserDto } from '@itag-berufsbildung/shared/util';
import { DrawerComponent, DrawerMode, DrawerSelectEvent } from '@progress/kendo-angular-layout';
import { environment } from '../../../../../environments/environment';

const signOutMenuItem: OrderedMenu = {
  level: 0,
  right: ModuleConstants.instance.availableRights.menu.canShowMainMenuSignOut,
  text: 'sign-out',
  title: 'Aus dem System ausloggen / abmelden!',
  icon: 'sign-out',
  path: `${UiConstants.instance.routing.base}/${UiConstants.instance.routing.sub.signOut}`,
  selected: false,
  expanded: false,
  order: 100000,
  moduleName: 'portal',
};

const versionMenuItem: OrderedMenu = {
  level: 0,
  right: ModuleConstants.instance.availableRights.menu.canShowMainMenuSignOut,
  text: `version: ${environment.version}`,
  title: 'UI Version!',
  icon: 'circle',
  path: '/',
  selected: false,
  expanded: false,
  order: 100001,
  moduleName: 'portal',
};


@Component({
  selector: 'itag-berufsbildung-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  items: Menu[] = [];
  item: IMenuItem | undefined;
  mode: DrawerMode = 'push';
  mini = false;
  appName = 'Portal';
  drawerExpanded = true;
  autoCollapse = false;
  userDto?: UiUserDto;

  // private routeUrl?: string;
  private urlArr: string[] = [];

  constructor(
    private readonly router: Router,
    private readonly title: Title,
    @Inject(UiConstants.instance.services.user) private readonly userService: UserService,
    @Inject(UiConstants.instance.services.websocket) private readonly websocket: WebSocketService,
    private readonly notification: NotificationService
  ) {
    this.websocket.newDataArrived$.subscribe((data: WebSocketPayload | undefined) => {
      if (data) {
        this.notification.showDebug(JSON.stringify(data));
      }
    });
  }

  ngOnInit() {
    // Update Drawer selected state when change router path
    this.router.events.subscribe((route) => {
      if (route instanceof NavigationStart) {
        let url = route.url;
        if (url.substring(0, 1) === '/') {
          url = url.substring(1);
        }
        this.urlArr = url.split('/');
        this.updatePath();
      }
    });

    this.userService.userDto$.subscribe((userDto) => {
      this.userDto = userDto;
      this.updatePath();
    });

    this.setDrawerConfig();

    window.addEventListener('resize', () => {
      this.setDrawerConfig();
    });
  }

  ngOnDestroy() {
    window.removeEventListener('resize', () => undefined);
  }

  setDrawerConfig() {
    const pageWidth = window.innerWidth;
    if (pageWidth <= 770) {
      this.mode = 'overlay';
      this.mini = false;
      this.autoCollapse = true;
    } else if (pageWidth <= 1200) {
      this.mode = 'push';
      this.mini = true;
      this.autoCollapse = true;
    } else {
      this.mode = 'push';
      this.mini = true;
      this.autoCollapse = false;
    }
  }

  toggleDrawer(drawer: DrawerComponent): void {
    drawer.toggle();
  }

  onSelect(ev: DrawerSelectEvent): void {
    ev.item.selected = true;
    this.item = ev.item;
    // if ( !this.item || !this.item['parent'] ) {
    if (ev.item.path) {
      this.router.navigate([ev.item.path]).then(() => {
        // erste Ebene sind die Module
        this.updatePath();
      });
    }
    // }
  }

  private updatePath(): void {
    // if the user is logged out, we take the minimum menu
    let menuItems: OrderedMenu[] = [];
    if (!this.userDto) {
      // version trotzdem hinzufügen
      menuItems.push(ObjectTools.cloneDeep<OrderedMenu>(versionMenuItem));
      this.items = menuItems;
      return;
    } else {
      // read the information from the userDto about the menus....
      if (this.userDto?.menuHash) {
        for (const moduleName of Object.keys(this.userDto.menuHash)) {
          const menu: Menu = this.userDto.menuHash[moduleName];
          const newMenu = ObjectTools.cloneDeep<OrderedMenu>(menu as OrderedMenu);
          if (newMenu) {
            newMenu.moduleName = moduleName;
            menuItems.push(newMenu);
          }
        }
      }
    }

    // sort toplevel on the name
    menuItems = menuItems.sort(SorterTools.dynamicSort('name'));
    let menuOrderIndex = 1;

    // erste Ebene sind die Module
    this.updateTitle();

    // selected item
    let arrSelectedPath = this.item?.path ? this.item.path.split('/') : [];
    if (arrSelectedPath.length === 0 && this.urlArr.length > 0) {
      arrSelectedPath = this.urlArr;
    }

    // main menu
    const mainMenu: OrderedMenu[] = ObjectTools.cloneDeep<OrderedMenu[]>(menuItems);
    menuItems = [];
    for (const item of mainMenu) {
      // check if we have already a modul selected
      menuOrderIndex = this.checkItem(item.moduleName, item, menuItems, arrSelectedPath, menuOrderIndex);
    }

    // final sort top level
    const sortedItems = menuItems.sort(SorterTools.dynamicSort('order'));


    // add a spacer, if there is any other menu
    if (sortedItems.length > 0) {
      // we use the signOutMenuItem order -1 to have it direct bevor
      sortedItems.push({ separator: true, order: signOutMenuItem.order - 1, moduleName: 'portal' });
    }
    // add the sign-out
    sortedItems.push(ObjectTools.cloneDeep<OrderedMenu>(signOutMenuItem));

    sortedItems.push({ separator: true, order: signOutMenuItem.order - 1, moduleName: 'portal' });
    sortedItems.push(ObjectTools.cloneDeep<OrderedMenu>(versionMenuItem));

    // final sort top level
    this.items = sortedItems;
    // this.items = menuItems;
  }

  private checkItem(moduleName: string, itemMenu: Menu, menuItems: OrderedMenu[], arrSelectedPath: string[], menuOrderIndex: number): number {
    if (!itemMenu || !this.userDto || !itemMenu.right) {
      return menuOrderIndex;
    }
    // check right for non separator
    if (itemMenu.right && !this.userDto.hasRight(moduleName, itemMenu.right)) {
      return menuOrderIndex;
    } else if (itemMenu.separator !== undefined && !itemMenu.separator) {
      // only separators must not have set a right
      return menuOrderIndex;
    }

    // clone it
    const item = ObjectTools.cloneDeep<OrderedMenu>(itemMenu as OrderedMenu);
    menuOrderIndex++;
    item.order = menuOrderIndex;

    const index = menuItems.findIndex((i) => i.path === item.path);
    menuItems.splice(index + 1, 0, ...[item]);

    if (item.separator) {
      return menuOrderIndex;
    }

    const itemUrlArr = item.path.split('/');
    let expanded = true;
    for (let i = 0; i < itemUrlArr.length; i++) {
      // check if the item is the current one
      if (this.urlArr[i] !== itemUrlArr[i]) {
        expanded = false;
        break;
      }
    }

    // check for selected
    item.selected = item.path === this.urlArr.join('/');

    // check if the old item is expanded and if so, close it
    if (this.item?.path === item.path && this.item.expanded) {
      expanded = false;
    }

    // set the result of the expanded check
    item.expanded = expanded;

    // add the children
    if (item.expanded && item.children && item.children.length > 0) {
      for (const childItem of item.children) {
        menuOrderIndex = this.checkItem(moduleName, childItem, menuItems, arrSelectedPath, menuOrderIndex);
      }
    }

    // set it public
    this.items = menuItems;

    return menuOrderIndex;
  }

  private updateTitle() {
    let title = this.appName;
    for (const path of this.urlArr) {
      title += ' - ' + path;
    }
    this.title.setTitle(title);
  }
}
