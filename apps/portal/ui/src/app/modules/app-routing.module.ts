import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleConstants as PortalConstants } from '@itag-berufsbildung/portal/data';
import { ModuleConstants as ProfileConstants } from '@itag-berufsbildung/profile/data';
import { ModuleConstants as StudyDocumentationConstants } from '@itag-berufsbildung/study-documentation/data';
import { ModuleConstants as TimeCounterConstants } from '@itag-berufsbildung/time-counter/data';
import { ModuleConstants as UserManagementConstants } from '@itag-berufsbildung/user-management/data';
import { ModuleConstants as PlanningConstants } from '@itag-berufsbildung/planning/data';
import { ModuleConstants as ReportConstants } from '@itag-berufsbildung/report/data';
import { ModuleRightGuard } from '@itag-berufsbildung/shared/feature-auth0';
import { InfoComponent } from '../components';

const routes: Routes = [
  { path: '', redirectTo: 'info', pathMatch: 'full' },
  { path: `${PortalConstants.instance.routing.base}/${PortalConstants.instance.routing.sub.information}`,
    component: InfoComponent },
  // lacy loading modules
  // profile
  {
    path: ProfileConstants.instance.routing.base,
    data: {
      module: ProfileConstants.instance.moduleName,
      right: ProfileConstants.instance.availableRights.menu.canShowMainMenu,
    },
    loadChildren: () => import('@itag-berufsbildung/profile/feature').then((m) => m.FeatureProfileModule),
  },
  // study-documentation
  {
    path: StudyDocumentationConstants.instance.routing.base,
    canActivate: [ModuleRightGuard],
    data: {
      module: StudyDocumentationConstants.instance.moduleName,
      right: StudyDocumentationConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    loadChildren: () => import('@itag-berufsbildung/study-documentation/feature').then((m) => m.FeatureStudyDocumentationModule),
  },
  // time-counter
  {
    path: TimeCounterConstants.instance.routing.base,
    canActivate: [ModuleRightGuard],
    data: {
      module: TimeCounterConstants.instance.moduleName,
      right: TimeCounterConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    loadChildren: () => import('@itag-berufsbildung/time-counter/feature').then((m) => m.FeatureTimeCounterModule),
  },
  // user-management
  {
    path: UserManagementConstants.instance.routing.base,
    canActivate: [ModuleRightGuard],
    data: {
      module: UserManagementConstants.instance.moduleName,
      right: UserManagementConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    loadChildren: () => import('@itag-berufsbildung/user-management/feature').then((m) => m.FeatureUserManagementModule),
  },
  // planning
  {
    path: PlanningConstants.instance.routing.base,
    canActivate: [ModuleRightGuard],
    data: {
      module: PlanningConstants.instance.moduleName,
      right: PlanningConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    loadChildren: () => import('@itag-berufsbildung/planning/feature').then((m) => m.FeaturePlanningModule),
  },
  // reports
  {
    path: ReportConstants.instance.routing.base,
    canActivate: [ModuleRightGuard],
    data: {
      module: ReportConstants.instance.moduleName,
      right: ReportConstants.instance.availableRights.menu.canShowMainMenuModule,
    },
    loadChildren: () => import('@itag-berufsbildung/report/feature').then((m) => m.FeatureReportModule),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
