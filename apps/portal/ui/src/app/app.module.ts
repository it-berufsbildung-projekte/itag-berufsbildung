import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule, registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import localeDe from '@angular/common/locales/de';

import { AuthService } from '@auth0/auth0-angular';
import '@progress/kendo-angular-intl/locales/de/all';
registerLocaleData(localeDe);

import { AuthenticationInterceptorService, FeatureAuthAuth0Module, UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { NotificationButtonComponent, NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { DateService, UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { KendoUiModules } from '@itag-berufsbildung/shared/data-ui';
import { LocalStoreService } from '@itag-berufsbildung/shared/feature-local-store';
import { WebSocketService } from '@itag-berufsbildung/shared/feature-web-socket';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

import { environment } from '../../../../../environments/environment';
import { AppRoutingModule } from './modules/app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent, InfoComponent } from './components';

import { InfoDetailComponent } from './components/info-detail/info-detail.component';
import { HealthStateComponent } from './components/health-state/health-state.component';

const webSocketServiceFactory = (localStore: LocalStoreService, notification: NotificationService) => new WebSocketService(localStore, notification, environment.apiUrl);

const userServiceFactory = (auth: AuthService, localStore: LocalStoreService, notification: NotificationService, http: HttpClient, router: Router) =>
  new UserService(auth, localStore, notification, http, router);

@NgModule({
  declarations: [AppComponent, HeaderComponent, InfoComponent, InfoDetailComponent, HealthStateComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule, // must be here, even if we don't use it direct
    AppRoutingModule,
    KendoUiModules,
    NgxJsonViewerModule,
    FeatureAuthAuth0Module,
    NotificationButtonComponent,
    StoreModule.forRoot(),
    EffectsModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [
    LocalStoreService,
    NotificationService,
    DateService,
    { provide: LOCALE_ID, useValue: 'de-CH' },
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptorService, multi: true },
    { provide: Window, useValue: window },
    {
      provide: UiConstants.instance.services.websocket,
      useFactory: webSocketServiceFactory,
      deps: [LocalStoreService, NotificationService],
    },
    {
      provide: UiConstants.instance.services.user,
      useFactory: userServiceFactory,
      deps: [AuthService, LocalStoreService, NotificationService, HttpClient, Router],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
