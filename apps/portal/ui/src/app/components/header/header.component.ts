import { Component, EventEmitter, HostListener, Inject, Input, OnInit, Output } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ScreenInfoService, UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { BreadCrumbItem } from '@progress/kendo-angular-navigation';
import { environment } from '../../../../../../../environments/environment';

@Component({
  selector: 'itag-berufsbildung-header-component',
  styleUrls: ['header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  @Output() public toggle = new EventEmitter();
  @Input() public appName?: string;

  items: BreadCrumbItem[] = [];

  title = environment.title;

  constructor(
    private readonly screenInfoService: ScreenInfoService,
    private readonly router: Router,
    @Inject(UiConstants.instance.services.user) public readonly userService: UserService,
    private readonly notification: NotificationService,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.items = event.url
          .split('/')
          .map((i) => {
            return {
              text: i,
              title: i,
            };
          })
          .filter((i) => i.text.length > 0);
      }
    });
  }

  onButtonClick(): void {
    this.toggle.emit();
  }

  onItemClick(item: BreadCrumbItem): void {
    const selectedItemIndex = this.items.findIndex((i) => i.text === item.text);
    const url = this.items.slice(0, selectedItemIndex + 1).map((i) => i.text?.toLowerCase());
    this.router.navigate(url);
  }

  ngOnInit() {
    this.screenInfoService.screenResized(window.innerWidth, window.innerHeight);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    this.notification.showDebug(event.type);
    if (event.type === 'resize') {
      this.screenInfoService.screenResized(window.innerWidth, window.innerHeight);
    }
  }
}
