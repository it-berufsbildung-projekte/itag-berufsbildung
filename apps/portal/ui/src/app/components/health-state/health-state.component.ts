import { HttpClient } from '@angular/common/http';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { AdminModuleInfoDto, GlobalConstants, HealthStateDto } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-health-state',
  templateUrl: './health-state.component.html',
  styleUrls: ['./health-state.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class HealthStateComponent {
  healthStateIcon: string | undefined = 'gears';
  healthStateClass: 'k-color-warning' | 'k-color-error' | 'k-color-success' = 'k-color-warning';
  message = 'initializing';
  constructor(private readonly http: HttpClient, private readonly notification: NotificationService) {}

  @Input()
  set info(info: Partial<AdminModuleInfoDto>) {
    if (!info?.apiUrl) {
      return;
    }
    const url = `${info.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.healthState}`;
    this.http.get<HealthStateDto>(url).subscribe({
      next: (info) => {
        this.message = info.message;
        switch (info.state) {
          case 'initializing':
            this.healthStateIcon = 'gears';
            this.healthStateClass = 'k-color-warning';
            break;
          case 'okay':
            this.healthStateIcon = 'success';
            this.healthStateClass = 'k-color-success';
            break;
          default:
            this.healthStateIcon = 'error';
            this.healthStateClass = 'k-color-error';
            break;
        }
      },
      error: (err) => {
        this.healthStateIcon = 'error';
        this.healthStateClass = 'k-color-error';
        this.message = err.message;
        this.notification.showError((err as Error).message, err);
      },
    });
  }
}
