import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';

import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { UiConstants } from '@itag-berufsbildung/shared/data-ui';
import { NotificationService } from '@itag-berufsbildung/shared/feature-notification';
import { AdminModuleInfoDto, GlobalConstants, ObjectTools, SorterTools, UiUserDto } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent {
  message = 'Es ist noch kein Benutzer angemeldet!';
  modules: Partial<AdminModuleInfoDto>[] = [];
  contactInfo = GlobalConstants.contactInfo;
  userDto?: UiUserDto;

  constructor(
    @Inject(UiConstants.instance.services.user) public readonly userService: UserService,
    private readonly http: HttpClient,
    private readonly notification: NotificationService
  ) {
    this.userService.userDto$.subscribe((userDto) => {
      if (userDto) {
        this.userDto = userDto;
        this.message = userDto.eMail;
        // get for all modules now the appInfo
        for (const moduleInfo of userDto.moduleInfoArr) {
          if (!moduleInfo?.moduleName) {
            // ignore if not exist
            continue;
          }
          const url = `${moduleInfo.apiUrl}/${GlobalConstants.api.generic.base}/${GlobalConstants.api.generic.sub.appInfo}`;
          this.http.get<Partial<AdminModuleInfoDto>>(url).subscribe({
            next: (adminModuleInfo) => {
              const arr = ObjectTools.cloneDeep(this.modules);
              if (!arr.find((i) => i.moduleName === adminModuleInfo.moduleName)) {
                // add it
                this.modules.push(adminModuleInfo);
                this.modules = this.modules.sort(SorterTools.dynamicSort('moduleName'));
              }
            },
            error: (err: Error) => this.notification.showError(err.message),
          });
        }
      } else {
        this.userDto = undefined;
        this.message = 'Es ist noch kein Benutzer angemeldet!';
      }
    });
  }
}
