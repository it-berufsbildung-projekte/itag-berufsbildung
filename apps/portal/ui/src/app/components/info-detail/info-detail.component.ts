import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ModuleConstants } from '@itag-berufsbildung/portal/data';
import { AdminModuleInfoDto, Menu, ObjectTools, UiUserDto } from '@itag-berufsbildung/shared/util';

@Component({
  selector: 'itag-berufsbildung-info-detail',
  templateUrl: './info-detail.component.html',
  styleUrls: ['./info-detail.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class InfoDetailComponent {
  adminModuleInfo?: Partial<AdminModuleInfoDto>;
  menus: Menu[] = [];

  @Input()
  userDto?: UiUserDto;

  @Input()
  set info(info: Partial<AdminModuleInfoDto>) {
    if (!info) {
      this.menus = [];
      this.adminModuleInfo = undefined;
      return;
    }
    if (InfoDetailComponent.isAdminModuleInfo(info)) {
      this.adminModuleInfo = info as AdminModuleInfoDto;
      // clone the main menu
      const main = this.adminModuleInfo?.menu ? ObjectTools.cloneDeep<Menu>(this.adminModuleInfo.menu) : undefined;
      // we remove the children to show only the relevant information
      if (main) {
        delete main.children;
        this.menus.push(main);
        const children = this.adminModuleInfo?.menu?.children ? this.adminModuleInfo.menu.children : [];
        this.menus = this.menus.concat(children);
      }
    } else {
      this.menus = [];
      this.adminModuleInfo = undefined;
    }
  }

  private static isAdminModuleInfo(obj: Partial<AdminModuleInfoDto>): boolean {
    return obj?.availableRoles !== undefined;
  }

  hasRight(rights: 'showAppSettings' | 'showDatabase'): boolean {
    // if no userDto is set, we can't now if the user has the right or not. so we assume always he does not have the right
    if (!this.userDto || !this.adminModuleInfo?.moduleName) {
      return false;
    }
    let right = 'not-set';
    switch (rights) {
      case 'showAppSettings':
        right = ModuleConstants.instance.availableRights.ui.information.canShowAppSettings;
        break;
      case 'showDatabase':
        right = ModuleConstants.instance.availableRights.ui.information.canShowDatabase;
        break;
    }
    return this.userDto.hasRight(this.adminModuleInfo.moduleName, right);
  }
}
