import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModuleConstants } from '@itag-berufsbildung/portal/data';
import { UserModuleInfoDto } from '@itag-berufsbildung/shared/util';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ModuleService {
  constructor(private readonly http: HttpClient) {}

  getModules() {
    return this.http.get<UserModuleInfoDto[]>(`${environment.apiUrl}/${ModuleConstants.instance.api.sub.modules}`);
  }
}
