import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    icon: 'accessibility',
    level: 0,
    right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    path: ModuleConstants.instance.routing.base,
    text: 'planning',
    title: 'Das Module planning',
    children: [
      {
        icon: 'preview',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.annualPlanning}`,
        text: 'Jahresplanung',
        title: 'Zum Erstellen der Jahresplanung',
      },
      {
        icon: 'preview',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.dailyPlanning}`,
        text: 'Tagesplanung',
        title: 'Zum Erstellen der Tagesplanung',
      },
    ],
  };

  readonly redisKeys = {
    projectTask: 'projectTask',
    eventCategory: 'eventCategory',
    annualPlanningEvent: 'annualPlanningEvent',
  };

}
