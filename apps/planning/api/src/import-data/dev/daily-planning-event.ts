export class DailyPlanningEvent {
  static readonly documents = {
    ['8c1eeb56-99c1-4c66-951f-4c104042c979']: {
      name: 'devData1',
      isDeleted: false,
    },
    ['2f094302-1a0c-407f-a307-0fb95ed73e5e']: {
      name: 'devData2',
      isDeleted: false,
    },
    ['8bcdde45-60e4-4542-8996-fbd7cb5784e0']: {
      name: 'devData3',
      isDeleted: true,
    },
    ['c37bebdc-3efe-4d1f-bd0d-3c6098d5fdc2']: {
      name: 'devData4',
      isDeleted: false,
    },
    ['91b0ccf1-7a9b-4e17-bee3-da6ee224e74b']: {
      name: 'devData5',
      isDeleted: true,
    },
  };
}
