import { Inject, Injectable } from '@nestjs/common';

import { UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { ApiCallerService, ApiConstants, BaseAppConfigService, BaseRedisService, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import * as fs from 'fs';
import * as path from 'path';

import { Constants } from '../../../constants';
import { environment } from '../../../environment/environment';

enum EnumFileType {
  methodProject,
  eventCategory,
  annualPlanningEvent,
}

interface IModuleProject {
  uuid?: string;
  name: string;
}

interface IEventCategory {
  uuid?: string;
  backgroundColor: string;
  isAbCategory: boolean;
  isBusinessCategory: boolean;
  isGroupCategory: boolean;
  isDeleted: boolean;
  name: string;
  textColor: string;
  weight: number;
}

interface IAnnualPlanningEvent {
  uuid: string;
  isDeleted: boolean;
  title: string;
  description: string;
  end: Date;
  eventCategoryUuid: string;
  eventCategory: IEventCategory;
  isAllDay: boolean;
  ownerCategoryKey: string;
  recurrenceException: string;
  recurrenceRule: string;
  start: Date;
}

/*
interface IProjectTask {
  uuid: string;
  isTask: boolean;
  isDeleted: boolean;
  name: string;
  ownerEmail: string[];
  state: string;
  timeBudgetHours: number;
}
*/

type ImportData = IModuleProject | IEventCategory | IAnnualPlanningEvent;

@Injectable()
export class AppConfigService extends BaseAppConfigService {
  constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.apiCallerService) protected readonly apiCaller: ApiCallerService
  ) {
    super(baseRedisService, apiCaller, ModuleConstants.instance, Constants.instance, environment.production);
  }

  async initDatabase(correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('initDatabase', correlationId);

    // define base path
    const dataPath = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment);

    // import project-task
    await this.importData(path.join(dataPath, 'project-task.json'), EnumFileType.methodProject, correlationId);

    // import event categories
    await this.importData(path.join(dataPath, 'event-category.json'), EnumFileType.eventCategory, correlationId);

    // import annual-planning-events
    await this.importData(path.join(dataPath, 'annual-planning-event.json'), EnumFileType.annualPlanningEvent, correlationId);

    // read the data depending on the environment
    const dataPathUserRole = path.join(dataPath, 'user-role.json');
    // check that the user role file exists
    if (fs.existsSync(dataPathUserRole)) {
      const userRoleHashDto: UserRoleHashDto = JSON.parse(fs.readFileSync(dataPathUserRole, { encoding: 'utf-8' }));
      await this.baseRedisService.registerUserRoleForModule(this.adminAppConfiguration.moduleName, userRoleHashDto, correlationId);
    } else {
      StaticLogger.warn(`file ${dataPathUserRole} not found`, correlationId);
    }

    StaticLogger.endMethod('initDatabase', undefined, correlationId);
  }

  private async importData(filePath: string, fileType: EnumFileType, correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('importData', correlationId);

    if (!fs.existsSync(filePath)) {
      return StaticLogger.warn(`File ${filePath} does not exists!`, correlationId);
    }

    const content = fs.readFileSync(filePath, { encoding: 'utf-8' });
    if (!content || content.length === 0) {
      const msg = `File ${filePath} is empty!`;
      StaticLogger.error(msg, correlationId);
      throw new Error(msg);
    }

    const data: ImportData[] = JSON.parse(content);
    let dbKey = '';
    switch (fileType) {
      case EnumFileType.methodProject:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.projectTask}`;
        break;
      case EnumFileType.eventCategory:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.eventCategory}`;
        break;
      case EnumFileType.annualPlanningEvent:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.annualPlanningEvent}`;
        break;
    }

    for (const item of data) {
      // check if key already exists in db
      if ( !await this.baseRedisService.hGet(dbKey, item.uuid, correlationId) ) {
        await this.baseRedisService.hSet(dbKey, item.uuid, item, correlationId);
      }
    }
  }
}
