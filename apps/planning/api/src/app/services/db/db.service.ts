import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { Injectable } from '@nestjs/common';

import { BaseRedisService } from '@itag-berufsbildung/shared/data-api';
import { RoleRight } from '../../role-rights/role-right';

@Injectable()
export class DbService extends BaseRedisService {
  constructor() {
    super(
      {
        canResetCacheForOthers: ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      },
      RoleRight.documents
    );
  }
}
