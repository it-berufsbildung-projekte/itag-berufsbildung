import { HttpModule, HttpService } from '@nestjs/axios';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import {
  ApiCallerService,
  ApiConstants,
  ApiSocketService,
  ApiUserService,
  AppGateway,
  BaseAppConfigService,
  JwtStrategy,
  NestAppCreator,
} from '@itag-berufsbildung/shared/data-api';

import { AppConfigService } from './services/app-config/app-config.service';
import { GenericController } from './controllers/generic/generic.controller';
import { DbService } from './services/db/db.service';
import { AnnualPlanningEventController } from './controllers/annual-planning-event/annual-planning-event.controller';
import { ProjectTaskController } from './controllers/project-task/project-task.controller';
import { EventCategoryController } from './controllers/event-category/event-category.controller';
import { DailyPlanningEventController } from './controllers/daily-planning-event/daily-planning-event.controller';
import { DailyPlanningLogController } from './controllers/daily-planning-log/daily-planning-log.controller';

@Module({
  imports: [JwtModule.register({}), HttpModule],
  controllers: [GenericController, AnnualPlanningEventController, ProjectTaskController, EventCategoryController, DailyPlanningEventController, DailyPlanningLogController],
  providers: [
    JwtStrategy,
    AppGateway,
    ApiUserService,
    {
      provide: ApiConstants.services.dbService,
      useClass: DbService,
    },
    {
      provide: ApiConstants.services.apiCallerService,
      inject: [HttpService, ApiConstants.services.dbService],
      useFactory: (httpService: HttpService, dbService: DbService) => new ApiCallerService(httpService, dbService),
    },
    {
      provide: ApiConstants.services.appConfigService,
      inject: [ApiConstants.services.dbService, ApiConstants.services.apiCallerService],
      useFactory: (dbService: DbService, apiCallerService: ApiCallerService) => new AppConfigService(dbService, apiCallerService),
    },
    {
      provide: ApiConstants.services.socketService,
      inject: [ApiConstants.services.appConfigService],
      useFactory: (appConfigService: BaseAppConfigService) => new ApiSocketService(appConfigService),
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    return NestAppCreator.addConfigure(consumer, 0);
  }
}
