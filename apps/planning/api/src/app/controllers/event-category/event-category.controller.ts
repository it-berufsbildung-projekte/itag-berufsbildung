import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';

import { EventCategoryDto } from '@itag-berufsbildung/planning/data';

@ApiTags(`${ModuleConstants.instance.documents.eventCategory}`)
@Controller(`${ModuleConstants.instance.documents.eventCategory}`)
export class EventCategoryController extends GenericDocumentController<EventCategoryDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.eventCategory.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.eventCategory.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.eventCategory.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.eventCategory.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.eventCategory.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.eventCategory.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.eventCategory.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.eventCategory, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
