import { ApiUserDto, BaseDocumentDto, IQueryParams } from '@itag-berufsbildung/shared/util';
import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants, ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
  StaticLogger,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';

import { AnnualPlanningEventDto } from '@itag-berufsbildung/planning/data';

@ApiTags(`${ModuleConstants.instance.documents.annualPlanningEvent}`)
@Controller(`${ModuleConstants.instance.documents.annualPlanningEvent}`)
export class AnnualPlanningEventController extends GenericDocumentController<AnnualPlanningEventDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService)  protected override readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.annualPlanningEvent.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.annualPlanningEvent, genericDocumentControllerRights);
  }

  override async deleteMethod<T>(
    userDto: ApiUserDto,
    uuid: string,
    queryParams: IQueryParams,
    correlationId: number | undefined,
  ): Promise<Partial<T>> {
    StaticLogger.startMethod('deleteMethod', correlationId);
    const annualPlanningEvent = await this.baseRedisService.hGet( this.getDocumentKey(), uuid, correlationId );
    // const eventOwnerEmail = (annualPlanningEvent as unknown as AnnualPlanningEventDto).ownerCategoryKey.split('|')[0];

    if ( (annualPlanningEvent as AnnualPlanningEventDto).creatorEmail !== userDto.eMail ) {
      throw this.createMethodNotAllowedException( `the user with the email ${userDto.eMail} can only delete events created by himself!`, correlationId);
    }

    const obj = await this.baseRedisService.hDel<Partial<T>>( this.getDocumentKey(), uuid, correlationId );
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;

    return StaticLogger.endMethod<Partial<T>>( 'deleteMethod', baseObj as unknown as Partial<T>, correlationId );
  }

  override async upsetMethod<T>(
    uuid: string,
    obj: Partial<T>,
    userDto: ApiUserDto,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    onlyIfNotExisting = false,
  ): Promise<Partial<T>> {
    StaticLogger.startMethod( 'upsetMethod', correlationId );

    // check if user is the owner of the event and throw an error if not eventOwnerEmail !== userDto.eMail
    if ( !onlyIfNotExisting && ( !userDto.hasRole( ModuleConstants.instance.availableRoles.bb ) && !userDto.hasRole( ModuleConstants.instance.availableRoles.pb ) ) ) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify an event (only allowed for bb and pb) !`, correlationId);
    }

    const ret = await this.baseRedisService.hSet<Partial<T>>( this.getDocumentKey(), uuid, obj, correlationId, onlyIfNotExisting );
    const updatedEvent = ret as unknown as BaseDocumentDto;
    updatedEvent.uuid = uuid;

    return StaticLogger.endMethod<T>('upsetMethod', updatedEvent as unknown as T, correlationId);
  }

}
