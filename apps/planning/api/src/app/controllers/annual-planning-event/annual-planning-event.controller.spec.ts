import { Test, TestingModule } from '@nestjs/testing';
import { AnnualPlanningEventController } from './annual-planning-event.controller';

describe('AnnualPlanningEventController', () => {
  let controller: AnnualPlanningEventController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnnualPlanningEventController],
    }).compile();

    controller = module.get<AnnualPlanningEventController>(AnnualPlanningEventController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
