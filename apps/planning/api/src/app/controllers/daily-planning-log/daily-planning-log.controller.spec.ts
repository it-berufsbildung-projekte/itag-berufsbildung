import { Test, TestingModule } from '@nestjs/testing';
import { DailyPlanningLogController } from './daily-planning-log.controller';

describe('DailyPlanningLogController', () => {
  let controller: DailyPlanningLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DailyPlanningLogController],
    }).compile();

    controller = module.get<DailyPlanningLogController>(DailyPlanningLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
