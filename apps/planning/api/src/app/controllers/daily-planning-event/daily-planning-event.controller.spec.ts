import { Test, TestingModule } from '@nestjs/testing';
import { DailyPlanningEventController } from './daily-planning-event.controller';

describe('DailyPlanningEventController', () => {
  let controller: DailyPlanningEventController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DailyPlanningEventController],
    }).compile();

    controller = module.get<DailyPlanningEventController>(DailyPlanningEventController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
