import { ApiUserDto, BaseDocumentDto, baseDocumentExample, baseDocumentExampleArr, IBaseUuidDtoHistory, IQueryParams, ObjectTools } from '@itag-berufsbildung/shared/util';
import { Body, Controller, Delete, Get, Inject, Param, Patch, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConflictResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import {
  ApiConstants, ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  CheckAccessMethodType,
  GenericAppController,
  GenericDocumentController,
  GetCorrelationId,
  GetUser,
  IGenericDocumentControllerRights,
  StaticLogger,
  UpsetMethodType,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';

import { DailyPlanningEventDto } from '@itag-berufsbildung/planning/data';
import { Request } from 'express';

const dataType = 'DailyPlanningEventDto';
const dataTypeDescription = 'An event that can be shown in a kendo scheduler component';
const dataDescription = `
 * depending on the role of the user we get only a subset back.
 * with the role admin all properties will be get back`;

@ApiTags(`${ModuleConstants.instance.documents.dailyPlanningEvent}`)
@Controller(`${ModuleConstants.instance.documents.dailyPlanningEvent}`)
export class DailyPlanningEventController extends GenericDocumentController<DailyPlanningEventDto> {
  private moduleConstants = ModuleConstants.instance;

  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.dailyPlanningEvent, genericDocumentControllerRights);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiOperation({
    description: '',
    summary: 'fully delete a field of the document by its uuid, date and ownerEmail',
  })
  @Delete('/:uuid/:date/:ownerEmail')
  async deleteByDate(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number | undefined,
    @Param('uuid') uuid: string,
    @Param('date') date: string,
    @Param('ownerEmail') ownerEmail: string,
    @Req() request: Request
  ): Promise<Partial<DailyPlanningEventDto>> {
    StaticLogger.startMethod('deleteByDate', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canRemove)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    const oldObj = await this.getOneMethodWithQuery(uuid, date, ownerEmail, correlationId);
    // checkAccess
    this.checkAccess(oldObj, userDto, CheckAccessMethodType.delete, queryParams, correlationId);

    // delete
    const obj = await this.deleteMethodWithQuery(uuid, date, ownerEmail, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodReadIsDeleted(userDto, sanitizedObj, correlationId);
    // remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodReadHistory(userDto, sanitizedIsDeletedObj, correlationId);
    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('deleteByDate', finalObj, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      items: {
        type: dataType,
      },
      example: baseDocumentExampleArr,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    description: '',
    summary: 'Get all daily planning events by date for one owner',
  })
  @Get('/:date/:ownerEmail')
  async getAllByDateAndOwner(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('date') date: string,
    @Param('ownerEmail') ownerEmail: string,
    @Req() request: Request
  ): Promise<Partial<DailyPlanningEventDto>[]> {
    StaticLogger.startMethod('getAllByDateAndOwner', correlationId);
    // check rights
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canRead)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }

    const objArr: Partial<DailyPlanningEventDto>[] = await this.getAllByDateAndOwnerMethod(date, ownerEmail, correlationId);
    // hint: if there is no result we send an empty array.
    if (objArr?.length < 1) {
      return StaticLogger.endMethod<Partial<DailyPlanningEventDto>[]>('getAllByDateAndOwner', [], correlationId);
    }
    // sanitize the array. Might be overridden by the child class
    const sanitizedObjArr = objArr.map((o) => this.sanitizeReadMethod(userDto, o, correlationId));
    // remove the entries if they are deleted and the user can't see the deleted entries or showDeleted is false
    let removedIsDeletedArr = sanitizedObjArr;
    const showDeleted = request?.query.isDeleted ? request.query.isDeleted : false;
    // hint: we filter them first before removing the property since it would return every document.
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canShowDeleted) || !showDeleted) {
      // hint: we can't use o.hasOwnProperty() or hasOwn() so the workaround is to convert it to a string and look if the string doesn't contain 'isDeleted' (happens when someone who can't modify the property posts a document)
      removedIsDeletedArr = sanitizedObjArr.filter((o) => !(o as unknown as BaseDocumentDto).isDeleted || !JSON.stringify(o as unknown as BaseDocumentDto).includes('isDeleted'));
    }
    // we remove the isDeleted property if the user doesn't have the right to see it
    const sanitizedShowDeletedObjArr = removedIsDeletedArr.map((s) => this.sanitizeMethodReadIsDeleted(userDto, s, correlationId));
    // we remove the history if the user doesn't habe the right to see it
    const sanitizedShowHistoryObjArr = sanitizedShowDeletedObjArr.map((s) => this.sanitizeMethodReadHistory(userDto, s, correlationId));

    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>[]>('getAllByDateAndOwner', sanitizedShowHistoryObjArr, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiConflictResponse(GenericAppController.conflictResponse)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiBody({
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Replace on an existing field some values',
  })
  @Patch('/:uuid/:date/:ownerEmail')
  async patchEventByDate(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Param('date') date: string,
    @Param('ownerEmail') ownerEmail: string,
    @Body() patchDto: DailyPlanningEventDto,
    @Req() request: Request
  ): Promise<Partial<DailyPlanningEventDto>> {
    StaticLogger.startMethod('patchEventByDate', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (
      !userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canUpdate) &&
      !userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canModifyDelete)
    ) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    // we try to get first the old value
    const oldValue: Partial<DailyPlanningEventDto> = await this.getOneMethodWithQuery(uuid, date, ownerEmail, correlationId);
    // check if uuid is equal,
    if (patchDto?.uuid && patchDto.uuid !== uuid) {
      // different uuid in the body and in the uri. we do not allow this. the user has to remove the uuid or set it to equal
      throw this.createConflictException(`You try to update a document with a different uuid in the body!`, correlationId);
    }
    // check if isDeleted is already true and the user doesn't have the right to modify a deleted field.
    if (oldValue.isDeleted && !userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify a deleted field!`, correlationId);
    }
    // checkAccess. Our check will always lose.
    this.checkAccess(oldValue, userDto, CheckAccessMethodType.patch, queryParams, correlationId, patchDto);
    // check if the oldValue isDeleted and the user doesn't have the right to modifyDelete
    const sanitizedIsDeletedPatchDto = this.sanitizeMethodWriteIsDeleted(userDto, patchDto, correlationId);
    const sanitizedWritePatchDto = this.sanitizeUpsetMethod(userDto, sanitizedIsDeletedPatchDto, oldValue, queryParams, UpsetMethodType.Update, correlationId);

    // clone the old value and remove history to prevent circular structure in the newHistory
    const oldValueWithoutHistory = ObjectTools.cloneDeep<Partial<DailyPlanningEventDto>>(oldValue);
    delete oldValueWithoutHistory.history;
    const newHistory: IBaseUuidDtoHistory = {
      oldValue: oldValueWithoutHistory,
      method: 'UPDATE',
      email: userDto.eMail,
      timestamp: Date.now(),
    };
    const sanitizedHistoryPatchDto = GenericDocumentController.sanitizeMethodHistoryWrite<DailyPlanningEventDto>(sanitizedWritePatchDto, oldValue, newHistory, correlationId);

    // now we update the oldValues with the new values
    for (const prop of Object.keys(sanitizedHistoryPatchDto)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      oldValue[prop] = sanitizedWritePatchDto[prop];
    }
    // remove field uuid since it's no longer needed.
    delete oldValue.uuid;
    // save the patched entry
    const obj = await this.upsetMethod<DailyPlanningEventDto>(uuid, oldValue, userDto, queryParams, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod<DailyPlanningEventDto>(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodReadIsDeleted(userDto, sanitizedObj, correlationId);
    // we remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodReadHistory(userDto, sanitizedIsDeletedObj, correlationId);

    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('patchEventByDate', finalObj, correlationId);
  }

  protected override async upsetMethod<T>(
    uuid: string,
    obj: Partial<T>,
    userDto: ApiUserDto,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    onlyIfNotExisting = false
  ): Promise<Partial<T>> {
    StaticLogger.startMethod('upsetMethod', correlationId);

    const dailyPlanningEvent = obj as unknown as DailyPlanningEventDto;
    const date = new Date(dailyPlanningEvent.start);
    // date.setHours( date.getHours() + ( new Date().getTimezoneOffset() / 60 ) );
    const key = `${this.getDocumentKey()}:${date.getFullYear()}:${date.getMonth() + 1}:${date.getDate()}:${dailyPlanningEvent.ownerEmail}`;

    // check if the user is teh owner of the event and throw an error if not
    if (dailyPlanningEvent.ownerEmail !== userDto.eMail) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify an event without being its owner!`, correlationId);
    }

    // when this function gets overridden then as T might not be possible.
    const ret = await this.baseRedisService.hSet<Partial<T>>(key, uuid, obj, correlationId, onlyIfNotExisting);
    // tell typescript ret is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = ret as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<T>('upsetMethod', baseObj as unknown as T, correlationId);
  }

  private async deleteMethodWithQuery(uuid: string, dateString: string, ownerEmail: string, correlationId: number | undefined): Promise<Partial<DailyPlanningEventDto>> {
    StaticLogger.startMethod('deleteMethod', correlationId);
    const date = new Date(dateString);
    const key = `${this.getDocumentKey()}:${date.getFullYear()}:${date.getMonth() + 1}:${date.getDate()}:${ownerEmail}`;

    const obj = await this.baseRedisService.hDel<Partial<DailyPlanningEventDto>>(key, uuid, correlationId);
    // tell typescript obj is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('deleteMethod', baseObj as unknown as Partial<DailyPlanningEventDto>, correlationId);
  }

  private async getAllByDateAndOwnerMethod(dateString: string, ownerEmail: string, correlationId: number | undefined): Promise<Partial<DailyPlanningEventDto>[]> {
    StaticLogger.startMethod('getAllByDateAndOwnerMethod', correlationId);
    // create the key
    const date = new Date(dateString);
    const key = `${this.getDocumentKey()}:${date.getFullYear()}:${date.getMonth() + 1}:${date.getDate()}:${ownerEmail}`;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objWithKeys = await this.baseRedisService.hGetAll<any, Partial<DailyPlanningEventDto>>(key, correlationId);

    const obj: Partial<DailyPlanningEventDto>[] = [];
    for (const key in objWithKeys) {
      obj.push({ ...objWithKeys[key], uuid: key });
    }
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>[]>('getAllByDateAndOwnerMethod', obj, correlationId);
  }

  private async getOneMethodWithQuery(uuid: string, dateString: string, ownerEmail: string, correlationId: number | undefined): Promise<Partial<DailyPlanningEventDto>> {
    StaticLogger.startMethod('getOneMethodWithQuery', correlationId);
    const date = new Date(dateString);
    const key = `${this.getDocumentKey()}:${date.getFullYear()}:${date.getMonth() + 1}:${date.getDate()}:${ownerEmail}`;

    const obj = await this.baseRedisService.hGet<DailyPlanningEventDto>(key, uuid, correlationId);
    if (!obj) {
      throw this.createNotFoundException(`no document with uuid ${uuid} was found!`, correlationId);
    }
    // tell typescript obj is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('getOneMethodWithQuery', baseObj as unknown as DailyPlanningEventDto, correlationId);
  }

  private sanitizeMethodReadHistory(userDto: ApiUserDto, data: Partial<DailyPlanningEventDto>, correlationId: number | undefined): Partial<DailyPlanningEventDto> {
    StaticLogger.startMethod('sanitizeMethodReadHistory', correlationId);
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canSeeHistory)) {
      // remove the field isDeleted from the entry
      delete data.history;
    }
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('sanitizeMethodReadHistory', data, correlationId);
  }

  private sanitizeMethodReadIsDeleted(userDto: ApiUserDto, data: Partial<DailyPlanningEventDto>, correlationId: number | undefined): Partial<DailyPlanningEventDto> {
    StaticLogger.startMethod('sanitizeMethodReadIsDeleted', correlationId);
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canShowDeleted)) {
      delete data.isDeleted;
    }
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('sanitizeMethodReadIsDeleted', data, correlationId);
  }

  private sanitizeMethodWriteIsDeleted(userDto: ApiUserDto, data: Partial<DailyPlanningEventDto>, correlationId: number | undefined): Partial<DailyPlanningEventDto> {
    StaticLogger.startMethod('sanitizeMethodIsDeletedWrite', correlationId);
    // We remove the isDeleted when it's the return value and the user doesn't have the right to see it.
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.dailyPlanningEvent.canModifyDelete)) {
      // remove the field isDeleted from the entry
      delete (data as unknown as BaseDocumentDto).isDeleted;
    }
    return StaticLogger.endMethod<Partial<DailyPlanningEventDto>>('sanitizeMethodIsDeletedWrite', data, correlationId);
  }
}
