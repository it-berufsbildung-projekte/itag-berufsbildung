import { ApiUserDto, IQueryParams } from '@itag-berufsbildung/shared/util';
import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants, ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
  StaticLogger,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/planning/data';

import { ProjectTaskDto } from '@itag-berufsbildung/planning/data';

@ApiTags(`${ModuleConstants.instance.documents.projectTask}`)
@Controller(`${ModuleConstants.instance.documents.projectTask}`)
export class ProjectTaskController extends GenericDocumentController<ProjectTaskDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.projectTask.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.projectTask.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.projectTask.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.projectTask.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.projectTask.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.projectTask.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.projectTask.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.projectTask, genericDocumentControllerRights);
  }

  protected override async getAllMethod<T>(
    userDto: ApiUserDto,
    filter: IQueryParams,
    correlationId: number | undefined,
  ): Promise<Partial<T>[]> {
    StaticLogger.startMethod('getAllMethod', correlationId);
    // any is the key of the field (uuid).
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objWithKeys = await this.baseRedisService.hGetAll<any, Partial<T>>(this.getDocumentKey(), correlationId);

    const canReadOnlyOwn = userDto.hasRight( ModuleConstants.instance.availableRights.api.projectTask.canReadOnlyOwnProjectTasks );
    const obj: Partial<T>[] = [];
    // add the uuid back for each object
    for (const key in objWithKeys) {
      // exclude all projects/tasks with another ownerEmail if the user can only read his own obj.
      if ( canReadOnlyOwn && objWithKeys[key].ownerEmail.includes( userDto.eMail ) ) {
        obj.push({ ...objWithKeys[key], uuid: key });
        continue;
      }

      // if the user doesn't have this right, then he can read all obj.
      obj.push({ ...objWithKeys[key], uuid: key });
    }
    return StaticLogger.endMethod<Partial<T>[]>('getAllMethod', obj, correlationId);
  }

}
