import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { ApiConstants } from '@itag-berufsbildung/shared/data-api';

export class RoleRight {
  static readonly documents = {
    [ModuleConstants.instance.availableRoles.api]: [ApiConstants.rights.module.canCreate],
    [ModuleConstants.instance.availableRoles.admin]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canDeleteOthersCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canWriteUserRoles,
      ApiConstants.rights.generic.canDeleteUserRoles,
      ApiConstants.rights.generic.canDeleteUser,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
      ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,

      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,

      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canModifyDelete,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canSeeHistory,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canShowDeleted,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canModifyDelete,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canSeeHistory,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canShowDeleted,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.projectTask.canCreate,
      ModuleConstants.instance.availableRights.api.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.api.projectTask.canRead,
      ModuleConstants.instance.availableRights.api.projectTask.canRemove,
      ModuleConstants.instance.availableRights.api.projectTask.canShowDeleted,
      ModuleConstants.instance.availableRights.api.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.api.projectTask.canSeeHistory,

      ModuleConstants.instance.availableRights.api.eventCategory.canCreate,
      ModuleConstants.instance.availableRights.api.eventCategory.canModifyDelete,
      ModuleConstants.instance.availableRights.api.eventCategory.canRead,
      ModuleConstants.instance.availableRights.api.eventCategory.canRemove,
      ModuleConstants.instance.availableRights.api.eventCategory.canShowDeleted,
      ModuleConstants.instance.availableRights.api.eventCategory.canUpdate,
      ModuleConstants.instance.availableRights.api.eventCategory.canSeeHistory,

      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canRemove,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canUpdate,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canCreate,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canSeeHistory,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canModifyDelete,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canShowDeleted,

      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,

      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canSeeProjectTaskList,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canRemove,

      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canShowDeleted,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdateState,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canSeeHistory,

      ModuleConstants.instance.availableRights.ui.projectTask.canCreate,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreateForOthers,
      ModuleConstants.instance.availableRights.ui.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.projectTask.canRead,
      ModuleConstants.instance.availableRights.ui.projectTask.canRemove,
      ModuleConstants.instance.availableRights.ui.projectTask.canShowDeleted,
      ModuleConstants.instance.availableRights.ui.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.ui.projectTask.canSeeHistory,

      ModuleConstants.instance.availableRights.ui.eventCategory.canCreate,
      ModuleConstants.instance.availableRights.ui.eventCategory.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,
      ModuleConstants.instance.availableRights.ui.eventCategory.canRemove,
      ModuleConstants.instance.availableRights.ui.eventCategory.canShowDeleted,
      ModuleConstants.instance.availableRights.ui.eventCategory.canUpdate,
      ModuleConstants.instance.availableRights.ui.eventCategory.canSeeHistory,

      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canRemove,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canUpdate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canCreate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canSeeHistory,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canShowDeleted,
    ],
    [ModuleConstants.instance.availableRoles.bb]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
      ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,

      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,

      ModuleConstants.instance.availableRights.api.projectTask.canRead,
      ModuleConstants.instance.availableRights.api.projectTask.canCreate,
      ModuleConstants.instance.availableRights.api.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.api.projectTask.canRemove,
      ModuleConstants.instance.availableRights.api.projectTask.canShowDeleted,
      ModuleConstants.instance.availableRights.api.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.api.projectTask.canSeeHistory,

      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canModifyDelete,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRead,

      ModuleConstants.instance.availableRights.api.eventCategory.canRead,

      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdateState,

      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canSeeProjectTaskList,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canRemove,

      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRead,

      ModuleConstants.instance.availableRights.ui.projectTask.canRead,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreate,
      ModuleConstants.instance.availableRights.ui.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.ui.projectTask.canRemove,
      ModuleConstants.instance.availableRights.ui.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreateForOthers,

      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,

      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canUpdate,
    ],
    [ModuleConstants.instance.availableRoles.pb]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
      ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,

      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,

      ModuleConstants.instance.availableRights.api.projectTask.canRead,
      ModuleConstants.instance.availableRights.api.projectTask.canCreate,
      ModuleConstants.instance.availableRights.api.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.api.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.api.projectTask.canRemove,

      ModuleConstants.instance.availableRights.api.eventCategory.canRead,

      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canModifyDelete,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canUpdate,

      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canSeeProjectTaskList,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canCreateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdate,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canUpdateEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteOwnEvent,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canDeleteEventForOthers,
      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canRemove,

      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,

      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdateState,

      ModuleConstants.instance.availableRights.ui.projectTask.canRead,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreate,
      ModuleConstants.instance.availableRights.ui.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.ui.projectTask.canRemove,
      ModuleConstants.instance.availableRights.ui.projectTask.canModifyDelete,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreateForOthers,

      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,

      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canRead,
      // ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canUpdate,
    ],
    [ModuleConstants.instance.availableRoles.ab]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowAnnualPlanning,
      ModuleConstants.instance.availableRights.menu.canShowDailyPlanning,

      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.projectTask.canRead,
      ModuleConstants.instance.availableRights.api.projectTask.canReadOnlyOwnProjectTasks,
      ModuleConstants.instance.availableRights.api.projectTask.canCreate,
      ModuleConstants.instance.availableRights.api.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.api.projectTask.canRemove,

      ModuleConstants.instance.availableRights.api.annualPlanningEvent.canRead,

      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.api.dailyPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.api.eventCategory.canRead,

      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canCreate,
      ModuleConstants.instance.availableRights.api.dailyPlanningLog.canUpdate,

      ModuleConstants.instance.availableRights.ui.annualPlanningEvent.canRead,

      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,

      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canCreate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canReadOnlyOwnEvents,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canRemove,
      ModuleConstants.instance.availableRights.ui.dailyPlanningEvent.canUpdate,

      ModuleConstants.instance.availableRights.ui.projectTask.canRead,
      ModuleConstants.instance.availableRights.ui.projectTask.canReadOnlyOwnProjectTasks,
      ModuleConstants.instance.availableRights.ui.projectTask.canCreate,
      ModuleConstants.instance.availableRights.ui.projectTask.canUpdate,
      ModuleConstants.instance.availableRights.ui.projectTask.canRemove,

      ModuleConstants.instance.availableRights.ui.eventCategory.canRead,

      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canCreate,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canRead,
      ModuleConstants.instance.availableRights.ui.dailyPlanningLog.canUpdate,
    ],
    [ModuleConstants.instance.availableRoles.user]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
    [ModuleConstants.instance.availableRoles.readOnly]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
  };
}
