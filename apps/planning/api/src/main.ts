// should be the first block of commands to read the env
import * as path from 'path';
// read the top level evn variables from the root
const rootPath = path.join('.env');
// read the module specific env variables
const appPath = path.join('apps', 'planning', 'api', '.env');
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: rootPath, debug: false });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: appPath, debug: false });

import { ModuleConstants } from '@itag-berufsbildung/planning/data';
import { ApiConstants, BaseAppConfigService, NestAppCreator, StaticLogger } from '@itag-berufsbildung/shared/data-api';

import { Constants } from './constants';
import { AppModule } from './app/app.module';
import { environment } from './environment/environment';

async function bootstrap() {
  // get the appConfiguration from the env variables and Constants file with defaults
  StaticLogger.startMethod('boostrap', ApiConstants.correlationIds.bootstrap);
  const appConfiguration = BaseAppConfigService.readApiEnvVariables(ModuleConstants.instance, Constants.instance, environment.production);

  // initialize the app
  const app = await NestAppCreator.initApp(AppModule, appConfiguration, ApiConstants.correlationIds.bootstrap);
  // do something between init und start, we usually have all middlewares already loaded in the init for all modules
  await NestAppCreator.startApp(app, appConfiguration, ApiConstants.correlationIds.bootstrap);
}

bootstrap()
  .then(() => {
    StaticLogger.endMethod('boostrap', ModuleConstants.instance.moduleName, ApiConstants.correlationIds.bootstrap);
  })
  .catch((reason) => {
    StaticLogger.error(reason, ApiConstants.correlationIds.bootstrap);
  });
