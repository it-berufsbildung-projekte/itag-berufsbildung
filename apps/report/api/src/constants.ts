import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    icon: 'accessibility',
    level: 0,
    right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    path: ModuleConstants.instance.routing.base,
    text: 'report',
    title: 'Das Module report',
    children: [
      {
        icon: 'preview',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowReportMenu,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.reports}`,
        text: 'Berichte',
        title: 'Liste der Berichte',
      }
    ],
  };

  readonly redisKeys = {
    contentType: 'contentType',
    reportType: 'reportType',
    reportBlock: 'reportBlock',
    question: 'question',
    reportBlockQuestion: 'reportBlockQuestion',
    reportTypeReportBlock: 'reportTypeReportBlock',
    reportAnswer: 'reportAnswer',
  };
}
