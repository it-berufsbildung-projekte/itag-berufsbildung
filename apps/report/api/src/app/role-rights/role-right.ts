import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { ApiConstants } from '@itag-berufsbildung/shared/data-api';

export class RoleRight {
  static readonly documents = {
    [ModuleConstants.instance.availableRoles.api]: [ApiConstants.rights.module.canCreate],
    [ModuleConstants.instance.availableRoles.admin]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canWriteUserRoles,
      ApiConstants.rights.generic.canDeleteUserRoles,
      ApiConstants.rights.generic.canDeleteUser,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canDeleteOthersCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowReportMenu,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,

      ModuleConstants.instance.availableRights.api.report.canCreate,
      ModuleConstants.instance.availableRights.api.report.canCreateForApprenticeship,
      ModuleConstants.instance.availableRights.api.report.canModifyDelete,
      ModuleConstants.instance.availableRights.api.report.canRead,
      ModuleConstants.instance.availableRights.api.report.canRemove,
      ModuleConstants.instance.availableRights.api.report.canShowDeleted,
      ModuleConstants.instance.availableRights.api.report.canUpdate,
      ModuleConstants.instance.availableRights.api.report.canSeeHistory,

      ModuleConstants.instance.availableRights.api.reportAnswer.canCreate,
      ModuleConstants.instance.availableRights.api.reportAnswer.canModifyDelete,
      ModuleConstants.instance.availableRights.api.reportAnswer.canRead,
      ModuleConstants.instance.availableRights.api.reportAnswer.canRemove,
      ModuleConstants.instance.availableRights.api.reportAnswer.canShowDeleted,
      ModuleConstants.instance.availableRights.api.reportAnswer.canUpdate,
      ModuleConstants.instance.availableRights.api.reportAnswer.canSeeHistory,

      ModuleConstants.instance.availableRights.api.reportType.canCreate,
      ModuleConstants.instance.availableRights.api.reportType.canModifyDelete,
      ModuleConstants.instance.availableRights.api.reportType.canRead,
      ModuleConstants.instance.availableRights.api.reportType.canRemove,
      ModuleConstants.instance.availableRights.api.reportType.canShowDeleted,
      ModuleConstants.instance.availableRights.api.reportType.canUpdate,
      ModuleConstants.instance.availableRights.api.reportType.canSeeHistory,

      ModuleConstants.instance.availableRights.api.reportBlock.canCreate,
      ModuleConstants.instance.availableRights.api.reportBlock.canModifyDelete,
      ModuleConstants.instance.availableRights.api.reportBlock.canRead,
      ModuleConstants.instance.availableRights.api.reportBlock.canRemove,
      ModuleConstants.instance.availableRights.api.reportBlock.canShowDeleted,
      ModuleConstants.instance.availableRights.api.reportBlock.canUpdate,
      ModuleConstants.instance.availableRights.api.reportBlock.canSeeHistory,

      ModuleConstants.instance.availableRights.api.contentType.canCreate,
      ModuleConstants.instance.availableRights.api.contentType.canModifyDelete,
      ModuleConstants.instance.availableRights.api.contentType.canRead,
      ModuleConstants.instance.availableRights.api.contentType.canRemove,
      ModuleConstants.instance.availableRights.api.contentType.canShowDeleted,
      ModuleConstants.instance.availableRights.api.contentType.canUpdate,
      ModuleConstants.instance.availableRights.api.contentType.canSeeHistory,

      ModuleConstants.instance.availableRights.api.question.canCreate,
      ModuleConstants.instance.availableRights.api.question.canModifyDelete,
      ModuleConstants.instance.availableRights.api.question.canRead,
      ModuleConstants.instance.availableRights.api.question.canRemove,
      ModuleConstants.instance.availableRights.api.question.canShowDeleted,
      ModuleConstants.instance.availableRights.api.question.canUpdate,
      ModuleConstants.instance.availableRights.api.question.canSeeHistory,

      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canCreate,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canModifyDelete,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRead,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRemove,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canShowDeleted,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canUpdate,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canSeeHistory,

      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canCreate,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canModifyDelete,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRead,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRemove,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canShowDeleted,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canUpdate,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canSeeHistory,

      ModuleConstants.instance.availableRights.api.noteFeedback.canCreate,
      ModuleConstants.instance.availableRights.api.noteFeedback.canModifyDelete,
      ModuleConstants.instance.availableRights.api.noteFeedback.canRead,
      ModuleConstants.instance.availableRights.api.noteFeedback.canRemove,
      ModuleConstants.instance.availableRights.api.noteFeedback.canShowDeleted,
      ModuleConstants.instance.availableRights.api.noteFeedback.canUpdate,
      ModuleConstants.instance.availableRights.api.noteFeedback.canSeeHistory,

      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
    ],
    [ModuleConstants.instance.availableRoles.bb]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowReportMenu,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,

      ModuleConstants.instance.availableRights.api.report.canCreate,
      ModuleConstants.instance.availableRights.api.report.canCreateForApprenticeship,
      ModuleConstants.instance.availableRights.api.report.canUpdate,
      ModuleConstants.instance.availableRights.api.report.canRead,
      ModuleConstants.instance.availableRights.api.report.canShowDeleted,

      ModuleConstants.instance.availableRights.api.reportAnswer.canRead,
      ModuleConstants.instance.availableRights.api.reportAnswer.canShowDeleted,

      ModuleConstants.instance.availableRights.api.reportType.canRead,
      ModuleConstants.instance.availableRights.api.reportType.canShowDeleted,

      ModuleConstants.instance.availableRights.api.reportBlock.canRead,
      ModuleConstants.instance.availableRights.api.reportBlock.canShowDeleted,

      ModuleConstants.instance.availableRights.api.contentType.canRead,
      ModuleConstants.instance.availableRights.api.contentType.canShowDeleted,

      ModuleConstants.instance.availableRights.api.question.canRead,
      ModuleConstants.instance.availableRights.api.question.canShowDeleted,

      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRead,
      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canShowDeleted,

      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRead,
      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canShowDeleted,

      ModuleConstants.instance.availableRights.api.noteFeedback.canCreate,
      ModuleConstants.instance.availableRights.api.noteFeedback.canRead,

      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
    ],
    [ModuleConstants.instance.availableRoles.pb]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,

      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowReportMenu,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,

      ModuleConstants.instance.availableRights.api.report.canRead,
      ModuleConstants.instance.availableRights.api.report.canUpdate,

      ModuleConstants.instance.availableRights.api.reportAnswer.canCreate,
      ModuleConstants.instance.availableRights.api.reportAnswer.canRead,
      ModuleConstants.instance.availableRights.api.reportAnswer.canUpdate,

      ModuleConstants.instance.availableRights.api.reportType.canRead,

      ModuleConstants.instance.availableRights.api.reportBlock.canRead,

      ModuleConstants.instance.availableRights.api.contentType.canRead,

      ModuleConstants.instance.availableRights.api.question.canRead,

      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRead,

      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRead,

      ModuleConstants.instance.availableRights.api.noteFeedback.canCreate,
      ModuleConstants.instance.availableRights.api.noteFeedback.canRead,

      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
    ],
    [ModuleConstants.instance.availableRoles.ab]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowReportMenu,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,

      ModuleConstants.instance.availableRights.api.report.canRead,
      ModuleConstants.instance.availableRights.api.report.canUpdate,

      ModuleConstants.instance.availableRights.api.reportAnswer.canCreate,
      ModuleConstants.instance.availableRights.api.reportAnswer.canRead,
      ModuleConstants.instance.availableRights.api.reportAnswer.canUpdate,

      ModuleConstants.instance.availableRights.api.reportType.canRead,

      ModuleConstants.instance.availableRights.api.reportBlock.canRead,

      ModuleConstants.instance.availableRights.api.contentType.canRead,

      ModuleConstants.instance.availableRights.api.question.canRead,

      ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRead,

      ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRead,

      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
    ],
    [ModuleConstants.instance.availableRoles.user]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
    [ModuleConstants.instance.availableRoles.readOnly]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
  };
}
