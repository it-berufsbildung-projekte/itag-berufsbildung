import { Inject, Injectable } from '@nestjs/common';

import { UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { ModuleConstants } from '@itag-berufsbildung/report/data';
import { ApiCallerService, ApiConstants, BaseAppConfigService, BaseRedisService, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import * as fs from 'fs';
import * as path from 'path';

import { Constants } from '../../../constants';
import { environment } from '../../../environment/environment';

enum EnumFileType {
  contentType,
  reportType,
  reportBlock,
  question,
  reportBlockQuestion,
  reportTypeReportBlock,
}

interface IContentType {
  uuid: string;
  isDeleted: boolean;
  typeName: string;
  version: string;
  key: string;
}

interface IReportType {
  uuid: string;
  isDeleted: boolean;
  type: string;
  version: string;
  sortOrder: number;
}

interface IReportBlock {
  uuid: string;
  isDeleted: boolean;
  title: string;
  onScreen: boolean;
  onPdf: boolean;
  readonly: boolean;
  pbReadWrite: boolean;
  abReadWrite: boolean;
}

interface IQuestion {
  uuid: string;
  isDeleted: boolean;
  title: string;
  description: string;
  helpText: string;
  contentTypeUuid: string;
}

interface IReportBlockQuestion {
  uuid: string;
  isDeleted: boolean;
  reportBlockUuid: string;
  questionUuid: string;
  sortOrder: number;
}

interface IReportTypeReportBlock {
  uuid: string;
  isDeleted: boolean;
  reportTypeUuid: string;
  reportBlockUuid: string;
  sortOrder: number;
}

type ImportData = IContentType | IReportType | IReportBlock | IQuestion | IReportBlockQuestion | IReportTypeReportBlock;

@Injectable()
export class AppConfigService extends BaseAppConfigService {
  constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.apiCallerService) protected readonly apiCaller: ApiCallerService
  ) {
    super(baseRedisService, apiCaller, ModuleConstants.instance, Constants.instance, environment.production);
  }

  async initDatabase(correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('initDatabase', correlationId);

    // define base path
    const dataPath = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment);

    // import content-type
    await this.importData(path.join(dataPath, 'content-type.json'), EnumFileType.contentType, correlationId);

    // import report-type
    await this.importData(path.join(dataPath, 'report-type.json'), EnumFileType.reportType, correlationId);

    // import report-block
    await this.importData(path.join(dataPath, 'report-block.json'), EnumFileType.reportBlock, correlationId);

    // import question
    await this.importData(path.join(dataPath, 'question.json'), EnumFileType.question, correlationId);

    // import report-block-question
    await this.importData(path.join(dataPath, 'report-block-question.json'), EnumFileType.reportBlockQuestion, correlationId);

    // import report-type-report-block
    await this.importData(path.join(dataPath, 'report-type-report-block.json'), EnumFileType.reportTypeReportBlock, correlationId);

    // read the data depending on the environment
    const dataPathUserRole = path.join(dataPath, 'user-role.json');
    // check that the user role file exists
    if (fs.existsSync(dataPathUserRole)) {
      const userRoleHashDto: UserRoleHashDto = JSON.parse(fs.readFileSync(dataPathUserRole, { encoding: 'utf-8' }));
      await this.baseRedisService.registerUserRoleForModule(this.adminAppConfiguration.moduleName, userRoleHashDto, correlationId);
    } else {
      StaticLogger.warn(`file ${dataPathUserRole} not found`, correlationId);
    }

    StaticLogger.endMethod('initDatabase', undefined, correlationId);
  }

  private async importData(filePath: string, fileType: EnumFileType, correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('importData', correlationId);

    if (!fs.existsSync(filePath)) {
      return StaticLogger.warn(`File ${filePath} does not exists!`, correlationId);
    }

    const content = fs.readFileSync(filePath, { encoding: 'utf-8' });
    if (!content || content.length === 0) {
      const msg = `File ${filePath} is empty!`;
      StaticLogger.error(msg, correlationId);
      throw new Error(msg);
    }

    const data: ImportData[] = JSON.parse(content);
    let dbKey = '';
    switch (fileType) {
      case EnumFileType.contentType:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.contentType}`;
        break;
      case EnumFileType.reportType:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportType}`;
        break;
      case EnumFileType.reportBlock:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportBlock}`;
        break;
      case EnumFileType.question:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.question}`;
        break;
      case EnumFileType.reportBlockQuestion:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportBlockQuestion}`;
        break;
      case EnumFileType.reportTypeReportBlock:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportTypeReportBlock}`;
        break;
    }

    for (const item of data) {
      // check if key already exists in db
      if ( !await this.baseRedisService.hGet(dbKey, item.uuid, correlationId) ) {
        await this.baseRedisService.hSet(dbKey, item.uuid, item, correlationId);
      }
    }
  }
}
