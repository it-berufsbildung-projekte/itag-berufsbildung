import { HttpModule, HttpService } from '@nestjs/axios';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import {
  ApiCallerService,
  ApiConstants,
  ApiSocketService,
  ApiUserService,
  AppGateway,
  BaseAppConfigService,
  JwtStrategy,
  NestAppCreator,
} from '@itag-berufsbildung/shared/data-api';

import { AppConfigService } from './services/app-config/app-config.service';
import { GenericController } from './controllers/generic/generic.controller';
import { DbService } from './services/db/db.service';
import { ReportTypeController } from './controllers/report-type/report-type.controller';
import { ReportBlockController } from './controllers/report-block/report-block.controller';
import { ContentTypeController } from './controllers/content-type/content-type.controller';
import { QuestionController } from './controllers/question/question.controller';
import { ReportBlockQuestionController } from './controllers/report-block-question/report-block-question.controller';
import { ReportTypeReportBlockController } from './controllers/report-type-report-block/report-type-report-block.controller';
import { ReportController } from './controllers/report/report.controller';
import { ReportAnswerController } from './controllers/report-answer/report-answer.controller';
import { NoteFeedbackController } from './controllers/note-feedback/note-feedback.controller';

@Module({
  imports: [JwtModule.register({}), HttpModule],
  controllers: [
    GenericController,
    ReportTypeController,
    ReportBlockController,
    ContentTypeController,
    QuestionController,
    ReportBlockQuestionController,
    ReportTypeReportBlockController,
    ReportController,
    ReportAnswerController,
    NoteFeedbackController,
  ],
  providers: [
    JwtStrategy,
    AppGateway,
    ApiUserService,
    {
      provide: ApiConstants.services.dbService,
      useClass: DbService,
    },
    {
      provide: ApiConstants.services.apiCallerService,
      inject: [HttpService, ApiConstants.services.dbService],
      useFactory: (httpService: HttpService, dbService: DbService) => new ApiCallerService(httpService, dbService),
    },
    {
      provide: ApiConstants.services.appConfigService,
      inject: [ApiConstants.services.dbService, ApiConstants.services.apiCallerService],
      useFactory: (dbService: DbService, apiCallerService: ApiCallerService) => new AppConfigService(dbService, apiCallerService),
    },
    {
      provide: ApiConstants.services.socketService,
      inject: [ApiConstants.services.appConfigService],
      useFactory: (appConfigService: BaseAppConfigService) => new ApiSocketService(appConfigService),
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    return NestAppCreator.addConfigure(consumer, 0);
  }
}
