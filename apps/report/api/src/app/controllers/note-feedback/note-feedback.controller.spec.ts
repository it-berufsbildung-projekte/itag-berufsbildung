import { Test, TestingModule } from '@nestjs/testing';
import { NoteFeedbackController } from './note-feedback.controller';

describe('NoteFeedbackController', () => {
  let controller: NoteFeedbackController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoteFeedbackController],
    }).compile();

    controller = module.get<NoteFeedbackController>(NoteFeedbackController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
