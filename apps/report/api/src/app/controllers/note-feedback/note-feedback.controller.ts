import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/report/data';

import { NoteFeedbackDto } from '@itag-berufsbildung/report/data';

@ApiTags(`${ModuleConstants.instance.documents.noteFeedback}`)
@Controller(`${ModuleConstants.instance.documents.noteFeedback}`)
export class NoteFeedbackController extends GenericDocumentController<NoteFeedbackDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.noteFeedback.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.noteFeedback.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.noteFeedback.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.noteFeedback.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.noteFeedback.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.noteFeedback.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.noteFeedback.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.noteFeedback, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
