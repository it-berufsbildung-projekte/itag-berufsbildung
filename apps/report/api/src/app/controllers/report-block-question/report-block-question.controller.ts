import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/report/data';

import { ReportBlockQuestionDto } from '@itag-berufsbildung/report/data';

@ApiTags(`${ModuleConstants.instance.documents.reportBlockQuestion}`)
@Controller(`${ModuleConstants.instance.documents.reportBlockQuestion}`)
export class ReportBlockQuestionController extends GenericDocumentController<ReportBlockQuestionDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.reportBlockQuestion.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.reportBlockQuestion, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
