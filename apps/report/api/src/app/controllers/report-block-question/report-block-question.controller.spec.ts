import { Test, TestingModule } from '@nestjs/testing';
import { ReportBlockQuestionController } from './report-block-question.controller';

describe('ReportBlockQuestionController', () => {
  let controller: ReportBlockQuestionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportBlockQuestionController],
    }).compile();

    controller = module.get<ReportBlockQuestionController>(ReportBlockQuestionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
