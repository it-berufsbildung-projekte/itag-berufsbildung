import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/report/data';

import { ReportTypeReportBlockDto } from '@itag-berufsbildung/report/data';

@ApiTags(`${ModuleConstants.instance.documents.reportTypeReportBlock}`)
@Controller(`${ModuleConstants.instance.documents.reportTypeReportBlock}`)
export class ReportTypeReportBlockController extends GenericDocumentController<ReportTypeReportBlockDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.reportTypeReportBlock.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.reportTypeReportBlock, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
