import { Test, TestingModule } from '@nestjs/testing';
import { ReportTypeReportBlockController } from './report-type-report-block.controller';

describe('ReportTypeReportBlockController', () => {
  let controller: ReportTypeReportBlockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportTypeReportBlockController],
    }).compile();

    controller = module.get<ReportTypeReportBlockController>(ReportTypeReportBlockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
