import { Test, TestingModule } from '@nestjs/testing';
import { ReportBlockController } from './report-block.controller';

describe('ReportBlockController', () => {
  let controller: ReportBlockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportBlockController],
    }).compile();

    controller = module.get<ReportBlockController>(ReportBlockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
