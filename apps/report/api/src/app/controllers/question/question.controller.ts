import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/report/data';

import { QuestionDto } from '@itag-berufsbildung/report/data';

@ApiTags(`${ModuleConstants.instance.documents.question}`)
@Controller(`${ModuleConstants.instance.documents.question}`)
export class QuestionController extends GenericDocumentController<QuestionDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.question.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.question.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.question.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.question.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.question.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.question.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.question.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.question, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
