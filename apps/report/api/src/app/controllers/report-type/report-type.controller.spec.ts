import { Test, TestingModule } from '@nestjs/testing';
import { ReportTypeController } from './report-type.controller';

describe('ReportTypeController', () => {
  let controller: ReportTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportTypeController],
    }).compile();

    controller = module.get<ReportTypeController>(ReportTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
