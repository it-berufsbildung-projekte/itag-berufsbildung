import { Test, TestingModule } from '@nestjs/testing';
import { ReportAnswerController } from './report-answer.controller';

describe('ReportAnswerController', () => {
  let controller: ReportAnswerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportAnswerController],
    }).compile();

    controller = module.get<ReportAnswerController>(ReportAnswerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
