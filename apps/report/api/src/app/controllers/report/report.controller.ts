import {
  ContentTypeDto,
  ModuleConstants,
  QuestionDto,
  ReportAnswerDto,
  ReportBlockDto,
  ReportBlockQuestionDto,
  ReportDto,
  ReportsForApprenticeshipCreateDto,
  ReportTypeDetailViewDto,
  ReportTypeDto,
  ReportTypeReportBlockDto,
  WebSocketSyncDto,
} from '@itag-berufsbildung/report/data';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService, CheckAccessMethodType,
  Email,
  GenericAppController,
  GenericDocumentController,
  GetCorrelationId,
  GetUser,
  GetWsId,
  IGenericDocumentControllerRights,
  StaticLogger, UpsetMethodType,
} from '@itag-berufsbildung/shared/data-api';
import { MailDto } from '@itag-berufsbildung/shared/data-ui';
import {
  ApiUserDto,
  BaseDocumentDto,
  baseDocumentExample,
  baseDocumentExampleArr,
  IBaseUuidDtoHistory,
  IQueryParams,
  ObjectTools,
  RoleHashDto,
  UserNameTool,
} from '@itag-berufsbildung/shared/util';
import { Body, Controller, Get, Inject, Param, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiBody, ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse, ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { join, leftJoin } from 'array-join';
import { Request } from 'express';
import { v4 } from 'uuid';
import { Constants } from '../../../constants';

const dataType = 'ReportDto';
const dataTypeDescription = 'An event that can be shown in a kendo scheduler component';
const dataDescription = `
 * depending on the role of the user we get only a subset back.
 * with the role admin all properties will be get back`;

@ApiTags(`${ModuleConstants.instance.documents.report}`)
@Controller(`${ModuleConstants.instance.documents.report}`)
export class ReportController extends GenericDocumentController<ReportDto> {
  private moduleConstants = ModuleConstants.instance;

  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.report.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.report.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.report.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.report.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.report.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.report.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.report.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.report, genericDocumentControllerRights);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiCreatedResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiBody({
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Create all reports for one apprenticeship',
  })
  @Post('/reportsForApprenticeship')
  async createReportsForApprenticeship(
    @Body() postDto: ReportsForApprenticeshipCreateDto,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @GetWsId() wsId: string,
  ): Promise<Partial<ReportDto>[]> {
    StaticLogger.startMethod('createReportsForApprenticeship', correlationId);

    // check rights
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.report.canCreateForApprenticeship)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }

    const createdReports = await this.createReportsForApprenticeshipMethod(userDto, postDto, correlationId );

    this.socketService.socket.emit(ApiConstants.webSocketEvents.dataChanged, { wsId: wsId, payload: {documentName: ModuleConstants.instance.documents.report} } );
    return StaticLogger.endMethod<Partial<ReportDto>[]>('createReportsForApprenticeship', createdReports, correlationId);
  }

  @Post('/socket')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    summary: 'Emits websocket events.',
  })
  async createWebsocketSyncEvent(
    @Body() websocketSyncDto: WebSocketSyncDto,
    @Req() request: Request,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @GetWsId() wsId: string,
  ): Promise<void> {
    StaticLogger.startMethod('createWebsocketSyncEvent', correlationId);
    // checkAccess
    if ( !userDto.hasRole( ModuleConstants.instance.availableRoles.ab) && !userDto.hasRole( ModuleConstants.instance.availableRoles.pb) ) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    return StaticLogger.endMethod<void>('createWebsocketSyncEvent', await this.emitWebsocketSyncEvent(websocketSyncDto, wsId), correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      items: {
        type: 'ReportTypeDetailViewDto',
      },
      example: baseDocumentExampleArr,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    description: '',
    summary: 'Get the complete view of the six base data documents.',
  })
  @Get('/reportTypeDetailView')
  async getReportTypeDetailView(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
  ): Promise<Partial<ReportTypeDetailViewDto>[]> {
    StaticLogger.startMethod('getReportTypeDetailView', correlationId);
    // check rights
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.report.canRead)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    const objArr: Partial<ReportTypeDetailViewDto>[] = await this.getReportTypeDetailViewMethod(correlationId);
    // hint: if there is no result we send an empty array.
    if (objArr?.length < 1) {
      return StaticLogger.endMethod<Partial<ReportTypeDetailViewDto>[]>('getReportTypeDetailView', [], correlationId);
    }
    // sanitize the array. Might be overridden by the child class
    const sanitizedObjArr = objArr.map((o) => this.sanitizeReadMethod(userDto, o, correlationId));
    // remove the entries if they are deleted and the user can't see the deleted entries or showDeleted is false
    let removedIsDeletedArr = sanitizedObjArr;
    const showDeleted = request?.query.isDeleted ? request.query.isDeleted : false;
    // hint: we filter them first before removing the property since it would return every document.
    if (!userDto.hasRight(this.moduleConstants.availableRights.api.report.canShowDeleted) || !showDeleted) {
      // hint: we can't use o.hasOwnProperty() or hasOwn() so the workaround is to convert it to a string and look if the string doesn't contain 'isDeleted' (happens when someone who can't modify the property posts a document)
      removedIsDeletedArr = sanitizedObjArr.filter((o) => !(o as unknown as BaseDocumentDto).isDeleted || !JSON.stringify(o as unknown as BaseDocumentDto).includes('isDeleted'));
    }
    // we remove the isDeleted property if the user doesn't have the right to see it
    const sanitizedShowDeletedObjArr = removedIsDeletedArr.map((s) => this.sanitizeMethodIsDeletedRead(userDto, s, correlationId));
    // we remove the history if the user doesn't habe the right to see it
    const sanitizedShowHistoryObjArr = sanitizedShowDeletedObjArr.map((s) => this.sanitizeMethodHistoryRead(userDto, s, correlationId));

    return StaticLogger.endMethod<Partial<ReportTypeDetailViewDto>[]>('getReportTypeDetailView', sanitizedShowHistoryObjArr, correlationId);
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: dataTypeDescription,
      type: dataType,
      example: baseDocumentExample,
    },
    description: dataDescription,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiConflictResponse(GenericAppController.conflictResponse)
  @ApiNotFoundResponse(GenericAppController.notFoundResponseOption)
  @ApiBody({
    schema: {
      description: dataTypeDescription,
      type: dataDescription,
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Replace on an existing field some values',
  })
  @Patch('/:uuid')
  async patch(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() patchDto: ReportDto,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<ReportDto>> {
    StaticLogger.startMethod('Patch', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(ModuleConstants.instance.availableRights.api.report.canUpdate) && !userDto.hasRight(ModuleConstants.instance.availableRights.api.report.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }
    // we try to get first the old value
    const oldValue: Partial<ReportDto> = await this.getOneMethod<ReportDto>(userDto, uuid, correlationId);
    // clear the answers array of the report to avoid a massive history due to useless information (history) from the answers. The history of all answers is already saved in the answer document.
    oldValue.answers = [];

    // check if uuid is equal,
    if (patchDto?.uuid && patchDto.uuid !== uuid) {
      // different uuid in the body and in the uri. we do not allow this. the user has to remove the uuid or set it to equal
      throw this.createConflictException(`You try to update a document with a different uuid in the body!`, correlationId);
    }
    // check if isDeleted is already true and the user doesn't have the right to modify a deleted field.
    if (oldValue.isDeleted && !userDto.hasRight(ModuleConstants.instance.availableRights.api.report.canModifyDelete)) {
      throw this.createMethodNotAllowedException(`the user with email ${userDto.eMail} is not allowed to modify a deleted field!`, correlationId);
    }
    // checkAccess. Our check will always lose.
    this.checkAccess(oldValue, userDto, CheckAccessMethodType.patch, queryParams, correlationId, patchDto);
    // check if the oldValue isDeleted and the user doesn't have the right to modifyDelete
    const sanitizedIsDeletedPatchDto = this.sanitizeMethodIsDeletedWrite(userDto, patchDto, correlationId);
    const sanitizedWritePatchDto = this.sanitizeUpsetMethod(userDto, sanitizedIsDeletedPatchDto, oldValue, queryParams, UpsetMethodType.Update, correlationId);

    // clone the old value and remove history to prevent circular structure in the newHistory
    const oldValueWithoutHistory = ObjectTools.cloneDeep<Partial<ReportDto>>(oldValue);
    delete oldValueWithoutHistory.history;
    const newHistory: IBaseUuidDtoHistory = {
      oldValue: oldValueWithoutHistory,
      method: 'UPDATE',
      email: userDto.eMail,
      timestamp: Date.now(),
    };
    const sanitizedHistoryPatchDto = GenericDocumentController.sanitizeMethodHistoryWrite(sanitizedWritePatchDto, oldValue, newHistory, correlationId);

    // now we update the oldValues with the new values
    for (const prop of Object.keys(sanitizedHistoryPatchDto)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      oldValue[prop] = sanitizedWritePatchDto[prop];
    }
    // remove field uuid since it's no longer needed.
    delete oldValue.uuid;
    // save the patched entry
    const obj = await this.upsetMethod<ReportDto>(uuid, oldValue, userDto, queryParams, correlationId);
    // sanitizer may be overridden by the child class
    const sanitizedObj = this.sanitizeReadMethod<ReportDto>(userDto, obj, correlationId);
    // we always remove as last by our self the deleted column
    const sanitizedIsDeletedObj = this.sanitizeMethodIsDeletedRead(userDto, sanitizedObj, correlationId);
    // we remove the history if the user doesn't have the right to see it
    const finalObj = this.sanitizeMethodHistoryRead(userDto, sanitizedIsDeletedObj, correlationId);
    // emit a websocket event
    this.socketService.socket.emit(ApiConstants.webSocketEvents.dataChanged, { wsId: wsId, payload: {documentName: ModuleConstants.instance.documents.report} } );
    //  return finally the single element in this case
    return StaticLogger.endMethod<Partial<ReportDto>>('Patch', finalObj, correlationId);
  }

  @Post('/sendMail')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    summary: 'Emits websocket events.',
  })
  async sendMail(
    @Body() mailDto: MailDto,
    @Req() request: Request,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
  ): Promise<void> {
    StaticLogger.startMethod('sendMail', correlationId);
    // checkAccess
    if ( !userDto.hasRole( ModuleConstants.instance.availableRoles.ab) && !userDto.hasRole( ModuleConstants.instance.availableRoles.pb) ) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }

    await Email.sendMail( mailDto.receiver, mailDto.subject, mailDto.contentHtml, correlationId);
    return StaticLogger.endMethod<void>('sendMail', undefined, correlationId);
  }

  protected override async getAllMethod<T>(
    userDto: ApiUserDto,
    filter: IQueryParams,
    correlationId: number | undefined,
  ): Promise<Partial<T>[]> {
    StaticLogger.startMethod('getAllMethod', correlationId);
    // any is the key of the field (uuid).
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objWithKeys = await this.baseRedisService.hGetAll<any, Partial<T>>(this.getDocumentKey(), correlationId);
    // get all answers
    const answersArr: ReportAnswerDto[] = await this.getAllAnswers( correlationId );

    let obj: Partial<T>[] = [];
    // add the uuid back for each object
    for (const key in objWithKeys) {
      if ( Object.keys(filter).length > 0 && objWithKeys[key]['abUserEmail'] !== filter['abUserId'] ) {
        continue;
      }
      // get the answers for the reports
      const answers = answersArr.filter( (a) => a.reportUuid === key );
      obj.push({ ...objWithKeys[key], uuid: key, answers: answers });
    }
    // hack: this is not
    obj = obj.map( (o) => this.sanitizeAnswersAndNotes( o as unknown as ReportDto, userDto ) as unknown as Partial<T> );
    return StaticLogger.endMethod<Partial<T>[]>('getAllMethod', obj, correlationId);
  }

  protected override async getOneMethod<T>(
    userDto: ApiUserDto,
    uuid: string,
    correlationId: number | undefined,
  ): Promise<Partial<T>> {
    StaticLogger.startMethod('getOneMethod', correlationId);
    const obj = await this.baseRedisService.hGet<T>(this.getDocumentKey(), uuid, correlationId);
    if (!obj) {
      throw this.createNotFoundException(`no document with uuid ${uuid} was found!`, correlationId);
    }

    // get all answers for the requested report and assign them to the property answers
    const answersArr: ReportAnswerDto[] = await this.getAllAnswers( correlationId );
    (obj as ReportDto).answers = answersArr.filter( ( a ) => a.reportUuid===uuid );

    // tell typescript obj is BasedUuid so the property uuid exist, so we can add it back.
    const baseObj = obj as unknown as BaseDocumentDto;
    baseObj.uuid = uuid;

    // sanitize the answers
    const returnObj = this.sanitizeAnswersAndNotes( baseObj as unknown as ReportDto, userDto ) as unknown as T;

    return StaticLogger.endMethod<Partial<T>>('getOneMethod', returnObj as unknown as T, correlationId);
  }

  protected override async upsetMethod<T>(
    uuid: string,
    obj: Partial<T>,
    userDto: ApiUserDto,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    onlyIfNotExisting = false,
  ): Promise<Partial<T>> {
    StaticLogger.startMethod('upsetMethod', correlationId);
    // clear the answers array, since they are saved in an own document
    const reportDto = (obj as unknown as ReportDto);
    // reassign the report's answers and sanitize them later
    const answersArr: ReportAnswerDto[] = await this.getAllAnswers( correlationId );
    reportDto.answers = answersArr.filter( (a) => a.reportUuid === uuid );

    // detect the report's submission (state = 4)
    if ( reportDto.state && reportDto.state === 4 ) {
      // check if a negative answer was found and set hasMarkD to true
      if ( await this.hasReportNegativeMarks( reportDto, correlationId ) ) {
        reportDto.hasMarkD = true;
      } else {
        // set hasMarkD to false and state to 5, if no question was answered negatively. No physical signatures required.
        reportDto.hasMarkD = false;
        reportDto.state = 5;
      }
    }

    // perform update
    await this.baseRedisService.hSet<Partial<ReportDto>>(this.getDocumentKey(), uuid, { ...reportDto, answers: [] }, correlationId, onlyIfNotExisting);

    // send mail to bb
    if ( reportDto.state === 4 || reportDto.state === 5 && !reportDto.hasMarkD ) {
      await this.sendMailToBb( reportDto, correlationId );
    }

    // sanitize answers
    const ret = this.sanitizeAnswersAndNotes( reportDto, userDto );

    // tell typescript ret is BasedUuid so the property uuid exist, so we can add it back.
    ret.uuid = uuid;
    return StaticLogger.endMethod<T>('upsetMethod', ret as unknown as T, correlationId);
  }

  private async createReportAndAnswers( reportDto: ReportDto, reportTypeDetailView: ReportTypeDetailViewDto[], correlationId: number | undefined ) {
    StaticLogger.startMethod('createReportAndAnswers', correlationId);
    // copy the report to avoid side effects
    reportDto = ObjectTools.cloneDeep<ReportDto>( reportDto );

    // get ab and bb user-roles and check if they exist. Otherwise, throw an error
    if ( Object.keys( await this.getUserRolesInfo( reportDto.abUserEmail, correlationId ) ).length < 1 ) {
      throw this.createNotFoundException(`report, user with email ${reportDto.abUserEmail} not found!`, correlationId);
    }
    if ( Object.keys( await this.getUserRolesInfo( reportDto.bbUserEmail, correlationId ) ).length < 1 ) {
      throw this.createNotFoundException(`report, user with email ${reportDto.bbUserEmail} not found!`, correlationId);
    }

    // check if the reportType exists
    if ( !reportTypeDetailView.find( (rtd) => rtd.reportTypeUuid === reportDto.reportTypeUuid ) ) {
      throw this.createNotFoundException(`report, reportType with uuid ${reportDto.bbUserEmail} not found!`, correlationId);
    }

    // save the report
    await this.baseRedisService.hSet<Partial<ReportDto>>( this.getDocumentKey(), reportDto.uuid, reportDto, correlationId );

    // to create the answers for the new report, find all details (with questions) of this reportType and filter the result to get only those blocks, that are shown on the screen.
    const questions = reportTypeDetailView.filter( (rtd) => rtd.reportTypeUuid === reportDto.reportTypeUuid && rtd.reportBlockOnScreen );
    const createdAnswers: ReportAnswerDto[] = [];
    const answerKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportAnswer}`;

    for ( const question of questions ){
      const newAnswer: ReportAnswerDto = {
        created: new Date(),
        abExplanation: undefined,
        abMark: undefined,
        history: undefined,
        isDeleted: false,
        pbExplanation: undefined,
        pbMark: undefined,
        uuid: v4(),
        reportUuid: reportDto.uuid,
        questionUuid: question.questionUuid,
        isAbAnswerComplete: false,
        isPbAnswerComplete: false
      };
      await this.baseRedisService.hSet<Partial<ReportAnswerDto>>( answerKey, newAnswer.uuid, newAnswer, correlationId );
      createdAnswers.push( newAnswer );
    }

    // assign createdAnswers to the created reports answers array
    reportDto.answers = createdAnswers;
    return StaticLogger.endMethod<Partial<ReportDto>>('createReportAndAnswers', reportDto, correlationId);

  }

  private async createReportsForApprenticeshipMethod(
    userDto: ApiUserDto,
    createDto: ReportsForApprenticeshipCreateDto,
    correlationId: number | undefined
  ): Promise<Partial<ReportDto>[]> {
    // hint: abUserId is the eMail. This property name was not changed to speed up the migration
    const apprenticeUserRoleInfo: RoleHashDto = await this.getUserRolesInfo( createDto.abUserId, correlationId );

    // calculate the apprenticeship duration
    const abRoleFrom = new Date(apprenticeUserRoleInfo['ab'].from);
    const abRoleUntil = new Date(apprenticeUserRoleInfo['ab'].until);
    const durationOfApprenticeship = abRoleUntil.getFullYear() - abRoleFrom.getFullYear();
    const createdReportArr: ReportDto[] = [];
    const reportTypeDetailView = await this.getReportTypeDetailViewMethod( correlationId );

    // create the 'Probezeit Zwischenbericht' report
    const createTrialPeriodReportDto = {
      uuid: v4(),
      dueDateReport: new Date(`${abRoleFrom.getFullYear()}-09-15`),
      period: `01.08.${abRoleFrom.getFullYear()}-15.09.${abRoleFrom.getFullYear()}`,
      sentReminders: 0,
      reportTypeUuid: '2',
      bbUserEmail: userDto.eMail,
      abUserEmail: createDto.abUserId,
      sortOrder: 0,
      state: -1,
      // created: new Date(),
    };

    createdReportArr.push(
      await this.createReportAndAnswers( createTrialPeriodReportDto as ReportDto, reportTypeDetailView as ReportTypeDetailViewDto[], correlationId ) as ReportDto
    );

    // create the 'Probezeit Abschlussbericht' report
    const createFinalTrialPeriodReportDto = {
      ...createTrialPeriodReportDto,
      uuid: v4(),
      dueDateReport: new Date(`${abRoleFrom.getFullYear()}-10-31`),
      reportTypeUuid: '1',
      period: `01.08.${abRoleFrom.getFullYear()}-31.10.${abRoleFrom.getFullYear()}`,
      // created: new Date(),
      sortOrder: 1,
    };

    createdReportArr.push(
      await this.createReportAndAnswers( createFinalTrialPeriodReportDto as ReportDto, reportTypeDetailView as ReportTypeDetailViewDto[], correlationId ) as ReportDto
    );

    // create a report ('Ausbildungsbericht') for each semester
    const semester = durationOfApprenticeship * 2;
    const periodStart = new Date(`${abRoleFrom.getFullYear()}-08-01`);
    const periodEnd = new Date(`${abRoleFrom.getFullYear()}-12-31`);
    const dueDateReport = new Date(`${abRoleFrom.getFullYear() + 1}-01-15`);

    for( let i = 0; i < semester; i++ ) {
      const report = {
        uuid: v4(),
        dueDateReport: dueDateReport,
        period: `${periodStart.toLocaleDateString()}-${periodEnd.toLocaleDateString()}`,
        sentReminders: 0,
        reportTypeUuid: '3',
        bbUserEmail: userDto.eMail,
        abUserEmail: createDto.abUserId,
        state: -1,
        // created: new Date(),
        sortOrder: (i + 2),
      };

      // save report
      createdReportArr.push(
        await this.createReportAndAnswers( report as ReportDto, reportTypeDetailView as ReportTypeDetailViewDto[], correlationId ) as ReportDto
      );

      // if 'i' is even, then it's the second semester of a year
      // Because of that, the period for the next report jumps to the next year
      if ( i % 2 === 0 ) {
        // set the dueDateReport to july
        dueDateReport.setMonth(6);

        // set the first of january as the start date of the year
        periodStart.setDate(1);
        periodStart.setMonth(0);
        periodStart.setFullYear(periodStart.getFullYear() + 1);

        // set the end of the period to 31. 7.
        periodEnd.setDate(31);
        periodEnd.setMonth(6);
        periodEnd.setFullYear(periodEnd.getFullYear() + 1);
      }
      // if 'i' is odd, then it's the first semester of a year
      // the next period stays in the same year
      else {
        // set the dueDateReport to january in the next year
        dueDateReport.setMonth(0);
        dueDateReport.setFullYear(dueDateReport.getFullYear() + 1);

        // set the start of the period to the first of august of the same year
        periodStart.setDate(1);
        periodStart.setMonth(7);

        // set the end of the period to the last day of december
        periodEnd.setDate(31);
        periodEnd.setMonth(11);
      }

    }

    return createdReportArr;
  }

  private async emitWebsocketSyncEvent(websocketSyncDto: WebSocketSyncDto, wsId: string): Promise<void> {
    this.socketService.socket.emit(websocketSyncDto.eventName, {
      websocketSyncDto: websocketSyncDto,
      wsId: wsId,
    });
  }

  private async getAllAnswers( correlationId: number | undefined ): Promise<ReportAnswerDto[]> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const answersMap = await this.baseRedisService.hGetAll<any, Partial<ReportAnswerDto>[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportAnswer}`, correlationId);
    const answersArr: ReportAnswerDto[] = [];

    for ( const key in answersMap ) {
      answersArr.push( {...answersMap[key], uuid: key} );
    }

    return answersArr;
  }

  private async getReportTypeDetailViewMethod( correlationId: number | undefined ): Promise<Partial<ReportTypeDetailViewDto>[]> {
    StaticLogger.startMethod('getReportTypeDetailViewMethod', correlationId);

    // get all reportType
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let reportTypeArr = await this.baseRedisService.hGetAll<any, ReportTypeDto>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportType}`, correlationId);
    reportTypeArr = Object.values(reportTypeArr);

    // get all reportTypeReportBlock
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let reportTypeReportBlockArr: ReportTypeReportBlockDto[] = await this.baseRedisService.hGetAll<any, ReportTypeReportBlockDto[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportTypeReportBlock}`, correlationId);
    reportTypeReportBlockArr = Object.values(reportTypeReportBlockArr);

    // get all reportBlock
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let reportBlockArr = await this.baseRedisService.hGetAll<any, ReportBlockDto[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportBlock}`, correlationId);
    reportBlockArr = Object.values(reportBlockArr);

    // get all reportBlockQuestion
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let reportBlockQuestionArr = await this.baseRedisService.hGetAll<any, ReportBlockQuestionDto[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.reportBlockQuestion}`, correlationId);
    reportBlockQuestionArr = Object.values(reportBlockQuestionArr);

    // get all question
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let questionArr = await this.baseRedisService.hGetAll<any, QuestionDto[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.question}`, correlationId);
    questionArr = Object.values(questionArr);

    // get all contentType
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let contentTypeArr = await this.baseRedisService.hGetAll<any, ContentTypeDto[]>(`${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.contentType}`, correlationId);
    contentTypeArr = Object.values(contentTypeArr);

    // hint: The full query is split in six joins for each "table/document", because it was not possible to merge all of them into one query. Therefore, the property that is used to perform the next join
    // is added to the partial view result and gets deleted while performing the next join.

    // join reportType and reportTypeReportBlock
    const partialView1 = join(
      reportTypeArr,
      reportTypeReportBlockArr,
      (rt: ReportTypeDto) => rt.uuid,
      (rtrb: ReportTypeReportBlockDto) => rtrb.reportTypeUuid,
      (rt, rtrb) => {
        return {
          reportTypeUuid: rt.uuid,
          reportTypeName: rt.type,
          reportTypeVersion: rt.version,
          reportBlockSortOrder: rtrb.sortOrder,
          reportBlockUuid: rtrb.reportBlockUuid,
        };
      },
    );

    // join partialView1 result and reportBlock
    const partialView2 = leftJoin(
      partialView1,
      reportBlockArr,
      (left) => left.reportBlockUuid,
      (right: ReportBlockDto) => right.uuid,
      (left, right) => {
        return {
          ...left,
          reportBlockTitle: right ? right.title:undefined,
          reportBlockOnScreen: right ? right.onScreen:undefined,
          reportBlockOnPdf: right ? right.onPdf:undefined,
          reportBlockReadonly: right ? right.readonly:undefined,
          reportBlockPbReadWrite: right ? right.pbReadWrite:undefined,
          reportBlockAbReadWrite: right ? right.abReadWrite:undefined,
          // is only used to make the next join with reportBlock. It will be deleted there.
          reportBlockUuid: right ? right.uuid:undefined,
        };
      }
    );

    // join partialView2 result and reportBlockQuestion
    const partialView3 = leftJoin(
      partialView2,
      reportBlockQuestionArr,
      (left) => left.reportBlockUuid,
      (right: ReportBlockQuestionDto) => right.reportBlockUuid,
      (left, right) => {
        const partial = {
          ...left,
          questionSortOrder: right ? right.sortOrder : undefined,
          // is only used to make the next join with reportBlock. It will be deleted there.
          reportBlockQuestionUuid: right ? right.questionUuid : undefined,
        };
        // delete the no longer used property that was used to make the join
        delete partial.reportBlockUuid;
        return partial;
      }
    );

    // join partialView3 result and question
    const partialView4 = leftJoin(
      partialView3,
      questionArr,
      (left) => left.reportBlockQuestionUuid,
      (right: QuestionDto) => right.uuid,
      (left, right) => {
        const partial = {
          ...left,
          questionUuid: right ? right.uuid : undefined,
          questionTitle: right ? right.title : undefined,
          questionDescription: right ? right.description : undefined,
          helpText: right ? right.helpText : undefined,
          // is only used to make the next join with reportBlock. It will be deleted there.
          contentTypeUuid: right ? right.contentTypeUuid : undefined,
        };
        // delete the no longer used property that was used to make the join
        delete partial.reportBlockQuestionUuid;
        return partial;
      }
    );

    // join partialView4 result and contentType
    const reportTypeDetailView = leftJoin(
      partialView4,
      contentTypeArr,
      (left) => left.contentTypeUuid,
      (right: ContentTypeDto) => right.uuid,
      (left, right) => {
        const partial = {
          ...left,
          contentTypeVersion: right ? right.version : undefined,
          contentTypeKey: right ? right.key : undefined,
        };
        // delete the no longer used property that was used to make the join
        delete partial.contentTypeUuid;
        return partial;
      }
    );

    // sort the view by reportBlockSortOrder and questionSortOrder. This is equal to 'ORDER BY report_block_sort_order, question_sort_order'
    reportTypeDetailView.sort( (a, b) => (a.reportBlockSortOrder - b.reportBlockSortOrder || a.questionSortOrder - b.questionSortOrder) );

    return StaticLogger.endMethod<Partial<ReportTypeDetailViewDto>[]>('getReportTypeDetailViewMethod', reportTypeDetailView, correlationId);
  }

  private async getUserRolesInfo( eMail: string, correlationId: number | undefined ): Promise<RoleHashDto> {
    const key = `${ ApiConstants.redisKeys.userRoleKey }:${ eMail }`;
    return await this.baseRedisService.hGetAll<RoleHashDto, string>( key, correlationId );
  }

  private async hasReportNegativeMarks( reportDto: ReportDto, correlationId: number | undefined ): Promise<boolean> {
    // reportTypeDetails for this reportType
    let reportTypeDetails = await this.getReportTypeDetailViewMethod( correlationId );
    reportTypeDetails = reportTypeDetails.filter( (rtd) => rtd.reportTypeUuid === reportDto.reportTypeUuid );

    let hasD = false;

    for ( const answer of reportDto.answers ) {
      if ( hasD ) {
        break;
      }
      // find the detail with the uuid of the question
      const reportTypeDetail = reportTypeDetails.find( (rtd) => rtd.questionUuid === answer.questionUuid );

      // if it is a question that can be answered by pbs, then the property pbMark is relevant (pb wins). If not, it is the property abMark (ab wins only if pbReadWrite=false)
      if ( reportTypeDetail.reportBlockPbReadWrite ) {
        hasD = answer.pbMark === 'D';
      } else {
        hasD = answer.abMark === 'D';
      }
    }

    return hasD;
  }

  private sanitizeAnswersAndNotes( reportDto: ReportDto, userDto: ApiUserDto ): ReportDto {

    if ( Object.keys(userDto.apiRoles).includes(ModuleConstants.instance.availableRoles.ab) ) {
      // check if the report has not been submitted
      if ( reportDto.state < 2 ) {
        // replace all pb-parts with empty strings
        reportDto.answers = reportDto.answers.map( (a) => {
            return {
              ...a,
              pbMark: '',
              pbExplanation: '',
            } as ReportAnswerDto;
          },
        );

      }
    }
    else if ( Object.keys(userDto.apiRoles).includes(ModuleConstants.instance.availableRoles.pb) ) {
      // check if the apprentice has not submitted his answers
      if ( reportDto.state < 1 ) {
        // replace all ab-parts with empty strings
        reportDto.answers = reportDto.answers.map( a => {
            return {
              ...a,
              abMark: '',
              abExplanation: '',
            } as ReportAnswerDto;
          },
        );
      }
    }

    return reportDto;
  }

  private async sendMailToBb( reportDto: ReportDto, correlationId: number ): Promise<void> {
    const receiver = reportDto.bbUserEmail;
    const subject = `Bericht von ${UserNameTool.getFullName( reportDto.abUserEmail )} besprochen`;
    const emailContent = `
      <h1>Hallo ${UserNameTool.getFirstName(reportDto.bbUserEmail)}</h1>
      <p>Es ist ein Bericht von ${UserNameTool.getFullName(reportDto.abUserEmail)} für die Periode ${reportDto.period} besprochen und freigegeben worden.</p>
      <p>Der Bericht enthält ${reportDto.hasMarkD ? 'eine negative Bewertung' : 'keine negative Bewertung'}.</p>
    `;

    await Email.sendMail( receiver, subject, emailContent, correlationId, '', '' );
  }

}
