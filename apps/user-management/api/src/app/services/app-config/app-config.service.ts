import { UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { ModuleConstants } from '@itag-berufsbildung/user-management/data';
import { Inject, Injectable } from '@nestjs/common';
import { ApiCallerService, ApiConstants, BaseAppConfigService, BaseRedisService, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import * as fs from 'fs';
import * as path from 'path';
import { Constants } from '../../../constants';
import { environment } from '../../../environment/environment';

@Injectable()
export class AppConfigService extends BaseAppConfigService {
  constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.apiCallerService) protected readonly apiCaller: ApiCallerService
  ) {
    super(baseRedisService, apiCaller, ModuleConstants.instance, Constants.instance, environment.production);
  }

  async initDatabase(correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('initDatabase', correlationId);

    // read the data depending on the environment
    const dataPathUserRole = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment, 'user-role.json');
    // check that the user role file exists
    if (fs.existsSync(dataPathUserRole)) {
      const userRoleHashDto: UserRoleHashDto = JSON.parse(fs.readFileSync(dataPathUserRole, { encoding: 'utf-8' }));
      await this.baseRedisService.registerUserRoleForModule(this.adminAppConfiguration.moduleName, userRoleHashDto, correlationId);
    } else {
      StaticLogger.warn(`file ${dataPathUserRole} not found`, correlationId);
    }

    StaticLogger.endMethod('initDatabase', undefined, correlationId);
  }
}
