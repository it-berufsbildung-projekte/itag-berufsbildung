import { Inject } from '@nestjs/common';

import { ApiConstants, BaseAppConfigService, BaseRedisService, GenericAppController } from '@itag-berufsbildung/shared/data-api';

export class GenericController extends GenericAppController {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService
  ) {
    super(appConfigService, baseRedisService);
  }
}
