import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';
import { ModuleConstants } from '@itag-berufsbildung/user-management/data';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    icon: 'accessibility',
    level: 0,
    right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    path: ModuleConstants.instance.routing.base,
    text: 'user mgmt',
    title: 'Das Module User Management für die Verwaltung der Benutzer',
    children: [
      {
        icon: 'lock',
        level: 1,
        text: 'Rollen',
        title: 'Verwaltung der Rollen!',
        right: ModuleConstants.instance.availableRights.menu.canShowSubMenuRoles,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.roles}`,
      },
    ],
  };
}
