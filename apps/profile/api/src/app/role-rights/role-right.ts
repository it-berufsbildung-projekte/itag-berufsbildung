import { ModuleConstants } from '@itag-berufsbildung/profile/data';
import { ApiConstants } from '@itag-berufsbildung/shared/data-api';

export class RoleRight {
  static readonly documents = {
    [ModuleConstants.instance.availableRoles.api]: [ApiConstants.rights.module.canCreate],
    [ModuleConstants.instance.availableRoles.admin]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canDeleteOthersCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ApiConstants.rights.module.canCreate,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canWriteUserRoles,
      ApiConstants.rights.generic.canDeleteUserRoles,
      ApiConstants.rights.generic.canDeleteUser,
      ModuleConstants.instance.availableRights.menu.canShowMainMenu,
      ModuleConstants.instance.availableRights.menu.canShowSubMenuInformation,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,
      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
    ],
    [ModuleConstants.instance.availableRoles.user]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ModuleConstants.instance.availableRights.menu.canShowMainMenu,
      ModuleConstants.instance.availableRights.menu.canShowSubMenuInformation,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
    ],
    [ModuleConstants.instance.availableRoles.readOnly]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
  };
}
