import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';
import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    icon: 'clock',
    level: 0,
    right: `${ModuleConstants.instance.availableRights.menu.canShowMainMenuModule}`,
    path: `${ModuleConstants.instance.routing.base}`,
    text: 'time-counter',
    title: 'Das Modul TimeCounter!',
    children: [
      {
        icon: 'calendar',
        level: 1,
        text: 'daily',
        title: 'Tagesansicht und erfassen!',
        right: ModuleConstants.instance.availableRights.menu.canShowSubMenuDaily,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.daily}`,
      },
      {
        separator: true,
        right: ModuleConstants.instance.availableRights.menu.canShowSubMenuDaily,
      },
      {
        icon: 'printer',
        level: 1,
        text: 'reporting',
        title: 'reports!',
        right: ModuleConstants.instance.availableRights.menu.canShowSubMenuReporting,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.reporting}`,
      },
      {
        separator: true,
        right: ModuleConstants.instance.availableRights.menu.canShowSubMenuReporting,
      },
      {
        icon: 'gear',
        level: 1,
        right: `${ModuleConstants.instance.availableRights.menu.canShowSubMenuAdmin}`,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.admin}`,
        text: 'admin',
        title: 'Admin funktionen!',
        children: [
          {
            icon: 'check-circle',
            level: 2,
            text: 'tasks',
            title: 'manage tasks!',
            right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminTasks,
            path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.admin}/${ModuleConstants.instance.routing.sub.tasks}`,
          },
          {
            icon: 'list-ordered',
            level: 2,
            text: 'interflex',
            title: 'manage interflex!',
            right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminInterFlex,
            path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.admin}/${ModuleConstants.instance.routing.sub.interFlex}`,
          },
          {
            icon: 'user',
            level: 2,
            text: 'users',
            title: 'manage users!',
            right: ModuleConstants.instance.availableRights.menu.canShowSubMenuAdminUsers,
            path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.admin}/${ModuleConstants.instance.routing.sub.users}`,
          },
        ],
      },
    ],
  };
  readonly redisKeys = {
    interFlex: 'inter-flex',
    tasks: 'tasks',
    users: 'users',
    workData: 'work-data',
  };
}
