# import-data

Wir unterscheiden drei Verzeichnisse. Pro Verzeichnis gibt es jeweils Dateien welche mit sample- beginnen.
- dev für die lokale Entwicklung
- test um tests auszuführen. hier sollten alle Daten auch eingecheckt werden
- pro für die Produktion. Meistens leer

im dev Ordner gibt es auch Beispiele, welche mit sample beginnen.

Diese sample Dateien werden im git eingecheckt.

Die Anwendung selber versucht aber Werte aus den Dateien ohne sample- zu lesen.

Beispiel:
- sample-interFlex-data.json
  - gesucht und gelesen wird interFlex-data.json

etc.

Die Idee dahinter ist, dass wir mit scharfen Daten arbeiten können aber genügend Beispieldaten liefern können.

