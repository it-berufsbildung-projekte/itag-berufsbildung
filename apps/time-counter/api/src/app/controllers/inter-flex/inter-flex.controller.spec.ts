import { Test, TestingModule } from '@nestjs/testing';
import { InterFlexController } from './inter-flex.controller';

describe('InterFlexController', () => {
  let controller: InterFlexController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InterFlexController],
    }).compile();

    controller = module.get<InterFlexController>(InterFlexController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
