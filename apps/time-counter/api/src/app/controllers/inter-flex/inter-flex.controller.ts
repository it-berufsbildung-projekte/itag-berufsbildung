/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { Body, Controller, Inject, NotImplementedException, Param, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController, GetCorrelationId, GetUser, GetWsId,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { InterFlexDto, ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { Request } from 'express';

@ApiTags(`${ModuleConstants.instance.controllerTags.interFlex}`)
@Controller(`${ModuleConstants.instance.controllerTags.interFlex}`)
export class InterFlexController extends GenericDocumentController<InterFlexDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.interFlex.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.interFlex.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.interFlex.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.interFlex.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.interFlex.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.interFlex.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.interFlex.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.interFlex, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }

  // turn it off
  override async delete(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number | undefined,
    @Param('uuid') uuid: string,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<InterFlexDto>> {   throw new NotImplementedException('please use another method!'); }
  override  async getByUuid(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Req() request: Request,
  ): Promise<Partial<InterFlexDto>> {
    throw new NotImplementedException('please use another method!');
  }
  override async patch(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() patchDto: Partial<InterFlexDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<InterFlexDto>> { throw new NotImplementedException('please use another method!'); }
  override async post(
    @Body() postDto: InterFlexDto,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<InterFlexDto>> { throw new NotImplementedException('please use another method!'); }
  override async put(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() putDto: Partial<InterFlexDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<InterFlexDto>> { throw new NotImplementedException('please use another method!'); }
}
