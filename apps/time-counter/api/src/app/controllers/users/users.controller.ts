/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiUserDto } from '@itag-berufsbildung/shared/util';
import { Body, Controller, Inject, NotImplementedException, Param, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController, GetCorrelationId, GetUser, GetWsId,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants, UsersDto } from '@itag-berufsbildung/time-counter/data';
import { Request } from 'express';

@ApiTags(`${ModuleConstants.instance.documents.users}`)
@Controller(`${ModuleConstants.instance.documents.users}`)
export class UsersController extends GenericDocumentController<UsersDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.users.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.users.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.users.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.users.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.users.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.users.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.users.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.users, genericDocumentControllerRights);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }

  // turn it off
  override async delete(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number | undefined,
    @Param('uuid') uuid: string,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<UsersDto>> {   throw new NotImplementedException('please use another method!'); }
  override  async getByUuid(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Req() request: Request,
  ): Promise<Partial<UsersDto>> {
    throw new NotImplementedException('please use another method!');
  }
  override async patch(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() patchDto: Partial<UsersDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<UsersDto>> { throw new NotImplementedException('please use another method!'); }
  override async post(
    @Body() postDto: UsersDto,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<UsersDto>> { throw new NotImplementedException('please use another method!'); }
  override async put(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() putDto: Partial<UsersDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<UsersDto>> { throw new NotImplementedException('please use another method!'); }

}
