/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiUserDto, baseDocumentExample, baseDocumentExampleArr, DateAndTimeTools, IQueryParams } from '@itag-berufsbildung/shared/util';
import { Body, Controller, Get, Inject, NotImplementedException, Param, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth, ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import {
  ApiConstants, ApiSocketService,
  BaseAppConfigService,
  BaseRedisService, GenericAppController,
  GenericDocumentController, GetCorrelationId, GetUser, GetWsId,
  IGenericDocumentControllerRights, StaticLogger,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';

import { WorkDataDto } from '@itag-berufsbildung/time-counter/data';
import { Request } from 'express';

@ApiTags( `${ ModuleConstants.instance.documents.workData }` )
@Controller( `${ ModuleConstants.instance.documents.workData }` )
export class WorkDataController extends GenericDocumentController<WorkDataDto> {

  constructor(
    @Inject( ApiConstants.services.appConfigService ) protected readonly appConfigService: BaseAppConfigService,
    @Inject( ApiConstants.services.dbService ) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.workData.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.workData.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.workData.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.workData.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.workData.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.workData.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.workData.canSeeHistory
    };

    super(
      baseRedisService,
      socketService,
      ModuleConstants.instance.documents.workData,
      genericDocumentControllerRights,
    );
  }

  // we turn off all not interesting methods
  override async delete(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number | undefined,
    @Param('uuid') uuid: string,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<WorkDataDto>> {   throw new NotImplementedException('please use another method!'); }
  override async getAll(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
  ): Promise<Partial<WorkDataDto>[]> { throw new NotImplementedException('please use another method!'); }
  override  async getByUuid(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Req() request: Request,
  ): Promise<Partial<WorkDataDto>> {
    throw new NotImplementedException('please use another method!');
  }

  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiOkResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: 'Array of WorkDataDto',
      items: {
        type: 'WorkDataDto',
      },
      example: baseDocumentExampleArr,
    },
    description: 'return an array of WorkDataDto for the singed-in user for a give date',
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiOperation({
    description: '',
    summary: 'Get all fields of the document',
  })
  @Get('/:isodate')
  async getForOneDay(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @Param('isodate') isoDate: string
  ): Promise<Partial<WorkDataDto>[]> {
    StaticLogger.startMethod('getForOneDay', correlationId);

    // documents:work-data:2022:April:13:2022-04-01:dirk.decher@ag.ch
    const date = DateAndTimeTools.getDateFromIsoDate(isoDate);
    const year = date.getFullYear();
    const monthName = DateAndTimeTools.getMonthNameForDate(date);
    const week = DateAndTimeTools.getWeekNumber(date);
    const key = `${this.getDocumentKey()}:${year}:${monthName}:${week}:${isoDate}:${userDto.eMail}`;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objWithKeys =  await this.baseRedisService.hGetAll<any, Partial<WorkDataDto>>(key, correlationId);
    const arr: Partial<WorkDataDto[]> = [];
    for (const key in objWithKeys) {
      arr.push({ ...objWithKeys[key], uuid: key });
    }
    return StaticLogger.endMethod<Partial<WorkDataDto[]>>('getForOneDay', arr, correlationId);
  }

  override async patch(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() patchDto: Partial<WorkDataDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<WorkDataDto>> { throw new NotImplementedException('please use another method!'); }

  override async post(
    @Body() postDto: WorkDataDto,
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<WorkDataDto>> { throw new NotImplementedException('please use another method!'); }


  @ApiInternalServerErrorResponse(GenericAppController.internalServerErrorResponseObject)
  @ApiCreatedResponse({
    ...GenericAppController.okResponseOption,
    schema: {
      description: 'save the data of one day',
      type: 'Array of WorkDataDtos',
      example: baseDocumentExample,
    },
    description: 'successfully saved',
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiUnauthorizedResponse(GenericAppController.unauthorizedResponseOption)
  @ApiForbiddenResponse(GenericAppController.forbiddenResponseOption)
  @ApiBody({
    schema: {
      description: 'Partial<WorkDataDto>',
      type: 'Partial<WorkDataDto>',
      example: baseDocumentExample,
    },
  })
  @ApiOperation({
    description: '',
    summary: 'Create or Update the information for a date',
  })
  @Post('/:isodate')
  async postOneDay(
    @Body() postDtoArr: WorkDataDto[],
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Req() request: Request,
    @GetWsId() wsId: string,
    @Param('isodate') isoDate: string
  ): Promise<WorkDataDto[]> {
    StaticLogger.startMethod('post', correlationId);
    const queryParams = request.query as IQueryParams;
    // check rights
    if (!userDto.hasRight(this.rights.canCreate)) {
      throw this.createMethodNotAllowedException(`user with email ${userDto.eMail} has no rights to access this function!`, correlationId);
    }

    // documents:work-data:2022:April:13:2022-04-01:dirk.decher@ag.ch
    const date = DateAndTimeTools.getDateFromIsoDate(isoDate);
    const year = date.getFullYear();
    const monthName = DateAndTimeTools.getMonthNameForDate(date);
    const week = DateAndTimeTools.getWeekNumber(date);
    const key = `${this.getDocumentKey()}:${year}:${monthName}:${week}:${isoDate}:${userDto.eMail}`;

    // frist delete the existing information for the day
    const deleted = await this.baseRedisService.del(key, correlationId);
    // now add every single entry
    for( const item of postDtoArr) {
      StaticLogger.verbose(`adding the hashKey: ${item.uuid} to the entry ${key}`, correlationId);
      const x = await this.baseRedisService.hSet<WorkDataDto>(key, item.uuid, item,correlationId);
    }

    //  return finally the single element in this case
    return StaticLogger.endMethod<WorkDataDto[]>('post', postDtoArr, correlationId);
  }

  override async put(
    @GetUser() userDto: ApiUserDto,
    @GetCorrelationId() correlationId: number,
    @Param('uuid') uuid: string,
    @Body() putDto: Partial<WorkDataDto>,
    @Req() request: Request,
    @GetWsId() wsId: string,
  ): Promise<Partial<WorkDataDto>> { throw new NotImplementedException('please use another method!'); }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }



}
