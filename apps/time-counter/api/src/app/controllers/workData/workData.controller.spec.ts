import { Test, TestingModule } from '@nestjs/testing';
import { WorkDataController } from './workDataController';

describe('work-dataController', () => {
  let controller: WorkDataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkDataController],
    }).compile();

    controller = module.get<WorkDataController>(WorkDataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
