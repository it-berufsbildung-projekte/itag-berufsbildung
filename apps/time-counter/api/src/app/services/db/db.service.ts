import { Injectable } from '@nestjs/common';

import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';

import { BaseRedisService } from '@itag-berufsbildung/shared/data-api';
import { RoleRight } from '../../role-rights/role-right';

@Injectable()
export class DbService extends BaseRedisService {
  constructor() {
    super(
      {
        canResetCacheForOthers: ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      },
      RoleRight.documents
    );
  }
}
