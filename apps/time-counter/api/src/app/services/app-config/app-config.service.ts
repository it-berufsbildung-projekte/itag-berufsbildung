import { Inject, Injectable } from '@nestjs/common';
import { ModuleConstants } from '@itag-berufsbildung/time-counter/data';
import { DateAndTimeTools, UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { ApiCallerService, ApiConstants, BaseAppConfigService, BaseRedisService, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import { Constants } from '../../../constants';
import * as fs from 'fs';
import * as path from 'path';
import { environment } from '../../../environment/environment';

enum EnumFileType {
  method,
  interFlex,
  user,
  workData,
}

interface IInterFlex {
  id?: number;
  guid?: string;
  uuid: string;
  name: string;
  sortOrder: number;
}
interface IMethod {
  id?: number;
  guid?: string;
  uuid: string;
  name: string;
  sortOrder: number;
  interFlexGuid?: string;
  interFlexUuid: string;
}
interface IUser {
  guid?: string;
  uuid: string;
  username: string;
  displayName: string;
  eMail: string;
}
interface IWorkData {
  guid?: string;
  uuid: string;
  methodGuid?: string;
  taskUuid: string;
  userGuid?: string;
  eMail: string;
  dateKey: string;
  fromTimeKey: string;
  durationInMin: number;
  text: string;
}

type ImportData = IInterFlex | IMethod | IUser | IWorkData;

@Injectable()
export class AppConfigService extends BaseAppConfigService {
  constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.apiCallerService) protected readonly apiCaller: ApiCallerService
  ) {
    super(baseRedisService, apiCaller, ModuleConstants.instance, Constants.instance, environment.production);
  }

  async initDatabase(correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('initDatabase', correlationId);

    // read the data depending on the environment
    const dataPathUserRole = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment, 'user-role.json');
    // check that the user role file exists
    if (fs.existsSync(dataPathUserRole)) {
      const userRoleHashDto: UserRoleHashDto = JSON.parse(fs.readFileSync(dataPathUserRole, { encoding: 'utf-8' }));
      await this.baseRedisService.registerUserRoleForModule(this.adminAppConfiguration.moduleName, userRoleHashDto, correlationId);
    } else {
      StaticLogger.warn(`file ${dataPathUserRole} not found`, correlationId);
    }

    // read the data depending on the environment
    switch (this.adminAppConfiguration.environment) {
      case 'test':
        break;
      case 'prod':
        break;
      default: {
        // dev
        // import daten
        const dataPath = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment);

        await this.importData(path.join(dataPath, 'interFlex-data.json'), EnumFileType.interFlex, correlationId);
        await this.importData(path.join(dataPath, 'method-data.json'), EnumFileType.method, correlationId);
        const users: IUser[] = (await this.importData(path.join(dataPath, 'user-data.json'), EnumFileType.user, correlationId)) as IUser[];

        // do it for every .json entry in the folder work-data
        const workDataPath = path.join(dataPath, 'work-data');
        const files = fs.readdirSync(workDataPath, { encoding: 'utf-8', withFileTypes: true });
        for (const file of files.filter((f) => path.extname(f.name) === '.json' && !f.name.startsWith('sample-'))) {
          await this.importData(path.join(workDataPath, file.name), EnumFileType.workData, correlationId, users, path.basename(file.name, '.json'));
        }
      }
    }

    StaticLogger.endMethod('initDatabase', undefined, correlationId);
  }

  private async importData(filePath: string, fileType: EnumFileType, correlationId: number | undefined, users: IUser[] = undefined, creator?: string): Promise<ImportData[]> {
    StaticLogger.startMethod('importData', correlationId);

    if (!fs.existsSync(filePath)) {
      StaticLogger.warn(`File ${filePath} does not exists!`, correlationId);
    }
    const content = fs.readFileSync(filePath, { encoding: 'utf-8' });
    if (!content || content.length === 0) {
      const msg = `File ${filePath} is empty!`;
      StaticLogger.error(msg, correlationId);
      throw new Error(msg);
    }
    // try to convert it into an obj
    const data: ImportData[] = JSON.parse(content);
    let dbKey = '';
    switch (fileType) {
      case EnumFileType.interFlex:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.interFlex}`;
        break;
      case EnumFileType.method:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.tasks}`;
        break;
      case EnumFileType.user:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.users}`;
        break;
      case EnumFileType.workData:
        dbKey = `${ApiConstants.redisKeys.documents}:${Constants.instance.redisKeys.workData}`;
        break;
    }

    for (const item of data) {
      let createDbKey = dbKey;
      let field: string;
      switch (fileType) {
        case EnumFileType.interFlex: {
          const typedItem = item as IInterFlex;
          field = typedItem.guid;
          typedItem.uuid = field;
          delete typedItem.guid;
          delete typedItem.id;
          item.uuid = typedItem.uuid;
          break;
        }
        case EnumFileType.method: {
          const typedItem = item as IMethod;
          field = typedItem.guid;
          typedItem.interFlexUuid = typedItem.interFlexGuid;
          typedItem.uuid = field;
          delete typedItem.interFlexGuid;
          delete typedItem.guid;
          delete typedItem.id;
          item.uuid = typedItem.uuid;
          break;
        }
        case EnumFileType.user: {
          const typedItem = item as IUser;
          typedItem.uuid = typedItem.guid;
          delete typedItem.guid;
          field = typedItem.eMail;
          item.uuid = typedItem.uuid;
          break;
        }
        case EnumFileType.workData: {
          const typedItem = item as IWorkData;
          field = typedItem.guid;
          delete typedItem.guid;
          typedItem.taskUuid = typedItem.methodGuid;
          delete typedItem.methodGuid;
          item.uuid = typedItem.uuid;
          if (typedItem.userGuid) {
            typedItem.eMail = users.find((u) => u.uuid === typedItem.userGuid)?.eMail;
          }
          delete typedItem.userGuid;
          const date = DateAndTimeTools.getDateFromIsoDate(typedItem.dateKey, typedItem.fromTimeKey);
          const year = date.getFullYear();
          const month = DateAndTimeTools.getMonthNameForDate(date);
          const week = DateAndTimeTools.getWeekNumber(date);
          createDbKey = `${dbKey}:${year}:${month}:${week}:${typedItem.dateKey}:${creator}`;
          break;
        }
      }
      await this.baseRedisService.hSet(createDbKey, field, item, correlationId);
    }
    return data;
  }
}
