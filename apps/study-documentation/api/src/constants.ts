import { ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { BaseApiConstants } from '@itag-berufsbildung/shared/data-api';
import { Menu } from '@itag-berufsbildung/shared/util';

export class Constants extends BaseApiConstants {
  private static _instance: Constants;
  static get instance(): Constants {
    return this._instance || (this._instance = new this());
  }
  readonly menu: Menu = {
    icon: 'file-txt',
    level: 0,
    right: ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
    path: ModuleConstants.instance.routing.base,
    text: 'lern-doku',
    title: 'Das Module study-documentation',
    children: [
      {
        icon: 'preview',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowMenuPbOverview,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.pbEntries}`,
        text: 'PB Übersicht',
        title: 'Für PB zum evaluieren',
      },
      {
        icon: 'preview',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowMenuBbOverview,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.bbEntries}`,
        text: 'BB Übersicht',
        title: 'Für BB zum anschauen',
      },
      {
        icon: 'folder-open',
        level: 1,
        right: ModuleConstants.instance.availableRights.menu.canShowMenuAbOverview,
        path: `${ModuleConstants.instance.routing.base}/${ModuleConstants.instance.routing.sub.abEntries}`,
        text: 'Übersicht',
        title: 'Für AB zum anschauen und/oder bearbeiten',
      },
    ],
  };
}
