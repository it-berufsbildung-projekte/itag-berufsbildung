export class EntryAccess {
  static readonly list = {
    ['michel.rohrbach@ag.ch']: {
      pb: ['email1@ag.ch', 'michel.rohrbach@ag.ch', 'dummyEmailPb', 'mike.ditton@ag.ch'],
      bb: ['email1@ag.ch', 'dummyEmailBb1', 'dummyEmailBb2', 'mike.ditton@ag.ch'],
    },
    ['admin@it-berufsbildung.ch']: {
      pb: ['email1@ag.ch', 'michel.rohrbach@ag.ch', 'dummyEmailPb', 'mike.ditton@ag.ch'],
      bb: ['email1@ag.ch', 'dummyEmailBb1', 'dummyEmailBb2', 'mike.ditton@ag.ch'],
    },
    ['benjamin.yildirim@ag.ch']: {
      pb: ['email1@ag.ch', 'email3@ag.ch', 'dummyEmailPb', 'mike.ditton@ag.ch'],
      bb: ['dummyEmailBb1', 'dummyEmailBb2', 'mike.ditton@ag.ch'],
    },
  };
}
