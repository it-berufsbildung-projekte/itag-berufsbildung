import { UserRoleHashDto } from '@itag-berufsbildung/shared/util';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { Inject, Injectable } from '@nestjs/common';
import { ApiCallerService, ApiConstants, BaseAppConfigService, BaseRedisService, StaticLogger } from '@itag-berufsbildung/shared/data-api';
import * as fs from 'fs';
import * as path from 'path';
import { Constants } from '../../../constants';
import { environment } from '../../../environment/environment';
import { Entry as DevEntry } from '../../../import-data/dev/entry';
import { Entry as ProdEntry } from '../../../import-data/prod/entry';
import { Entry as TestEntry } from '../../../import-data/prod/entry';

// hint: Könnte später noch an einem anderen Ort benötigt werden, momentan nur hier.
export interface EntryHash {
  [email: string]: Partial<EntryDto>;
}

@Injectable()
export class AppConfigService extends BaseAppConfigService {
  constructor(
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.apiCallerService) protected readonly apiCaller: ApiCallerService
  ) {
    super(baseRedisService, apiCaller, ModuleConstants.instance, Constants.instance, environment.production);
  }

  async importEntries(objArr: EntryHash, correlationId: number | undefined): Promise<void> {
    for (const key in objArr) {
      await this.baseRedisService.hSet(`${ApiConstants.redisKeys.documents}:${ModuleConstants.instance.documents.entry}`, key, objArr[key], correlationId, true);
    }
  }

  async initDatabase(correlationId: number | undefined): Promise<void> {
    StaticLogger.startMethod('initDatabase', correlationId);

    // read the data depending on the environment
    const dataPathUserRole = path.join(__dirname, 'import-data', this.adminAppConfiguration.environment, 'user-role.json');
    // check that the user role file exists
    if (fs.existsSync(dataPathUserRole)) {
      const userRoleHashDto: UserRoleHashDto = JSON.parse(fs.readFileSync(dataPathUserRole, { encoding: 'utf-8' }));
      await this.baseRedisService.registerUserRoleForModule(this.adminAppConfiguration.moduleName, userRoleHashDto, correlationId);
    } else {
      StaticLogger.warn(`file ${dataPathUserRole} not found`, correlationId);
    }

    let entryHashDto: EntryHash;
    switch (this.adminAppConfiguration.environment) {
      case 'test':
        entryHashDto = TestEntry.documents as unknown as EntryHash;
        break;
      case 'prod':
        entryHashDto = ProdEntry.documents as unknown as EntryHash;
        break;
      default: // dev
        entryHashDto = DevEntry.documents as unknown as EntryHash;
    }
    await this.importEntries(entryHashDto, correlationId);

    StaticLogger.endMethod('initDatabase', undefined, correlationId);
  }
}
