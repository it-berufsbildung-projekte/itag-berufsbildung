import { ModuleConstants } from '@itag-berufsbildung/study-documentation/data';
import { ApiConstants } from '@itag-berufsbildung/shared/data-api';

export class RoleRight {
  static readonly documents = {
    [ModuleConstants.instance.availableRoles.api]: [ApiConstants.rights.module.canCreate],
    [ModuleConstants.instance.availableRoles.admin]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canDeleteOthersCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ApiConstants.rights.generic.canReadUsers,
      ApiConstants.rights.generic.canReadUserRoles,
      ApiConstants.rights.generic.canWriteUserRoles,
      ApiConstants.rights.generic.canDeleteUserRoles,
      ApiConstants.rights.generic.canDeleteUser,
      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.menu.canShowMenuBbOverview,
      ModuleConstants.instance.availableRights.menu.canShowMenuAbOverview,
      ModuleConstants.instance.availableRights.menu.canShowMenuPbOverview,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canResetCacheForOthers,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,
      ModuleConstants.instance.availableRights.api.entry.canRead,
      ModuleConstants.instance.availableRights.api.entry.canUpdate,
      ModuleConstants.instance.availableRights.api.entry.canSeeHistory,
      ModuleConstants.instance.availableRights.api.entry.canSetAcceptedAndRejectedState,
      ModuleConstants.instance.availableRights.api.entry.canRemove,
      ModuleConstants.instance.availableRights.api.entry.canSeeTagsText,
      ModuleConstants.instance.availableRights.api.entry.canSetSavedAndPublishedState,
      ModuleConstants.instance.availableRights.api.entry.pb,
      ModuleConstants.instance.availableRights.api.entry.ab,
      ModuleConstants.instance.availableRights.api.entry.bb,
      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
      ModuleConstants.instance.availableRights.ui.entry.canCreate,
      ModuleConstants.instance.availableRights.ui.entry.canEdit,
      ModuleConstants.instance.availableRights.ui.entry.canSeeFeedback,
      ModuleConstants.instance.availableRights.ui.entry.canView,
      ModuleConstants.instance.availableRights.ui.entry.pbCanView,
      ModuleConstants.instance.availableRights.ui.entry.pb,
      ModuleConstants.instance.availableRights.ui.entry.abCanView,
      ModuleConstants.instance.availableRights.ui.entry.abCanCreate,
      ModuleConstants.instance.availableRights.ui.entry.ab,
      ModuleConstants.instance.availableRights.ui.entry.bbCanView,
      ModuleConstants.instance.availableRights.ui.entry.bb,
    ],
    [ModuleConstants.instance.availableRoles.user]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canDeleteOwnCache,
      ApiConstants.rights.generic.canDeleteOthersCache,
      ApiConstants.rights.generic.canReadMenuInfo,
      ModuleConstants.instance.availableRights.menu.canShowMainMenuModule,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.entry.canRead,
      ModuleConstants.instance.availableRights.api.entry.canUpdate,
      ModuleConstants.instance.availableRights.api.entry.canSeeHistory,
      ModuleConstants.instance.availableRights.api.entry.canSeeTagsText,
      ModuleConstants.instance.availableRights.api.entry.canSetSavedAndPublishedState,
      ModuleConstants.instance.availableRights.api.entry.canSetAcceptedAndRejectedState,
      ModuleConstants.instance.availableRights.ui.information.canShowDatabase,
      ModuleConstants.instance.availableRights.ui.information.canShowAppSettings,
      ModuleConstants.instance.availableRights.api.generic.canResetOwnCache,
      ModuleConstants.instance.availableRights.api.generic.canShowAdminInfo,
      ModuleConstants.instance.availableRights.api.entry.canRead,
      ModuleConstants.instance.availableRights.api.entry.canUpdate,
    ],
    [ModuleConstants.instance.availableRoles.readOnly]: [
      ApiConstants.rights.generic.canReadAppInfo,
      ApiConstants.rights.generic.canReadUserInfo,
      ApiConstants.rights.generic.canReadMenuInfo,
    ],
    [ModuleConstants.instance.availableRoles.pb]: [
      ModuleConstants.instance.availableRights.api.entry.canRead,
      ModuleConstants.instance.availableRights.api.entry.canUpdate,
      ModuleConstants.instance.availableRights.api.entry.canSeeHistory,
      ModuleConstants.instance.availableRights.api.entry.canSetAcceptedAndRejectedState,
      ModuleConstants.instance.availableRights.ui.entry.pbCanView,
      ModuleConstants.instance.availableRights.api.entry.pb,
      ModuleConstants.instance.availableRights.menu.canShowMenuPbOverview,
      ModuleConstants.instance.availableRights.ui.entry.pb,
    ],
    [ModuleConstants.instance.availableRoles.ab]: [
      ModuleConstants.instance.availableRights.api.entry.canRead,
      ModuleConstants.instance.availableRights.api.entry.canRemove,
      ModuleConstants.instance.availableRights.api.entry.canUpdate,
      ModuleConstants.instance.availableRights.api.entry.canSeeTagsText,
      ModuleConstants.instance.availableRights.api.entry.canSetSavedAndPublishedState,
      ModuleConstants.instance.availableRights.ui.entry.canCreate,
      ModuleConstants.instance.availableRights.ui.entry.abCanView,
      ModuleConstants.instance.availableRights.ui.entry.abCanCreate,
      ModuleConstants.instance.availableRights.menu.canShowMenuAbOverview,
      ModuleConstants.instance.availableRights.ui.entry.canEdit,
      ModuleConstants.instance.availableRights.ui.entry.canSeeFeedback,
      ModuleConstants.instance.availableRights.ui.entry.canView,
      ModuleConstants.instance.availableRights.api.entry.ab,
      ModuleConstants.instance.availableRights.ui.entry.ab,
    ],
    [ModuleConstants.instance.availableRoles.bb]: [
      ModuleConstants.instance.availableRights.api.entry.bb,
      ModuleConstants.instance.availableRights.menu.canShowMenuBbOverview,
      ModuleConstants.instance.availableRights.ui.entry.bbCanView,
      ModuleConstants.instance.availableRights.ui.entry.bb,
    ],
  };
}
