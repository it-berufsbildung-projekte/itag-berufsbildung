import { HttpModule, HttpService } from '@nestjs/axios';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import {
  ApiCallerService,
  ApiConstants,
  ApiSocketService,
  ApiUserService,
  AppGateway,
  BaseAppConfigService,
  JwtStrategy,
  NestAppCreator,
} from '@itag-berufsbildung/shared/data-api';

import { AppConfigService } from './services/app-config/app-config.service';
import { GenericController } from './controllers/generic/generic.controller';
import { DbService } from './services/db/db.service';
import { EntryController } from './controllers/entry/entry.controller';

@Module({
  imports: [JwtModule.register({}), HttpModule],
  controllers: [GenericController, EntryController],
  providers: [
    JwtStrategy,
    AppGateway,
    ApiUserService,
    {
      provide: ApiConstants.services.dbService,
      useClass: DbService,
    },
    {
      provide: ApiConstants.services.apiCallerService,
      inject: [HttpService, ApiConstants.services.dbService],
      useFactory: (httpService: HttpService, dbService: DbService) => new ApiCallerService(httpService, dbService),
    },
    {
      provide: ApiConstants.services.appConfigService,
      inject: [ApiConstants.services.dbService, ApiConstants.services.apiCallerService],
      useFactory: (dbService: DbService, apiCallerService: ApiCallerService) => new AppConfigService(dbService, apiCallerService),
    },
    {
      provide: ApiConstants.services.socketService,
      inject: [ApiConstants.services.appConfigService],
      useFactory: (appConfigService: BaseAppConfigService) => new ApiSocketService(appConfigService),
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    return NestAppCreator.addConfigure(consumer, 0);
  }
}
