import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants, ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  CheckAccessMethodType,
  GenericDocumentController,
  IGenericDocumentControllerRights,
  StaticLogger,
  UpsetMethodType,
} from '@itag-berufsbildung/shared/data-api';
import { ApiUserDto, DateAndTimeTools, IBaseDocumentDtoUpdateHistory, IQueryParams, WeekdayEnum } from '@itag-berufsbildung/shared/util';
import { EntryDto, ModuleConstants } from '@itag-berufsbildung/study-documentation/data';

import { EntryAccess } from '../../entry-access/entry-access';

@ApiTags(`${ModuleConstants.instance.documents.entry}`)
@Controller(`${ModuleConstants.instance.documents.entry}`)
export class EntryController extends GenericDocumentController<EntryDto> {
  constructor(
    @Inject(ApiConstants.services.appConfigService) protected readonly appConfigService: BaseAppConfigService,
    @Inject(ApiConstants.services.dbService) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.entry.ab,
      canModifyDelete: ModuleConstants.instance.availableRights.api.entry.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.entry.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.entry.ab,
      canShowDeleted: ModuleConstants.instance.availableRights.api.entry.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.entry.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.entry.canSeeHistory,
    };

    super(baseRedisService, socketService, ModuleConstants.instance.documents.entry, genericDocumentControllerRights);
  }

  override checkAccess(
    oldObj: Partial<EntryDto>,
    userDto: ApiUserDto,
    methodName: CheckAccessMethodType,
    queryParams: IQueryParams,
    correlationId: number | undefined,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    newObj?: Partial<EntryDto>
  ): void {
    StaticLogger.startMethod('checkAccess', correlationId);

    // only if the user has the correct right we can access the filter.
    if (
      (queryParams['activeRole'] === ModuleConstants.instance.availableRoles.ab && userDto.hasRight(ModuleConstants.instance.availableRights.api.entry.ab)) ||
      (queryParams['activeRole'] === ModuleConstants.instance.availableRoles.pb && userDto.hasRight(ModuleConstants.instance.availableRights.api.entry.pb)) ||
      (queryParams['activeRole'] === ModuleConstants.instance.availableRoles.bb && userDto.hasRight(ModuleConstants.instance.availableRights.api.entry.bb))
    ) {
      throw this.createForbiddenException(`user ${userDto.eMail} has no right to use this query param!`, correlationId);
    }

    switch (methodName) {
      case CheckAccessMethodType.getByUuid: {
        switch (queryParams['activeRole']) {
          case ModuleConstants.instance.availableRoles.ab:
            if (oldObj.feedbackFromEmail !== userDto.eMail) {
              throw this.createForbiddenException(`user ${userDto.eMail} has no right to access this entry!`, correlationId);
            }
            break;
          case ModuleConstants.instance.availableRoles.pb: {
            const accessibleUsers = EntryAccess.list[userDto.eMail].pb;
            if (!accessibleUsers?.includes(oldObj.creatorEmail)) {
              throw this.createForbiddenException(`user ${userDto.eMail} has no right to access this entry!`, correlationId);
            }
            break;
          }
          case ModuleConstants.instance.availableRoles.bb: {
            const accessibleUsers = EntryAccess.list[userDto.eMail].bb;
            if (!accessibleUsers?.includes(oldObj.creatorEmail)) {
              throw this.createForbiddenException(`user ${userDto.eMail} has no right to access this entry!`, correlationId);
            }
            break;
          }
        }
        break;
      }

      case CheckAccessMethodType.delete: {
        if (queryParams['activeRole'] !== ModuleConstants.instance.availableRoles.ab) {
          throw this.createForbiddenException(`user ${userDto.eMail} has no right to use this function!`, correlationId);
        }
        if (oldObj.creatorEmail !== userDto.eMail) {
          throw this.createForbiddenException(`user ${userDto.eMail} has no right to delete this entry!`, correlationId);
        }
        if (oldObj.wasAlreadyPublished) {
          throw this.createForbiddenException(`this entry can't be deleted!`, correlationId);
        }
        break;
      }

      case CheckAccessMethodType.patch:
      case CheckAccessMethodType.put: {
        switch (queryParams['activeRole']) {
          case ModuleConstants.instance.availableRoles.ab: {
            if (oldObj.creatorEmail !== userDto.eMail && (oldObj.state === 'published' || oldObj.state === 'accepted')) {
              throw this.createForbiddenException(`user ${userDto.eMail} has no right to update this entry!`, correlationId);
            }
            break;
          }
          case ModuleConstants.instance.availableRoles.pb: {
            const accessibleUsers = EntryAccess.list[userDto.eMail].pb;
            if (accessibleUsers?.includes(userDto.eMail) && (oldObj.state === 'saved' || oldObj.state === 'rejected')) {
              throw this.createForbiddenException(`user ${userDto.eMail} has no right to update this entry!`, correlationId);
            }
            break;
          }
          case ModuleConstants.instance.availableRoles.bb: {
            throw this.createForbiddenException(`user ${userDto.eMail} has no right to update entries!`, correlationId);
          }
        }
        break;
      }
    }
    StaticLogger.endMethod<null>('checkAccess', null, correlationId);
  }

  override async getAllMethod<T>(userDto: ApiUserDto, filter: IQueryParams, correlationId: number | undefined): Promise<Partial<T>[]> {
    StaticLogger.startMethod('getAllMethod', correlationId);
    // any is the key of the field (uuid).
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const objArrWithKeys = await this.baseRedisService.hGetAll<any, Partial<T>>(this.getDocumentKey(), correlationId);
    const objArr: Partial<T>[] = [];
    // add the uuid back for each object
    for (const key in objArrWithKeys) {
      objArr.push({ ...objArrWithKeys[key], uuid: key });
    }

    const activeRole = filter['activeRole'];
    let filteredObjArr: Partial<T>[] = [];
    switch (activeRole) {
      case ModuleConstants.instance.availableRoles.ab: {
        // abs can only see their own entries
        filteredObjArr = objArr.filter((o) => (o as unknown as EntryDto).creatorEmail === userDto.eMail);
        break;
      }
      case ModuleConstants.instance.availableRoles.pb: {
        const filterUsers = EntryAccess.list[userDto.eMail].pb;
        filteredObjArr = objArr.filter((o) => filterUsers.includes((o as unknown as EntryDto).creatorEmail) && (o as unknown as EntryDto).state !== 'saved');
        break;
      }
      case ModuleConstants.instance.availableRoles.bb: {
        const filterUsers = EntryAccess.list[userDto.eMail].bb;
        filteredObjArr = objArr.filter((o) => filterUsers.includes((o as unknown as EntryDto).creatorEmail) && (o as unknown as EntryDto).state !== 'saved');
        break;
      }
      default:
        throw this.createForbiddenException(`role filter for this role doesn't exist!`, correlationId);
    }
    return StaticLogger.endMethod<Partial<T>[]>('getAllMethod', filteredObjArr, correlationId);
  }

  // hint: to just have specific entries in the history and not all. Also we cut the informations about the method and the user who did it.
  sanitizeEntryHistory(entry: Partial<EntryDto>, correlationId): Partial<EntryDto>[] {
    StaticLogger.startMethod('sanitizeEntryHistory', correlationId);
    let entryHistory = [];
    // check if a history exists
    if (entry.history) {
      const historyFiltered: IBaseDocumentDtoUpdateHistory[] = entry.history.filter((h) => h.method === 'UPDATE') as IBaseDocumentDtoUpdateHistory[];
      // check if there are UPDATE methods
      if (historyFiltered) {
        // map the oldValue since we don't need timestamp etc.
        const entryHistoryMapped: Partial<EntryDto>[] = historyFiltered.map((h) => h.oldValue);
        // filter only accepted and rejected
        entryHistory = entryHistoryMapped.filter((e) => e.state === 'accepted' || e.state === 'rejected');
      }
    }
    return StaticLogger.endMethod<EntryDto[]>('sanitizeEntryHistory', entryHistory, correlationId);
  }

  override sanitizeReadMethod<T>(userDto: ApiUserDto, data: Partial<T>, correlationId: number | undefined): Partial<T> {
    StaticLogger.startMethod('sanitizeReadMethod', correlationId);
    const dataAsEntry = data as unknown as EntryDto;
    // hint: in redis it gives us a string as return value, so we have to manually convert it.
    dataAsEntry.targetDate = new Date(dataAsEntry.targetDate);
    // hint: with the base grid (ui: entry grid component) we can filter and if it's an array it does arr.includes(filter) and we have to type the full tag.
    // In textformat we don't have to do this since string.includes() also works and checks the characters
    if (userDto.hasRight(ModuleConstants.instance.availableRights.api.entry.canSeeTagsText)) {
      dataAsEntry.tagsText = dataAsEntry.tags?.toString();
    }

    if (dataAsEntry.targetDate) {
      // add yearKw for displaying
      const targetDate = dataAsEntry.targetDate;
      dataAsEntry.yearKw = DateAndTimeTools.getYearKw(targetDate);

      // hint: we have a problem since some functions modify the date, so we need this constants. It probably could be refactored
      const dateOfPublished = new Date(dataAsEntry.timestampLastPublish);
      const publishedDay = dateOfPublished.getDay();
      const publishedYear = dateOfPublished.getFullYear();
      const localTimeStringPublished = dateOfPublished.toLocaleTimeString();
      const hoursOfPublishedDate = Number(localTimeStringPublished.split(':')[0]);
      const minutesOfPublishedDate = Number(localTimeStringPublished.split(':')[1]);
      const kwOfPublishedDate = DateAndTimeTools.getWeekNumber(dateOfPublished);

      const kwFromEntryTargetDate = Number(dataAsEntry.yearKw.split('-')[1]);
      const yearFromEntryTargetDate = Number(dataAsEntry.yearKw.split('-')[0]);
      // check if the kw is the same or bigger and also if the year is the same or bigger than the published date
      const kwCheck = kwFromEntryTargetDate >= kwOfPublishedDate && yearFromEntryTargetDate >= publishedYear;

      const dayCheck =
        // check if the entry was published later than 16:30 on Friday
        !(hoursOfPublishedDate >= 16 && minutesOfPublishedDate > 30 && kwFromEntryTargetDate === kwOfPublishedDate && publishedDay === WeekdayEnum.Friday) &&
        // check if the entry was published later (day) than Friday on that kw
        !(kwFromEntryTargetDate === kwOfPublishedDate && (publishedDay >= WeekdayEnum.Saturday || publishedDay === WeekdayEnum.Sunday));

      dataAsEntry.publishedOnTime = kwCheck && dayCheck;
    }
    dataAsEntry.entryHistory = this.sanitizeEntryHistory(dataAsEntry, correlationId);
    return StaticLogger.endMethod<Partial<T>>('sanitizeReadMethod', data, correlationId);
  }

  override sanitizeUpsetMethod<T>(
    userDto: ApiUserDto,
    newData: Partial<T>,
    oldData: Partial<T>,
    queryParams: IQueryParams,
    methodType: UpsetMethodType,
    correlationId: number | undefined
  ): Partial<T> {
    StaticLogger.startMethod('sanitizeUpsetMethod', correlationId);
    const activeRole = queryParams['activeRole'];

    const newDataAsEntryDto = newData as unknown as EntryDto;
    delete newDataAsEntryDto.entryHistory;
    newDataAsEntryDto.creatorEmail = userDto.eMail;
    // set the new creatorEmail here to access a check later. Will be ignored if the user can't fill the entry
    switch (methodType) {
      case UpsetMethodType.Create: {
        // hint: edge case: probably can only create if the user is allowed to fill.
        if (activeRole !== ModuleConstants.instance.availableRoles.ab) {
          delete newDataAsEntryDto.tags;
          delete newDataAsEntryDto.content;
          delete newDataAsEntryDto.targetDate;
          delete newDataAsEntryDto.title;
        }
        break;
      }
      case UpsetMethodType.Update: {
        const oldDataAsEntryDto = oldData as unknown as EntryDto;
        // is here to fix a problem with put: we replace everything. So if the user isn't allowed to fill the entry we set it to the old state, and it will get saved.
        // also ignore it if the new entry has another creatorEmail
        if (activeRole === ModuleConstants.instance.availableRoles.pb) {
          newDataAsEntryDto.content = oldDataAsEntryDto.content;
          // if the state is reopened we reset the target date to not get a problem with on time or not. Also we reset the feedback since it's like a new entry and we don't need the older feedback.
          if (newDataAsEntryDto.state !== 'reopened') {
            newDataAsEntryDto.targetDate = oldDataAsEntryDto.targetDate;
          } else {
            newDataAsEntryDto.targetDate = undefined;
            newDataAsEntryDto.feedbackFromEmail = undefined;
            newDataAsEntryDto.feedback = undefined;
          }
          // make sure the data is correct
          newDataAsEntryDto.creatorEmail = oldDataAsEntryDto.creatorEmail;
          newDataAsEntryDto.feedbackFromEmail = userDto.eMail;
          newDataAsEntryDto.title = oldDataAsEntryDto.title;
          newDataAsEntryDto.tags = oldDataAsEntryDto.tags;
          newDataAsEntryDto.wasAlreadyPublished = oldDataAsEntryDto.wasAlreadyPublished;
        } else {
          newDataAsEntryDto.feedback = oldDataAsEntryDto.feedback;
          newDataAsEntryDto.feedbackFromEmail = oldDataAsEntryDto.feedbackFromEmail;
        }
        if (newDataAsEntryDto.state === 'published' || newDataAsEntryDto.state === 'reopened') {
          newDataAsEntryDto.wasAlreadyPublished = true;
        } else {
          newDataAsEntryDto.wasAlreadyPublished = oldDataAsEntryDto.wasAlreadyPublished;
        }
        break;
      }
    }

    // set the state to saved if the user can only set those two states and, it isn't one of them.
    if (activeRole === ModuleConstants.instance.availableRoles.ab && newDataAsEntryDto.state !== 'saved' && newDataAsEntryDto.state !== 'published') {
      // hint: saved lets the user edit it again so the wrong state can be corrected
      newDataAsEntryDto.state = 'saved';
    }
    // set the state to rejected if the user only can set accepted, rejected or reopened and the new state isn't one of them.
    if (
      activeRole === ModuleConstants.instance.availableRoles.pb &&
      newDataAsEntryDto.state !== 'accepted' &&
      newDataAsEntryDto.state !== 'rejected' &&
      newDataAsEntryDto.state !== 'reopened'
    ) {
      // hint: we set the entry to rejected if the state is wrong so other users can see it and could "complain" why it is rejected.
      newDataAsEntryDto.state = 'rejected';
    }

    return StaticLogger.endMethod<Partial<T>>('sanitizeUpsetMethod', newDataAsEntryDto as unknown as Partial<T>, correlationId);
  }

  // your can easily override the method if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }
}
