// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import authConfig from './auth_config.json';

const apiUrl = 'https://portal-api.it-berufsbildung.ch';

const { domain, clientId, audience, errorPath } = authConfig as {
  domain: string;
  clientId: string;
  audience?: string;
  errorPath: string;
};

export const environment = {
  title: '',
  version: '0.0.7',
  production: true,
  apiUrl,
  auth: {
    domain,
    clientId,
    ...(audience && audience !== 'YOUR_API_IDENTIFIER' ? { audience } : null),
    redirectUri: window.location.origin,
    errorPath,
  },
  httpInterceptor: {
    allowedList: [`${apiUrl}/*`],
  },
};
