// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import authConfig from './auth_config.json';

const apiUrl = 'http://localhost:3331';

const { domain, clientId, audience, errorPath } = authConfig as {
  domain: string;
  clientId: string;
  audience?: string;
  errorPath: string;
};

export const environment = {
  title: 'Dev ',
  version: '0.0.7',
  production: false,
  apiUrl,
  auth: {
    domain,
    clientId,
    ...(audience && audience !== 'YOUR_API_IDENTIFIER' ? { audience } : null),
    redirectUri: window.location.origin,
    errorPath,
  },
  httpInterceptor: {
    allowedList: [`${apiUrl}/*`],
  },
};
