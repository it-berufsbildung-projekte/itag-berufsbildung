# Grundkonzept CI/CD
![My Diagram](pipeline_diagram.drawio.svg)

# Environment Variables
Für das Deployment werden in Portainer folgende ENV Variablen benötigt:
## Base API
| Variable | Beschreibung |
| -------- | ------------ |
| AUTH_ISSUER | Issuer für auth0 |
| BASE_API_URL | Externer Domain Name, unter der das API erreichbar ist |
| REDIS_URL | URL für den Redis Host. Name des Docker Containers |
| REDIS_PORT | Port unterdem Redis erreichbar ist |
| REDIS_DATABASE | Anzahl Redis Datenbanken |
| CORS_ORIGIN | Angabe von wem der Request kommen darf |
| LOG_LEVELS | Angabe, was geloggt werden soll |
| API_PROTOCOL | Internes API Protokoll |
| API_HOST | Interne URL Des API Hosts. Name des Dockker Containers |
| API_PORT | Interner API Port |
| ENVIRONMENT | Deployment Umgebung (int/prod/dev) |
| AUTH_AUD | Email für auth0 |
| AUTH_CLIENT_ID | Client ID für auth0 |
| AUTH_CLIENT_SECRET | CLient Secret für auth0 |
| REDIS_PASSWORD | Passwort für die Redis Datenbank |
| SERVER_PROTOCOL | Externes Protokoll für den Zugriff auf die das API |
| SERVER_HOST | Externer Domain name, unter der das API erreichbar ist |
| SERVER_PORT | Externer Server Port für zugriff auf das API |

## Beispiels ENV
```
AUTH_ISSUER=https://hugo.eu.auth0.com
BASE_API_URL=https://portal-api.int.it-berufsbildung.ch
REDIS_URL=portal-redis
REDIS_PORT=6379
REDIS_DATABASE=1
CORS_ORIGIN=*
LOG_LEVELS=["verbose","debug","log","warn","error"]
API_PROTOCOL=http
API_HOST=portal-api
API_PORT=3331
ENVIRONMENT=int
AUTH_AUD=hugo@it-berufsbildung.ch
AUTH_CLIENT_ID=D1234
AUTH_CLIENT_SECRET=SECRET1234
REDIS_PASSWORD=SamplePass1234
SERVER_PROTOCOL=https
SERVER_HOST=portal-api.int.it-berufsbildung.ch
SERVER_PORT=443
```

# Gitlab Runners
Gitlab CI/CD Pipelines werden von Gitlab Runners ausgeführt. Diese Runners können selbst gehosted werden oder es können fertige Cloud Runenrs verwendet werden.
> **_HINWEIS_**  Wenn lokale Runner verwendet werden, gibt es keine Begränzung an CI/CD Minuten pro Monat. Diese Limitation existiert nur bei Cloud Runners. 
# Dockerfile
> **⚠ WARNUNG**  
> Für die benützung der NX CLoud bzw. das Abrufen von NX-Cloud Daten, wir d zwingend Git benötigt. Dockerfile-Befehl : `RUN apk add --no-cache git`
# Pipeline

## Stages

### lint-test
| **Overview** |              |
|--------------|--------------|
| Stage Name   | lint-test    |
| Content      | npm lints    |
| Image        | node:18.12-alpine |
| Tags         | docker       |

Wie funktioneirt der lint Tester?

    - npm install
    - npm run lint-portal
    - npm run lint-shared



# Notizen

Monitoring: Disk Space usage


NPM CI: 
https://docs.npmjs.com/cli/v8/commands/npm-ci
npm-ci | npm Docs
Clean install a project

