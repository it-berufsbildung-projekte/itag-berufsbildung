# GenericDocumentController

Die ursprüngliche Idee hinter dem GenericDocumentController war die Filegrössen (in Zeilen) zu reduzieren und nicht mehrere hunderte Zeilen lange Files zu haben, die praktisch das Gleiche machen.
Es wurden also die CRUD Funktionen umgesetzt mit einigen anderen Features, welche später erklärt werden.

### Welche Daten brauchen wir/geben wir mit?
Der GenericDocumentController hat den Typen T extends BaseDocumentDto, bedeutet wir geben von der Child Klasse unser DTO mit. Das BaseDocument DTO hilft uns, auf bestimmte, vordefinierte Felder
zuzugreifen, dazu gehören:
- History: Ist ein Array aus verschiedenen Properties: email, timestamp, method und oldValue. Die Email ist vom Benutzer der diesen Eintrag erstellt/bearbeitet hat, der Timestamp steht für wann und die Methode kann "POST" oder "UPDATE" sein. OldValue wird nur gesetzt, wenn die ausgeführte Funktion zu "UPDATE" gehört und beinhaltet den Wert, vor dem Update.
- UUID: Der Identifier des Eintrags.
- isDeleted: Ist ein boolean, bei dem Einträge für einige User gelöscht werden können (mehr dazu später). Der Eintrag verschwindet nicht aus Redis.

Da die meisten Sachen aber Partial T sind, kann es zu Fehlern führen und wir wandeln um: (obj as unknown) as T.

Im Constructor wird der dbService mitgegeben, der documentName und die Rechte. Die Rechte helfen uns das ganze Generisch zu machen (da wir z.B. api-[documentName]-can-read haben und diese nicht anders übergeben können). In diesem Objekt hat es die Properties:
- canShowDeleted: Die Berechtigung, die Einträge ansehen kann, die isDeleted auf true haben. Wir entfernen auch das Property isDeleted, wenn der User nicht die Berechtigung hat die Einträge zu sehen.
- canRead: Die Berechtigung um die beiden GET Funktionen aufzurufen.
- canCreate: Die Berechtigung um einen Eintrag zu erstellen.
- canUpdate: Die Berechtigung um einen Eintrag zu bearbeiten.
- canModifyDelete: Die Berechtigung um isDeleted zu ändern. 
- canRemove: Die Berechtigung um einen Eintrag permanent aus Redis zu löschen.
- canSeeHistory: Die Berechtigung um die History der Einträge zu sehen.

#### Grundsätzliche Funktionen:
Der Controller besitzt die Funktionen, welche den Einstiegspunkt von Swagger darstellen:
- delete: Permanentes löschen eines Eintrags
- getAll: Hat für die Benutzer eine Möglichkeit x-beliebige Query Params zu setzen. In der getAllMethod kann dann danach gefiltert werden. Gelöschte Einträge anzeigen wird hier als Standard behandelt und ist bereits im Generic Controller implementiert und kann nicht von uns überschrieben werden! Dieser Filter muss aber nicht gesetzt sein.
- getByUuid: Gibt einen Eintrag zurück.
- patch: Patched einen Eintrag, hier wird auch das isDeleted Flag gesetzt.  
- post: Erstellt einen Eintrag, hier muss die UUID mitgegeben werden, da wir diese nicht auf dem Server erzeugen.
- put: Ersetzt einen Eintrag, hier kann auch das Flag isDeleted gesetzt werden.

Jede dieser Funktionen hat x-beliebige Query Params. Damit kann z.B. die "ausgewählte Rolle" angegeben werden. Damit soll verhindert werden bei Rechtüberschneidungen einen Fehler zu bekommen.
Beispiel: User hat das Recht Feld "Name" zu bearbeiten aber kann Feld "Datum" nicht ändern und hat gleichzeitig das Recht, das Gegenteil (sprich Datum anpassen und Namen nicht). Durch die Query Params könnte dann das Recht mitgegeben werden, welches gerade "aktiv" ist.

In diesen Funktionen werden die Berechtigungen geprüft, die Objekte sanitized (mehr dazu später) und die overridebaren (mehr dazu ebenfalls später) Funktionen aufgerufen.

Damit der Entwickler selber entscheiden kann, was er bei den CRUD Funktionen macht, wurden einige protected Methoden erstellt. Diese kommunizieren momentan mit dem Service und machen "Standard" Sachen, können aber beliebig angepasst werden (z.B. bei Delete muss was Zusätzliches gelöscht werden und nicht nur der Eintrag):
- deleteMethod: Wird in delete aufgerufen. Momentan wird im BaseRedisService die hDel Funktion aufgerufen, die UUID dem gelöschten Eintrag hinzugefügt und dieser zurückgegeben.
- getAllMethod: Wird in getAll aufgerufen. Momentan wird im BaseRedisService die hGetAll aufgerufen und die UUIDs den korrekten Beiträgen zugewiesen und zurückgegeben.
- getOneMethod: Wird in getByUuid, patch und put aufgerufen. Momentan wird im BaseRedisService die Funktion hGet aufgerufen, ein Fehler ausgegeben, wenn das Objekt nicht existiert, die UUID dem Eintrag zugewiesen und dann den Eintrag zurückgegeben.
- upsetMethod: Wird in patch, post und put aufgerufen. Momentan wird im BaseRedisService die Funktion hSet aufgerufen, die UUID dem Objekt hinzugefügt und zurückgegeben.


#### checkAccess
Wir haben eine überschreibbare Funktion, bei der wir für die verschiedenen Funktionen, also put, patch, delete, post, getByUuid, getAll, unsere Berechtigungen überprüfen kann.
Diese gibt entweder Fehler zurück oder schränkt bei der getAll die Resultate ein. Beispiel: Der User darf alle Einträge von User 1, 2 und 3 sehen, aber nicht von 4. Wenn er ein getAll macht,
können einfach die Einträge von User 1, 2 und 3 aus allen herausgefiltert werden und die Einträge von User 4 werden nicht zurückgegeben. Bei getByUuid ist das aber anders: Entweder darf ich den Eintrag von dem User sehen oder nicht, schmeisst also einen Fehler.
Standardmässig macht diese Funktion nichts.
Die Idee dahinter ist, dass wir zusätzliche Checks gebrauchen könnten.

#### Sanitizer
Die Idee der Sanitizer ist, dass wir entscheiden können welche Properties entfernt und/oder hinzugefügt werden.
Unsere Sanitizer frischen uns nur die Daten auf, das passiert beim Schreiben und beim Lesen. Die Überlegung ist, dass ein User nur property a, b und c setzen kann,
aber d und e ebenfalls gesetzt sind. Diese können dann entfernt werden oder auf den alten Stand gesetzt werden in den Überschreibbaren Sanitizern.

Der Controller besitzt auch verschiedene sanitizer, die nicht angepasst werden können:
- sanitizeMethodHistoryRead: Entfernt die History beim Zurückgeben des Eintrags, wenn der User nicht die benötigte Berechtigung hat.
- sanitizeMethodIsDeleteRead: Entfernt das Property beim Zurückgeben des Eintrags, wenn der User nicht die benötigte Berechtigung hat.
- sanitizeMethodHistoryWrite (achtung static): Entfernt allenfalls aus dem newData (z.B. putDto) das property history und fügt die history aus dem alten Eintrag diesem hinzu. In vorderster Position wird noch der neue History Eintrag hinzugefügt. Grund für diese Funktion ist, dass wir die history von aussen nicht beeinflussbar machen, sondern nur intern und so haben wir volle Macht über dieses Property.
- sanitizeMethodIsDeletedWrite: Entfernt das Property isDeleted vor dem Speichern. Hier wird geschaut ob der User das Recht hat canModifyDelete, wenn ja, wird nichts gemacht und sonst das Property entfernt. Hier hat das zur Folge, dass evtl. bei Post/Patch/Put das Feld fehlt. Hat auf den Code im GenericDocumentController aber keinen Einfluss!

Der Controller besitzt aber auch überschreibbare sanitizer, diese können beliebig angepasst werden:
- sanitizeMethodRead: Wird in delete, getAll, getByUuid, patch, post und put vor dem Zurückgeben des Eintrages aufgerufen. Momentan macht diese Funktion nichts.
- sanitizeUpsetMethod: Wird in put, patch und post vor dem Erstellen aufgerufen. Momentan macht diese Funktion nichts. Es kann auch unterschieden werden, ob ein Create oder Update gemacht wurde durch einen Parameter.

Damit klar ist, welcher Sanitizer wann gewinnt ist hier eine Auflistung der Reihenfolge welche Methode aufgerufen wird:
- Post/Put/Patch: sanitizeMethodIsDeletedWrite --> sanitizeUpsetMethod --> sanitizeMethodHistoryWrite --> upsetMethod --> sanitizeReadMethod --> sanitizeMethodIsDeletedRead --> sanitizeMethodHistoryRead
- Delete: deleteMethod --> sanitizeReadMethod --> sanitizeMethodIsDeletedRead --> sanitizeMethodHistoryRead
- GetAll: sanitizeReadMethod --> sanitizeMethodIsDeletedRead --> sanitizeMethodHistoryRead
