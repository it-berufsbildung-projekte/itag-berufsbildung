# Generator neues Modul

Die Idee hinter diesem Generator ist, uns Arbeit zu ersparen und ein Grundgerüst für ein neues Modul zu haben. Dazu zählen die Libraries (data, feature) und das NestJS API.

Das API enthält bereits den GenericController, welcher für grundsätzliche Funktionen der verschiedenen APIs zuständig ist wie z.B. die UserInfo bekommen oder die MenuInfo. Importdaten mit den UserRoles sind vorhanden und werden automatisch beim Start importiert. Rollen und deren Rechte sind als "Template" vorhanden.
Konstanten für das Menü wurden gesetzt und dadurch sollte nach dem Starten und registrieren in der Portal-API, je nach Rolle, das Menü im Portal-UI angezeigt werden.

Das Feature ist ein Angular Module, welches ein Grundgerüst ohne Komponente ist. Es hat ein Routing und das FeatureModule. Wichtig: wir nennen die FeatureModule feature-[Modul Name].. und nicht, wie durch NX generiert [Modul Name]-feature... das hat zur Folge, dass wir das index.ts anpassen müssen und das Module nicht überschreiben, sondern löschen und unseres hinzufügen.

Data hat die Modulkonstanten, die sowohl im Feature wie auch im API gebraucht werden können. Hier geht es darum Rechtschreibfehler oder andere Benennungen (z.B. im UI im Routing "[Modul Name]/test" aber Rückgabewert von Api "[Modul Name]/testpage") zu vermeiden. Dieses Feature darf KEINEN Angular und/oder NestJS Import enthalten, da sonst Fehler auftauchen! Es soll Plain Typescript sein!


### Befehl
ACHTUNG! Der Modul Name und das Frontend Project müssen im kebab-case geschrieben werden. Port und Redis Datenbank Nummer sollten zusammenpassen (3331 = 1, 3336 = 6, 3341 = 11 usw).
```console
nx workspace-generator api-generator [Modul Name] [Port] [Redis Datenbank Nummer] [Frontend Project]
```
Beispiele:
```console
nx workspace-generator api-generator heinz-test 3336 6 portal-ui
```
```console
nx workspace-generator api-generator heinz 3341 11 portal-ui
```
Hinweis: Beim Ausprobieren kam es mehrfach zum Phänomen, dass für ein anderes Modul im API und in den beiden Features das project.json als geändert angezeigt wird. Das hat KEINEN Einfluss und ändert nichts.
Beispiel: Das Modul hugo wurde erstellt. Jetzt wird ein Modul Namens herbert erstellt und während dem Erstellen werden die drei project.json von hugo einem "UPDATE" unterzogen. In GIT wird das als Änderung aufgefasst, obwohl nichts ersetzt oder abgeändert wurde.


### Anpassungen
Es sind einige Anpassungen nötig, damit das API startet. Einige davon sind Pflicht, andere nur stark zu empfehlen:
1. Im .env vom erzeugten API das Passwort für die Redis Datenbank eintragen. 
2. Für die Modulregistrierung müssen die API-Daten in das .env hinzugefügt werden.
3. Die Befehle zum Starten, Testen, Builden und Linten im package.json einfügen:
```json lines
{
  "serve-[moduleName]-api": "npx nx serve [moduleName]-api",
  "build-[moduleName]-api": "npx nx build [moduleName]-api",
  "test-[moduleName]-api": "npx nx test [moduleName]-api",
  "lint-[moduleName]": "nx lint [moduleName]-api && nx lint [moduleName]-data && nx lint [moduleName]-feature",
}
```
4. Damit das Feature funktionieren könnte (also z.B. eine Komponente anzeigen) muss es im app.module.ts des Frontend Projects eingefügt werden
5. Einfügen der Jobs in der Pipeline für das neue Modul
6. Docker-compose und Dockerfile einfügen im Modul
7. Neues Modul auf dem Portainer hinzufügen (.env, webhook, docker-compose)
8. Variablen auf GitLab deklarieren (falls nötig)

Das Modul kann dann direkt mit dem serve Befehl gestartet werden und befindet sich unter localhost:[port] erreichbar. Es können ab diesem Zeitpunkt auch beliebig viele Dokumente mit dem Document Generator hinzugefügt werden.

### Bekannte Limitationen:
1. Es wird im UI nichts angezeigt, wenn auf die Route gegangen wird, da keine Komponente erstellt wird.


WAS 

alles angepasst werden muss.

## api

### neuen controller hinzufügen

```console
nx g @nrwl/nest:controller --project=[module]-api --directory app/controllers --name [controller Name]
```

### neues Service hinzufügen

```console
nx g @nrwl/nest:service --project=[module]-api --directory app/service --name [service Name]
```

## feature

### neue angular-component hinzufügen

```console
nx g @schematics/angular:component --path=/libs/user-management/feature/src/lib/components --project=[moduleName]]-feature --name=[compnentName] --style=scss
```

### neuen angular-service hinzufügen

```console
nx g @schematics/angular:service --path=/libs/[moduleName]/feature/src/lib/services --project=[moduleName]]-feature --name=[service Name]
```

## data
Wenn neue Klassen im data angelegt werden müssen, so müssen diese Klassen im index.ts, im Projekt Root Verzeichnis, nachgeführt werden.
