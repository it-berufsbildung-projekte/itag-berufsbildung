# Database Restore
Anleitung für den Restore der Datenbank

## Integration
1. Im Portainer den gewünschten API Stack stoppen
2. Backup Share `int_redis_backup` unter Windows Mounten --> Siehe [Infrastructure](https://gitlab.com/it-berufsbildung-projekte/infrastructure/-/blob/main/labor/produktion/physische%20devices/bbfsl01/readme.md)
3. Gewünschtes Backup auf den Desktop kopieren
4. Via Filezilla mit [int-docker001](https://gitlab.com/it-berufsbildung-projekte/infrastructure/-/tree/main/labor/produktion/virtuelle%20server/int-docker001) verbinden
5. Files ins Home Verzeichnis Kopieren
6. SSH Verbindung mit [int-docker001](https://gitlab.com/it-berufsbildung-projekte/infrastructure/-/tree/main/labor/produktion/virtuelle%20server/int-docker001) herstellen
7. Volume bereinigen mit `sudo rm -r /var/lib/docker/volumes/redis-<apiname>/*`
8. Backup Daten kopieren mit `cp -r ./redis-<apiname>/* /var/lib/docker/volumes/redis-<apiname>/`
9. Im Portainer den gestoppten API Stack starten
