# Entwickler-Informationen

<img src="/_how-tos/images/main-screen-demo-001.png" width="1178" alt="sample portal screen">

Grobe Übersicht über alle Themen bzw. verlinkung zu den Dateien:

- [Begriffe](/_how-tos/begriffe.md)
- [Datenbank](/_how-tos/datenbank.md)
  - [Datenbank Restore](/_how-tos/datenbank-restore.md)
- [Umgebungsvariablen](/_how-tos/environment.md)
- [Generator neues Modul](/_how-tos/generator-new-module.md)
- [Generator neues Dokument](/_how-tos/generator-new-document.md)
- [Generic Document Controller](/_how-tos/generic-document-controller.md)
- [Aufbau](/_how-tos/genereller-aufbau.md)
- [Rollen und Rechte](/_how-tos/rollen-rechte.md)
