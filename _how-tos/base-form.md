# Base Edit Form

Die Idee hinter dem Base Edit Form ist es eine Möglichkeit zu haben schnell ein Formular erstellen zu können, ohne viel anzupassen.
Deshalb wurde die Funktionalität eingefügt, um einen Eintrag zu bearbeiten, zu erstellen, zu löschen oder zu entfernen, basierend auf x angezeigten Feldern.


### Welche Daten brauchen wir/geben wir mit?
Das Base Form hat einen generischen Datentypen der BaseDocumentDto erweitert. So haben wir Zugriff auf das wichtigste Feld: UUID.

Im Constructor werden die folgenden Daten mitgegeben:
- route: Damit wird auf die Aktive Route abonniert und so kann die uuid ausgelesen werden und ein "Bearbeiten" statt "Erstellen" erzwungen werden.
- router: Damit wir navigieren können auf die cancelRoute
- rights: Die Rechte zum anzeigen der verschiedenen Buttons.
- formBuilder: Wir arbeiten mit reactiveForms
- displayedInputs: Ist ein Array von Objekte welche das Form beschreiben. Dazu gehört der Name des Inputs, den Typ und ob das Feld readonly ist.
- entityCollectionService: Der Service hilft uns die Daten beim Bearbeiten zu Laden und dann zu speichern bei einem update oder hinzufügen.
- cancelRoute: Damit wird bei Cancel oder nach dem Erstellen/Bearbeiten auf diese Seite navigiert.
- moduleName: wird benötigt, um die Rechte zu checken.

Wichtig ist, dass wir im UserService auf den momentanen User abonnieren, und zwar auf der Komponente, der von der BaseForm erbt. Da wir auf dem BaseForm Zirkuläre Dependencies erhalten. Dieser wird benötigt um die Buttons anzuzeigen.

### Funktionen
- onSave: speichert den Wert aus dem Formular. Hier wird das Speichern für editieren und das Speichern für Erstellen gemacht und dann zur cancelRoute navigiert.
- onCancel: Navigiert zur cancelRoute
- openRemoveDialog: Öffnet den Dialog mit dem bestätigt werden soll, ob der Eintrag entfernt oder gelöscht werden soll. Setzt auch ein Property, welches uns hilft zu entscheiden, ob der Eintrag entfernt oder isDeleted auf true gesetzt wurde.
- closeRemoveDialog: Schliesst den Dialog und setzt isDeleted auf true oder löscht den Eintrag je nach Funktion.
- getEditForm: Erstellt unser Form basierend auf den displayedInputs. Standardmässig macht es uns ein neues Form, oder füllt es mit den Werten aus dem Eintrag (nur bei Edit). Wandelt uns ebenfalls Strings zu Dates um.
Alle Funktionen können überschrieben werden!

### Bekannte Limitationen
1. Es gibt nur beschränkte Eingabefelder. Für Texteingabe, Nummer, Checkbox und Datum.
2. Der User muss in der vererbten Komponente geladen werden, ist weniger optimal da der Code zum Laden des aktiven User immer gleich ist. 





