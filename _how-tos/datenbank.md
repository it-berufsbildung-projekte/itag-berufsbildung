# Datenbank
Wir verwenden Redis als Datenbank.

Vom Grundsatz her ist jedes Modul für seine Datenhaltung verantwortlich. D.h. Daten werden immer über das Modul gespeichert.

Mit Redis können wir pro Modul eine eigene Redis instanz starten oder "nur" eine eigene Datenbank verwenden.
Für die Entwicklung empfiehlt es sich, nur eine Redis instanz hochzufahren und mit den Datenbanken zu arbeiten. In der Default Installation werden bis zu 16 Datenbanken unterstützt.

Die Datenbank 0 soll in diesem Fall nicht verwendet werden.

Die Datenbank 1 soll in diesem fall für das Portal selber verwendet werden.
Das Portal speichert für sich die Informationen über die Module (vom Grundsatz her nur den Modulnamen und die API Url).


### Idee dahinter
Wir haben absichtlich darauf verzichtet z.b. MongoDb mit mongoose zu verwenden da wir auf das Schema verzichten möchten.

Wir arbeiten mit: als Trennzeichen zwischen unterschiedlichen Ebenen des Keys.

In der Datenbank verwenden wir neben "normalen" string values (das können auch JSON.stringify(objekte) sein), auch noch Hash.
Hash --> setzen wir immer dann ein, wenn auf ein Key des Hash zugegriffen werden muss. z.B. auf ein Modul oder auf die Rollen eines spezifischen Benutzers

- string --> also ein normaler Text. Dort legen wir auch mit JSON.stringify() Objekte ab
- hash --> das ist selber ein key-value speicher. hier ist der value auch wieder ein string

<img src="/_how-tos/images/redis-commander-001.png" alt="Print screen of a redis commander sample screen">

## Editor / Viewer
Wir Installieren mit unserem Docker-compose direkt einen lokalen Editor für Redis.
der Redis-Commander wird dann über [webseite](http://localhost:8081) aufgerufen, das Bild oben zeigt die Darstellung einer Muster Datenbank in diesem Editor.
Im Editor können auch Befehle direkt ausgeführt werden. Achtung bei Befehlen wie:
- flushdb --> löschen der ganzen Datenbank <h4>OHNE Rückfrage, es wird direkt die Datenbank gelöscht</h2>


## Aufbau der Datenstruktur
Vom Grundsatz her kennt Redis keine Gruppierung, sondern alle keys sind einfach nur keys.

Um die Keys verständlicher zu machen werden die Keys so aufgebaut, dass zwischen "Logischen Begriffen" mit einem Doppelpunkt getrennt wird. Die meisten Editor Tools verwenden diese Trennung um einen Verzeichnisbaum auszugeben

Unsere Struktur sieht wie folgt aus:

db0 
- modules --> [ type hash ]. Hier registrieren sich alle APIs selbständig beim starten.z.B. 



Der Einstieg in die Datenbank ist der Key:
Pro Modul:
- module-info ( string type, mit TTL von knapp 60s: JSON.stringify(modulInfo). Hier werden alle Informationen zu einem Modul, vom Modul selber beim Starten gespeichert. Dieser Key wird im Hash modules dann verwendet als value im hash  :-)
- user-role ( hash: e-mail mit JSON.stringify(Array[Roles]) )
- role-right (hash: role as key: JSON.stringify(Array[right]) )
- cache:user:[e-mail] ( string type, mit ttl von ca. 15min: JSON.stringify(userInfo für das Modul) )
- documents:[documentName] (hash)

### wichtig: registrieren von Modulen
Ein Modul muss sich alle 60s beim Hauptmodul portal anmelden. D.h. über einen Webservice call soll sich das Modul beim Hauptmodul melden.
Das Hauptmodul speichert für den Zeitraum von 61s die Informationen zwischen. 

Das ui-portal fragt beim api-server an, welche Module registriert sind.

das ui-portal fragt jetzt für jedes Modul nach informationen nach. D.h. jedes Modul muss seine eigene Daten liefern und nicht der zentrale Service.





### docker-compose Beispiel für Redis
Wir verwenden für die Entwicklung docker-desktop mit der Extension portainer.

Das hat den Vorteil, dass wir direkt im Editor die Information hinzufügen können und auch gleich eine Umgebungsvariable für das PAsswort setzen können.

Wichtig: die Umgebungsvariable <b>PASSWORD</b> muss gesetzt werden.

```yaml
version: "3.9"
services:
  redis:
    image: "redislabs/redismod"
    restart: always
    networks:
      - back
      - front
    ports:
      - "6379:6379"
    volumes:
      - redis-cache:/data
    entrypoint: >
      redis-server  
        --loadmodule /usr/lib/redis/modules/redisearch.so
        --loadmodule /usr/lib/redis/modules/rejson.so
        --appendonly yes
        --loglevel warning
        --requirepass $PASSWORD
        --save 60 1
    healthcheck:
      test: ["CMD-SHELL", "redis-cli -a ${PASSWORD} ping | grep PONG"]
      interval: 1s
      timeout: 3s
      retries: 5
      start_period: 5s

  redis-commander:
    image: rediscommander/redis-commander
    restart: always
    environment:
      - REDIS_HOST=redis
      - REDIS_PASSWORD=$PASSWORD
    ports:
      - "8081:8081"
    networks:
      - back
      - front
    healthcheck:
      test: ["CMD-SHELL", "wget --spider http://localhost:8081 || exit 1"]
      interval: 5s
      timeout: 3s
      retries: 5
      start_period: 5s

volumes:
  redis-cache:
    name: redis-cache

networks:
  back:
    driver: bridge
    internal: true
  front:
    driver: bridge

```
