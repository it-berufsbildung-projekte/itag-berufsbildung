# Generator neues Dokument

Um ein tieferes Verständnis für die enthaltenen Funktionen zu bekommen, bitte unter [Generic Document Controller](generic-document-controller.md) nachlesen!

Die Idee hinter diesem Generator ist uns ein Controller für ein vollständiges Dokument zu erstellen. Dieser soll vom GenericDocumentController erben. 

# TODO: falls UI hier dokumentieren
Um ein genaueres Verständnis zum BaseGrid zu bekommen, bitte unter [Base Grid](base-grid.md) nachlesen!

Um ein neues Document anzulegen werden folgende Schritte benötigt:

### Befehl
ACHTUNG! controllerName muss PascalCase sein (ohne "Controller" im Namen) und moduleName kebab-case also
```console
nx workspace-generator document-generator [Dokumentname] [moduleName]
```

Beispiele:
```console
nx workspace-generator document-generator HugoTest heinz-test
```
```console
nx workspace-generator document-generator Hugo heinz
```

### Anpassungen api und libs/data
Es werden einige Anpassungen benötigt, damit der Controller läuft und/oder die korrekte Funktion hat. Hier werden diese erwähnt. Es sind auch freiwillige Anpassungen dabei, also was angepasst werden kann (oder oft werden muss).
1. AvailableRights eintragen, API: Unter libs/[Modul Name]/api/src/module-constants.ts müssen die Rechte unter availableRights eingetragen werden. Diese sollen dem Muster "[Rechte Typ (z.B. api)]-[Dokumenten Name]-[Recht z.B. can-see-history]" folgen. Die folgenden Rechte sind zwingend: canShowDeleted, canRead, canCreate, canUpdate,
   canModifyDelete, canRemove und canSeeHistory. Falls diese nicht eingetragen werden, wird beim Kompilieren im Controller ein Fehler auftreten.
2. AvailableRights eintragen, UI: Unter libs/[Modul Name]/api/src/module-constants.ts müssen die Rechte unter availableRights eingetragen werden. Diese sollen dem Muster "[Rechte Typ (ui)]-[Dokumenten Name]-[Recht z.B. can-see-history]" folgen. Die folgenden Rechte sind zwingend: canShowDeleted, canCreate, canUpdate,
   canModifyDelete und canRemove.
3. AvailableRight eintragen: Menü: Unter libs/[Modul Name]/api/src/module-constants.ts muss ein Recht für die Menüanzeige dieses Dokumentes hinzugefügt werden. Das Recht wird so benannt: canShow[Dokumentname]Module.
4. Dokumentenname hinzufügen: Unter libs/[Modul Name]//api/srcmodule-constants.ts muss der Dokumentenname unter documents eingetragen werden. Falls diese nicht eingetragen werden, wird beim Kompilieren im Controller ein Fehler auftreten.
5. Dokumentennamen für NGRX Data hinzufügen: Unter libs/[Modul Name]/api/src/module-constants.ts unter documentNames bei single und plural den Dokumentennamen genau gleich eintragen.
6. DTO anpassen: Unter libs/[Modul Name]/data/src/models/[Controller Name].dto.ts befindet sich ein DTO, dass beliebig angepasst werden kann. Standardmässig hat es den Namen. Falls diese nicht eingetragen werden, wird beim Kompilieren im Controller ein Fehler auftreten.
7. index.ts Datei um DTOs zu exportieren: Unter libs/[Modul Name]/data/src/models muss die Datei index.ts erstellt werden. Darin muss das DTO exportiert werden (export * from './[Dokumentname].dto';). Zudem muss diese index.ts Datei in der index.ts Datei unter libs/[Modul Name]/data/src exportiert werden (export * from './models';).
8. Berechtigungen setzen: Unter api/src/app/role-rights/role-rights.ts des Moduls sollten die Rechte verteilt werden. Passiert das nicht, hat keine Rolle Zugriff auf Funktionen.
9. Routing anpassen: Unter libs/[Modul Name]/api/src/module-constants.ts muss das Routing erweitert werden. Dieses kann z.B. der Name des Dokumentes sein. Die Idee ist eine Konstante dafür zu haben.
10. Menü anpassen: Unter api/src/constants.ts des Modules befindet sich das Menü. Dieses kann angepasst werden mit einem neuen child mit dem Path "${ModuleConstants.instance.routing.base}/[neue Route aus ModuleConstants]" und dem benötigten Recht. Es gibt noch andere Properties, die können aber beliebig sein.
11. Importdaten anpassen: Unter api/src/import-data/dev/[Dokument Name].ts des Moduls befinden sich die Importdaten. Diese können beliebig angepasst und erweitert werden. Die Prod und Testdaten sind dabei leer.
12. Import schreiben: Der Generator erstellt uns keine Funktion, die die Daten automatisch importiert. Dieser muss also von Hand geschrieben werden. Das Ganze wird unter src/app/services/app-config.ts des Moduls eingefügt.
13. Controller Funktionen: Wenn der Controller eine nicht Standard Funktion hat können einige Methoden aus dem GenericDocumentController überschrieben werden. Diese heissen jeweils [Funktion]Method also z.B. upsetMethod. Es können natürlich auch komplett neue Einstiegspunkte erstellt werden (z.B. neues GET), es muss jedoch auf die Route aufgepasst werden!
14. Sanitizer Funktionen: Wenn der Controller z.B. eine neue Rolle hat, die ein bestimmtes Feld/Property nicht sehen kann, können wir zwei Sanitizer überschreiben: sanitizeReadMethod und sanitizeUpsetMethod. Die sanitizeReadMethod kommt vor dem Zurückgeben zum Einsatz und die sanitizeUpsetMethod vor dem Erstellen. !Achtung, wir haben interne Sanitizer, bitte in der Dokumentation vom GenericDocumentController nachlesen, welche "Gewinnt"

### Anpassungen libs/feature
Der Variablenname ist der Dokumentenname in camelCase.
1. Grid-Komponente im feature Module eintragen: Unter libs/[Modul Name]/feature/src/lib/feature-[Modul Name].module.ts bei declarations "[Dokumentname]GridComponent" eintragen.
2. Kendo-Module im feature Module eintragen: Unter libs/[Modul Name]/feature/src/lib/feature-[Modul Name].module.ts bei imports die Module GridModule, DialogsModule, PDFModule, ExcelModule, ButtonsModule, InputsModule, FormsModule, ReactiveFormsModule, DateInputsModule, LabelModule, IconsModule importieren.
3. Providers im feature Module eintragen: Unter libs/[Modul Name]/feature/src/lib/feature-[Modul Name].module.ts bei providers "[Dokumentname]Resolver", "[Dokumentname]DataService", "[Dokumentname]EntityService" eintragen. Zudem muss der "[Dokumentname]EditService" so auch zu den providers hinzugefügt werden:
```code
  {
      deps: [ HttpClient, [Dokumentname]EntityService ],
      provide: [Dokumentname]EditService,
      useFactory:
        ( httpClient: HttpClient, [Variablenname]EntityService: [Dokumentname]EntityService ) => () => new [Dokumentname]EditService( httpClient, [Variablenname]EntityService ),
  }
```
4. DataService beim EntityService registrieren: Unter libs/[Modul Name]/feature/src/lib/feature-[Modul Name].module.ts im Konstruktor der Klasse den DataService importieren und danach diesen DataService beim EntityService des neuen Dokuments registrieren:
```code
  constructor(
    private entityDataService: EntityDataService,
    private [Variablenname]DataService: [Dokumentname]DataService,
  ) {
    entityDataService.registerService(
      ModuleConstants.instance.documentNames.single.[Variablenname],
      [Variablenname]DataService,
    );
  }
```
6. Routing Module erweitern: Unter libs/[Modul Name]/feature/src/lib/modules/feature-[Modul Name]-router.module.ts bei den routes das Array children mit der Route für die Grid-Komponente des neuen Dokuments einfügen:
```code
  {
    path: ModuleConstants.instance.routing.sub.[Variablenname],
    component: [Dokumentname]GridComponent,
    canActivate: [ModuleRightGuard],
    data: {
      module: ModuleConstants.instance.moduleName,
      right: ModuleConstants.instance.availableRights.menu.canShow[Dokumentname]Module,
    },
    resolve: {
      [Variablenname]: [Dokumentname]Resolver,
    }
	}
```

### Momentane Limitationen
1. Wenn bei getAll etwas basierend auf einer Param aus dem Request gefiltert werden muss, braucht es einen neuen Einstiegspunkt. 
2. Der GenericDocumentController überprüft nicht, ob alle Properties gesetzt sind. Es könnte zu fehlerhaften Daten führen. Das ganze kann aber in der jeweiligen überschreibbaren Funktion geprüft werden. Falls das Property nicht gesetzt wird, wird es im Objekt fehlen (wichtig für Sanitizer).
3. Da wir meistens mit Partial T arbeiten, kann es sein, dass die BaseDocumentDto Properties oder die vom Typ T fehlen. Dann muss eine Umwandlung stattfinden z.B. (patchDto as unknown) as BaseDocumentDto.
4. Dadurch, dass ein Sanitizer "gewinnt", könnten Fehler auftauchen, sollte aber selten der Fall sein, da nur BaseDocumentDto Properties sanitized werden.
5. Das Grid sieht unschön aus, da es nicht als Card im base-grid.html ist. Die Begründung dafür ist, falls wir es in einem Layout brauchen oder in einer anderen Komponente könnte das zu doppelten Titeln führen. Lösung: neue Komponente erstellen als Card und das ganze Grid in den Body davon packen.
6. Das Grid unterstützt nur Inline Edit mit einer nummerischen Textbox, einem Dateninput, einem Textinput und einer Checkbox.
