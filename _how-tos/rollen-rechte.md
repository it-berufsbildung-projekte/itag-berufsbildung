# Rollen und Rechte

Es gibt unterschiede zwischen Rollen und Rechten.

Jedes Modul hat eine eigene Verwaltung von Rechten und Rollen.

Wichtig ist zu Verstehen:
- Rollen machen eine Aussage welches set von Rechten ein Benutzer hat
- Rechte sind ein individuelles Recht etwas machen zu dürfen.

Die Rechte werden unterteilt nach:

- api.[document].[recht]
- ui.[component].[recht]

für alle Module gibt es das api.document.[generic]. Dieses muss bei allen Module umgesetzt werden.

Was ist jetzt aber die Idee hinter den Rollen und Rechten:

Wir weisen einem Benutzer genau eine Rolle zu innerhalb eines Modules.
In der Anwendung werden die Rollen und Rechte zuweisungen dann fix definiert vorgenommen und in einem Document hash role-right abgelegt.

Beim Aufstarten eines Modules sollen diese zuweisungen geladen / gesetzt werden und hier gilt:
- wenn eine Rolle im Hash vorhanden ist, wird keine Anpassung gemacht.
- wenn eine Rolle im Hash nicht vorhanden ist, wird aus dem entsprechenden Order (dev,test,prod) gelesen.

Im Modul wird es dann so umgesetzt, dass es nur noch ein Recht gibt etwas zu machen oder einzuschränken etc. es wird nicht mehr mehrere Rollen geben welche geprüft werden müssen.

Ein Beispiel einer ModuleConstants.ts Datei:
``` typescript
{
  routing: {
    profile: {
      showInfo: 'ui.routes.profile.showInfo', // have access to the route profile show-info
    }
  },
  api:{
    master: {
      canRead: 'api.generic.canRead', // get and getAll
      canCreate: 'api.generic.canCreate', // post
      canUpdate: 'api.generic.canUpdate', // put and patch
      canDelete: 'api.generic.canDelete', // we only mark records as deleted
      canRemove: 'api.generic.canremove', // with this role we can remove items that are marked as deleted
      showDeleted: 'api.generic.showDeleted', // right to show deleted items
    }
  }
  ui: {
    generic: {
      showDeleted: 'ui.generic.showDeleted', // show column deleted
      canCreate: 'ui.generic.canCreate', // show create button
      canEdit: 'ui.gneric.canEdit', // show edit button
      canDelete: 'ui.generic.canDelete', // show delete button
      canRemove: 'ui.generic.canRemove', // show the remove button
    },
  }
}
```

