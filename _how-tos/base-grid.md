# BaseGrid

Die Idee ist, dass bei einem neuen Document eine Komponente erzeugt wird, die ein Grid enthält, welches die Basisfunktionen bereitstellt.
Dazu gehören die CRUD Funktionen, Paging, Sorting und PDF/Excel Exporte und mehr. Diese können in den Optionen zum Grid ein- oder ausgeschaltet werden, um das zu zeigen, was gebraucht wird.
Es gibt die Möglichkeit, die Einträge direkt im Grid zu bearbeiten (inline) oder über eine weitere Komponente mit Formular extern.

Für die externe Bearbeitung wird unter folgendem Link beschrieben, wie das Formular erstellt werden muss: [externes Formular](base-form.md)

### Welche Daten brauchen wir/geben wir mit?
Die BaseGrid Klasse ist vom Typ T extends BaseDocumentDto, was bedeutet, dass wir in der Child Klasse beim Erben (extends) der BaseGrid Klasse unser DTO setzen. Das BaseDocument DTO hilft uns, auf bestimmte, vordefinierte Felder
zuzugreifen, dazu gehören:
- History
- UUID
- isDeleted

Im Konstruktor muss folgendes mitgegeben werden:
- Router: Damit aus der BaseGrid Klasse zu externen Formularen navigiert werden kann, wenn die externe Bearbeitung aktiviert wurde.
- editService: Ein Service, mit dem das Grid bei der inline Bearbeitung die CRUD Operationen ausführen kann.
- formBuilder: Wird gebraucht, um die Formulare (FormGroups) erzeugen zu können, die im Grid beim Editieren und Erstellen gebraucht werden.
- displayedColumns: Ein Array von Objekten, welche das Interface IDisplayedColumns implementieren. Ein Objekt stellt eine Spalte im Grid dar, wie z.B. "name". Wichtig ist, dass der Wert beim Property "columnName" identisch sein muss, wie der Name des Property im DTO, welches im Grid angezeigt werden soll. Damit aber deutsche Titel möglich sind in der Anzeige existiert das freiwillige property "columnTitle", wenn dieses gesetzt ist, wird der Spalten Name dazu und sonst columnName. 
Das Property "width" legt fest, welche Breite die Spalte hat. "type" legt den Typ der Spalte und des Formularfelds fest. Es gibt dazu 4 unterstütze Typen (string, number, date und checkbox). Beim Typ String wird eine Textbox in das Formular gesetzt, bei number eine numerictextbox, bei date ein Datepicker
und bei checkbox eine Checkbox (hier sind dann auch nur boolean Werten einsetzbar). Das Property "readonly" legt fest, ob die Spalte bearbeitbar ist oder nicht.
- gridOptions: Ein Objekt, welches das Interface IGridOptions implementiert. Mit ihm wird das Grid konfiguriert und hat folgende Properties welchen allen entweder true oder false als Wert gesetzt wird:
  - isInlineEdit: true heisst es wird inline, also im Grid selbst editiert/erstellt, false bedeutet externe Bearbeitung.
  - showToolbar: Toolbar, die oberhalb des Grids steht, wird damit ein- und ausgeblendet. Sie beinhaltet den Knopf zum Hinzufügen eines Eintrags, dem PDF oder Excel Export.
  - showCommandColumn: Zeigt die Spalte ganz rechts im Grid, welche die Knöpfe für die CRUD Operationen beinhaltet.
  - sortable: Wenn auf true gesetzt, können die Einträge nach den Spalten aufsteigend oder absteigend sortiert werden.
  - pageable: Auf true gesetzt, werden im Grid immer nur so viele Einträge angezeigt, wie im Konstruktor für "take" übergeben wurde. Es kann vor und zurückgeblättert werden.
  - selectable: Damit können Zeilen im Grid angeklickt werden, um bestimmte Aktionen auszulösen.
  - filterable: aktiviert die Filterfunktion im Grid, bei der bei den Spalten beispielsweise nach Einträgen gesucht werden kann.
  - maxHeightPx: legt die maximale Höhe des Grids in Pixeln fest. Es muss eine Ganzzahl übergeben werden.
  - showAddButton: Zeigt den Knopf zum Erstellen "+". Dieser befindet sich in der Toolbar. Wenn er gezeigt werden soll, muss auch die Toolbar gezeigt werden.
  - pdfExport: Blendet den Knopf zum PDF-Export in der Toolbar ein. Auch hier muss die Toolbar gezeigt werden, wenn die Funktion gewünscht ist.
  - excelExport: Blendet den Knopf zum Excel-Export in der Toolbar ein. Auch hier muss die Toolbar gezeigt werden, wenn die Funktion gewünscht ist.
- moduleName: Das ist der Name des Moduls, der aus den ModuleConstants entnommen werden kann.
- rights: Es sind die Rechte, die auch aus den Konstanten entnommen werden. Das muss gemacht werden, weil die BaseGrid Klasse nicht weiss, von welchem Modul die Rechte stammen und von der erbenden Klasse übergeben werden müssen.
- sort: Wenn schon nach etwas bestimmtem sortieren will, muss man hier ein Array von SortDescriptor übergeben. Das Interface SortDescriptor stammt von Kendo UI und beinhaltet das Feld und die Richtung (asc/desc) nach der sortiert werden muss.
- take: Die Anzahl der im Grid gezeigten Einträgen, wenn das Paging aktiviert ist.
- externEditBaseRoute: Das ist der Basispfad zur Komponente, in der extern Einträge bearbeitet werden. Er muss nur mitgegeben werden, wenn extern editiert wird. Der Pfad muss in dem ModuleConstants unter routing->sub eingetragen werden. Er darf den Parameter ":uuid" nicht enthalten.
- externCreateRoute: Das ist der Pfad zur Komponente, in der extern neue Einträge erstellt werden. Er muss nur mitgegeben werden, wenn extern editiert wird. Der Pfad muss in dem ModuleConstants unter routing->sub eingetragen werden.

Mit den Beschriebenen Properties zum Interface IGridOptions, wurden implizit auch die Funktionen des Grids beschrieben.

### Rückfrage vor dem Löschen (delete/remove)
Eine Rückfrage bei Löschvorgängen, egal ob delete oder remove, wird dem Benutzer immer ein Dialog zum Bestätigen gezeigt. Dieser Dialog kann in den Konfigurationen nicht deaktiviert werden.

### Methoden, die eventuell in der Child Klasse überschrieben werden müssen
In der BaseGrid Klasse, gibt es jedoch Methoden, die eventuell in der Child Klasse überschrieben werden müssen, da ihre Standardimplementation nicht alle Fälle abdecken kann:

#### Methode getAddFormGroup()
Sie erzeugt das Formular (FormGroup) dynamisch mit den Spaltennamen aus dem Array "displayedColumns". Sollte ein Formularfeld z.B. einen weiteren Validator benötigen (Standard ist required), dann muss die Methode in der Child Klasse überschrieben werden.
Speziell ist, dass hier auch ein Formularfeld für die UUID erstellt wird und dessen Wert eine neu generierte UUID ist. Dieses Formularfeld wird aber nie ersichtlich sein. Es dient nur zur übertragung des Werts, denn die BaseGrid Klasse nutzt die FormGroup aus Einfachheit direkt als DTO.

#### Methode getEditFormGroup()
Sie ist ähnlich, wie die getAddFormGroup() Methode, setzt aber im Formular die Werte aus dem Eintrag ein. Sollte auch hier z.B. ein weiterer Validator benötigt werden, damm muss diese Methode überschrieben werden.

#### selectionChange()
Diese Methode wird aufgerufen, wenn im Grid eine Zeile angeklickt wurde. Dazu muss die Option "selectable" in den GridOptions auf true gesetzt sein. die Standardimplementation navigiert über den Router zum Pfad, der aus dem im Konstruktor übergebenen "externEditBaseRoute" 
und angehängt die UUID des angeklickten Eintrags zusammengesetzt wird. Wenn hier z.B. nur bei bestimmten Einträgen etwas geschehen soll, muss sie überschrieben werden.


### Wichtig
In der Child Klasse wird auf das userDto$ Observable abonniert. Im Observer wird geprüft, welche UI-Rechte der Benutzer hat. Falls er ein Recht nicht hat, welches für das Sehen einer Spalte benötigt wird, dann muss ein Check eingebaut werden.
Generiert wird bereits das Ausblenden der Spalte für das "isDeleted" Property. Das Entfernen der Spalte geschieht über die Methode removeDisplayedColumn(), aus der BaseGrid Klasse, der der Spaltenname als Argument übergeben wird.

Der UserService kann nicht direkt in der BaseGrid Klasse importiert werden, weil das eine circular dependency gibt. Der UserService muss deshalb in der Child Klasse importiert werden. 
 
