# Begriffe
Wir beschreiben hier unsere Begriffe:

- Anwendung, die Anwendung ist der Einstieg für alle Module 
- Modul, ist ein in sich geschlossener Teil. Früher hätte man dies als Anwendung genannt. Module haben einen Namen. Die Module werden immer an zwei verschiedenen Orten gepflegt:
  - apps/[module name]
    - api, hier wird das nestjs Backend gepflegt
  - libs/[module-name]
    - feature, hier wird der Angular Code für das Frontende gepflegt. Das Routing etc. wird hier definiert
    - data, hier werden alle Daten abgelegt, welche im api und im feature benötigt werden. Es darf keine Referenzen auf @angular oder @nest enthalten. In der Regel sind dies Konstanten oder Datentypen.

