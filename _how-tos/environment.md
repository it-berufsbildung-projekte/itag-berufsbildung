# Umgebungsvariablen bzw. .env files

Wir arbeiten mit Umgebungsvariablen welche wir in der Regel in .env Dateien ablegen.

Es gibt eine Hierarchie von Umgebungsvariablen welche gesetzt werden müssen.

## /.env
Im root Verzeichnis liegt das File mit den Informationen:
- JWT_APPID=[ string ] --> der JWT AppId Token, z.B. von Auth0
- BASE_API_URL=[ string ] --> die URL des BASE API (in der regel portal-api)

Diese Datei muss vorhanden sein!


## /apps/[moduleName]/api/.env
Pro Module und api gibt es ein eigenes .env file mit:

- REDIS_URL=[ string ] --> pfad zum Redis Server, für lokale Entwicklung: localhost
- REDIS_USERNAME=[ string ] --> optionaler Name des Benutzers, wenn der Redis Server im Internet läuft wird teilweise auch ein Username benötigt
- REDIS_PASSWORD=[ string ] --> das Passwort für den Zugriff auf den Redis Server. Bitte niemals ohne Passwort einen Redis Server aufbauen
- REDIS_PORT=[ number ] --> der Port des Redis Server. Defaultport ist 6379
- REDIS_DATABASE=[ number ] --> auf welcher Datenbank soll gearbeitet werden. Default wäre hier 0
- CORS_ORIGIN=[ string ] --> welche zugriffe erlauben wir auf unseren api servern. Default wäre hier *
- LOG_LEVELS=[ string[]: "verbose","debug","log","warn","error"] --> ein Array mit einer Auswahl der aufgeführten strings

- API_PROTOCOL=[ string: http oder https ] --> das zu verwendende protokoll
- API_HOST=[ string ] --> die Adresse auf dem diese Api Server. Entwicklung meistens localhost 
- API_PORT=[ number ] --> der Port auf dem diese API läuft
- API_PREFIX=[ string ] --> wenn das API nicht in der Root angesprochen werden soll.
- ENVIRONMENT=[ string: dev oder test oder prod ] --> gibt an aus welchem Verzeichnis Daten beim starten importiert werden soll. Entwicklung solle auf dev stehen 


## neuen Umgebungsvariablen
Wenn neue Umgebungsvariablen angelegt werden sollen so sollen diese am richtigen Ort nachgeführt werden. Wir verwenden für den Zugriff auf die Umgebungsvariablen immer Konstanten um Tippfehler zu vermeiden.

Die Konstanten für die API Umgebungsvariablen sind alle in der Datei

-[api-constants.ts](/libs/shared/data-api/src/api-constants.ts)

abgelegt.



