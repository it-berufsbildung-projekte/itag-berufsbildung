# Planning

## Jahresplanung für PB

Die Jahresplanung entspricht einem Grobplan für einen oder mehrere Lernende und bildet die Basis für die Tagesplanung. Es können Ereignisse mit unterschiedlichen Kategorien erstellt werden.

Kategorien:
- Schule
- Ferien/Abwesenheiten
- Gruppe
- Betrieb
- Ausbildungsmodul/Projekt (werden in der Tagesplanung aufgelistet, damit die Lernenden diese einplanen können)

Die Seite Jahresplanung ist in zwei Tabs unterteilt: Planungsansicht und Übersicht.

In der Planungsansicht können Ereignisse eingeplant werden und in der Übersicht können die einzelnen Personen mit ihren Ereignissen tabellarisch betrachtet werden.

## Orientierung Tab Planungsansicht

![Planungsansicht](./images/planning/jahresplanung-planungsansicht.jpg)

1. Tabs
2. Filter
3. Liste der Projekte/Aufgaben
4. Knopf zum Hinzufügen von Projekten/Aufgaben
5. Knöpfe zum Ändern des angezeigten Monats
6. Date Picker zum Ändern des angezeigten Monats
7. Kalender, in dem die Ereignisse angezeigt und eingetragen werden

## Erstellen eines Projektes oder Aufgabe

1. Auf den Knopf '+' klicken.
2. Im geöffneten Dialog beim Feld 'Name' den Namen des Projektes/Aufgabe eingeben.
3. Bei 'Zeitbudget in Stunden' die vom PB vorgesehene Zeit für das Projekt/Aufgabe eingeben.
4. Das Feld Besitzer leer lassen. Der Besitzer wird später festgelegt.
5. Auf den Knopf mit dem Diskettensymbol klicken, um zu speichern.

![Projekt/Aufgabe erstellen](./images/planning/jahresplanung-projekt-erstellen.jpg)

## Projekt/Aufgabe einem oder mehreren Lernenden einplanen

1. In der Liste der Projekte/Aufgaben das gewünschte Element per Drag and Drop an den gewünschten Tag in den Kalender ziehen.
2. Im geöffneten Dialog beim Feld Besitzer die Personen aussuchen, denen dieses Projekt zugewiesen werden soll.
3. Auf 'Save' klicken.

![Projekt/Aufgabe einplanen](./images/planning/jahresplanung-projekt-einplanen.jpg)

Mit diesem Schritt wird das Projekt oder die Aufgabe einem Lernenden zugewiesen. Das heisst, dass die Lernende Person dieses Projekt in Teilaufgaben (Tasks) unterteilen kann, die anschliessend in den Tagesplan gezogen werden.

## Erstellen anderer Ereignisse mit oder ohne Wiederholung

Neben Projekten/Aufgaben können auch einfach so Ereignisse erstellt werden, die dann auch einer anderen Kategorie angehören können.

1. Doppelklick auf den Tag im Kalender, an dem das Ereignis eingeplant werden soll.
2. Bei 'Titel' den Titel des Ereignisses eingeben.
3. Bei 'Besitzer' die Personen auswählen, die das Ereignis betrifft.
4. bei 'Kategorie' die gewünschte Kategorie auswählen.
5. Wenn es ein wiederholendes Ereignis ist, kann bei 'Repeat' angeklickt werden, ob es eine tägliche, wöchentliche, monatliche oder jährliche Wiederholung sein soll. Danach können weitere Konfigurationen gemacht werden, z.B. wann die Wiederholung fertig ist.
6. Auf den Knopf 'save' klicken um zu speichern

![anderes Ereignis einplanen](./images/planning/jahresplanung-anderes-event-erstellen.jpg)

## Löschen von Ereignissen

1. Mit dem Cursor über das zu löschende Ereignis fahren.
2. Auf das kleine Kreuz auf der rechten Seite des Ereignisses klicken.
3. Im Dialog auf den Knopf 'Delete' klicken.

Wenn es sich um ein Projekt/Aufgabe handelt und alle Ereignisse davon für einen Besitzer gelöscht werden, wird es für den Lernenden nicht mehr sichtbar sein in der Tagesplanung. Es können dadurch auch keine Tasks mehr erstellt werden.

## Filter

Mit dem Filter können in der Jahresplanung die Besitzer und die Kategorien ausgewählt werden, die im Kalender angezeigt werden sollen.

Um den Filter zu benutzen, klickt man auf den Knopf mit dem Trichtersymbol.

Die Besitzer oder Kategorien können per anklicken in der Liste ausgewählt werden. Mit dem Kreuz (bei jedem Element sichtbar) können Besitzer oder Kategorien wieder entfernt werden.

Der Filter muss wieder durch einen Klick auf den Knopf mit dem Trichtersymbol geschlossen werden.

![Filter](./images/planning/jahresplanung-filter.jpg)

## Übersichtsseite

Um sich einen Gesamtüberblick zu verschaffen, gibt es die Übersichtsseite. Der Zeitraum kann in Monaten festgelegt werden (1).
Die Ereignisse, die sich in diesem Zeitraum befinden werden angezeigt, alle anderen nicht.

Es gibt für jeden Besitzer, der im Filter ausgewählt wurde, eine eigene Spalte (2).
In ihr werden die Ereignisse pro Datum angezeigt und sind mit dem Zeichen '|' voneinander getrennt (3).

![Uebersichtsseite](./images/planning/jahresplanung-uebersichtsseite.jpg)

