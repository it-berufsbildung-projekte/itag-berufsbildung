# IT AG Berufsbildung

Yes, don't be afraid to learn german as we are documenting in german. :-)

Dieses Projekt ist ein Projekt der Auszubildenden der Abteilung Informatik der Kantonalen Verwaltung Aargau.

Mit diesem Projekt möchten wir unseren Auszubildenden die Möglichkeit geben, anhand eines realen Projektes Entwicklungserfahrungen im Front- und Backendbereich zu sammeln.



Allgemeine Informationen für die Entwicklung.

- nx als Basis
- angular als Frontend
- nestjs als Backend
- redis als Datenbank mit dem Redis-Commander als Tool
- kendo-ui-angular von Telerik (!!! Commercial Licence required )
- Sourcecode wird auf gitlab.com verwaltet
- [Gitkraken](https://www.gitkraken.com/) ist unser Tool für die Verwaltung / den Zugriff auf gitlab.com
- Als Editor verwenden wir [Webstorm](https://www.jetbrains.com/webstorm/) der Firma JET Brains 

## Disclaimer for Telerik products

Be aware that we're using a commercial licence product from Telerik [kendo-ui-angular](https://www.telerik.com/kendo-angular-ui). If you like to work with this project you have to purchase an individual [licence](https://www.telerik.com/purchase/kendo-ui) from telerik to be able to use the components for development.
See also the [Terms of Use from Telerik](https://www.telerik.com/about/terms-of-use).



## Entwickler Dokumentationen

[Entwickler Dokumentation](./_how-tos/README.md)


### Konstanten
Wenn immer möglich arbeiten wir statt mit "Strings" mit Konstanten.

Dies betrifft z.B. auch das Routing etc.

Zu diesem Zweck gibt es 3 Ebenen von Konstanten:
- GlobalConstants, auf der Stufe shared-data-api, hier werden Konstanten abgelegt, welche für alle Module benötigt werden. Da wir einiges im Hintergrund machen, ist es wichtig, dass alle Module die gleichen Keys etc. verwenden. Informationen zu Umgebungsvariablennamen werden dort abgelegt 
- ModuleConstants, auf der Stufe libs-[ModuleName]-data, hier werden alle Konstanten abgelegt welche z.B. auf dem Feature und auf dem Server zum Einsatz kommen. Oft sind das z.B. Router Links welche auf dem Menü und im Router benötigt werden.
- Constants, auf der Stufe API werden noch zwingende Konstanten benötigt, welche über eine Vererbung gesetzt werden (BaseConstants). Diese Konstanten stehen nur innerhalb des APIs zur Verfügung 

Also statt einem Wert (ausser z.B. Fehlermeldungen, Rückgabewerte von Methoden etc., wobei aber auch hier kann man sich fragen...) immer die Option Konstante im Auge behalten.

### User-Roles
Jedes Modul pflegt seine eigene Liste von User-Roles.
Die User-Roles sind ein hash mit der E-Mail Adresse und den eigentlichen Daten für diese E-Mail welche wieder ein hash mit Rolle und den Objektdetails ist:
{ [e-Mail]: { [roleName]: Object } }

Mit dieser Anordnung kann jede Anwendung zusätzliche informationen im Objekt ablegen.
Pro Rolle haben wir die zwei Felder:
- from (default: 0), als timestamp formatiert
- until (default: undefined), als timestamp formatiert
Beides sind Felder, welche 
Ev. machen wir hier optionale from / until properties welche dann Zentral geprüft werden können


### Menu
pro Anwendung wird ein Menü erstellt. Das Menü wird beim aufstarten, bzw. beim Module aktualisieren aus der Konfiguration gelesen und in die Datenbank geschrieben.


### Testing

Wir möchten von Beginn an, dass Test auf allen ebenen erstellt werden aber nur dort wo es wirklich sinnvoll ist.
z.b. 
API hier werden alle Methoden von Services getestet und hier soll auch eine Coverage von 100% erreicht werden. Wenn dies nicht möglich ist, gilt es abzuklären warum wir dann diesen Teil Code überhaupt haben.

abzuklären / bzw. zu definieren gilt es noch:
- ui / feature
- data
- util
- shared

### Api Guards
Wir haben einen Guard für die Rollen. Dieser prüft ob der User eingeloggt ist, diesem Modul angehörig und ob die Rollen passen.
Der Decorator ModuleRoles unterstützt diesen Guard. 

Dem ModuleRoles Decorator kann ein Modul Name und ein Array von Rollen Namen übergeben werden. Ist dieser nicht nicht als Decorator gesetzt, wird nur geprüft ob der User authorisiert ist.
Ist der Modul Name gesetzt und ein leeres Rollen Namen Array bedeutet das, dass der User das Property des Modulnamens in den Rollen haben muss. Die Rollen spielen aber keine Rolle.










<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>





















This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img alt="logo" src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450" ></p>

🔎 **Smart, Fast and Extensible Build System**

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are our core plugins:

- [React](https://reactjs.org)
  - `npm install --save-dev @nrwl/react`
- Web (no framework frontends)
  - `npm install --save-dev @nrwl/web`
- [Angular](https://angular.io)
  - `npm install --save-dev @nrwl/angular`
- [Nest](https://nestjs.com)
  - `npm install --save-dev @nrwl/nest`
- [Express](https://expressjs.com)
  - `npm install --save-dev @nrwl/express`
- [Node](https://nodejs.org)
  - `npm install --save-dev @nrwl/node`

There are also many [community plugins](https://nx.dev/community) you could add.

## Generate an application

Run `nx g @nrwl/react:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `nx g @nrwl/react:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@itag-berufsbildung/mylib`.

## Development server

Run `nx serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `nx g @nrwl/react:component my-component --project=my-app` to generate a new component.

## Build

Run `nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `nx e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.



## ☁ Nx Cloud

### Distributed Computation Caching & Distributed Task Execution

<p style="text-align: center;"><img alt="logo" src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-cloud-card.png"></p>

Nx Cloud pairs with Nx in order to enable you to build and test code more rapidly, by up to 10 times. Even teams that are new to Nx can connect to Nx Cloud and start saving time instantly.

Teams using Nx gain the advantage of building full-stack applications with their preferred framework alongside Nx’s advanced code generation and project dependency graph, plus a unified experience for both frontend and backend developers.

Visit [Nx Cloud](https://nx.app/) to learn more.
