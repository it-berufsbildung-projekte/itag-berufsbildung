import { BaseModuleConstants } from '@itag-berufsbildung/shared/util';

export class ModuleConstants extends BaseModuleConstants {
  // singleton
  private static _instance: ModuleConstants;
  static get instance(): ModuleConstants {
    return this._instance || (this._instance = new this());
  }

  readonly availableRights = {
    api: {
      generic: {
        canRead: 'api-generic-can-read',
        canResetOwnCache: 'api-generic-can-delete',
        canResetCacheForOthers: 'api-generic-can-delete-others-cache',
        canShowAdminInfo: 'api-generic-can-show-admin-info',
      },
    },
    menu: {
      canShowMainMenuModule: 'menu-can-show-main-menu-module',
    },
    ui: {
      information: {
        canShowDatabase: 'ui-information-can-show-database',
        canShowAppSettings: 'ui-information-can-show-app-settings',
      },
    },
  };

  readonly availableRoles = {
    admin: 'admin',
    user: 'user',
    readOnly: 'readOnly',
    bb: 'bb',
    pb: 'pb',
    ab: 'ab'
  };

  readonly description = 'Beschreibung des <%=moduleName%> Modules';

  readonly documentNames = {
    single: {
    },
    plural: {
    },
  };

  readonly documents = {
  };

  readonly moduleName = '<%=pascalCaseName%>';

  readonly routing = {
    base: '<%=moduleName%>',
    sub: {
    },
  };

  readonly version = '1.0.0';
}
