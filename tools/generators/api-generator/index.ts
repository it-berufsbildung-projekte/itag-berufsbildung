import { libraryGenerator as angularLibraryGenerator } from '@nrwl/angular/generators';
import { Tree, installPackagesTask, generateFiles, joinPathFragments, readProjectConfiguration, formatFiles } from '@nrwl/devkit';
import { applicationGenerator } from '@nrwl/nest';
import { libraryGenerator } from '@nrwl/workspace';
import * as fs from 'fs';

export default async function (tree: Tree, schema: any) {

  await applicationGenerator(tree, { name: 'api', directory: schema.moduleName, frontendProject: schema.frontendProject})

  // hint: da feature nur von Angular benutzt wird ist es eine Angular Library.
  await angularLibraryGenerator(tree, { name: 'feature', directory: schema.moduleName});
  await libraryGenerator(tree, { name: 'data', directory: schema.moduleName});

  // app
  const applicationRoot = readProjectConfiguration(tree, `${ schema.moduleName }-api`).root;
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/app'),
    applicationRoot,
    { tmpl: '', moduleName: schema.moduleName, port: schema.port, redisDb: schema.redisDb },
  );

  // lib/data
  const libDataRoot = readProjectConfiguration(tree, `${ schema.moduleName }-data`).root;
  // convert kebab-case to PascalCase
  const words = schema.moduleName.split('-');
  let pascalCaseName = '';
  for (const word of words) {
    pascalCaseName += word.charAt(0).toUpperCase() + word.slice(1);
  }
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/lib/data'),
    libDataRoot,
    { tmpl: '', moduleName: schema.moduleName, pascalCaseName }
  );

  // lib/feature
  const libFeatureRoot = readProjectConfiguration(tree, `${ schema.moduleName }-feature`).root;
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/lib/feature'),
    libFeatureRoot,
    { tmpl: '', moduleName: schema.moduleName, pascalCaseName }
  )

  await formatFiles(tree);

  return () => {
    // lösche alle unbenutzten Files.
    //app
    fs.unlinkSync(tree.root + '/' + applicationRoot + '/src/app/app.controller.ts');
    fs.unlinkSync(tree.root + '/' + applicationRoot + '/src/app/app.controller.spec.ts');
    fs.unlinkSync(tree.root + '/' + applicationRoot + '/src/app/app.service.ts');
    fs.unlinkSync(tree.root + '/' + applicationRoot + '/src/app/app.service.spec.ts');
    //lib/data
    fs.unlinkSync(tree.root + '/' + libDataRoot + `/src/lib/${schema.moduleName}-data.spec.ts`);
    fs.unlinkSync(tree.root + '/' + libDataRoot + `/src/lib/${schema.moduleName}-data.ts`);
    // lib/feature
    // hint: unser erzeugtes File heisst anders. Damit haben wir es nicht "doppelt".
    fs.unlinkSync(tree.root + '/' + libFeatureRoot + `/src/lib/${schema.moduleName}-feature.module.ts`);

    installPackagesTask(tree);
  };
}
