import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { <%=controllerName%>Dto, ModuleConstants } from '@itag-berufsbildung/<%=moduleName%>/data';
import { AppNotificationService } from '@itag-berufsbildung/shared/data-ui';
import { UserService } from '@itag-berufsbildung/shared/feature-auth0';
import { ObjectTools } from '@itag-berufsbildung/shared/util';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, switchMap } from 'rxjs';

export interface I<%=controllerName%>State {
  <%=variableName%>s: <%=controllerName%>Dto[];
}

@Injectable()
export class <%=controllerName%>Store extends ComponentStore<I<%=controllerName%>State> {
  private apiUrl = '';

  // hint: is used to revert changes if an error occurred while performing a CRUD operation on the api
  private oldState!: I<%=controllerName%>State;

  constructor(
    private readonly http: HttpClient,
    private readonly notificationService: AppNotificationService,
    private readonly userService: UserService,
  ) {
    super(
      {
        <%=variableName%>s: [],
      }
    );

    this.userService.userDto$.subscribe( {
          next: (user) => {
            if ( user ) {
              const moduleInfo = user.moduleInfoArr.find( module => module.moduleName === ModuleConstants.instance.moduleName );
              this.apiUrl = moduleInfo?.apiUrl as string;

              this.load();
            }
          }
        } );
  }

  readonly <%=variableName%>s$: Observable<<%=controllerName%>Dto[]> = this.select( state => state.<%=variableName%>s );

  private load = this.effect( (origin$: Observable<void>) =>
    origin$.pipe(
      switchMap( () => {
        const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.<%=variableName%>}`;
        return this.http.get<<%=controllerName%>Dto[]>( url );
      } ),
      tapResponse(
        (<%=variableName%>s: <%=controllerName%>Dto[]) => this.patchState( () => ( { <%=variableName%>s } ) ),
        console.error,
      ),
    ),
  );

  add( <%=variableName%>Dto: <%=controllerName%>Dto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.<%=variableName%>}`;
      this.http.post<<%=controllerName%>Dto>( url, <%=variableName%>Dto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      return { <%=variableName%>s: [  ...state.<%=variableName%>s, <%=variableName%>Dto ] };
    } );
  }

  update( <%=variableName%>Dto: <%=controllerName%>Dto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.<%=variableName%>}/${<%=variableName%>Dto}`;
      this.http.patch( url, <%=variableName%>Dto ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );

      const index = state.<%=variableName%>s.findIndex( e => e.uuid === <%=variableName%>Dto.uuid );
      const <%=variableName%>s = [  ...state.<%=variableName%>s ];
      <%=variableName%>s[index] = <%=variableName%>Dto;
      return { <%=variableName%>s };
    } );
  }

  delete( <%=variableName%>Dto: <%=controllerName%>Dto ): void {
    this.patchState( (state) => {
      this.oldState = ObjectTools.cloneDeep( state );

      const url = `${this.apiUrl}/${ModuleConstants.instance.documentNames.single.<%=variableName%>}/${<%=variableName%>Dto.uuid}`;
      this.http.delete( url ).subscribe( {
        error: (err) => {
          this.revertChangesAndShowError( err );
        }
      } );
      return { <%=variableName%>s: state.<%=variableName%>s.filter( e => e.uuid !== <%=variableName%>Dto.uuid ) };
    } );
  }

  private revertChangesAndShowError( error: HttpErrorResponse ): void {
    this.patchState( () => {
      this.notificationService.showError( error.message );
      return {...this.oldState};
    } );
  }

}
