import { Controller, Inject } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  ApiConstants,
  ApiSocketService,
  BaseAppConfigService,
  BaseRedisService,
  GenericDocumentController,
  IGenericDocumentControllerRights,
} from '@itag-berufsbildung/shared/data-api';
import { ModuleConstants } from '@itag-berufsbildung/<%=moduleName%>/data';

import { <%=controllerName%>Dto } from '@itag-berufsbildung/<%=moduleName%>/data';

@ApiTags( `${ ModuleConstants.instance.documents.<%=variableName%> }` )
@Controller( `${ ModuleConstants.instance.documents.<%=variableName%> }` )
export class <%=controllerName%>Controller extends GenericDocumentController<<%=controllerName%>Dto> {

  constructor(
    @Inject( ApiConstants.services.appConfigService ) protected readonly appConfigService: BaseAppConfigService,
    @Inject( ApiConstants.services.dbService ) protected readonly baseRedisService: BaseRedisService,
    @Inject(ApiConstants.services.socketService) protected readonly socketService: ApiSocketService,
  ) {
    // rechte für GenericController definieren.
    const genericDocumentControllerRights: IGenericDocumentControllerRights = {
      canCreate: ModuleConstants.instance.availableRights.api.<%=variableName%>.canCreate,
      canModifyDelete: ModuleConstants.instance.availableRights.api.<%=variableName%>.canModifyDelete,
      canRead: ModuleConstants.instance.availableRights.api.<%=variableName%>.canRead,
      canRemove: ModuleConstants.instance.availableRights.api.<%=variableName%>.canRemove,
      canShowDeleted: ModuleConstants.instance.availableRights.api.<%=variableName%>.canShowDeleted,
      canUpdate: ModuleConstants.instance.availableRights.api.<%=variableName%>.canUpdate,
      canSeeHistory: ModuleConstants.instance.availableRights.api.<%=variableName%>.canSeeHistory
    };

    super(
      baseRedisService,
      socketService,
      ModuleConstants.instance.documents.<%=variableName%>,
      genericDocumentControllerRights,
    );
  }


  // you can easy override the methods if you need it
  // override async deleteMethod<T>( key: string, correlationId: number | undefined ): Promise<Partial<T>> {
  //   StaticLogger.startMethod( 'deleteMethod', correlationId );
  //   const obj: T = await this.baseRedisService.del( key, correlationId );
  //   return StaticLogger.endMethod<T>( 'deleteMethod', obj, correlationId );
  // }

}
