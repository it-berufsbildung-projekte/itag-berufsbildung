import { Tree, formatFiles, installPackagesTask, readProjectConfiguration, generateFiles, joinPathFragments } from '@nrwl/devkit';
import { controllerGenerator } from '@nrwl/nest';

export default async function (tree: Tree, schema: any) {

  // kebab-case
  const fileName = schema.controllerName.split(/(?=[A-Z])/).join('-').toLowerCase();
  // camelCase
  const variableName = schema.controllerName.charAt(0).toLowerCase() + schema.controllerName.slice(1);

  await controllerGenerator(tree, {
    language: 'ts',
    name: schema.controllerName,
    project: `${schema.moduleName}-api`,
    directory: 'app/controllers'
  })

  // replaceFiles in apps
  const applicationRoot = readProjectConfiguration(tree, `${ schema.moduleName }-api`).root;
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/apps'),
    `${applicationRoot}/src`,
    { tmpl: '', fileName, controllerName: schema.controllerName, variableName, moduleName: schema.moduleName },
  );


  // hint: this might work in the future but is not exported... don't ask me why
  // await componentGenerator(tree, {
  //   name: `${schema.controllerName}Grid`,
  //   style: 'scss',
  // });
  //
  // await componentGenerator(tree, {
  //   name: `${schema.controllerName}GridEditForm`,
  //   style: 'scss',
  // });

// replaceFiles in libs/data
  const libDataRoot = readProjectConfiguration(tree, `${ schema.moduleName }-data`).root;
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/libs/data'),
    libDataRoot,
    { tmpl: '', controllerName: schema.controllerName, fileName, variableName }
  );

  // replaceFiles in libs/feature

  const libFeatureRoot = readProjectConfiguration(tree, `${ schema.moduleName }-feature`).root;
  generateFiles(
    tree,
    joinPathFragments(__dirname, './files/libs/feature'),
    libFeatureRoot,
    { tmpl: '', controllerName: schema.controllerName, variableName, fileName, moduleName: schema.moduleName }
  );

  await formatFiles(tree);
  return () => {
    installPackagesTask(tree);
  };
}
